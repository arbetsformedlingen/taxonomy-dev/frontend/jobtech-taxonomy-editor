import React from 'react';
import ReactDOM from 'react-dom';
import Support from './support.jsx';
import Settings from './context/settings.jsx';
import EventDispatcher from './context/event_dispatcher.jsx';
import Constants from './context/constants.jsx';
import Util from './context/util.jsx';
import App from './context/app.jsx';
import Localization from './context/localization.jsx';
import DialogWindow from './control/dialog_window.jsx';
import ControlUtil from './control/util.jsx';
import MenuPanel from './view/menu_panel/menu_panel.jsx';
import SidePanel from './view/side_panel/side_panel.jsx';
import MainPanel from './view/main_panel/main_panel.jsx';
import Login from './login.jsx';

class Index extends React.Component { 

	constructor() {
        super();
        Support.init();
        Settings.load();
        // state
        this.state = {
            popupText: Localization.get("saving") + "...",
            overlay: [],
            errors: [],
            loginComplete: false,
            cache: {
                current: 1,
                max: 10
            },
        };
        // callbacks
        this.boundShowPopupIndicator = this.onShowPopup.bind(this);
        this.boundHidePopIndicator = this.onHidePopup.bind(this);
        this.boundShowSaveIndicator = this.onShowSaveIndicator.bind(this);
        this.boundShowOverlayWindow = this.onShowOverlayWindow.bind(this);
        this.boundHideOverlayWindow = this.onHideOverlayWindow.bind(this);
        this.boundShowCache = this.onShowCache.bind(this);
        this.boundHideCache = this.onHideCache.bind(this);
        this.boundAddError = this.onAddError.bind(this);
        this.boundLoginComplete = this.onLoginComplete.bind(this);
        this.boundSetPopupMessage = this.onSetPopupMessage.bind(this);
    }

    componentDidMount() {
        window.addEventListener('focus', this.onWindowGainedFocus.bind(this));
        window.addEventListener('blur', this.onWindowLostFocus.bind(this));
        EventDispatcher.add(this.boundShowPopupIndicator, Constants.EVENT_SHOW_POPUP_INDICATOR);
        EventDispatcher.add(this.boundHidePopIndicator, Constants.EVENT_HIDE_POPUP_INDICATOR);
        EventDispatcher.add(this.boundShowSaveIndicator, Constants.EVENT_SHOW_SAVE_INDICATOR);
        EventDispatcher.add(this.boundShowOverlayWindow, Constants.EVENT_SHOW_OVERLAY);
        EventDispatcher.add(this.boundHideOverlayWindow, Constants.EVENT_HIDE_OVERLAY);
        EventDispatcher.add(this.boundShowCache, Constants.EVENT_CACHE_STATUS_SHOW);
        EventDispatcher.add(this.boundHideCache, Constants.EVENT_CACHE_STATUS_HIDE);
        EventDispatcher.add(this.boundAddError, Constants.EVENT_SHOW_ERROR);
        EventDispatcher.add(this.boundLoginComplete, Constants.EVENT_LOGIN_COMPLETE);
        EventDispatcher.add(this.boundSetPopupMessage, Constants.EVENT_SET_POPUP_MESSAGE);
        this.onLoggedIn(Constants.REST_API_KEY);
    }

    onWindowGainedFocus() {
        if(this.cacheUpdateId) {
            // user returned before we could initiate cache update
            // this method of updating cache can be aborted since its considered more relaxed 
            // and is a quality of life function, user goes on lunch or is away for a short while
            clearTimeout(this.cacheUpdateId);
            this.cacheUpdateId = null;
        }
        App.onStopInactiveCacheUpdate();
        App.tryUpdateCache();
    }

    onWindowLostFocus() {

        // NOTE: we might also wanna track activity inside the app, ex "user havent moved mouse in the last 5 min"
        //       this would allow the inactive cache updates to start even if the user is still in the application

        // NOTE: this needs to retrigger while the user is inactive (setInterval somehow)

        // if the user is away from the application for more then 5 minutes, update cache
        this.cacheUpdateId = setTimeout(() => {
            App.onStartInactiveCacheUpdate();
        }, 5 * 60 * 1000);
    }

    onLoginComplete() {
        this.setState({loginComplete: true});
    }

    onAddError(message) {
        var error = {
            message: message,
            active: true,
        };
        this.state.errors.push(error);
        this.setState({errors: this.state.errors});
        // set timeout to inactivate the element
        setTimeout(() => {
            error.active = false;
            this.setState({errors: this.state.errors});
            // set timeout check if we should clear the list
            setTimeout(() => {
                var keep = this.state.errors.find((x) => {
                    return x.active;
                }) != null;
                if(!keep) {
                    this.setState({errors: []});
                }
            }, 500);
        }, 10000);
    }

    onSetPopupMessage(text) {
        this.setState({popupText: text});
    }

    onShowPopup(text) {
        var indicator = document.getElementById("save_indicator");
        indicator.classList.add("save_enter_margin");
        this.setState({popupText: text});
    }

    onHidePopup() {
        var indicator = document.getElementById("save_indicator");
        indicator.classList.remove("save_enter_margin");
    }

    onShowSaveIndicator() {
        this.onShowPopup(Localization.get("saving") + "...");
    }

    onShowOverlayWindow(data) {
        App.usage.addFlow(Constants.USAGE_DIALOG, Constants.USAGE_SHOW, ControlUtil.getInnerText(data.title));
        this.state.overlay.push(data);
        if(this.state.overlay.length == 1) {
            var container = document.getElementById("main_container");
            container.classList.add("overlay_effect");
        }
        this.setState({overlay: this.state.overlay});
    }

    onShowCache(cache) {
        var indicator = document.getElementById("cache_indicator");
        indicator.classList.add("save_enter_margin");
        this.setState({cache: cache});
    }

    onHideCache() {
        var indicator = document.getElementById("cache_indicator");
        indicator.classList.remove("save_enter_margin");
    }

    onHideOverlayWindow() {
        App.usage.addFlow(Constants.USAGE_DIALOG, Constants.USAGE_HIDE);
        this.state.overlay.pop();        
        if(this.state.overlay.length == 0) {
            var container = document.getElementById("main_container");
            container.classList.remove("overlay_effect");
        }
        this.setState({overlay: this.state.overlay});
    }

    onLoggedIn(userId) {
        if(userId != null && userId.trim().length > 0) {
            Constants.REST_API_KEY = userId;
            Constants.USER_ID = userId.split("|")[0];
            App.onLoggedIn();
            //this.forceUpdate();
        }
    }

    renderErrors() {
        if(this.state.errors.length) {
            var items = this.state.errors.map((element, i) => {
                return (
                    <div
                        className={element.active ? "" : "app_error_inactive"} 
                        key={i}>
                        {element.message}
                    </div>
                );
            });
            return (
                <div className="app_error_content">
                    <div className="app_error_list font">
                        {items}
                    </div>
                </div>
            );
        }
    }

    renderSaveIndicator() {
        return (
            <div className="save_indicator_content">
                <div
                    id="save_indicator" 
                    className="save_indicator font">
                    <div className="loader"/>
                    <div>
                        {this.state.popupText}
                    </div>
                </div>
            </div>
        );
    }

    renderCachingIndicator() {
        return (
            <div className="save_indicator_content">
                <div
                    id="cache_indicator" 
                    className="save_indicator font">
                    <div className="loader"/>
                    <div>
                        Förnyar cache ({this.state.cache.current} / {this.state.cache.max})
                    </div>
                </div>
            </div>
        );
    }

    renderOverlay() {
        if(this.state.overlay.length > 0) {
            var list = this.state.overlay.map((data, i) => {
                var isTopMost = i == this.state.overlay.length - 1;
                var css = isTopMost ? "overlay_window" : "overlay_window overlay_effect";
                return (                    
                    <div key={i} 
                        className={css}>
                        <DialogWindow 
                            key={data.title + i}
                            title={data.title}
                            isTopMost={isTopMost}>
                            {data.content}
                        </DialogWindow>
                    </div>
                );
            });
            return list;
        }
    }

    renderContent() {
        if(!this.state.loginComplete) {
            return ( <Login onSetUserId={this.onLoggedIn.bind(this)}/> );
        } else {
            return (
                <div 
                    className="root_container"
                    id="main_container">
                    <SidePanel/>
                    <div className="main_content">
                        <MenuPanel/>
                        <MainPanel/>
                    </div>
                </div>
            );
        }
    }

    render() {        
        return (
            <div className="main">
                {this.renderContent()}
                <div id="overlay_window"/>
                {this.renderOverlay()}
                {this.renderErrors()}
                {this.renderCachingIndicator()}
                {this.renderSaveIndicator()}
            </div>
        );
    }
	
}

ReactDOM.render(<Index/>, document.getElementById('content'));