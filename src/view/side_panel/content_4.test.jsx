import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content4 from "./content_4"
import React from "react"

describe("Content4", () => {
  test("renders", () => {
    render(<Content4 />);
    expect(screen.getByText(/Relationssökning/)).toBeDefined();
    expect(screen.getByText(/Begreppsförändringar/)).toBeDefined();
    expect(screen.getByText(/Hänvisningar/)).toBeDefined();
    expect(screen.getByText(/Importera begrepp/)).toBeDefined();
    expect(Content4).toBeInstanceOf(Function);
    expect(Content4).toHaveProperty("prototype");
    expect(Content4.prototype).toBeInstanceOf(Object);
    expect(Content4.prototype.constructor).toBe(Content4);
  });
});