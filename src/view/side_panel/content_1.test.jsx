import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content1 from "./content_1"
import React from "react"

describe("Content1", () => {
  test("renders", () => {
    render(<Content1 />);
    expect(screen.getByText(/Sök/)).toBeDefined();
    expect(screen.getByText(/Utbildning/)).toBeDefined();
    expect(screen.getByText(/Visa avaktualiserade/)).toBeDefined();
    expect(screen.getByText(/Laddar.../)).toBeDefined();
    expect(screen.getAllByRole("img", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("combobox", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("option", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("checkbox", { hidden: true })).toBeDefined();
    expect(Content1).toBeInstanceOf(Function);
    expect(Content1).toHaveProperty("prototype");
    expect(Content1.prototype).toBeInstanceOf(Object);
    expect(Content1.prototype.constructor).toBe(Content1);
  });
});