import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content3 from "./content_3"
import React from "react"

describe("Content3", () => {
  test("renders", () => {
    render(<Content3 />);
    expect(screen.getByText(/Sök/)).toBeDefined();
    expect(screen.getByText(/Utbildning/)).toBeDefined();
    expect(screen.getByText(/Visa avaktualiserade/)).toBeDefined();
    expect(screen.getByText(/Laddar.../)).toBeDefined();
    expect(screen.getAllByRole("img", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("combobox", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("option", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("checkbox", { hidden: true })).toBeDefined();
    expect(Content3).toBeInstanceOf(Function);
    expect(Content3).toHaveProperty("prototype");
    expect(Content3.prototype).toBeInstanceOf(Object);
    expect(Content3.prototype.constructor).toBe(Content3);
  });
});