import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import SidePanel from "./side_panel"
import React from "react"

describe("SidePanel", () => {
  test("renders", () => {
    render(<SidePanel />);
    expect(screen.getByText(/Sök/)).toBeDefined();
    expect(screen.getByText(/Utbildning/)).toBeDefined();
    expect(screen.getByText(/Visa avaktualiserade/)).toBeDefined();
    expect(screen.getByText(/Laddar.../)).toBeDefined();
    expect(screen.getAllByRole("img", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("combobox", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("option", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("checkbox", { hidden: true })).toBeDefined();
    expect(SidePanel).toBeInstanceOf(Function);
    expect(SidePanel).toHaveProperty("prototype");
    expect(SidePanel.prototype).toBeInstanceOf(Object);
    expect(SidePanel.prototype.constructor).toBe(SidePanel);
  });
});