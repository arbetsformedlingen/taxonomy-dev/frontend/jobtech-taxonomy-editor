import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import ConceptsSearch from "./concepts_search"
import React from "react"

describe("ConceptsSearch", () => {
  test("renders", () => {
    render(<ConceptsSearch />);
    expect(screen.getByText(/Sök/)).toBeDefined();
    expect(screen.getByText(/Utbildning/)).toBeDefined();
    expect(screen.getByText(/Visa avaktualiserade/)).toBeDefined();
    expect(screen.getByText(/Laddar.../)).toBeDefined();
    expect(screen.getAllByRole("img", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("combobox", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("option", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("checkbox", { hidden: true })).toBeDefined();
    expect(ConceptsSearch).toBeInstanceOf(Function);
    expect(ConceptsSearch).toHaveProperty("prototype");
    expect(ConceptsSearch.prototype).toBeInstanceOf(Object);
    expect(ConceptsSearch.prototype.constructor).toBe(ConceptsSearch);
  });
});