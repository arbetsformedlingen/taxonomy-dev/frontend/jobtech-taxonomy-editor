import React from 'react';
import Constants from '../../context/constants.jsx';
import Localization from '../../context/localization.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import App from '../../context/app.jsx';
import Button from '../../control/button.jsx';

class Content4 extends React.Component { 

    constructor() {
        super();
    }

    onRelationSearchClicked() {
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CLEAR);
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_RELATION_SEARCH_SELECTED);
    }

    onChangeLogClicked() {
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CLEAR);
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CHANGES_SEARCH_SELECTED);
    }

    onShowReferredClicked() {
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CLEAR);
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_REFERRED_SELECTED);
    }

    onImportNewConcepts() {
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CLEAR);
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_IMPORT_SELECTED);
    }
    
    render() {
        return (
            <div className="side_content_4">
                <Button 
                    text={Localization.get("relation_search")}
                    onClick={this.onRelationSearchClicked.bind(this)}/>
                <Button 
                    text={Localization.get("concept_changes")}
                    onClick={this.onChangeLogClicked.bind(this)}/>
                <Button 
                    text={Localization.get("referred")} 
                    onClick={this.onShowReferredClicked.bind(this)}/>
                <Button 
                    text={Localization.get("import_concepts")}
                    onClick={this.onImportNewConcepts.bind(this)}/>
            </div>
        );
    }
	
}

export default Content4;