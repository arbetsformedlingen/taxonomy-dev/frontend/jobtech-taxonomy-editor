import React from 'react';
import ConceptsSearch from './concepts_search.jsx';
import Settings from './../../context/settings.jsx';
import Localization from './../../context/localization.jsx';
import App from './../../context/app.jsx';
import Constants from './../../context/constants.jsx';
import Util from './../../context/util.jsx';
import Excel from './../../context/excel.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import NewConcept from './../dialog/new_concept.jsx';
import Export from './../dialog/export.jsx';
import Button from './../../control/button.jsx';
import Keybindings from './../../context/keybindings.jsx';

class Content1 extends React.Component { 

    constructor() {
        super();
        this.state = {
            isExportEnabled: true,
        };
        this.type = Settings.getPreferences().tab_1_type;
        this.boundGlobalInput = this.onGlobalInput.bind(this);
    }

    componentDidMount() {
        document.addEventListener('keydown', this.boundGlobalInput);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.boundGlobalInput);
    }

    onGlobalInput(e) {
        if(Keybindings.onKeyBinding(e, Keybindings.BINDING_NEW_CONCEPT)) {
            this.onNewConceptClicked();
        }
    }

    onTypeChanged(type) {
        this.type = type;
        this.setState({isExportEnabled: this.type != "geography_" && this.type != "education_"});
    }

    onNewConceptClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("new_value"),
            content: <NewConcept />
        });
    }

    onExportClicked() {
        var getEdges = (id, edges) => {
            return edges.filter((e) => {
                return e.source === id || e.target === id;
            });
        };
        var getNodesOfType = (type, nodes) => {
            return nodes.filter((x) => {
                return x.type == type;
            });
        };
        var getAllConcepts = (edges, key, concepts) => {
            return concepts.filter((x) => {
                return edges.find((e) => {
                    return e[key] == x.id;
                }) != null;
            });
        };
        var downloadGraphRelations = async (fieldType, data, concepts) => {
            var fields = getNodesOfType(fieldType, data.nodes);
            Util.sortByKey(fields, "preferredLabel", true);
            var version = await Rest.awaitVersionsPromis();
            if(version != null) {
                version = version[version.length - 1].version;
                // setup excel
                var name = Localization.getTypeName(this.type);
                var context = Excel.create(name, name, version);
                var nr = context.addRow();
                context.setCell("C" + nr, name, true);
                context.setCell("G" + nr, "SSYK", true);
                context.setCell("H" + nr, "Namn", true);
                context.setCell("I" + nr, "Databas-ID", true);
                context.addRow();
                // insert data
                for(var i=0; i<fields.length; ++i) {
                    var field = fields[i];
                    var edges = getEdges(field.id, data.edges);
                    var ssyks = getAllConcepts(edges, "source", concepts);
                    Util.sortByKey(ssyks, "ssyk-code-2012", true);
                    if(ssyks.length > 0) {
                        for(var j=0; j<ssyks.length; ++j) {
                            var rowNr = context.addGroupRow(field.preferredLabel, ssyks[j]["ssyk-code-2012"], ssyks[j].preferredLabel);
                            context.setCell("I" + rowNr, ssyks[j].id);
                        }
                        context.addRow();
                    } else {
                        context.addGroupRow(field.preferredLabel);
                    }
                }
                context.download(name + ".xlsx");
                EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
            } else {
                App.showError("Export - getVersion exceptions");
            }
        };
        var exportSsykRelations = (relationType, fieldType) => {
            Rest.getGraph(relationType, this.type, Constants.CONCEPT_SSYK_LEVEL_4, (data) => {
                data = data.graph;
                Rest.getConceptsSsyk(Constants.CONCEPT_SSYK_LEVEL_4, (concepts) => {
                    downloadGraphRelations(fieldType, data, concepts);
                }, (status) => {
                    App.showError(Util.getHttpMessage(status) + " : Misslyckades exportera relationer");
                });
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : Misslyckades exportera relationer");
            });
        };
        // excel
        var onSaveExcel = () => {
            if(this.type == Constants.CONCEPT_OCCUPATION_FIELD) {
                exportSsykRelations(Constants.RELATION_NARROWER, Constants.CONCEPT_OCCUPATION_FIELD);
            } else if(this.type == Constants.CONCEPT_SKILL) {
                exportSsykRelations(Constants.RELATION_RELATED, Constants.CONCEPT_SKILL);
            } else if(this.type == "geography_") {
                // TODO: how to correctly export geography?
            } else if(this.type == "education_") {
                // TODO: how to correctly export education?
            } else {
                Util.getConcepts(this.type, async (data) => {
                    var code = null;
                    var label = null;
                    if(this.type.startsWith("ssyk-")) {
                        code = "ssyk";
                        label = "SSYK";
                    } else if(this.type.startsWith("isco-")) {
                        code = "isco";
                        label = "ISCO";
                    }
                    Util.sortByKey(data, code ? code : "preferredLabel", true);
                    var version = await Rest.awaitVersionsPromis();
                    if(version != null) {
                        version = version[version.length - 1].version;
                        // config
                        var config = {
                            mergeStart: code == null ? 'C' : 'D',
                        };
                        // setup excel
                        var name = Localization.getTypeName(this.type);
                        var context = Excel.create(name, name, version);
                        var nr = context.addRow();
                        if(code != null) {
                            context.setCell('C' + nr, label, true);
                        }
                        context.setCell(config.mergeStart + nr, "Namn", true);
                        context.setCell("I" + nr, "Databas-ID", true);
                        context.addRow();
                        // insert data
                        for(var i=0; i<data.length; ++i) {
                            var rowNr = context.addRow(data[i].preferredLabel, config);
                            if(code != null) {
                                context.setCell('C' + rowNr, data[i][code]);
                            }
                            context.setCell("I" + rowNr, data[i].id);
                        }
                        context.download(name + ".xlsx");
                        EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
                    } else {
                        App.showError("Export - getVersion exceptions");
                    }
                }, (status) => {
                    App.showError(Util.getHttpMessage(status) + " : Misslyckades exportera koncept");
                });
            }
        };
        // event
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("export") + " " + Localization.getTypeName(this.type),
            content: <Export 
                        onSaveExcel={onSaveExcel}/>
        });
    }

    render() {
        return (
            <div className="side_content_1">
                <ConceptsSearch 
                    queryType={this.type}
                    onTypeChangedCallback={this.onTypeChanged.bind(this)}/>
                <div className="side_content_1_buttons">
                    <Button 
                        isEnabled={this.state.isExportEnabled}
                        text={Util.renderExportButtonText()}
                        onClick={this.onExportClicked.bind(this)}/>
                    <Button 
                        text={Localization.get("new_value")}
                        onClick={this.onNewConceptClicked.bind(this)}/>
                </div>
            </div>
        );
    }
	
}

export default Content1;