import React from 'react';
import Button from '../../control/button.jsx';
import Loader from '../../control/loader.jsx';
import TreeView from '../../control/tree_view.jsx';
import ControlUtil from '../../control/util.jsx';
import Rest from '../../context/rest.jsx';
import Util from '../../context/util.jsx';
import Constants from '../../context/constants.jsx';
import Localization from '../../context/localization.jsx';
import Settings from '../../context/settings.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import App from '../../context/app.jsx';
import Label from '../../control/label.jsx';
import ConceptWrapper from '../../control/concept_wrapper.jsx';
import Keybindings from './../../context/keybindings.jsx';

class ConceptsSearch extends React.Component { 

    constructor() {
        super();

        this.TYPE_SEARCH = "search";
        this.TYPE_CONTINENT = "continent";
        this.TYPE_COUNTRY = "country";
        this.TYPE_MUNICIPALITY = "municipality";
        this.TYPE_REGION = "region";
        this.TYPE_SKILL = "skill";
        this.TYPE_SKILL_HEADLINE = "skill-headline";
        this.TYPE_GENERIC_SKILL = "generic-skill";
        this.TYPE_SKILL_GROUP = "skill-group";
        this.TYPE_GENERIC_SKILL_GROUP = "generic-skill-group";
        this.NOTYPE_GEOGRAPHY = "geography_";
        this.NOTYPE_EDUCATION = "education_";
        this.FILTER_CONTAINS = "contains";
        this.FILTER_NOTCONTAINS = "not_contains";
        this.FILTER_STARTSWITH = "starts_with";
        this.FILTER_ENDSWITH = "ends_with";

        this.state = {
            data: [],
            queryType: this.TYPE_SSYK_LEVEL_4,
            selectableType: true,
            loadingData: false,
            showDeprecated: false,
            isQuickEditEnabled: false,
            filter: "",
            filterType: this.FILTER_CONTAINS,
            quickLinks: [],
            quickSearches: [],
            showChooseFilter: false,
        };
        this.isCtrlDown = false;

        this.autoselectFirst = true;
        this.options = this.populateOptions();
        this.queryTreeView = ControlUtil.createTreeView();
        this.queryTreeView.onItemSelected = this.onQueryItemSelected.bind(this);
        this.queryTreeView.onAllowItemSelection = this.onAllowItemSelection.bind(this);
        this.boundMainItemSelected = this.onMainItemSelected.bind(this);
        this.boundNewConcept = this.onNewConcept.bind(this);
        this.boundUserState = this.onUserState.bind(this);
        this.boundQuickEditChange = this.onQuickEditChanged.bind(this);
        this.boundCacheUpdated = this.onCacheUpdated.bind(this);
        this.boundQuickEditModeChanged = this.onQuickEditModeChanged.bind(this);
        this.boundGlobalInputDown = this.onGlobalInputDown.bind(this);
        this.boundGlobalInputUp = this.onGlobalInputUp.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundMainItemSelected, Constants.EVENT_MAINPANEL_ITEM_SELECTED);
        EventDispatcher.add(this.boundNewConcept, Constants.EVENT_NEW_CONCEPT);
        EventDispatcher.add(this.boundUserState, Constants.EVENT_SOCKET_USER_STATE);
        EventDispatcher.add(this.boundQuickEditChange, Constants.EVENT_QUICK_EDITS_CHANGED);
        EventDispatcher.add(this.boundCacheUpdated, Constants.EVENT_CACHE_UPDATED);
        EventDispatcher.add(this.boundQuickEditModeChanged, Constants.EVENT_QUICK_EDIT);
        if(this.props.queryType) {
            this.state.queryType = this.props.queryType;
            App.onViewType(this.state.queryType);
        }
        if(this.props.lockToType) {
            this.state.selectableType = false;
            this.state.queryType = this.props.lockToType;
        } else {
            this.state.selectableType = true;
        }
        var quickLinks = localStorage.getItem(Settings.getKey("quickLinks"));
        if(quickLinks) {
            this.state.quickLinks = JSON.parse(quickLinks);
        }
        var quickSearches = localStorage.getItem(Settings.getKey("quickSearches"));
        if(quickSearches) {
            this.state.quickSearches = JSON.parse(quickSearches);
        }
        this.search();
        document.addEventListener('keydown', this.boundGlobalInputDown);
        document.addEventListener('keyup', this.boundGlobalInputUp);
    }

    UNSAFE_componentWillReceiveProps(props) {
        var prevType = this.state.queryType;
        if(props.queryType) {
            this.state.queryType = props.queryType;
        }
        if(props.lockToType) {
            this.state.selectableType = false;
            this.state.queryType = props.lockToType;
        } else {
            this.state.selectableType = true;
        }
        if(prevType != this.state.queryType) {
            this.search();
        }
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundMainItemSelected);
        EventDispatcher.remove(this.boundNewConcept);
        EventDispatcher.remove(this.boundUserState);
        EventDispatcher.remove(this.boundQuickEditChange);
        EventDispatcher.remove(this.boundCacheUpdated);
        EventDispatcher.remove(this.boundQuickEditModeChanged);
        document.removeEventListener('keydown', this.boundGlobalInputDown);
        document.removeEventListener('keyup', this.boundGlobalInputUp);
    }

    onQuickEditModeChanged(enabled) {
        this.queryTreeView.allowMultiSelect = enabled;
        this.queryTreeView.clearSelected();
        this.setState({isQuickEditEnabled: enabled});
    }

    onCacheUpdated(ids) {
        for(var i=0; i<ids.length; ++i) {
            var child = this.queryTreeView.findChild((x) => {
                return x.data && x.data.id == ids[i];
            });
            if(child) {
                var cached = App.findCachedConcept(ids[i], child.type);
                if(cached) {
                    child.data.preferredLabel = cached.preferredLabel;
                    this.setupItemLabel(child.data);
                    this.setupTreeViewItemText(child, child.data);
                }
            }
        }
    }

    onQuickEditChanged(change) {
        if(change.type == Constants.QUICK_EDIT_NAME) {
            // TODO: we need to handle these special cases in order to update correctly
            /*if(this.state.queryType == this.TYPE_SKILL) {
                
            } else if(this.state.queryType == this.NOTYPE_GEOGRAPHY) {
                
            } else if(this.state.queryType == this.NOTYPE_EDUCATION) {
                
            }*/
            if(change.concept.type == this.state.queryType) {
                var child = this.queryTreeView.findChild((x) => {
                    return x.data && x.data.id == change.concept.id;
                });
                if(child) {
                    Util.applyQuickEdits(child.data);
                    this.setupItemLabel(child.data);
                    this.setupTreeViewItemText(child, child.data);
                }
            }
        }
    }

    onUserState(user) {
        for(var i=0; i<user.states.length; ++i) {
            var state = user.states[i];
            if(state.type == "new_concept") {
                var value = state.value;
                this.onNewConcept({
                    concept: {
                        id: value.id,
                        type: value.type,
                        preferredLabel: value.preferredLabel,
                        label: value.preferredLabel, 
                    }
                });
            } else if(state.type == "deprecated_concept") {
                var value = state.value;
                var type = value.type == Constants.CONCEPT_SKILL_HEADLINE ? Constants.CONCEPT_SKILL : value.type;
                if(type == this.state.queryType) {
                    // NOTE: we dont handle when a concept becomes re-enabled (its super rare)
                    var concept = this.findById(this.state.data, value.id);
                    if(concept && value.deprecated) {
                        concept.deprecated = true;
                        // TODO: remove item from list
                    }
                }
            }
        }
    }

    onDragStart(concept, e) {
        var items = this.queryTreeView.selection.map((item) => {
            return {
                id: item.data.id,
                type: item.data.type,
                preferredLabel: item.data.preferredLabel,
                label: item.data.label
            };
        });
        if(items.find((x) => {return x.id == concept.id;}) == null) {
            items.push({
                id: concept.id,
                type: concept.type,
                preferredLabel: concept.preferredLabel,
                label: concept.label,
            });
        }
        e.dataTransfer.setData("text", JSON.stringify(items));
    }

    onDragEnd(item, e) {
        // force reselct of item
        this.queryTreeView.reselect(item);
    }

    onGlobalInputDown(e) {
        if(!e.repeat && e.key == "Control") {
            this.queryTreeView.isCtrlDown = true;
        }
        if(Keybindings.onKeyBinding(e, Keybindings.BINDING_FOCUS_SEARCH)) {
            document.getElementById("concept_search").focus();
        } else 
        if(Keybindings.onKeyBinding(e, Keybindings.BINDING_FOCUS_TYPES)) {
            document.getElementById("concept_type").focus();
        }
    }
    
    onGlobalInputUp(e) {
        if(!e.repeat && e.key == "Control") {
            this.queryTreeView.isCtrlDown = false;
        }
    }

    onInputKeyUp(e) {
        if(this.state.queryType == this.TYPE_SEARCH && e.key === "Enter") {
            this.onSearchClicked();
        }
    }

    setupTreeViewItemText(item, concept) {
        item.setText(
            <div 
                onDragStart={this.onDragStart.bind(this, concept)}
                onDragEnd={this.onDragEnd.bind(this, item)}
                draggable="true">
                <ConceptWrapper concept={concept}>{concept.label}</ConceptWrapper>
            </div>
        );
    }

    addOption(options, type){
        if(Settings.isVisible(type)) {
            options.push({value: type, text: Localization.getTypeName(type)});    
        }
    }

    populateOptions() {
        var options = [];        
        for(var i=0; i<App.types.length; ++i) {
            if(!App.isSpecialType(App.types[i])) {
                this.addOption(options, App.types[i]);
            }
        }
        // manually add special type
        this.addOption(options, this.NOTYPE_GEOGRAPHY);
        this.addOption(options, this.NOTYPE_EDUCATION);
        this.addOption(options, "skill");
        options.sort((a, b) => {
            if(a.text < b.text) {
                return -1;
            }
            if(a.text > b.text) {
                return 1;
            }
            return 0;
        });
        return options;
    }

    hasVisibleChild(items, query) {
        for(var i=0; i<items.length; ++i) {
            if(this.isValidToShow(items[i], query)) {
                return true;
            }
        }
        return false;
    }
    
    isValidToShow(item, query) {
        var isDeprecated = item.deprecated ? item.deprecated : false;
        var show = false;
        var filterResult = () => {
            switch(this.state.filterType) {
                default:
                case this.FILTER_CONTAINS:
                    return item.label.toLowerCase().includes(query);
                case this.FILTER_STARTSWITH:
                    return item.label.toLowerCase().startsWith(query) || (item.preferredLabel && item.preferredLabel.toLowerCase().startsWith(query));
                case this.FILTER_ENDSWITH:
                    return item.label.toLowerCase().endsWith(query);                    
                case this.FILTER_NOTCONTAINS:
                    return !item.label.toLowerCase().includes(query);
            }
        };
        if(this.state.showDeprecated == isDeprecated) {
            show = true;
            if(query.length > 0) {                                
                if(!filterResult() && 
                   (item.id == null || (item.id.length < 3 || item.id.toLowerCase() !== query))) {
                    show = false;
                }
            }
        }
        if(show == false && item.children) {
            show = this.hasVisibleChild(item.children, query);
        }
        return show;
    }

    populateTree(data, query) {
        var addChildNodes = null;
        var setupChildren = (items, node) => {
            if(items.length > 500) {
                var count = items.length / 500;
                for(var j=0; j<count; ++j) {
                    var children = items.slice(j * 500, (j + 1) * 500);
                    var first = children[0].label;
                    var last = children[children.length - 1].label;
                    var label = first[0] + " - " + last[0];
                    var subItem = {
                        id: "" + j,
                        label: label,
                        children: children,
                    };
                    if(this.isValidToShow(subItem, query)) {
                        var subNode = ControlUtil.createTreeViewItem(this.queryTreeView, subItem);
                        subNode.setShowButton(subItem.children && subItem.children.length > 0);
                        subNode.setText(subItem.label);
                        node.addChild(subNode);
                        setupChildren(subItem.children, subNode);
                    }
                } 
            } else {
                addChildNodes(items, node);
            }
        };

        addChildNodes = (items, node) => {
            for(var i=0; i<items.length; ++i) {
                var item = items[i];
                if(this.isValidToShow(item, query)) {
                    var child = ControlUtil.createTreeViewItem(this.queryTreeView, item);
                    child.setShowButton(item.children && item.children.length > 0);
                    this.setupTreeViewItemText(child, item);
                    node.addChild(child);
                    if(item.children) {
                        setupChildren(item.children, child);
                    }
                }
            }
        };
        // setup the treeviewitem
        this.queryTreeView.shouldUpdateState = false;
        var roots = [];
        for(var i=0; i<data.length; ++i) {
            var item = data[i];            
            if(this.isValidToShow(item, query)) {                
                var node = ControlUtil.createTreeViewItem(this.queryTreeView, item);
                node.setShowButton(item.children && item.children.length > 0);
                this.setupTreeViewItemText(node, item);
                if(this.state.queryType == this.TYPE_SEARCH) {
                    // only add a type root if we are in searchmode, multiple concept with same name but different type might show up
                    var root = roots.find((r) => { return r.data ? item.type == r.data.type : false; });
                    if(!root) {
                        root = ControlUtil.createTreeViewItem(this.queryTreeView, {type: item.type});
                        root.setText(Localization.getTypeName(item.type));           
                        roots.push(root);
                        this.queryTreeView.addRoot(root);
                    }
                    root.addChild(node);
                } else {
                    this.queryTreeView.addRoot(node);
                }
                if(item.children) {
                    setupChildren(item.children, node);
                }
            }
        }
        if(this.state.queryType == this.TYPE_SEARCH) {
            Util.sortByKey(this.queryTreeView.roots, "text", true);
        }
        // expand roots when applicable
        for(var i=0; i<this.queryTreeView.roots.length; ++i) {
            var root = this.queryTreeView.roots[i];
            if(root.children.length < 3) {
                root.setExpanded(true);
            }
        }
        if(this.queryTreeView.roots.length == 1) {
            // always expand when only one root
            this.queryTreeView.roots[0].setExpanded(true);
        }
        this.queryTreeView.shouldUpdateState = true;
        this.queryTreeView.invalidate();
    }

    updateQuickLinks(type) {
        var quickLinks = this.state.quickLinks.filter((l) => {return l != type;});
        quickLinks.unshift(type);
        if(quickLinks.length > 4) {
            // make sure array is not to big
            quickLinks = quickLinks.slice(0, 4);
        }
        quickLinks = quickLinks.filter((l) => {return l != null;});
        localStorage.setItem(Settings.getKey("quickLinks"), JSON.stringify(quickLinks));
        this.setState({quickLinks: quickLinks});
    }

    updateQuickSearch(type, query) {
        var element = this.state.quickSearches.find((x) => {
            return x.type == type && x.query == query;
        });
        if(element == null && query.length > 0) {
            element = {
                type: type,
                query: query,
            };
            this.state.quickSearches.unshift(element);
            if(this.state.quickSearches.length > 3) {
                this.state.quickSearches.splice(3, 1);
            }
            localStorage.setItem(Settings.getKey("quickSearches"), JSON.stringify(this.state.quickSearches));
            this.setState({quickSearches: this.state.quickSearches});
        }
    }

    onFilterChanged(e) {
        this.setState({filter: e.target.value}, () => {
            if(this.state.queryType != this.TYPE_SEARCH) {
                this.filterAndPopulate(this.state.filter);
            }
            if(this.updateId != null) {
                clearTimeout(this.updateId);
            }
            this.updateId = setTimeout(() => {
                if(this.state.queryType != this.TYPE_SEARCH) {
                    this.updateQuickLinks(this.state.queryType);
                } else {
                    this.updateQuickSearch(this.state.queryType, this.state.filter);
                }
                this.updateId = null;
            }, 2500);
        });
    }

    onFetchComplete() {
        if(this.preSelectId) {
            var item = this.queryTreeView.findChild((item) => {
                return item.data.id == this.preSelectId;
            });
            if(item) {
                this.preSelectId = null;
                this.queryTreeView.setSelected(item, true);
                if(item.parent) {                    
                    var parent = item.parent;
                    parent.setExpanded(true, () => {
                        var element = document.getElementById("" + item.uniqueId);
                        if(element) {
                            element.scrollIntoView({
                                behavior: "auto", 
                                block: "center", 
                                inline: "start"
                            });
                        }
                    });
                    parent = parent.parent;
                    while(parent) {
                        parent.setExpanded(true);
                        parent = parent.parent;
                    }
                }
            } else if(this.queryTreeView.roots.length == 0) {
                //Try again when we have data
            } else {
                this.preSelectId = null;
            }
        } else if(this.autoselectFirst) {
            if(this.queryTreeView.roots.length > 0) {
                this.queryTreeView.setSelected(this.queryTreeView.roots[0], true);
            }
        }
        this.autoselectFirst = false;
    }

    onDataFetchSuccess(data) {
        this.state.data.push(...data);
        this.prepareData(this.state.data);
        this.sortData(this.state.data);
        this.filterAndPopulate(this.state.filter);        
        this.setState({loadingData: false}, this.onFetchComplete());
    }

    fetchSkills() {
        Rest.getConceptsSkillsAndHedlines(this.onDataFetchSuccess.bind(this), (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
        });
    }

    fetchGenericSkills() {
        Rest.getConceptsGenericSkillsAndGroups(this.onDataFetchSuccess.bind(this), (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
        });
    }
    fetchGeography() {
        Rest.getConceptsGeography(this.onDataFetchSuccess.bind(this), (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
        });
    }

    fetchEducation() {
        Rest.getConceptsEducation(this.onDataFetchSuccess.bind(this), (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
        });
    }

    fetch() {
        var types = App.getCachedTypes(this.state.queryType);
        if(types == null || types.length == 0) {
            Util.getConcepts(this.state.queryType, this.onDataFetchSuccess.bind(this), (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
            });
        } else {
            this.onDataFetchSuccess(types);
        }
    }

    setupItemLabel(concept) {
        concept.label = concept.preferredLabel;
        if(concept.type === Constants.CONCEPT_ISCO_LEVEL_4) {
            concept.label = concept.isco ? concept.isco + " - " + concept.label : concept.label;
        } else if(concept.type === Constants.CONCEPT_SSYK_LEVEL_4 ||
            concept.type === Constants.CONCEPT_SSYK_LEVEL_3 ||
            concept.type === Constants.CONCEPT_SSYK_LEVEL_2 ||
            concept.type === Constants.CONCEPT_SSYK_LEVEL_1) {
            concept.label = concept.ssyk ? concept.ssyk + " - " + concept.label : concept.label;
        } else if(concept.type == Constants.CONCEPT_SUN_EDUCATION_LEVEL_1 ||
            concept.type == Constants.CONCEPT_SUN_EDUCATION_LEVEL_2 ||
            concept.type == Constants.CONCEPT_SUN_EDUCATION_LEVEL_3) {
            concept.label = concept.sun_education_level_code_2020 ? concept.sun_education_level_code_2020 + " - " + concept.label : concept.label;
        } else if(concept.type == Constants.CONCEPT_SUN_EDUCATION_FIELD_1 ||
            concept.type == Constants.CONCEPT_SUN_EDUCATION_FIELD_2 ||
            concept.type == Constants.CONCEPT_SUN_EDUCATION_FIELD_3 ||
            concept.type == Constants.CONCEPT_SUN_EDUCATION_FIELD_4) {
            concept.label = concept.sun_education_field_code_2020 ? concept.sun_education_field_code_2020 + " - " + concept.label : concept.label;
        }
    }

    prepareData(data) {
        for(var i=0; i<data.length; ++i) {
            var concept = data[i];
            Util.applyQuickEdits(concept);
            this.setupItemLabel(concept);
            if(concept.children) {
                this.prepareData(concept.children);
            }
        }
    }

    sortData(data) {
        Util.sortByKey(data, "label", true);
        Util.sortByKey(data, "sort_order", true);
        for(var i=0; i<data.length; ++i) {
            if(data[i].children) {
                this.sortData(data[i].children);
            }
        }
    }

    search(query) {
        this.queryTreeView.clear();
        this.state.data = [];
        Rest.abort();
        if(this.state.queryType == this.TYPE_SEARCH) {
            if(query && query.length > 0) {
                this.setState({loadingData: true});
                Rest.searchConcepts(query, (data) => {
                    this.state.data = data;
                    this.prepareData(data);
                    this.filterAndPopulate("");
                    this.setState({loadingData: false});
                }, (status) => {
                    App.showError(Util.getHttpMessage(status) + " : sökning misslyckades");
                });
            }
        } else {
            this.updateQuickLinks(this.state.queryType);
            this.setState({loadingData: true});
            if(this.state.queryType == this.TYPE_SKILL) {
                this.fetchSkills();
            } else if(this.state.queryType == this.NOTYPE_GEOGRAPHY) {
                this.fetchGeography();
            } else if(this.state.queryType == this.NOTYPE_EDUCATION) {
                this.fetchEducation();
            } else if(this.state.queryType == this.TYPE_GENERIC_SKILL) {
                this.fetchGenericSkills();
            } else {
                //this.loadStartTime = new Date().getTime();
                this.fetch();
            }
        }
    }

    findById(data, id) {
        for(var i=0; i<data.length; ++i) {
            var item = data[i];
            if(item.id === id) {
                return item;
            } else if(item.children) {
                var foundItem = this.findById(item.children, id);
                if(foundItem != null) {
                    return foundItem;
                }
            }
        }
        return null;
    }

    filterVisibleData(data, query) {
        return data.filter((item) => { 
            var isDeprecated = item.deprecated ? item.deprecated : false;
            if(this.state.showDeprecated == isDeprecated) {
                return true;
            } else if(item.children) {
                return this.hasVisibleChild(item.children, query == null ? "" : query.toLowerCase());
            } else {
                return false;
            }
        });
    }

    filterAndPopulate(query) {
        this.queryTreeView.clear();
        if(this.preSelectId) {
            //find item and check if deprecated
            var item = this.findById(this.state.data, this.preSelectId);
            if(item) {
                var isDeprecated = item.deprecated ? item.deprecated : false;
                this.state.showDeprecated = isDeprecated;
            }
        }
        var data = this.filterVisibleData(this.state.data, query);
        this.sortData(data);
        this.populateTree(data, query.toLowerCase());
    }

    onSaveDialogResult(item, result) {
        if(result != Constants.DIALOG_OPTION_ABORT) {
            item.setSelected(true);
            // user saved or discared changes and wants to continue
            EventDispatcher.fire(Constants.EVENT_SIDEPANEL_ITEM_SELECTED, item.data);
        }
    }

    onQueryItemSelected(item) {
        if(item.data && item.data.id && !this.state.isQuickEditEnabled) {
            Util.gotoConcept(item.data);
        }
    }

    onMainItemSelected(item) {
        if(item && item.id) {
            this.setState({
                showDeprecated: item.deprecated ? true : false,
            }, () => {
                this.state.filter = "";
                this.expandedItem = null;
                this.preSelectId = item.id;
                var type = item.type;
                if(item.type == this.TYPE_SKILL_HEADLINE) {
                    type = this.TYPE_SKILL;
                } else if(item.type == this.TYPE_GENERIC_SKILL_GROUP || item.type == this.TYPE_SKILL_GROUP) {
                    type = this.TYPE_GENERIC_SKILL;
                } else if(item.type == this.TYPE_CONTINENT || item.type == this.TYPE_COUNTRY || item.type == this.TYPE_MUNICIPALITY || item.type == this.TYPE_REGION) {
                    type = this.NOTYPE_GEOGRAPHY;
                } else if(item.type == this.TYPE_CONTINENT || item.type == this.TYPE_COUNTRY || item.type == this.TYPE_MUNICIPALITY || item.type == this.TYPE_REGION) {
                    type = this.NOTYPE_GEOGRAPHY;
                }

                if(type == this.state.queryType) {
                    this.filterAndPopulate(this.state.filter);
                    this.onFetchComplete();
                } else {
                    this.setState({                        
                        queryType: type,
                    }, () => {
                        this.search();
                    });
                }    
            });
        }
    }
    
    onNewConcept(data) {
        if(data.concept) {
            data.concept.deprecated = false;
            this.prepareData([data.concept]);
            var type = data.concept.type == Constants.CONCEPT_SKILL_HEADLINE ? Constants.CONCEPT_SKILL : data.concept.type;
            if(type == this.state.queryType) {
                // create tree node for item
                var node = ControlUtil.createTreeViewItem(this.queryTreeView, data.concept);
                this.setupTreeViewItemText(node, data.concept);
                // update structure correctly
                if(data.concept.type == Constants.CONCEPT_SKILL) {
                    if(data.parent != null) {
                        var parent = this.state.data.find((a) => {
                            return a.id === data.parent.id;
                        });
                        if(parent) {
                            parent.children.push(data.concept);
                        } else {
                            // super fel
                        }
                    }
                } else {
                    this.state.data.push(data.concept);
                }
                // update tree
                if(data.parent) {
                    var root = this.queryTreeView.roots.find((d) => {
                        return d.data.id === data.parent.id;
                    });
                    if(root) {
                        // update tree item
                        root.setForceShowButton(true);
                        root.setShowButton(true);
                        root.addChild(node);
                        root.sortChildren((children) => {
                            Util.sortByCmp(children, (v) => {
                                return v.data.label;
                            }, true);
                        });
                    }
                } else {
                    this.queryTreeView.addRoot(node);
                    Util.sortByCmp(this.queryTreeView.roots, (v) => {
                        return v.data.label;
                    }, true);
                }
                this.forceUpdate();
            } else {
                if(this.options.find((e) => { return e.type == data.concept.type; }) == null) {
                    this.options = this.populateOptions();
                    this.forceUpdate();
                }
            }
        }
    }

    onAllowItemSelection(item) {
        if(App.hasUnsavedChanges()) {
            App.showSaveDialog(this.onSaveDialogResult.bind(this, item));
            return false;
        }
        return true;
    }

    onQuickLinkClicked(type) {
        this.expandedItem = null;
        this.state.queryType = type;
        this.setState({
            filter: "",
        }, () => {
            App.onViewType(this.state.queryType);
            this.search();
        });
        if(this.props.onTypeChangedCallback) {
            this.props.onTypeChangedCallback(type);
        }
    }

    onQuickSearchLinkClicked(search) {
        if(search.type != this.state.queryType || search.type == "search") {
            this.setState({
                queryType: search.type,
                filter: search.query,
            }, () => {
                App.onViewType(this.state.queryType);
                this.search(search.query);
            });
        } else {
            this.setState({filter: search.query}, () => {
                this.filterAndPopulate(search.query);
            });
        }
    }

    onTypeChanged(e) {
        this.expandedItem = null;
        this.state.queryType = e.target.value;
        this.setState({
            filter: "",
        }, () => {
            App.onViewType(this.state.queryType);
            this.search();
        });
        if(this.props.onTypeChangedCallback) {
            this.props.onTypeChangedCallback(e.target.value);
        }
    }

    onSearchClicked() {
        if(this.state.queryType == this.TYPE_SEARCH) {
            this.search(this.state.filter);
        } else {
            this.filterAndPopulate(this.state.filter);
        }
    }

    onShowDeprecatedChanged(e) {
        this.setState({showDeprecated: e.target.checked}, () => {
            this.filterAndPopulate(this.state.filter);
        });
    }

    onChooceFilteTypeClicked() {
        this.setState({
            showChooseFilter: !this.state.showChooseFilter,
        });
    }

    onFilterTypeSelected(type) {
        this.setState({
            filterType: type,
            showChooseFilter: false,
        }, () => {
            if(this.state.filter.length > 0) {
                this.filterAndPopulate(this.state.filter);
            }
        });
    }

    renderOptions() {
        var options = this.options.map((o, i) => {
            return this.renderOption(o.value, o.text, i);
        });
        return options;
    }

    renderOption(value, text, key) {
        return (
            <option 
                value={value} 
                key={key}>
                {Localization.get(text)}
            </option>
        );
    }

    renderDropDown() {
        if(this.state.selectableType) {
            return(
                <select 
                    id="concept_type"
                    className="sub_panel_select rounded search_type_offset"
                    value={this.state.queryType}
                    onChange={this.onTypeChanged.bind(this)}>
                    {this.renderOption(this.TYPE_SEARCH, "search")}
                    {this.renderOptions()}
                </select>
            );
        }
    }

    renderChooseFilter() {
        var renderFilter = (type) => {
            return (
                <div
                    className={this.state.filterType == type ? "selected" : ""}
                    onClick={this.onFilterTypeSelected.bind(this, type)}>
                    {Localization.get(type)}
                </div>
            );
        };        
        if(this.state.showChooseFilter) {
            // {renderFilter(this.FILTER_NOTCONTAINS)}
            return (
                <div className="sub_panel_choose_filter font">
                    {renderFilter(this.FILTER_CONTAINS)}
                    {renderFilter(this.FILTER_STARTSWITH)}
                    {renderFilter(this.FILTER_ENDSWITH)}                    
                </div>
            );
        }
    }

    renderSearch() {
        var element;
        if(this.state.queryType == this.TYPE_SEARCH) {
            element = (
                <div className="sub_panel_filter">
                    <Button                         
                        text={<img src={Constants.ICON_FILTER} className="sub_panel_chooce_filter_icon" />} 
                        onClick={this.onChooceFilteTypeClicked.bind(this)}/>
                    {this.renderChooseFilter()}
                    <Button 
                        text={Localization.get("search")} 
                        onClick={this.onSearchClicked.bind(this)}/>
                </div>
            );
        } else {
            element = (
                <div className="sub_panel_filter">
                    <Button                         
                        text={<img src={Constants.ICON_FILTER} className="sub_panel_chooce_filter_icon" />} 
                        onClick={this.onChooceFilteTypeClicked.bind(this)}/>
                    {this.renderChooseFilter()}
                </div>
            );
        }
        return(
            <div className="sub_panel_search">
                <input 
                    id="concept_search"
                    type="text"
                    className="rounded"
                    value={this.state.filter}
                    placeholder={this.state.queryType == this.TYPE_SEARCH ? "" : Localization.get("filter")}
                    onChange={this.onFilterChanged.bind(this)}
                    onKeyUp={this.onInputKeyUp.bind(this)}/>
                {element}                
            </div>
        );
    }

    renderShowDeprecated() {
        return(
            <div className="sub_panel_show_deprecated">
                <input 
                    type="checkbox"                        
                    onChange={this.onShowDeprecatedChanged.bind(this)}
                    checked={this.state.showDeprecated}/>
                <Label text={Localization.get("show_deprecated")}/>
            </div>
        );
    }

    renderLatestVisit() {
        if(this.state.quickLinks.length > 1) {
            var links = [];
            for(var i=1; i<this.state.quickLinks.length; ++i) {
                links.push(
                    <div 
                        key={i}
                        className="sub_panel_quick_link font" 
                        title={Localization.getTypeName(this.state.quickLinks[i])}
                        onClick={this.onQuickLinkClicked.bind(this, this.state.quickLinks[i])}>
                        {Localization.getTypeName(this.state.quickLinks[i])}
                    </div>
                );
            }        
            return (
                <div className="sub_panel_quick_links">                
                    {links}
                </div>
            );
        }
    }

    renderLatestSearches() {
        if(this.state.quickSearches.length > 0) {
            var links = this.state.quickSearches.map((element, index) => {
                return (
                    <div 
                        key={index}
                        className="sub_panel_quick_link font" 
                        title={Localization.getTypeName(element.type) + " - " + element.query}
                        onClick={this.onQuickSearchLinkClicked.bind(this, element)}>
                        {element.query}
                    </div>
                );
            });   
            return (
                <div className="sub_panel_quick_links">                
                    {links}
                </div>
            );
        }
    }

    renderQuickLinks() {
        var header = null;
        if(this.state.quickLinks.length > 1 && this.state.quickSearches.length > 0) {
            header = (
                <div>
                    <div>Besökt</div>
                    <div>Sökningar</div>
                </div>
            );
        } else if(this.state.quickLinks.length > 1) {
            header = ( <div>Besökt</div> );
        } else {
            header = ( <div>Sökningar</div> );
        }
        return (
            <div className="sub_panel_quick_links_group font">
                {header}
                <div>
                    {this.renderLatestVisit()}
                    {this.renderLatestSearches()}
                </div>
            </div>
        );
    }

    renderLoader() {
        if(this.state.loadingData) {
            return <Loader/>;
        }
    }

    render() {
        return (
            <div className="concepts_search">
                <div>
                    {(this.state.quickLinks.length > 1 || this.state.quickSearches.length > 0) &&
                        this.renderQuickLinks()
                    }
                    {this.renderDropDown()}
                    {this.renderSearch()}                    
                    {this.renderShowDeprecated()}
                </div>
                <TreeView context={this.queryTreeView} css="no_background">
                    {this.renderLoader()}
                </TreeView>
            </div>
        );
    }
}

export default ConceptsSearch;