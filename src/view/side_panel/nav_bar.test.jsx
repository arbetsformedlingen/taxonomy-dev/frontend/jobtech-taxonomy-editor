import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import NavBar from "./nav_bar"
import React from "react"

describe("NavBar", () => {
  test("renders", () => {
    render(<NavBar />);
    expect(screen.getAllByRole("img", { hidden: true })).toBeDefined();
    expect(NavBar).toBeInstanceOf(Function);
    expect(NavBar).toHaveProperty("prototype");
    expect(NavBar.prototype).toBeInstanceOf(Object);
    expect(NavBar.prototype.constructor).toBe(NavBar);
  });
});