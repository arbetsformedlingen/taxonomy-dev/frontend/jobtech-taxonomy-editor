import React from 'react';
import Constants from '../../context/constants.jsx';
import App from '../../context/app.jsx';
import Button from '../../control/button.jsx';
import Localization from '../../context/localization.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import UserManual from '../dialog/user_manual.jsx';

class Content5 extends React.Component { 

    constructor() {
        super();       
    }

    onVisibilityClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_SETTINGS_PAGE, Constants.SETTINGS_PAGE_VISIBILITY);
    }

    onTypeLocalizationClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_SETTINGS_PAGE, Constants.SETTING_PAGE_TYPE_LOCALIZATION);
    }
    
    onValidationClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_SETTINGS_PAGE, Constants.SETTINGS_PAGE_VALIDATION);
    }

    onKeyBindingsClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_SETTINGS_PAGE, Constants.SETTINGS_PAGE_KEY_BINDINGS)
    }

    onUserManualClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("settings_user_manual"),
            content: <UserManual />
        });
    }

    onInfoClicked() {
        EventDispatcher.fire(Constants.EVENT_SET_SETTINGS_PAGE, Constants.SETTINGS_PAGE_INFO)
    }

    onRefreshCache() {
        App.tryUpdateCache(0);
    }

    render() {
        return (
            <div className="side_content_5">
                <Button 
                    text={Localization.get("settings_visibility")}
                    onClick={this.onVisibilityClicked.bind(this)}/>
                <Button 
                    text={Localization.get("settings_type_localization")}
                    onClick={this.onTypeLocalizationClicked.bind(this)}/>
                <Button 
                    text={Localization.get("settings_validation")}
                    onClick={this.onValidationClicked.bind(this)}/>
                <Button 
                    text={Localization.get("settings_key_bindings")}
                    onClick={this.onKeyBindingsClicked.bind(this)}/>
                <Button 
                    text={Localization.get("settings_user_manual")}
                    onClick={this.onUserManualClicked.bind(this)}/>
                <Button 
                    text={Localization.get("settings_info")}
                    onClick={this.onInfoClicked.bind(this)}/>
                <div className="side_content_5_divider"/>
                <Button 
                    text={Localization.get("refresh_cache")}
                    onClick={this.onRefreshCache.bind(this)}/>
            </div>
        );
    }
	
}

export default Content5;