import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content5 from "./content_5"
import React from "react"

describe("Content5", () => {
  test("renders", () => {
    render(<Content5 />);
    expect(screen.getByText(/Synlighet/)).toBeDefined();
    expect(screen.getByText(/Typbenämningar/)).toBeDefined();
    expect(screen.getByText(/Versions undantag/)).toBeDefined();
    expect(screen.getByText(/Kortkommandon/)).toBeDefined();
    expect(screen.getByText(/Användarmanual/)).toBeDefined();
    expect(screen.getByText(/Information/)).toBeDefined();
    expect(screen.getByText(/Uppdatera cache/)).toBeDefined();
    expect(Content5).toBeInstanceOf(Function);
    expect(Content5).toHaveProperty("prototype");
    expect(Content5.prototype).toBeInstanceOf(Object);
    expect(Content5.prototype.constructor).toBe(Content5);
  });
});