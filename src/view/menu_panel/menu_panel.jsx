import React from 'react';
import Constants from './../../context/constants.jsx';
import Container from './../container.jsx';
import App from '../../context/app.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Panel from './../../control/panel.jsx';
import Button from './../../control/button.jsx';
import QuickEditViewer from '../dialog/quick_edit_viewer.jsx';
import Content1 from './content_1.jsx';
import Content2 from './content_2.jsx';
import Content3 from './content_3.jsx';
import Content4 from './content_4.jsx';
import Content5 from './content_5.jsx';
import Keybindings from '../../context/keybindings.jsx';

class MenuPanel extends React.Component { 

    constructor() {
        super();
        this.boundQuickEditChanged = this.onQuickEditChanged.bind(this);
        this.boundGlobalInput = this.onGlobalInput.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundQuickEditChanged, Constants.EVENT_QUICK_EDITS_ADDED);
        EventDispatcher.add(this.boundQuickEditChanged, Constants.EVENT_QUICK_EDITS_REMOVED);
        EventDispatcher.add(this.boundQuickEditChanged, Constants.QUICK_EDIT_EVENT_REFRESH);
        document.addEventListener('keydown', this.boundGlobalInput);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundQuickEditChanged);
        document.removeEventListener('keydown', this.boundGlobalInput);
    }

    onGlobalInput(e) {
        if(Keybindings.onKeyBinding(e, Keybindings.BINDING_OPEN_QUICK_EDITS)) {
            if(App.getQuickEdits().getEdits().length > 0) {
                this.onQuickEditClicked();
            }
        }
    }

    onQuickEditChanged(change) {
        this.forceUpdate();
    }

    onQuickEditClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: "Snabbredigeringar",
            content: <QuickEditViewer                         
                        onClosed={this.forceUpdate.bind(this)}/>
        });
    }

    renderQuickEditInfo() {    
        var count = App.getQuickEdits().getEdits().length;    
        if(count > 0) {
            return (
                <Button 
                    text={count + "st opublicerade förändringar"}
                    onClick={this.onQuickEditClicked.bind(this)}/>
            );
        }
    }

    render() {
        return (
            <Panel css="menu_panel">
                {this.renderQuickEditInfo()}
                <Container  
                    css="menu_container"
                    eventId={Constants.ID_MAINPANEL_CONTAINER}
                    defaultView={Constants.WORK_MODE_1}
                    views={[{
                        content: (<Content1/>),
                        id: Constants.WORK_MODE_1,
                    },{
                        content: (<Content2/>),
                        id: Constants.WORK_MODE_2,
                    },{
                        content: (<Content3/>),
                        id: Constants.WORK_MODE_3,
                    },{
                        content: (<Content4/>),
                        id: Constants.WORK_MODE_4,
                    },{
                        content: (<Content5/>),
                        id: Constants.WORK_MODE_5,
                    }]}
                />
            </Panel>
        );
    }    
	
}

export default MenuPanel;