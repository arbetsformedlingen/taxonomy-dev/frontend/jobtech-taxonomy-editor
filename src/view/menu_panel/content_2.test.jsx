import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content2 from "./content_2"
import React from "react"

describe("Content1", () => {
  test("renders", () => {
    render(<Content2 />);
    expect(Content2).toBeInstanceOf(Function);
    expect(Content2).toHaveProperty("prototype");
    expect(Content2.prototype).toBeInstanceOf(Object);
    expect(Content2.prototype.constructor).toBe(Content2);
  });
});