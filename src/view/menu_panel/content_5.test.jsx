import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content5 from "./content_5"
import React from "react"

describe("Content5", () => {
  test("renders", () => {
    expect(Content5).toBeInstanceOf(Function);
    expect(Content5).toHaveProperty("prototype");
    expect(Content5.prototype).toBeInstanceOf(Object);
    expect(Content5.prototype.constructor).toBe(Content5);
  });
});