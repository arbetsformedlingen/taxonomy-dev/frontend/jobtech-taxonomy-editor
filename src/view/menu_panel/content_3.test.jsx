import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content3 from "./content_3"
import React from "react"

describe("Content3", () => {
  test("renders", () => {
    render(<Content3 />);
    expect(Content3).toBeInstanceOf(Function);
    expect(Content3).toHaveProperty("prototype");
    expect(Content3.prototype).toBeInstanceOf(Object);
    expect(Content3.prototype.constructor).toBe(Content3);
  });
});