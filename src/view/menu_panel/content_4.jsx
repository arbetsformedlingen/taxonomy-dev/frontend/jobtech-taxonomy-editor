import React from 'react';
import Constants from './../../context/constants.jsx';
import CacheManager from './../../context/cache_manager.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import Button from './../../control/button.jsx';
import ChooseImportFile from '../dialog/choose_import_file.jsx';
import SearchChanges from '../dialog/search_changes.jsx';
import SearchRelations from '../dialog/search_relations.jsx';

class Content4 extends React.Component { 

    constructor() {
        super();
        this.state = {
            components: [],
        };
        this.boundRelationSearch = this.onRelationSearch.bind(this);
        this.boundChangesSearch = this.onChangesSearch.bind(this);
        this.boundClearView = this.onClearView.bind(this);
        this.boundImportConcepts = this.onImportConcepts.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundRelationSearch, Constants.EVENT_SIDEPANEL_RELATION_SEARCH_SELECTED);
        EventDispatcher.add(this.boundChangesSearch, Constants.EVENT_SIDEPANEL_CHANGES_SEARCH_SELECTED);
        EventDispatcher.add(this.boundClearView, Constants.EVENT_SIDEPANEL_REFERRED_SELECTED);
        EventDispatcher.add(this.boundImportConcepts, Constants.EVENT_SIDEPANEL_IMPORT_SELECTED);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundRelationSearch);
        EventDispatcher.remove(this.boundChangesSearch);
        EventDispatcher.remove(this.boundClearView);
        EventDispatcher.remove(this.boundImportConcepts);
    }

    onNewRelationSearchClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("search"),
            content: <SearchRelations/>,
        });
    }

    onNewChangesSearchClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("search"),
            content: <SearchChanges/>,
        });
    }

    onNewImportClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("choose_file"),
            content: <ChooseImportFile/>,
        });
    }

    onClearView() {
        this.setState({components: []});
    }

    onRelationSearch() {
        var key = 0;
        var components = [];
        if(CacheManager.isCached(Constants.KEY_RELATIONS_SEARCH_DATA, true)) {
            EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CONNECTIONS_SELECTED, CacheManager.getCachedValue(Constants.KEY_RELATIONS_SEARCH_DATA));
            components.push(
                <Button
                    key={key++}
                    text="Rensa lokal data"
                    onClick={() => { 
                        CacheManager.clearValue(Constants.KEY_RELATIONS_SEARCH_DATA);
                        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CLEAR);
                    }}/>
            );
        }
        components.push(
            <Button
                key={key++}
                text="Ny sökning"
                onClick={this.onNewRelationSearchClicked.bind(this)}/>
        );
        this.setState({components: components});
    }

    onChangesSearch() {
        var key = 0;
        var components = [];
        if(CacheManager.isCached(Constants.KEY_CHANGES_SEARCH_DATA, true)) {
            EventDispatcher.fire(Constants.EVENT_SEARCH_CHANGES, CacheManager.getCachedValue(Constants.KEY_CHANGES_SEARCH_DATA));
            components.push(
                <Button
                    key={key++}
                    text="Rensa lokal data"
                    onClick={() => { 
                        CacheManager.clearValue(Constants.KEY_CHANGES_SEARCH_DATA);
                        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CLEAR);
                    }}/>
            );
        }
        components.push(
            <Button
                key={key++}
                text="Ny sökning"
                onClick={this.onNewChangesSearchClicked.bind(this)}/>
        );
        this.setState({components: components});
    }

    onImportConcepts() {
        var key = 0;
        var components = [];
        if(CacheManager.isCached(Constants.KEY_IMPORT_DATA, true)) {
            EventDispatcher.fire(Constants.EVENT_IMPORT_DATA, CacheManager.getCachedValue(Constants.KEY_IMPORT_DATA));
            components.push(
                <Button
                    key={key++}
                    text="Rensa lokal data"
                    onClick={() => { 
                        CacheManager.clearValue(Constants.KEY_IMPORT_DATA); 
                        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CLEAR);
                    }}/>
            );
        }
        components.push(
            <Button
                key={key++}
                text="Ny import"
                onClick={this.onNewImportClicked.bind(this)}/>
        );
        this.setState({components: components});
    }

    render() {
        return (
            <div className="menu_panel_context_part">
                {this.state.components}
            </div>
        );
    }
	
}

export default Content4;    