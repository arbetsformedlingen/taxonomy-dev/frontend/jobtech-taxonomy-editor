import React from 'react';
import Constants from './../../context/constants.jsx';
import Settings from '../../context/settings.jsx';
import Util from './../../context/util.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import Button from './../../control/button.jsx';
import EditConcept from '../dialog/edit_concept.jsx';
import Export from '../dialog/export.jsx';
import Excel from '../../context/excel.jsx';
import Rest from '../../context/rest.jsx';
import App from '../../context/app.jsx';
import Keybindings from '../../context/keybindings.jsx';

class Content1 extends React.Component { 

    constructor() {
        super();
        this.state = {
            quickEdit: false,
            item: null,
        };
        this.boundSideItemSelected = this.onSideItemSelected.bind(this);   
        this.boundGlobalInput = this.onGlobalInput.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundSideItemSelected, Constants.EVENT_SIDEPANEL_ITEM_SELECTED);
        document.addEventListener('keydown', this.boundGlobalInput);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundSideItemSelected);       
        document.removeEventListener('keydown', this.boundGlobalInput); 
    }

    onGlobalInput(e) {
        if(this.isEditable()) {
            if(Keybindings.onKeyBinding(e, Keybindings.BINDING_OPEN_CONCEPT_EDIT)) {
                this.onEditClicked();
            } else if(Keybindings.onKeyBinding(e, Keybindings.BINDING_NEW_RELATION)) {
                this.onEditClicked(Constants.EDIT_TYPE_ADD_RELATION);
            } else if(Keybindings.onKeyBinding(e, Keybindings.BINDING_TOGGLE_QUICK_EDIT)) {
                this.onQuickEditChanged();
            }
        }
    }

    onSideItemSelected(item) {
        this.setState({
            quickEdit: false,
            item: item,
        });
    }    

    onQuickEditChanged() {
        var enabled = !this.state.quickEdit;
        this.setState({quickEdit: enabled});
        EventDispatcher.fire(Constants.EVENT_QUICK_EDIT, enabled);
    }

    onItemSaved() {
        Util.getConcept(this.state.item.id, this.state.item.type, (data) => {
            // merge item
            for(var member in data[0]) {
                if(this.state.item[member] == null) {
                    this.state.item[member] = data[0][member];
                }
            }
            this.state.item.broader = data[0].broader;
            this.state.item.narrower = data[0].narrower;
            this.state.item.related = data[0].related;
            this.state.item.substitutes = data[0].substitutes;
            this.state.item.broad_match = data[0].broad_match;
            this.state.item.narrow_match = data[0].narrow_match;
            this.state.item.close_match = data[0].close_match;
            this.state.item.exact_match = data[0].exact_match;
            this.state.item.possible_combinations = data[0].possible_combinations;
            this.state.item.unlikely_combinations = data[0].unlikely_combinations;
            EventDispatcher.fire(Constants.EVENT_CONCEPT_EDITED, this.state.item);
            this.onSideItemSelected(this.state.item);
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
        });
    }

    onExportClicked() {
        var item = this.state.item;
        // values
        var values = [{
            text: "Inkludera kvalitetssäkring",
            selected: true,
            id: 0,
        }, {
            text: "Inkludera databas-ID",
            selected: false,
            id: 1,
        }];
        // excel
        var onSaveExcel = (values) => {
            var splitRelationTypes = (relations) => {
                var result = [];
                if(relations) {
                    for(var i=0; i<relations.length; ++i) {
                        var concept = relations[i];
                        if(concept.deprecated != null && concept.deprecated) {
                            continue;
                        }
                        var slot = result.find((x) => {
                            return x.type == concept.type;
                        });
                        if(slot == null) {
                            slot = {
                                type: concept.type,
                                items: [],
                            };
                            result.push(slot);
                        }
                        slot.items.push(relations[i]);
                    }
                }
                return result;
            };
            Util.getFullyPopulatedConcept(item.id, item.type, async (concept) => {
                // extract special title
                var specialTitle = null;
                if(concept.type.startsWith("ssyk")) {
                    specialTitle = "SSYK " + concept.ssyk;
                } else if(concept.type.startsWith("isco")) {
                    specialTitle = "ISCO " + concept.isco;
                }
                // find last change
                var lastChange = null;
                if(concept.local_history && concept.local_history.length) {
                    lastChange = concept.local_history[0].date.toLocaleString();
                }
                var version = await Rest.awaitVersionsPromis();
                if(version != null) {
                    version = version[version.length - 1].version;
                    // setup excel writer
                    var context = Excel.create(concept.preferredLabel, concept.preferredLabel, version, specialTitle, lastChange);
                    context.addRow();
                    if(concept.type == "ssyk-level-4") {
                        // broader relations
                        var broaderRelations = splitRelationTypes(concept.broader);
                        for(var i=0; i<broaderRelations.length; ++i) {
                            var collection = broaderRelations[i];
                            Util.sortByCmp(collection.items, (child) => {
                                return child.preferredLabel;
                            }, true);
                            context.addRow(Localization.getTypeName(collection.type), { bold: true });
                            for(var j=0; j<collection.items.length; ++j) {
                                context.addRow(collection.items[j].preferredLabel);
                            }
                            context.addRow();
                        }
                    }
                    // database id
                    context.addRow("Databas-ID", { bold: true });
                    context.addRow(concept.id);
                    context.addRow();
                    // definition
                    context.addRow(Localization.get("description"), { bold: true });
                    context.addRow(concept.definition, { height: 28, wrapText: true });
                    context.addRow();
                    if(concept.type == "ssyk-level-4") {
                        var relations = splitRelationTypes(concept.narrower);
                        relations = relations.concat(splitRelationTypes(concept.related));
                        for(var i=0; i<relations.length; ++i) {
                            Util.sortByCmp(relations[i].items, (child) => {
                                return child.preferredLabel;
                            }, true);
                        }
                        // find skill-headline and occupation-name
                        var skills = relations.find((x) => {
                            return x.type == "skill-headline";
                        });
                        skills = skills == null ? [] : skills.items;
                        var names = relations.find((x) => {
                            return x.type == "occupation-name";
                        });
                        names = names == null ? [] : names.items;
                        // headline
                        context.addRow();
                        context.addHeadlines(Localization.getTypeName("occupation-name"), Localization.getTypeName("skill"));
                        // create virtual rows
                        var nextSkillIndex = 0;
                        var nextNameIndex = 0;
                        var rows = [];
                        var count = skills.length > names.length ? skills.length : names.length;
                        for(var i=0; i<count; ++i) {
                            var skillHeadline = i < skills.length ? skills[i] : null;
                            var name = i < names.length ? names[i] : null;
                            // add row
                            rows.push({});
                            // setup row
                            if(skillHeadline) {
                                // add headline
                                rows[nextSkillIndex++].right = {
                                    bold: true,
                                    text: skillHeadline.preferredLabel,
                                    idLabel: true,
                                };
                                Util.sortByCmp(skillHeadline.children, (child) => {
                                    return child.preferredLabel;
                                }, true);
                                // add skills
                                for(var j=0; j<skillHeadline.children.length; ++j) {
                                    rows.push({
                                        right: { 
                                            text: skillHeadline.children[j].preferredLabel,
                                            id: skillHeadline.children[j].id,
                                        },
                                    });
                                    nextSkillIndex++;
                                }
                            }
                            if(name) {
                                rows[nextNameIndex++].left = { text: name.preferredLabel };
                            }
                        }
                        // add rows
                        for(var i=0; i<rows.length; ++i) {
                            var nr = context.addLeftRight(rows[i].left, rows[i].right);
                            if(rows[i].right) {
                                if(rows[i].right.idLabel) {
                                    context.setCell("I" + nr, "Databas-ID", true);
                                }
                                if(rows[i].right.id) {
                                    context.setCell("I" + nr, rows[i].right.id);
                                }
                            }
                        }
                    } else {
                        var relations = splitRelationTypes(concept.broader);
                        relations = relations.concat(splitRelationTypes(concept.narrower));
                        relations = relations.concat(splitRelationTypes(concept.related));
                        for(var i=0; i<relations.length; ++i) {
                            Util.sortByCmp(relations[i].items, (child) => {
                                return child.preferredLabel;
                            }, true);
                        }
                        for(var i=0; i<relations.length; ++i) {
                            var collection = relations[i];
                            if(collection.type == 'skill-headline' && concept.type == 'skill-headline') {
                                // skip this since the concept is the headline
                                continue;
                            }
                            context.addRow(Localization.getTypeName(collection.type), { 
                                bold: true, 
                                italic: true,
                                fontSize: 12,
                            });
                            if(collection.type == 'skill-headline') {
                                for(var j=0; j<collection.items.length; ++j) {
                                    var skillHeadline = collection.items[j];
                                    context.addRow(skillHeadline.preferredLabel, { bold: true });
                                    for(var k=0; k<skillHeadline.children.length; ++k) {
                                        var nr = context.addRow(skillHeadline.children[k].preferredLabel, { indent: 1 });
                                        context.setCell("I" + nr, skillHeadline.children[k].id);
                                    }
                                }
                            } else {
                                for(var j=0; j<collection.items.length; ++j) {
                                    var nr = context.addRow(collection.items[j].preferredLabel);
                                    context.setCell("I" + nr, collection.items[j].id);
                                }
                            }
                            context.addRow();
                        }
                    }
                    // download the file
                    context.download(concept.preferredLabel + ".xlsx");
                    EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
                } else {
                    App.showError("Export - getVersion exceptions");
                }
            });
        };
        // event
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("export") + " " + this.state.item.preferredLabel,
            content: <Export 
                        values={values}
                        onSaveExcel={onSaveExcel}/>
        });
    }
    
    onEditClicked(type) {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("edit") + " " + this.state.item.preferredLabel,
            content: <EditConcept 
                        defaultType={type}
                        item={this.state.item}
                        onItemUpdated={this.onItemSaved.bind(this)}/>
        });
    }    

    isEditable() {
        var item = this.state.item;
        var editable = false;
        if(item != null && 
            Settings.isEditable(item.type) &&
            item.id != "ja7J_P8X_YC9" && //Bygg 
            item.id != "8too_bEs_7NU" && //Kultur
            item.id != "XouD_cCj_HZN") { //Sjöfart
            editable = true;
        }
        return editable;
    }

    renderEditButton() {
        return <Button
            text={Localization.get("edit")}
            onClick={this.onEditClicked.bind(this)}/>;
    }    

    render() {
        if(this.state.item) {
            var editable = this.isEditable();
            return (
                <div className="menu_panel_context_part">
                    {editable == true &&
                    <input 
                        id="do_qe"
                        type="checkbox"
                        checked={this.state.quickEdit}
                        onChange={this.onQuickEditChanged.bind(this)}/>
                    }
                    {editable == true &&
                    <label 
                        htmlFor="do_qe" 
                        className="quick_edit_checkbox font">
                        Snabbredigering
                    </label>
                    }
                    <Button 
                        text={Util.renderExportButtonText()}
                        onClick={this.onExportClicked.bind(this)}
                        hint="Exportera"/>
                    {editable == true &&
                        this.renderEditButton()
                    }
                </div>
            );
        } else {
            return null;
        }
    }

}

export default Content1;    