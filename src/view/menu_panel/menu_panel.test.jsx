import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import MenuPanel from "./menu_panel"
import React from "react"

describe("MenuPanel", () => {
  test("renders", () => {
    expect(MenuPanel).toBeInstanceOf(Function);
    expect(MenuPanel).toHaveProperty("prototype");
    expect(MenuPanel.prototype).toBeInstanceOf(Object);
    expect(MenuPanel.prototype.constructor).toBe(MenuPanel);
  });
});