import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import VersionDetail from "./version_detail"
import React from "react"

describe("VersionDetail", () => {
  test("renders", () => {
    render(<VersionDetail />);
    expect(VersionDetail).toBeInstanceOf(Function);
    expect(VersionDetail).toHaveProperty("prototype");
    expect(VersionDetail.prototype).toBeInstanceOf(Object);
    expect(VersionDetail.prototype.constructor).toBe(VersionDetail);
  });
});