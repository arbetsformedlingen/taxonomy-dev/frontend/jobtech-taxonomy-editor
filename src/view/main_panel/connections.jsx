import React from 'react';
import Button from '../../control/button.jsx';
import TreeView from '../../control/tree_view.jsx';
import ControlUtil from '../../control/util.jsx';
import Label from '../../control/label.jsx';
import Constants from '../../context/constants.jsx';
import App from '../../context/app.jsx';
import Rest from '../../context/rest.jsx';
import Localization from '../../context/localization.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import Util from '../../context/util.jsx';
import ConceptWrapper from '../../control/concept_wrapper.jsx';

class Connections extends React.Component { 

    constructor() {
        super();
        this.state = {
            showDeprecated: false,
            groupedByType: false,
            hasSelection: false,
            treeViews: [this.createTreeView("", [], [])],
            activeEdits: [],
            droppedConcepts: [],
            relationType: "broader",
            substitutability: "25",
        };
        this.enableQuickEdit = false;
        this.relations = [];
        this.checkedForRemoval = [];
        this.selectedItem = null;
        this.boundUserState = this.onUserState.bind(this);
        this.boundQuickEditRemoved = this.onQuickEditRemoved.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundUserState, Constants.EVENT_SOCKET_USER_STATE);
        EventDispatcher.add(this.boundQuickEditRemoved, Constants.EVENT_QUICK_EDITS_REMOVED);
        this.init(this.props);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundUserState);
        EventDispatcher.remove(this.boundQuickEditRemoved);
    }

    UNSAFE_componentWillReceiveProps(props) {
        if(props.enableQuickEdit) {
            this.state.groupedByType = true;
        }
        this.checkedForRemoval = [];
        this.enableQuickEdit = props.enableQuickEdit;
        this.init(props);
    }
    
    onUserState(user) {
        var edits = this.state.activeEdits;
        var shouldRefresh = false;
        // remove all states from this user
        edits = edits.filter((x) => {
            return x != user.name;
        });
        // add all states for user
        for(var i=0; i<user.states.length; ++i) {
            var s = user.states[i];
            var isConnectionsEdit = 
                    s.field == "add_connection" || 
                    s.field == "remove_connection" ||
                    s.field == "new_value";
            if(s.id == this.item.id && isConnectionsEdit) {
                if(s.type == "edit") {
                    edits.push(user.name);
                } else if(s.type == "edit_saved") {
                    shouldRefresh = true;
                }
            }
        }
        this.setState({activeEdits: edits});
        if(shouldRefresh) {
            this.init(this.props);
        }
    }

    onQuickEditRemoved(qe) {
        if(this.item && qe.concept.id == this.item.id) {
            if(qe.type == Constants.QUICK_EDIT_ADDED_RELATION) {
                Util.removeRelation(this.item, qe.value.to, qe.value.relationType, true);   
            } else if(qe.type == Constants.QUICK_EDIT_REMOVED_RELATION) {
                var value = {
                    id: qe.value.to,
                    type: qe.value.type,
                    preferredLabel: qe.value.preferredLabel,
                    substitutability_percentage: qe.value.substitutability,
                };
                Util.addRelation(this.item, qe.value.relationType, value);
            }
            this.selectedItem = null;
            this.setupRelationsFor(this.item);
            this.setState({hasSelection: false});
        }
    }

    onDrop(e) {
        e.preventDefault();
        var data = JSON.parse(e.dataTransfer.getData("text"));
        if(data.length > 0) {
            var type = Constants.getRelationType(this.item.type, data[0].type);
            this.setState({
                droppedConcepts: data,
                relationType: type == null ? "broader" : type,
                substitutability: "25",
            });
        }
    }

    onDragOver(e) {
        e.preventDefault();
    }

    onTypeSelected(e) {
        this.setState({relationType: e.target.value});
    }

    onSubstituabilityChanged(e) {
        this.setState({substitutability: e.target.value});
    }

    onRemoveChecked(item, e) {
        item.checked = e.target.checked;
        if(item.checked) {
            this.checkedForRemoval.push(item);
        } else {
            var index = this.checkedForRemoval.indexOf(item);
            this.checkedForRemoval.splice(index, 1);
        }
        this.forceUpdate();
    }

    onRemoveClicked() {
        for(var i=0; i<this.checkedForRemoval.length; ++i) {
            var relation = this.checkedForRemoval[i];
            // setup quick edit item
            var qe = {
                from: this.item.id,
                to: relation.id,
                relationType: relation.relationType,
                preferredLabel: relation.preferredLabel,
                type: relation.type,
                substitutability: relation.substitutability_percentage,
            };
            App.getQuickEdits().add(this.item, Constants.QUICK_EDIT_REMOVED_RELATION, qe);
            // update treeview
            var treeView = this.findTreeViewFor(this.state.treeViews, relation.type, relation.relationType);
            var child = treeView.context.findChild((node) => {
                return node.data.id == relation.id;
            });
            if(child) {
                var removeNode = (node) => {
                    if(node.parent) {
                        node.parent.removeChild(node);
                        if(node.parent.children.length == 0) {
                            removeNode(node.parent);
                        }
                    } else {
                        treeView.context.removeRoot(node);
                    }
                };
                removeNode(child);
            }
            if(treeView.context.roots.length == 0) {
                var index = this.state.treeViews.indexOf(treeView);
                this.state.treeViews.splice(index, 1);
            }
            // update item
            Util.removeRelation(this.item, relation.id, relation.relationType, true);
        }
        this.checkedForRemoval = [];
        this.forceUpdate();
    }

    onAddRelationClicked() {
        // TODO: check if we already have this relation
        for(var i=0; i<this.state.droppedConcepts.length; ++i) {
            var concept = this.state.droppedConcepts[i];
            // setup quick edit item
            var qe = {
                from: this.item.id,
                to: concept.id,
                relationType: this.state.relationType,
                preferredLabel: concept.preferredLabel,
                label: concept.label,
                type: concept.type,
                substitutability: this.state.relationType == Constants.RELATION_SUBSTITUTABILITY ? this.state.substitutability : null
            };
            App.getQuickEdits().add(this.item, Constants.QUICK_EDIT_ADDED_RELATION, qe);
            // update treeview
            var relation = {
                id: concept.id,
                type: concept.type,
                preferredLabel: concept.preferredLabel,
                label: concept.label,
                substitutability_percentage: this.state.relationType == Constants.RELATION_SUBSTITUTABILITY ? parseInt(this.state.substitutability) : null,
            };
            this.addRelationToTree(this.state.treeViews, relation, this.state.relationType);
            // update item
            Util.addRelation(this.item, this.state.relationType, relation);
        }
        // clear drop area
        this.setState({droppedConcepts: []});
    }

    getCss(css) {
        return css + (this.state.activeEdits.length > 0 ? " user_state_edit_border" : "");
    }

    init(props) {
        this.item = props.item;
        this.selectedItem = null;
        this.setupRelationsFor(props.item);
        this.setState({hasSelection: false});
    }

    createTreeView(text, types, connectionTypes) {
        var treeView = {context: ControlUtil.createTreeView(), text: text, types: types, connectionTypes: connectionTypes};
        treeView.context.onItemSelected = this.onItemSelected.bind(this);
        return treeView;
    }

    createTreeViewsFor(item) {
        var treeViews = [];
        if(this.state.groupedByType) {
            if(item.broader && item.broader.length > 0) {
                treeViews.push(this.createTreeView("Broader", [], ["broader"]));
            }
            if(item.narrower && item.narrower.length > 0) {
                treeViews.push(this.createTreeView("Narrower", [], ["narrower"]));
            }
            if(item.related && item.related.length > 0) {
                treeViews.push(this.createTreeView("Related", [], ["related"]));
            }
            if(item.substitutes && item.substitutes.length > 0) {
                treeViews.push(this.createTreeView("Substitutes", [], ["substitutes"]));
            }
            if(item.broad_match && item.broad_match.length > 0) {
                treeViews.push(this.createTreeView("Broad match", [], ["broad_match"]));
            }
            if(item.narrow_match && item.narrow_match.length > 0) {
                treeViews.push(this.createTreeView("Narrow match", [], ["narrow_match"]));
            }
            if(item.close_match && item.close_match.length > 0) {
                treeViews.push(this.createTreeView("Close match", [], ["close_match"]));
            }
            if(item.exact_match && item.exact_match.length > 0) {
                treeViews.push(this.createTreeView("Exact match", [], ["exact_match"]));
            }
            if(item.possible_combinations && item.possible_combinations.length > 0) {
                treeViews.push(this.createTreeView("Possible combinations", [], ["possible_combinations"]));
            }
            if(item.unlikely_combinations && item.unlikely_combinations.length > 0) {
                treeViews.push(this.createTreeView("Unlikely combinations", [], ["unlikely_combinations"]));
            }
            if(treeViews.length == 0) {
                treeViews.push(this.createTreeView(Localization.get("other_connections"), [], []));
            }
        } else {
            var type = item != null ? item.type : null
            treeViews.push(this.createTreeView(Localization.get("other_connections"), [], []));
            if(type) {
                switch(type) { 
                    case "occupation-name": 
                        treeViews.push(this.createTreeView(Localization.get("substitutability_connections"), ["occupation-name"], []));
                        treeViews.push(this.createTreeView(Localization.getTypeName("keyword"), ["keyword", "job-title"], []));
                        treeViews.push(this.createTreeView(Localization.get("matches_connections"), [], ["broad_match", "narrow_match", "close_match", "exact_match"]));
                        break;
                    case "ssyk-level-4": 
                        treeViews.push(this.createTreeView(Localization.getTypeName("occupation-name"), ["occupation-name"], []));
                        treeViews.push(this.createTreeView(Localization.getTypeName("skill"), ["skill"], []));
                        break;
                    case "isco-level-4":
                        treeViews.push(this.createTreeView(Localization.getTypeName("occupation-name"), ["occupation-name"], []));
                        treeViews.push(this.createTreeView(Localization.get("matches_connections"), [], ["broad_match", "narrow_match", "close_match", "exact_match"]));
                        break;
                    case "occupation-field":
                        treeViews.push(this.createTreeView(Localization.getTypeName("occupation-name"), ["occupation-name"], []));
                        break;
                    case "esco-occupation":
                        treeViews.push(this.createTreeView(Localization.get("matches_connections"), [], ["broad_match", "narrow_match", "close_match", "exact_match"]));
                        break;
                    case "sun-education-field-2":
                    case "sun-education-field-3":
                    case "sun-education-field-4":
                        treeViews.push(this.createTreeView(Localization.get("combinations"), [], ["possible_combinations", "unlikely_combinations"]));
                        break;
                }            
            }
        }
        return treeViews;
    }

    onShowDeprecatedChanged(e) {
        this.selectedItem = null;
        this.state.showDeprecated = e.target.checked;
        this.setupRelationsFor(this.item);
        this.setState({
            hasSelection: false,
        });
    }

    onGroupedByTypeChanged(e) {        
        this.selectedItem = null;
        this.state.groupedByType = e.target.checked;
        this.setupRelationsFor(this.item);
        this.setState({
            hasSelection: false,
        });
    }

    onVisitClicked() {
        if(this.selectedItem && this.selectedItem.parent) {
            if(App.hasUnsavedChanges()) {
                App.showSaveDialog(this.onSaveDialogResult.bind(this, this.selectedItem.data));
            } else {
                Util.gotoConcept(this.selectedItem.data);
            }
        }
    }

    onItemSelected(item) {
        this.selectedItem = item;
        //Make sure that no other tree view has a selected item
        for(var i=0; i<this.state.treeViews.length; ++i) {
            var treeView = this.state.treeViews[i];
            if(treeView.context.getSelected() != null && treeView.context.getSelected() != item) {
                treeView.context.setSelected(treeView.context.getSelected(), false);
            }
        }
        this.setState({hasSelection: this.selectedItem.parent ? true : false});
    }

    findTreeViewFor(treeViews, type, connectionType) {
        if(connectionType && connectionType.length > 0) {
            for(var i=1; i<treeViews.length; ++i) {
                var treeView = treeViews[i];                    
                for(var j=0; j<treeView.connectionTypes.length; ++j) {
                    if(connectionType == treeView.connectionTypes[j]) {
                        return treeView;
                    }
                }
            }
        }
        for(var i=1; i<treeViews.length; ++i) {
            var treeView = treeViews[i];
            for(var j=0; j<treeView.types.length; ++j) {
                if(type == treeView.types[j]) {
                    return treeView;
                }
            }
        }
        return treeViews[0];
    };

    async setupRelationsFor(item) {
        var sortChildren = (element) => {
            if(element.children.length > 0) {
                for(var i=0; i<element.children.length; ++i) {
                    sortChildren(element.children[i]);                    
                }
                Util.sortByCmp(element.children, (child) => {
                    return child.sortKey;
                }, true);
            }
        };
        var addItemsToTree = async (items, connectionType) => {
            if(items) {
                if(items.length > 500) {
                    //sort list
                    Util.sortByKey(items, "preferredLabel", true);
                }
                for(var i=0; i<items.length; ++i) {
                    await this.addRelationToTree(treeViews, items[i], connectionType);
                }                
            }
        };
        var treeViews = this.createTreeViewsFor(item);       
        if(item) {           
            await addItemsToTree(item.narrower, "narrower");
            await addItemsToTree(item.broader, "broader");
            await addItemsToTree(item.related, "related");
            await addItemsToTree(item.substitutes, "substitutes");
            await addItemsToTree(item.broad_match, "broad_match");
            await addItemsToTree(item.narrow_match, "narrow_match");
            await addItemsToTree(item.close_match, "close_match");
            await addItemsToTree(item.exact_match, "exact_match");
            await addItemsToTree(item.possible_combinations, "possible_combinations");
            await addItemsToTree(item.unlikely_combinations, "unlikely_combinations");

            //sort
            for(var j=0; j<treeViews.length; ++j) {                
                var treeView = treeViews[j];
                if(treeView.context.roots.length > 0) {
                    for(var i=0; i<treeView.context.roots.length; ++i) {
                        var root = treeView.context.roots[i];                
                        sortChildren(root);
                    }
                    Util.sortByKey(treeView.context.roots, "text", true);
                } 
            }
        }
        this.setState({treeViews: treeViews});
    }

    async addRelationToTree(treeViews, element, connectionType) {
        var findRootFor = (treeView, item) => {
            if(item.type == "occupation-name" && item.substitutability_percentage) {
                return treeView.roots.find((root) => {
                    return root.data ? item.type == root.data.type && item.substitutability_percentage == root.data.substitutability_percentage : false;
                });
            }
            return treeView.roots.find((root) => {
                return root.data ? item.type == root.data.type : false;
            });
        };

        var setupRootForConnectionType = (treeView, type) => {
            var root = null;
            var typeRoot = treeView.context.roots.find((root) => {
                return root.data === type;
            });
            if(!typeRoot) {            
                typeRoot = ControlUtil.createTreeViewItem(treeView.context, type);
                typeRoot.setText(Localization.get(type));
                treeView.context.addRoot(typeRoot);
                typeRoot.setExpanded(true);
            }            
            for(var i=0; i<typeRoot.children.length; ++i) {
                var child = typeRoot.children[i];
                if(child.data && child.data.type == element.type) {
                    root = child;
                    break;
                }
            }
            if(!root) {
                var text = Localization.getTypeName(element.type);
                root = ControlUtil.createTreeViewItem(treeView.context, element);
                root.setText(text);
                root.setExpanded(true);
                typeRoot.addChild(root);
            }
            return root;
        }

        if(!this.state.showDeprecated && element.deprecated != null && element.deprecated) {
            // dont show deprecated items
            return;
        }
        var treeView = this.findTreeViewFor(treeViews, element.type, connectionType);
        var root = findRootFor(treeView.context, element);
        if(connectionType && connectionType.indexOf("match") >= 0) {
            root = setupRootForConnectionType(treeView, connectionType);
        } else if(connectionType == "possible_combinations" || connectionType == "unlikely_combinations") {
            root = setupRootForConnectionType(treeView, connectionType);
        } else if(!root) {
            var text = Localization.getTypeName(element.type);
            if(element.type == "occupation-name" && element.substitutability_percentage) {
                text = element.substitutability_percentage > 50 ? Localization.get("substitutability_high") : Localization.get("substitutability_low");
            }
            root = ControlUtil.createTreeViewItem(treeView.context, {
                type: element.type,
                substitutability_percentage: element.substitutability_percentage,
            });
            root.setText(text);
            treeView.context.addRoot(root);
            root.setExpanded(true);
        } 
        if(element.type == Constants.CONCEPT_SKILL) {
            // check children of root for actuall root
            for(var i=0; i<root.children.length; ++i) {
                var child = root.children[i];
                if(child.data.narrower) {
                    var result = child.data.narrower.find((e) => {
                        return e.id == element.id;
                    });
                    if(result) {
                        root = child;
                        break;
                    }
                }
            }
            if(root.data.type == Constants.CONCEPT_SKILL) {
                // invalid root found, fetch real root
                var query = 
                    "concepts(id: \"" + element.id + "\", version: \"next\") { " + 
                        "broader(type: \"skill-headline\") { " + 
                            "id type preferredLabel:preferred_label " + 
                            "narrower(type: \"skill\") { "+
                                "id " +
                            "} " +
                        "} " +
                    "}";
                var data = await Rest.getGraphQlPromise(query);
                if(data.data.concepts.length > 0) {
                    data = data.data.concepts[0].broader[0];
                    var headline = ControlUtil.createTreeViewItem(treeView.context, data);
                    headline.sortKey = data.preferredLabel;
                    headline.setText(
                        <ConceptWrapper 
                            showVisit={true}
                            concept={data}>
                            {data.preferredLabel}
                        </ConceptWrapper>);
                    headline.setExpanded(true);
                    root.addChild(headline);
                    root = headline;
                }
            }
            if(root.children.length > 499 || root.mustSplit) {
                //To boost performance in chrome we need to split large branches
                if(!root.mustSplit) {
                    root.mustSplit = true;
                    var headline = ControlUtil.createTreeViewItem(treeView.context, null);                    
                    var children = root.children;
                    root.children = [];                    
                    root.addChild(headline);
                    for(var i=0; i<children.length; ++i) {
                        headline.addChild(children[i]);
                    }
                    var first = headline.children[0].data.preferredLabel;
                    var last = headline.children[headline.children.length -1].data.preferredLabel;
                    headline.setText(first[0] + " - " + last[0]);                
                } 
                var lastChild = root.children[root.children.length - 1];
                if(lastChild.children.length > 499) {
                    lastChild = ControlUtil.createTreeViewItem(treeView.context, null);                    
                    root.addChild(lastChild);
                }
                //fix caption of lastChild
                var first = lastChild.children.length > 0 ? lastChild.children[0].data.preferredLabel : element.preferredLabel;
                var last = element.preferredLabel;
                var label = first[0] + " - " + last[0];
                lastChild.setText(label); 
                root = lastChild;                
            }
        }
        var child = ControlUtil.createTreeViewItem(treeView.context, element);
        var text = element.preferredLabel;
        if(element.isco) {
            text = element.isco + " - " + text;
        } else if(element.ssyk) {
            text = element.ssyk + " - " + text;
        }
        if(element.label != null) {
            text = element.label;
        }
        element.relationType = connectionType;
        child.sortKey = text;
        child.setText(
            <ConceptWrapper
                showVisit={true}
                concept={element}>
                <div 
                    className="connections_item_body"
                    title={connectionType}>
                    {this.enableQuickEdit &&
                        <input
                            type="checkbox"
                            value={element.checked == null ? false : element.checked}
                            onChange={this.onRemoveChecked.bind(this, element)}/>
                    }
                    {(element.deprecated != null && element.deprecated) &&
                        <div className="connections_item_body_deprecated">
                            ({Localization.get("DEPRECATED")})
                        </div>
                    }
                    <div>{text}</div>
                </div>
            </ConceptWrapper>);
        root.addChild(child);
    }

    renderTreeViews() {
        if(this.state.treeViews.length > 0) {
            if(this.state.treeViews.length == 1) {
                return (
                    <div>
                        <TreeView context={this.state.treeViews[0].context}/>
                    </div>
                );
            }
            return this.state.treeViews.map((treeView, i) => {
                return (
                    <div key={i}>
                        <Label text={treeView.text}/>
                        <TreeView context={treeView.context}/>
                    </div>
                );
            });
        }
    }

    renderEdit() {
        if(this.state.activeEdits.length > 0) {
            var names = this.state.activeEdits.map((x, i) => {
                return x + (this.state.activeEdits.length - 1 == i ? "" : ", ");
            });
            return (
                <div className="user_state_edit font">
                    {names} arbetar här...
                </div>
            );
        }
    }

    renderQuickEdit() {
        var concepts = this.state.droppedConcepts;
        return (
            <div className="connections_quick_edit">
                <div 
                    className="drop_zone font"
                    onDrop={this.onDrop.bind(this)}
                    onDragOver={this.onDragOver.bind(this)}>
                    Dra och släpp nya relationer här
                </div>
                {concepts.length > 0 &&
                    <div className="conncections_quick_edit_concept font">
                        <div className="conncections_quick_edit_concept_info">
                            <input 
                                className="rounded"
                                type="text" 
                                disabled={true}
                                value={Localization.getTypeName(concepts[0].type)}/>
                            <select>
                                {concepts.map((element, key) => {
                                    return (
                                        <option key={key}>{element.label}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="conncections_quick_edit_concept_props">
                            <div>
                                <div>Relations typ</div>
                                <select 
                                    className="rounded"
                                    value={this.state.relationType}
                                    onChange={this.onTypeSelected.bind(this)}>
                                    <option value="broader">Broader</option>
                                    <option value="related">Related</option>
                                    <option value="narrower">Narrower</option>
                                    <option value="substitutability">Substitutability</option>
                                    <option value="broad-match">Broad match</option>
                                    <option value="narrow-match">Narrow match</option>
                                    <option value="close-match">Close match</option>
                                    <option value="exact-match">Exact match</option>
                                    <option value="possible-combinations">Possible combinations</option>
                                    <option value="unlikely-combinations">Unlikely combinations</option>
                                </select>
                            </div>
                            {this.state.relationType == "substitutability" &&
                                <div>
                                    <div>Släktskap</div>
                                    <select 
                                        className="rounded"
                                        value={this.state.substitutability}
                                        onChange={this.onSubstituabilityChanged.bind(this)}>
                                        <option value="25">25 - Lägre släktskap</option>
                                        <option value="75">75 - Högt släktskap</option>
                                    </select>
                                </div>
                            }
                        </div>
                        <div className="conncections_quick_edit_concept_info">
                            <Button 
                                text={concepts.length > 1 ? "Lägg till alla" : Localization.get("add")}
                                onClick={this.onAddRelationClicked.bind(this)}/>
                            {this.item.no_esco_relation &&
                                <b className="no_esco_relation_warning">
                                    Begreppet är markerat med "Ingen ESCO relation"
                                </b>
                            }
                        </div>
                    </div>
                }
            </div>
        );
    }

    render() {
        return (
            <div className="connections">
                {this.props.enableQuickEdit &&
                    this.renderQuickEdit()
                }
                <div className={this.getCss("connections_tree_view_container")}>
                    {this.renderTreeViews()}
                    {this.renderEdit()}
                </div>
                <div className="connections_controls">
                    <Button 
                        isEnabled={this.state.hasSelection}
                        text={Localization.get("visit")} 
                        onClick={this.onVisitClicked.bind(this)}/>
                    {this.props.enableQuickEdit &&
                        <Button 
                            isEnabled={this.checkedForRemoval.length > 0}
                            text={Localization.get("remove_selected")} 
                            onClick={this.onRemoveClicked.bind(this)}/>
                    }
                    <div>
                        <input 
                            id="show_dc"
                            type="checkbox"
                            value={this.state.showDeprecated}
                            onChange={this.onShowDeprecatedChanged.bind(this)}/>
                        <label 
                            htmlFor="show_dc" 
                            className="font">
                            {Localization.get("show_deprecated_connections")}
                        </label>
                    </div>
                    <div>
                        <input 
                            id="grouped_by_type"
                            type="checkbox"
                            value={this.state.groupedByType}
                            disabled={this.props.enableQuickEdit}
                            onChange={this.onGroupedByTypeChanged.bind(this)}/>
                        <label 
                            htmlFor="grouped_by_type" 
                            className="font">
                            {Localization.get("grouped_by_type")}
                        </label>
                    </div>
                </div>
            </div>
        );
    }
	
}

export default Connections;