import React from 'react';
import Label from '../../control/label.jsx';
import Constants from '../../context/constants.jsx';
import Localization from '../../context/localization.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import App from '../../context/app.jsx';
import SettingsVisible from './settings/settings_visible.jsx';
import SettingsValidation from './settings/settings_validation.jsx';
import SettingsTypeLocalization from './settings/settings_type_localization.jsx';
import SettingsKeyBindings from './settings/settings_key_bindings.jsx';
import SettingsInfo from './settings/settings_info.jsx';

class Content5 extends React.Component { 

    constructor() {
        super();
        this.state = {
            page: Constants.SETTINGS_PAGE_VISIBILITY,
        };
        this.boundPage = this.onPage.bind(this);
    }

    componentDidMount() {
        App.usage.addFlow(Constants.USAGE_CONTENT, Constants.USAGE_ENTER, "5");
        EventDispatcher.add(this.boundPage, Constants.EVENT_SET_SETTINGS_PAGE);
    }

    componentWillUnmount() {
        App.usage.addFlow(Constants.USAGE_CONTENT, Constants.USAGE_LEAVE, "5");
        EventDispatcher.remove(this.boundPage);
    }

    onPage(page) {
        this.setState({page: page});
    }

    renderView() {
        if(this.state.page == Constants.SETTINGS_PAGE_VISIBILITY) {
            return ( <SettingsVisible/> );
        } else if(this.state.page == Constants.SETTINGS_PAGE_VALIDATION) {
            return ( <SettingsValidation/> );
        } else if(this.state.page == Constants.SETTING_PAGE_TYPE_LOCALIZATION) {
            return ( <SettingsTypeLocalization/> );
        } else if(this.state.page == Constants.SETTINGS_PAGE_KEY_BINDINGS) {
            return ( <SettingsKeyBindings/> );
        } else if(this.state.page == Constants.SETTINGS_PAGE_INFO) {
            return ( <SettingsInfo/> );
        }
    }

    render() {
        return (
            <div className="main_content_5">
                <Label 
                    css="main_content_title" 
                    text={Localization.get("settings")}/>
                {this.renderView()}
            </div>
        );
    }
	
}

export default Content5;