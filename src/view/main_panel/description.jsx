import React from 'react';
import Label from '../../control/label.jsx';
import Loader from '../../control/loader.jsx';
import Constants from '../../context/constants.jsx';
import Socket from '../../context/socket.jsx';
import Util from '../../context/util.jsx';
import Button from './../../control/button.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import App from '../../context/app.jsx';

class Description extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            isLocked: true,
            type: null,
            preferredLabel: Util.getObjectValue(props.item, "preferredLabel", ""),
            definition: Util.getObjectValue(props.item, "definition", ""),
            shortDescription: Util.getObjectValue(props.item, "short_description", ""),
            qualityLevel: Util.getObjectValue(props.item, "quality_level", "undefined"),
            alternativeLabels: Util.getObjectValue(props.item, "alternativeLabels", []),
            hiddenLabels: Util.getObjectValue(props.item, "hiddenLabels", []),
            iscoCodes: [],
            activeEdits: [],            
        };
        this.boundUserState = this.onUserState.bind(this);
        this.boundQuickEditRemoved = this.onQuickEditRemoved.bind(this);
        this.item = props.item;
    }

    componentDidMount() {
        EventDispatcher.add(this.boundUserState, Constants.EVENT_SOCKET_USER_STATE);
        EventDispatcher.add(this.boundQuickEditRemoved, Constants.EVENT_QUICK_EDITS_REMOVED);
        this.init(this.props);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundUserState);
        EventDispatcher.remove(this.boundQuickEditRemoved);
    }

    UNSAFE_componentWillReceiveProps(props) {
        this.init(props);
    }

    onUserState(user) {
        var edits = this.state.activeEdits;
        // remove all states from this user
        edits = edits.filter((x) => {
            return x.name != user.name;
        });
        // add all states for user
        for(var i=0; i<user.states.length; ++i) {
            var s = user.states[i];
            if(s.id == this.item.id && s.type == "edit") {
                edits.push({
                    name: user.name,
                    field: s.field,
                });
            }
        }
        this.setState({
            preferredLabel: Util.getObjectValue(this.item, "preferredLabel", ""),
            definition: Util.getObjectValue(this.item, "definition", ""),
            shortDescription: Util.getObjectValue(this.item, "short_description", ""),
            qualityLevel: Util.getObjectValue(this.item, "quality_level", "undefined"),
            alternativeLabels: Util.getObjectValue(this.item, "alternativeLabels", []),
            hiddenLabels: Util.getObjectValue(this.item, "hiddenLabels", []),
            activeEdits: edits,
        });
    }    

    onQuickEditRemoved(qe) {
        if(this.item && qe.concept.id == this.item.id) {
            if(qe.type == Constants.QUICK_EDIT_NAME) {
                this.setState({preferredLabel: qe.originalValue});
            } else if(qe.type == Constants.QUICK_EDIT_DEFINITION) {
                this.setState({definition: qe.originalValue});
            } else if(qe.type == Constants.QUICK_EDIT_ALTERNATIVE_LABEL) {
                this.setState({alternativeLabels: qe.originalValue});
            } else if(qe.type == Constants.QUICK_EDIT_HIDDEN_LABEL) {
                this.setState({hiddenLabels: qe.originalValue});
            } else if(qe.type == Constants.QUICK_EDIT_QUALITY_LEVEL) {
                this.setState({qualityLevel: qe.originalValue});
            }
        }
    }

    getEditUser(field) {
        var edit = this.state.activeEdits.find((x) => {
            return x.field == field;
        });
        return edit ? edit.name : null;
    }

    getCss(css, field) {
        var user = this.getEditUser(field);
        return css + (user ? " user_state_edit_border" : "");
    }

    init(props) {
        this.item = props.item;
        for(var i=0; i<Socket.users.length; ++i) {
            this.onUserState(Socket.users[i]);
        }
        this.setState({
            type: props.item.type,
            preferredLabel: Util.getObjectValue(props.item, "preferredLabel", ""),
            definition: Util.getObjectValue(props.item, "definition", ""),
            shortDescription: Util.getObjectValue(props.item, "short_description", ""),
            qualityLevel: Util.getObjectValue(props.item, "quality_level", "undefined"),
            alternativeLabels: Util.getObjectValue(props.item, "alternativeLabels", []),
            hiddenLabels: Util.getObjectValue(props.item, "hiddenLabels", []),
            iscoCodes: [],
        }, () => {
            if(props.item["ssyk"] && props.item["ssyk"].length > 3) {
                // TODO leta i relatedlistan som redan fins populerad i props.item
                if(props.item.related) {
                    for(var i=0; i < props.item.related.length; ++i) {
                        var c = props.item.related[i];
                        if(c.type === Constants.CONCEPT_ISCO_LEVEL_4) {
                            this.state.iscoCodes.push(c.isco);
                        }
                    }
                    this.state.iscoCodes.sort();
                    this.setState({iscoCodes: this.state.iscoCodes});
                }
            }
        });
    }

    getAsString(list) {
        var s = "";
        if(list.length > 0) {
            s = list[0];
            for(var i=1; i<list.length; ++i) {
                s += ", " + list[i];
            }
        }
        return s;
    }

    updateAlternativeLabels(newValue) {
        App.getQuickEdits().add(this.item, Constants.QUICK_EDIT_ALTERNATIVE_LABEL, newValue, this.state.alternativeLabels);
        this.item.alternativeLabels = newValue;
        this.setState({alternativeLabels: newValue});
    }

    updateHiddenLabels(newValue) {
        App.getQuickEdits().add(this.item, Constants.QUICK_EDIT_HIDDEN_LABEL, newValue, this.state.hiddenLabels);
        this.item.hiddenLabels = newValue;
        this.setState({hiddenLabels: newValue});
    }

    onChangeName(e) {
        if(e.target.value && this.item) {
            App.getQuickEdits().add(this.item, Constants.QUICK_EDIT_NAME, e.target.value, this.item.preferredLabel);
            this.item.preferredLabel = e.target.value;
            this.setState({preferredLabel: this.item.preferredLabel});
        }
    }

    onChangeDefinition(e) {
        if(e.target.value && this.item) {            
            App.getQuickEdits().add(this.item, Constants.QUICK_EDIT_DEFINITION, e.target.value, this.item.definition);
            this.item.definition = e.target.value;
            this.setState({definition: this.item.definition});
        }
    }

    onAlternativeLabelChanged(index, e) {
        var newValue = JSON.parse(JSON.stringify(this.state.alternativeLabels));
        newValue[index] = e.target.value;
        this.updateAlternativeLabels(newValue);
    }
    
    onAlternativeLabelRemoved(index) {
        var newValue = JSON.parse(JSON.stringify(this.state.alternativeLabels));
        newValue.splice(index, 1);
        this.updateAlternativeLabels(newValue);
    }

    onAlternativeLabelAdded() {
        var newValue = JSON.parse(JSON.stringify(this.state.alternativeLabels));
        newValue.push("");
        this.updateAlternativeLabels(newValue);
    }

    onHiddenLabelChanged(index, e) {
        var newValue = JSON.parse(JSON.stringify(this.state.hiddenLabels));
        newValue[index] = e.target.value;
        this.updateHiddenLabels(newValue);
    }
    
    onHiddenLabelRemoved(index) {
        var newValue = JSON.parse(JSON.stringify(this.state.hiddenLabels));
        newValue.splice(index, 1);
        this.updateHiddenLabels(newValue);
    }

    onHiddenLabelAdded() {
        var newValue = JSON.parse(JSON.stringify(this.state.hiddenLabels));
        newValue.push("");
        this.updateHiddenLabels(newValue);
    }

    onQualityLevelChanged(e) {
        if(e.target.value == "undefined") {
            if(this.qualityChange != null) {
                this.setState({qualityLevel: this.qualityChange.originalValue}, () => {
                    App.getQuickEdits().remove(this.qualityChange);
                    this.qualityChange = null;
                });
            }
        } else {
            this.qualityChange = App.getQuickEdits().add(this.item, Constants.QUICK_EDIT_QUALITY_LEVEL, e.target.value, "" + this.state.qualityLevel);
            this.item.quality_level = e.target.value;
            this.setState({qualityLevel: e.target.value});
        }
    }

    onNoEscoRelationChanged(e) {
        App.getQuickEdits().add(this.item, Constants.QUICK_EDIT_NO_ESCO_RELATION, e.target.checked, this.item.no_esco_relation);
        this.item.no_esco_relation = e.target.checked;
        this.forceUpdate();
    }

    renderSpecialValue(elements, key, text) {
        if(this.item[key]) {
            elements.push(
                <div 
                    className={"description_special_value" + (key == "id" ? " description_db_id" : "")} 
                    key={key}>
                    <Label text={text}/>
                    <input 
                        type="text" 
                        className="rounded"
                        disabled="disabled"
                        value={this.item[key]}/>
                </div>
            );
        } 
    }

    renderIscoCodes() {
        if(this.item["ssyk"] && this.item["ssyk"].length > 3) {
            var codes = this.state.iscoCodes.map((element, i) => {
                return (
                    <div key={i}>{element}</div>
                );
            });
            if(codes.length == 0) {
                codes = <Loader />;
            }
            return (
                <div className="description_isco_container description_special_value font">
                    <Label text="ISCO"/>
                    <div className="description_isco_values">
                        {codes}
                    </div>
                </div>
            );
        }
    }

    renderNameAndMisc() {
        var elements = [];
        elements.push(
            <div 
                key="name-key"
                className="description_name">
                <Label text={Localization.get("name")}/>
                <input 
                    type="text" 
                    className={this.getCss("rounded", "name")}
                    disabled={this.props.enableQuickEdit ? "" : "disabled"}
                    value={this.state.preferredLabel}
                    onChange={this.onChangeName.bind(this)}/>
                {this.renderEdit("name")}
            </div>
        );
        if(this.state.type == Constants.CONCEPT_OCCUPATION_NAME || this.state.type == Constants.CONCEPT_SKILL) {
            if(this.props.enableQuickEdit) {
                elements.push(
                    <div key="quality-key" className="description_quality_value">
                        <Label text={Localization.get("quality_control")}/>
                        <select 
                            className="rounded"
                            value={this.state.qualityLevel}
                            onChange={this.onQualityLevelChanged.bind(this)}>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option value="undefined">{Localization.get("undefined")}</option>
                        </select>
                    </div>
                );
            } else {
                elements.push(
                    <div key="quality-key" className="description_quality_value">
                        <Label text={Localization.get("quality_control")}/>
                        <input 
                            type="text" 
                            className="rounded"
                            disabled="disabled"
                            value={this.state.qualityLevel == "undefined" ? "" : this.state.qualityLevel}/>
                    </div>
                );
            }
        }
        //esco_uri
        this.renderSpecialValue(elements, "id", "ID");
        this.renderSpecialValue(elements, "ssyk", "SSYK");
        this.renderSpecialValue(elements, "isco", "ISCO");
        this.renderSpecialValue(elements, "iso_3166_1_alpha_2_2013", "Kod"); // land
        this.renderSpecialValue(elements, "iso_3166_1_alpha_3_2013", Localization.get("name"));
        this.renderSpecialValue(elements, "driving_licence_code_2013", "Typ"); // körkort
        this.renderSpecialValue(elements, "eures_code_2014", "Typ"); // anställningsvaraktighet
        this.renderSpecialValue(elements, "iso_639_3_alpha_2_2007", "Kod"); // språk
        this.renderSpecialValue(elements, "iso_639_3_alpha_3_2007", Localization.get("name")); 
        this.renderSpecialValue(elements, "national_nuts_level_3_code_2019", "NNUTS");  // eu region
        this.renderSpecialValue(elements, "nuts_level_3_code_2013", "NUTS");  // eu region
        this.renderSpecialValue(elements, "sni_level_code_2007", "SNI"); 
        this.renderSpecialValue(elements, "sun_education_level_code_2020", "SUN"); 
        this.renderSpecialValue(elements, "sun_education_field_code_2020", "SUN"); 
        this.renderSpecialValue(elements, "lau_2_code_2015", "LAU");
        this.renderSpecialValue(elements, "unemployment_fund_code", "A-kassekod");
        this.renderSpecialValue(elements, "unemployment_type_code", "A-kassetyp")
        this.renderSpecialValue(elements, "sort_order", Localization.get("sort_order_short"));
        return (
            <div className="description_name_and_misc">
                {elements}
                {this.renderIscoCodes()}
            </div>
        );
    }

    renderEdit(field) {
        var user = this.getEditUser(field);
        if(user) {
            return (
                <div className="user_state_edit font">
                    {user} arbetar här...
                </div>
            );
        }
    }

    renderArrayFieldAsEditField(labelName, field) {
        return (
            <div className="description_alt_name">
                <Label text={Localization.get(labelName)}/>
                <input 
                    type="text" 
                    className="rounded"
                    disabled="disabled"
                    value={this.getAsString(this.state[field])}/>
                {this.renderEdit(field)}
            </div>
        );
    }

    renderAlternativeLabels() {
        if(this.props.enableQuickEdit) {
            var items = this.state.alternativeLabels.map((element, index) => {
                return (
                    <div className="description_array_element font" key={index}>
                        <input 
                            type="text" 
                            className="rounded"
                            value={element}
                            onChange={this.onAlternativeLabelChanged.bind(this, index)}/>
                        <Button 
                            text={Localization.get("remove")}
                            onClick={this.onAlternativeLabelRemoved.bind(this, index)}/>
                    </div>
                );
            });
            return (
                <div className="description_alt_name">
                    <div className="description_add_container">
                        <Label text={Localization.get("alternative_names")}/>
                        <Button 
                            text={Localization.get("add")}
                            onClick={this.onAlternativeLabelAdded.bind(this)}/>
                    </div>
                    {items}
                </div>
            );
        } else if(this.state.alternativeLabels.length > 0) {
            return this.renderArrayFieldAsEditField("alternative_names", "alternativeLabels");
        }
    }

    renderHiddenLabels() {
        if(this.props.enableQuickEdit) {
            var items = this.state.hiddenLabels.map((element, index) => {
                return (
                    <div className="description_array_element font" key={index}>
                        <input 
                            type="text" 
                            className="rounded"
                            value={element}
                            onChange={this.onHiddenLabelChanged.bind(this, index)}/>
                        <Button 
                            text={Localization.get("remove")}
                            onClick={this.onHiddenLabelRemoved.bind(this, index)}/>
                    </div>
                );
            });
            return (
                <div className="description_alt_name">
                    <div className="description_add_container">
                        <Label text={Localization.get("hidden_names")}/>
                        <Button 
                            text={Localization.get("add")}
                            onClick={this.onHiddenLabelAdded.bind(this)}/>
                    </div>
                    {items}
                </div>
            );
        } else if(this.state.hiddenLabels.length > 0) {
            return this.renderArrayFieldAsEditField("hidden_names", "hiddenLabels");
        }
    }

    renderEscoUriAndNoRelation() {        
        if(this.item.type.indexOf("esco") >= 0 || this.item.type == "skill") {
            var elements = [];
            
            if(this.item["esco_uri"]) {
                elements.push(
                    <div 
                        className="description_esco_uri"
                        key="esco_uri">
                        <Label text={Localization.get("esco_uri")}/>
                        <input 
                            type="text" 
                            className="rounded"
                            disabled="disabled"
                            value={this.item["esco_uri"]}/>
                    </div>
                );                
            }
            elements.push(
                <div 
                    className="description_no_esco_relation" 
                    key="no_esco_relation">
                    <Label text="Ingen ESCO-relation"/>
                    <input 
                        type="checkbox" 
                        className="rounded"
                        disabled={this.props.enableQuickEdit ? "" : "disabled"}
                        checked={this.item.no_esco_relation ? true : false}
                        onChange={this.onNoEscoRelationChanged.bind(this)}/>
                </div>
            ); 
            return (
                <div className="description_name_and_misc">
                    {elements}
                </div>
            );
        }        
    }

    renderAlternativeAndHidden() {
        return (
            <div className="description_alt_and_hidden">
                {this.renderAlternativeLabels()}
                {this.renderHiddenLabels()}
            </div>
        );
    }

    renderDefinitionAndDescription() {        
        if(this.state.shortDescription.length > 0) {
            return (
                <div className="description_definition_and_short">
                    <div className="description_definition">
                        <Label text={Localization.get("description")}/>
                        <textarea 
                            rows="10" 
                            className={this.getCss("rounded", "description")}
                            disabled={this.props.enableQuickEdit ? "" : "disabled"}
                            value={this.state.definition}
                            onChange={this.onChangeDefinition.bind(this)}/>
                        {this.renderEdit("description")}
                    </div>
                    <div className="description_short_description">
                        <Label text={Localization.get("short-description")}/>
                        <textarea 
                            rows="10" 
                            className="rounded"
                            disabled="disabled"
                            value={this.state.shortDescription}/>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="description_definition">
                    <Label text={Localization.get("description")}/>
                    <textarea 
                        rows="10" 
                        className={this.getCss("rounded", "description")}
                        disabled={this.props.enableQuickEdit ? "" : "disabled"}
                        value={this.state.definition}
                        onChange={this.onChangeDefinition.bind(this)}/>
                    {this.renderEdit("description")}
                </div>
            );
        }
    }
    
    render() {
        return (
            <div className="description">
                {this.renderNameAndMisc()}
                {this.renderEscoUriAndNoRelation()}
                {this.renderAlternativeAndHidden()}
                {this.renderDefinitionAndDescription()}
            </div>
        );
    }
	
}

export default Description;