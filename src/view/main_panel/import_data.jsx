import React from 'react';
import ExcelLib from 'exceljs';
import Button from '../../control/button.jsx';
import Pager from '../../control/pager.jsx';
import Label from '../../control/label.jsx';
import List from '../../control/list.jsx';
import Group from '../../control/group.jsx';
import SortArrow from '../../control/sort_arrow.jsx';
import Constants from '../../context/constants.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import Util from '../../context/util.jsx';
import Rest from '../../context/rest.jsx';
import App from '../../context/app.jsx';
import CacheManager from '../../context/cache_manager.jsx';
import ImportWorkWithItem from '../dialog/import_work_with_item.jsx';
import ImportMarkItemAs from '../dialog/import_mark_item_as.jsx';

class SearchResult extends React.Component { 

    constructor(props) {
        super();
        this.IMPORT_DATA_LIST_EVENT_ID = "IMPORT_DATA_LIST_EVENT_ID";
        this.STATE_DUPLICATE = "duplicate";
        this.STATE_TRASH = "trash";
        this.STATE_NEW = "new";
        this.STATE_INVESTIGATE = "investigate";
        this.STATE_ALTERNATIVE_LABEL = "alternative label";
        this.state = {
            data: [],
            filename: "",
            filteredData: [],
            selected: null,
            range: null,
            showProcessed: true,    
            orderBy: "order_name",
            showChooceOrderBy: false,
            sortOrder: true,
        };
        this.ssyks = null;
    }

    componentDidMount() {
        //fetch ssyk-level-4 populated with relations against occupation-name 
        Rest.getSsyksAndOccupationNameRelations((data) => {
            for(var i=0; i<data.length; ++i) {
                var item = data[i];
                item.label = item.ssyk_code_2012 + " - " + item.preferredLabel;
                for(var j=0; j<item.narrower.length; ++j) {
                    item.narrower[j].label = item.narrower[j].preferredLabel;
                }
            }
            this.ssyks = data;            
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
        });
        //only initialize if first cashing is done
        if(App.getCachedTypes(Constants.CONCEPT_OCCUPATION_NAME) && 
            App.getCachedTypes(Constants.CONCEPT_SSYK_LEVEL_4) &&
            App.getCachedTypes(Constants.CONCEPT_KEYWORD) &&
            App.getCachedTypes(Constants.CONCEPT_JOB_TITLE) ) {
            var data = []; 
            if(this.props.importData != undefined && this.props.importData != null) {
                data = this.props.importData.data;
                this.state.filename = this.props.importData.name;
            }
            this.state.data = this.initData(data);
            this.saveWorkingCopy();
            this.setState({
                filteredData: this.getFilteredData(this.state.showProcessed),
            });
        }
    }

    UNSAFE_componentWillReceiveProps(props) {
        var data = []; 
        if(props.importData != undefined && props.importData != null) {
            data = props.importData.data;
            this.state.filename = this.props.importData.name;
        }
        this.state.data = this.initData(data);
        this.saveWorkingCopy();
        this.setState({
            filteredData: this.getFilteredData(this.state.showProcessed),
        });
    }

    componentWillUnmount() {
        if(this.interval) {
            clearInterval(this.interval);
        }
        this.saveWorkingCopy();
    }
    
    saveWorkingCopy() {
        CacheManager.cache(Constants.KEY_IMPORT_DATA, {
            name: this.state.filename, 
            data: this.getUnmanagedData(),
        });       
    }

    initData(data) {
        //Remove duplicates
        data = data.filter((e, i) => {
            for(var j = i+1; j<data.length; ++j) {
                if(e.name == data[j].name) {
                    return false;
                }
            }
            return true;
        });
        
        var tmp = 0;
        var getMatches = (list, name)=>{
            return list.filter((e) => {
                if(e.preferredLabel.toLowerCase() == name) {
                    return true;
                }                
                if(e.hiddenLabels) {
                    for(var i=0; i<e.hiddenLabels.length; ++i) {                        
                        if(e.hiddenLabels[i].toLowerCase() == name) {    
                            return true;
                        }
                    }
                }
                if(e.alternativeLabels) {
                    for(var i=0; i<e.alternativeLabels.length; ++i) {                        
                        if(e.alternativeLabels[i].toLowerCase() == name) {
                            return true;
                        }
                    }
                }
                return false;
            });
        };

        var occupationNames = App.getCachedTypes(Constants.CONCEPT_OCCUPATION_NAME);
        var ssyks = App.getCachedTypes(Constants.CONCEPT_SSYK_LEVEL_4);
        var keyword = App.getCachedTypes(Constants.CONCEPT_KEYWORD);
        var jobTitles = App.getCachedTypes(Constants.CONCEPT_JOB_TITLE);
        for(var i = 0; i < data.length; ++i) {            
            var item = data[i];
            if(!item.state) {
                item.state = "";
            }
            if(!item.comment) {
                item.comment = "";
            }
            var lowerName = item.name.toLowerCase();
            var matches = getMatches(occupationNames, lowerName);
            if(matches.length > 0) {
                item.state = this.STATE_DUPLICATE;
                item.trash = true;
                item.occupationName = matches[0];
            }
            matches = getMatches(ssyks, lowerName);
            if(matches.length > 0) {
                item.state = this.STATE_DUPLICATE;
                item.trash = true;
                item.ssyk = matches[0];
            }
            matches = getMatches(keyword, lowerName);
            if(matches.length > 0) {
                item.state = this.STATE_DUPLICATE;
                item.trash = true;
                item.keyword = matches[0];
            }
            matches = getMatches(jobTitles, lowerName);
            if(matches.length > 0) {
                item.state = this.STATE_DUPLICATE;
                item.trash = true;
                item.jobTitle = matches[0];
            }
        }
        return data;
    }

    sortData() {        
        this.setState({
            filteredData: this.getFilteredData(this.state.showProcessed),
        });
    }

    getFilteredData(showProcessed) {       
        this.state.data = Util.sortByKey(this.state.data, this.state.orderBy == "order_name" ? "name" : "state", this.state.sortOrder);
        if(showProcessed) {
            return this.state.data;
        } else {
            return this.state.data.filter((item) => {
                if(item.trash || item.alternateLabelCreatedOn || item.itemCreated) {
                    return false;
                } else {
                    return true;
                }
            });
        }
    }

    getPageData() {
        if(this.state.filteredData.length == 0 || this.state.range == null) {
            return [];
        }
        var range = this.state.range;
        return this.state.filteredData.slice(range.start, range.end);
    }

    getUnmanagedData() {
        return this.state.data.filter((item) => {
            if(item.state == "duplicate" || item.alternateLabelCreatedOn || item.itemCreated) {
                return false;
            } else {
                return true;
            }
        });        
    }

    onNewRange(range) {
        this.setState({range: range});
    }

    onItemSelected(item) {
        this.setState({selected: item});
    }

    onMarkItemClicked(item) {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("Bearbeta") + " " + item.name,            
            content: <ImportMarkItemAs 
                onItemUpdated={()=>{
                    this.setState({filteredData: this.getFilteredData(this.state.showProcessed)});
                }}
                ssyks={this.ssyks}
                item={item}                
                message={Localization.get("imported_from") + " " + this.state.filename}/>,
        });
    }

    onWorkWithItemClicked(item) {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("Bearbeta") + " " + item.name,            
            content: <ImportWorkWithItem 
                onItemUpdated={()=>{
                    this.setState({filteredData: this.getFilteredData(this.state.showProcessed)});
                }}
                ssyks={this.ssyks}
                item={item}                
                message={Localization.get("imported_from") + " " + this.state.filename}/>,
        });
    }

    onSaveClicked() {
        var workbook = new ExcelLib.Workbook();
        var today = new Date().toLocaleDateString();
        var sheet = workbook.addWorksheet("Remaining " + today);
        var data = this.getUnmanagedData();
        for(var i=0; i<data.length; ++i) {
            var item = data[i];            
            sheet.addRow([item.name, item.state, item.comment]);
        }
        var filename = "Import-" + today + ".xlsx";
        workbook.xlsx.writeBuffer().then((buffer) => {
            var blob = new Blob([buffer], { type: "excel/xlsx" });
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;
            link.click();
        });
    }

    onShowProcessedChanged(e) {
        this.state.showProcessed = !this.state.showProcessed;
        this.setState({filteredData: this.getFilteredData(this.state.showProcessed)});
    }

    onOrderBySelected(orderBy) {  
        this.setState({
            orderBy: orderBy,
            showChooceOrderBy: false,
        }, () => {
            this.sortData();
        });
    }

    onSelectOrderByClicked() {
        this.setState({
            showChooceOrderBy: !this.state.showChooceOrderBy
        }, () => {
            this.sortData();
        });
    }

    onSortOrderClicked() {
        this.setState({sortOrder: !this.state.sortOrder});
    }

    renderItem(item) {
        var isMatchInList = (label, array) => {
            var name = label.toLowerCase();
            for(var i=0; i<array.length; ++i) {
                if(array[i].toLowerCase() == name) {
                    return true;
                }                
            }
            return false;
        };
        var buildMatchTypeString = (type, name, array) => {            
            var str = type;
            if(isMatchInList(name, array.hiddenLabels)) {
                str += " - " + Localization.get("hidden_label");
            } else if(isMatchInList(name, array.alternativeLabels)) {
                str += " - " + Localization.get("alternative_label");
            }
            return str;
        }
        var enableManage = true;
        var css = "import_data_item";
        if(item.trash || item.alternateLabelCreatedOn || item.itemCreated) {
            css += " trash";
            enableManage = false;
        }
        var name = "";
        if(item.alternateLabelCreatedOn || item.itemCreated) {
            if(item.alternateLabelCreatedOn) {
                name = item.name + "(" + Localization.get("alternate_label_created") + " " + item.alternateLabelCreatedOn.preferredLabel + ")";
            } else {
                name = item.name + "(" + Localization.get("item_created") + ")";
            }            
        } else {
            if(item.occupationName) {   
                name += buildMatchTypeString(Localization.getTypeName(Constants.CONCEPT_OCCUPATION_NAME), item.name, item.occupationName);
            }
            if(item.ssyk) {
                if(name.length > 0) {
                    name += ", ";
                }
                name += buildMatchTypeString(Localization.getTypeName(Constants.CONCEPT_SSYK_LEVEL_4), item.name, item.ssyk);
            }
            if(item.keyword) {
                if(name.length > 0) {
                    name += ", ";
                }
                name += buildMatchTypeString(Localization.getTypeName("keyword"), item.name, item.keyword);
            }
            if(item.jobTitle) {
                if(name.length > 0) {
                    name += ", ";
                }
                name += buildMatchTypeString(Localization.getTypeName("job-title"), item.name, item.jobTitle);
            }
            if(name.length > 0) {
                name = item.name + " (" + name + ")";
            } else {
                if(item.trash) {
                    name = item.name + " (" + Localization.get("marked_as_trash") + ")";
                    enableManage = true;
                } else {
                    name = item.name;
                }
            }            
        }
        return (
            <div className={css}>
                <div>{name}</div>
                <div>
                    <div title={item.comment}>{Localization.get(item.state)}</div>
                    <div className="import_data_item_buttons">
                        <Button 
                            isEnabled={enableManage}
                            text={Localization.get("mark")}
                            onClick={this.onMarkItemClicked.bind(this, item)}/>
                        <Button 
                            isEnabled={enableManage}
                            text={Localization.get("process")}
                            onClick={this.onWorkWithItemClicked.bind(this, item)}/>
                    </div>
                </div>
            </div>
        );
    }

    renderSaveButton() {
        return (
            <div className="export_button_content">
                <div>{Constants.ICON_SVG_EXCEL}</div>
                <div>{Localization.get("export_not_processed")}</div>
            </div>
        );
    }

    renderChooseOrderBy() {
        var renderOrderBy = (type) => {
            return (
                <div
                    className={this.state.orderBy == type ? "selected" : ""}
                    onClick={this.onOrderBySelected.bind(this, type)}>
                    {Localization.get(type)}
                </div>
            );
        };        
        if(this.state.showChooceOrderBy) {
            return(
                <div className="sub_panel_choose_filter font">
                    {renderOrderBy("order_name")}
                    {renderOrderBy("order_state")}
                </div>
            );
        }
    }

    renderHeader() {
        return (
            <div className="import_data_order font">
                <div>{Localization.get("order_by")}</div>
                <div onPointerUp={this.onSelectOrderByClicked.bind(this)}>
                    {Localization.get(this.state.orderBy)}
                </div>
                <div onPointerUp={this.onSortOrderClicked.bind(this)}>
                    {<SortArrow css={this.state.sortOrder ? "down" : "up"}/>}
                </div>
                {this.renderChooseOrderBy()}
            </div>
        );
    }

    renderData() {
        return (
            <List 
                eventId={this.IMPORT_DATA_LIST_EVENT_ID}
                noItemSelect={true}
                data={this.getPageData()} 
                onItemSelected={this.onItemSelected.bind(this)}
                dataRender={this.renderItem.bind(this)}/>
        );
    }

    render() {        
        var title = Localization.get("import_concepts");
        if(this.state.data.length > 0) {
            var notProcessed = this.getFilteredData(false).length;
            title += " (" + notProcessed + " " + Localization.get("of") + " " + this.state.data.length + " " + Localization.get("left_to_process") + ")";
        }
        return (
            <Group 
                css="import_data_group"
                text={title}>
                {this.renderHeader()}                
                {this.renderData()}
                <Pager
                    data={this.state.filteredData}
                    itemsPerPage={50}
                    onNewRange={this.onNewRange.bind(this)}/>
                <div className="import_data_show_processed">
                    <input 
                        type="checkbox"  
                        onChange={this.onShowProcessedChanged.bind(this)}
                        checked={this.state.showProcessed}/>
                    <Label text={Localization.get("show_processed")}/>
                </div>
                <div className="dialog_content_buttons">
                    <Button
                        text={this.renderSaveButton()}
                        onClick={this.onSaveClicked.bind(this)}
                    />
                </div>
            </Group>
        );
    }
	
}

export default SearchResult;