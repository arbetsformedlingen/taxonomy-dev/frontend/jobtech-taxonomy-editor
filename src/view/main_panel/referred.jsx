import React from 'react';
import Button from '../../control/button.jsx';
import Group from '../../control/group.jsx';
import List from '../../control/list.jsx';
import Loader from '../../control/loader.jsx';
import Pager from '../../control/pager.jsx';
import SortArrow from '../../control/sort_arrow.jsx';
import Constants from '../../context/constants.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import Rest from '../../context/rest.jsx';
import Excel from './../../context/excel.jsx';
import Localization from '../../context/localization.jsx';
import Util from '../../context/util.jsx';
import Export from '../dialog/export.jsx';

class Referred extends React.Component { 

    constructor() {
        super();
        this.EVENT_CLEAR_REFERRED_CONCEPTS_LIST = "EVENT_CLEAR_REFERRED_CONCEPTS_LIST";
        this.EVENT_CLEAR_NOT_REFERRED_CONCEPTS_LIST = "EVENT_CLEAR_NOT_REFERRED_CONCEPTS_LIST";
        this.SORT_EVENT_LABEL = "SORT_EVENT_LABEL";
        this.SORT_EVENT_TYPE = "SORT_EVENT_TYPE";
        this.SORT_EVENT_REPLACED_BY_LABEL = "SORT_EVENT_REPLACED_BY_LABEL";
        this.SORT_EVENT_REPLACED_BY_TYPE = "SORT_EVENT_REPLACED_BY_TYPE";
        this.state = {            
            rData: [],
            nrData: [],
            loadingData: true,
            rSelected: null,
            nrSelected: null,
            rRange: null,
            nrRange: null,
        }
        this.rSortBy = this.SORT_EVENT_LABEL;
        this.rSortDesc = true;
        this.nrSortBy = this.SORT_EVENT_LABEL;
        this.nrSortDesc = true;
    }

    componentDidMount() {
        Rest.getDeprecatedConcepts((data) => {
            var withReplaced = data.filter((d) => {
                return d["replaced-by"] ? true : false;
            });
            var notReplaced = data.filter((d) => {
                return d["replaced-by"] ? false : true;
            });
            //Make sure we only got deprecated concepts
            notReplaced = notReplaced.filter((d) => {
                return d.deprecated;
            })

            this.setState({
                rData: this.sortData(withReplaced),
                nrData: this.sortData(notReplaced),
                loadingData: false,
            });
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : " + "Misslyckades med att hämta konsept");
            this.setState({
                loadingData: false,
            });
        });
    }

    sortData(data, sortBy, sortDesc) {
        var cmp;
        switch(sortBy) {
            default:
            case this.SORT_EVENT_LABEL:
                cmp = (a) => {return a.preferredLabel;};
                break;
            case this.SORT_EVENT_TYPE:
                cmp = (a) => {return Localization.getTypeName(a.type);};
                break;
            case this.SORT_EVENT_REPLACED_BY_LABEL:
                cmp = (a) => {return a["replaced-by"][0].preferredLabel;};
                break;
            case this.SORT_EVENT_REPLACED_BY_TYPE:
                cmp = (a) => {return Localization.getTypeName(a["replaced-by"][0].type);};
                break;
        }
        return Util.sortByCmp(data, cmp, sortDesc);
    }

    onItemSelected(item) {       
        this.setState({rSelected: item});
    }

    onItemNotReferredSelected(item) {
        this.setState({nrSelected: item})
    }

    onSortClicked(sortBy) {
        if(this.rSortBy === sortBy) {
            this.rSortDesc = !this.sortDesc;
        } else {
            this.rSortBy = sortBy;
            this.rSortDesc = false;
        }
        EventDispatcher.fire(this.EVENT_CLEAR_REFERRED_CONCEPTS_LIST);
        this.setState({
            rData: this.sortData(this.state.rData, this.rSortBy, this.rSortDesc),
            rSelected: null,
        });
    }

    onSortNotReferredClicked(sortBy) {
        if(this.nrSortBy === sortBy) {
            this.nrSortDesc = !this.nrSortDesc;
        } else {
            this.nrSortBy = sortBy;
            this.nrSortDesc = false;
        }
        EventDispatcher.fire(this.EVENT_CLEAR_NOT_REFERRED_CONCEPTS_LIST);
        this.setState({
            nrData: this.sortData(this.state.nrData, this.nrSortBy, this.nrSortDesc),
            nrSelected: null,
        });
    }

    onVisitClicked() {
        if(this.state.rSelected != null) {
            EventDispatcher.fire(Constants.ID_NAVBAR, Constants.WORK_MODE_1);
            setTimeout(() => {
                EventDispatcher.fire(Constants.EVENT_MAINPANEL_ITEM_SELECTED, this.state.rSelected);
            }, 500);
        }
    }

    onVisitReplacedByClicked() {
        if(this.state.rSelected != null) {
            Rest.getConcept(this.state.rSelected["replaced-by"][0].id, (data) => {                
                EventDispatcher.fire(Constants.ID_NAVBAR, Constants.WORK_MODE_1);
                setTimeout(() => {
                    EventDispatcher.fire(Constants.EVENT_MAINPANEL_ITEM_SELECTED, data[0]);
                }, 500);
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : " + "Misslyckades med att hämta konsept");
            })
        }
    }

    onVisitNotReferredClicked() {
        if(this.state.nrSelected != null) {
            EventDispatcher.fire(Constants.ID_NAVBAR, Constants.WORK_MODE_1);
            setTimeout(() => {
                EventDispatcher.fire(Constants.EVENT_MAINPANEL_ITEM_SELECTED, this.state.nrSelected);
            }, 500);
        }
    }

    onExportClicked() {
        var onSaveExcel = (values) => {
            var columns = [{
                text: Localization.get("concept") + " - " + Localization.get("type"),
                width: 25,
            }, {
                text: Localization.get("concept") + " - " + Localization.get("name"),
                width: 35,
            }, {
                text: Localization.get("concept") + " - " + Localization.get("database_id"),
                width: 20,
            }, {
                text: Localization.get("replaced_by_concept") + " - " + Localization.get("type"),
                width: 25,
            }, {
                text: Localization.get("replaced_by_concept") + " - " + Localization.get("name"),
                width: 35,
            }, {
                text: Localization.get("replaced_by_concept") + " - " + Localization.get("database_id"),
                width: 20,
            }];

            var context = Excel.createSimple(Localization.get("referred"), "Next", columns)
            for(var i=0; i<this.state.rData.length; ++i) {
                var item = this.state.rData[i];
                var row = [
                    "", 
                    Localization.getTypeName(item.type),
                    item.preferredLabel,
                    item.id,
                    Localization.getTypeName(item["replaced-by"][0].type),
                    item["replaced-by"][0].preferredLabel,
                    item["replaced-by"][0].id
                ];
                context.addRow(row);
            }
            context.download(Localization.get("referred") + ".xlsx");
            EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
        }        

        var values = [];

        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("export"),
            content: <Export 
                        values={values}
                        onSaveExcel={onSaveExcel}
                    />
        });
    }

    onExportNotReferredClicked() {
        var onSaveExcel = (values) => {
            var columns = [{
                text: Localization.get("concept") + " - " + Localization.get("type"),
                width: 25,
            }, {
                text: Localization.get("concept") + " - " + Localization.get("name"),
                width: 35,
            }, {
                text: Localization.get("concept") + " - " + Localization.get("database_id"),
                width: 20,
            }];

            var context = Excel.createSimple(Localization.get("referred"), "Next", columns)
            for(var i=0; i<this.state.nrData.length; ++i) {
                var item = this.state.nrData[i];
                var row = [
                    "", 
                    Localization.getTypeName(item.type),
                    item.preferredLabel,
                    item.id,
                ];
                context.addRow(row);
            }
            context.download(Localization.get("not_referred") + ".xlsx");
            EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
        }        

        var values = [];

        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("export"),
            content: <Export 
                        values={values}
                        onSaveExcel={onSaveExcel}
                    />
        });
    }

    getPageData(data, range) {
        if(data.length == 0 || range == null) {
            return [];
        }
        return data.slice(range.start, range.end);
    }

    onNewRange(range) {
        this.setState({rRange: range});
        if(this.state.rSelected != null) {
            EventDispatcher.fire(this.EVENT_CLEAR_REFERRED_CONCEPTS_LIST);
            this.onItemSelected(null);
        } 
    }

    onNewRangeNotReferred(range) {
        this.setState({nrRange: range});
        if(this.state.nrSelected != null) {
            EventDispatcher.fire(this.EVENT_CLEAR_NOT_REFERRED_CONCEPTS_LIST);
            this.onItemNotReferredSelected(null);
        } 
    }

    renderItem(item) {
        return (
            <div className="referred_concepts_item">
                <div title={Localization.getTypeName(item.type)}>{Localization.getTypeName(item.type)}</div>
                <div title={item.preferredLabel}>{item.preferredLabel}</div>
                <div title={Localization.getTypeName(item["replaced-by"][0].type)}>{Localization.getTypeName(item["replaced-by"][0].type)}</div>
                <div title={item["replaced-by"][0].preferredLabel}>{item["replaced-by"][0].preferredLabel}</div>
            </div>
        );
    }

    renderItemNotReferred(item) {
        return (
            <div className="referred_concepts_item">
                <div title={Localization.getTypeName(item.type)}>{Localization.getTypeName(item.type)}</div>
                <div title={item.preferredLabel}>{item.preferredLabel}</div>
            </div>
        );
    }

    renderTopHeader() {
        return (            
            <div className="referred_concepts_top_header no_select font">
                <div>
                    {Localization.get("concept")}
                </div>
                <div>
                    {Localization.get("replaced_by_concept")}
                </div>
            </div>
        )
    }

    renderHeader() {
        var renderArrow = (type) => {
            if(type == this.rSortBy) {
                return (
                    <SortArrow css={this.rSortDesc ? "down" : "up"}/>
                );
            }
        };
        return (
            <div className="referred_concepts_header no_select font">               
                <div onClick={this.onSortClicked.bind(this, this.SORT_EVENT_TYPE)}>
                    {Localization.get("type")}
                    {renderArrow(this.SORT_EVENT_TYPE)}
                </div>
                <div onClick={this.onSortClicked.bind(this, this.SORT_EVENT_LABEL)}>
                    {Localization.get("name")}
                    {renderArrow(this.SORT_EVENT_LABEL)}
                </div>
                <div onClick={this.onSortClicked.bind(this, this.SORT_EVENT_REPLACED_BY_TYPE)}>
                    {Localization.get("type")}
                    {renderArrow(this.SORT_EVENT_REPLACED_BY_TYPE)}
                </div>
                <div onClick={this.onSortClicked.bind(this, this.SORT_EVENT_REPLACED_BY_LABEL)}>
                    {Localization.get("name")}
                    {renderArrow(this.SORT_EVENT_REPLACED_BY_LABEL)}
                </div>
            </div>            
        );
    }

    renderHeaderNotReferred() {
        var renderArrow = (type) => {
            if(type == this.nrSortBy) {
                return (
                    <SortArrow css={this.nrSortDesc ? "down" : "up"}/>
                );
            }
        };
        return (
            <div className="referred_concepts_header no_select font">               
                <div onClick={this.onSortNotReferredClicked.bind(this, this.SORT_EVENT_TYPE)}>
                    {Localization.get("type")}
                    {renderArrow(this.SORT_EVENT_TYPE)}
                </div>
                <div onClick={this.onSortNotReferredClicked.bind(this, this.SORT_EVENT_LABEL)}>
                    {Localization.get("name")}
                    {renderArrow(this.SORT_EVENT_LABEL)}
                </div>
            </div>            
        );
    }

    renderLoader() {
        if(this.state.loadingData) {
            return (
                <Loader/>
            );
        }
    }

    render() {
        return (
            <div className="referred_concepts_main">
                <Group
                    text={Localization.get("referred")}>
                    <div className="referred_concepts">
                        {this.renderTopHeader()}
                        {this.renderHeader()}
                        <List
                            eventId={this.EVENT_CLEAR_REFERRED_CONCEPTS_LIST}
                            data={this.getPageData(this.state.rData, this.state.rRange)}
                            onItemSelected={this.onItemSelected.bind(this)}
                            dataRender={this.renderItem.bind(this)}>
                            {this.renderLoader()}
                        </List>
                        <Pager
                            data={this.state.rData}
                            itemsPerPage={50}
                            onNewRange={this.onNewRange.bind(this)}/> 
                        <div className="referred_button">
                            <Button 
                                isEnabled={this.state.rSelected != null}
                                onClick={this.onVisitClicked.bind(this)}
                                text={Localization.get("visit_concept")}/>
                            <Button 
                                isEnabled={this.state.rSelected != null}
                                onClick={this.onVisitReplacedByClicked.bind(this)}
                                text={Localization.get("visit_replaced_by")}/>
                            <Button                             
                                onClick={this.onExportClicked.bind(this)}
                                text={Util.renderExportButtonText()}/>
                        </div>
                    </div>
                </Group>
                <Group
                    text={Localization.get("not_referred")}>
                    <div className="referred_concepts">
                        {this.renderHeaderNotReferred()}
                        <List
                            eventId={this.EVENT_CLEAR_NOT_REFERRED_CONCEPTS_LIST}
                            data={this.getPageData(this.state.nrData, this.state.nrRange)}
                            onItemSelected={this.onItemNotReferredSelected.bind(this)}
                            dataRender={this.renderItemNotReferred.bind(this)}>
                            {this.renderLoader()}
                        </List>
                        <Pager
                            data={this.state.nrData}
                            itemsPerPage={50}
                            onNewRange={this.onNewRangeNotReferred.bind(this)}/> 
                        <div className="referred_button">
                            <Button 
                                isEnabled={this.state.nrSelected != null}
                                onClick={this.onVisitNotReferredClicked.bind(this)}
                                text={Localization.get("visit_concept")}/>
                            <Button                             
                                onClick={this.onExportNotReferredClicked.bind(this)}
                                text={Util.renderExportButtonText()}/>
                        </div>
                    </div>
                </Group>
            </div>
        );
    }
}

export default Referred;