import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content1 from "./content_1"
import React from "react"

describe("Content1", () => {
  test("renders", () => {
    render(<Content1 />);
    expect(screen.getByText(/Värdeförråd/)).toBeDefined();
    expect(Content1).toBeInstanceOf(Function);
    expect(Content1).toHaveProperty("prototype");
    expect(Content1.prototype).toBeInstanceOf(Object);
    expect(Content1.prototype.constructor).toBe(Content1);
  });
});