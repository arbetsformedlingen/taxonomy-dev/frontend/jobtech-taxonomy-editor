import React from 'react';
import HistoryDetails from '../dialog/history_details.jsx';
import List from '../../control/list.jsx';
import Loader from '../../control/loader.jsx';
import Button from '../../control/button.jsx';
import Constants from '../../context/constants.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import App from '../../context/app.jsx';
import Rest from '../../context/rest.jsx';
import Util from '../../context/util.jsx';

class ItemHistory extends React.Component { 

    constructor() {
        super();
        this.LIST_EVENT_ID = "ITEMHISTORY_LIST_ID";
        this.state = {
            concept: null,
            changes: [],
            selected: null,
            loading: false,
            from: null,
            to: null
        };
    }

    componentDidMount() {
        this.init(this.props);
    }

    UNSAFE_componentWillReceiveProps(props) {
        EventDispatcher.fire(this.LIST_EVENT_ID);
        this.init(props);
    }

    init(props) {
        this.setState({
            concept: props.item,
            changes: [],
            selected: null,
            loading: true,
            from: props.from,
            to: props.to
        }, () => {
            if(props.item && props.item.type) {
                Rest.getChangesForConcept(props.item.id, (data) => {
                    this.setState({
                        changes: Util.sortByKey(data, "timestamp", false),
                        loading: false,
                    });
                }, (status) => {
                    this.setState({loading: false});
                    App.showError(Util.getHttpMessage(status) + " : Hämta daganteckningar (relationer) misslyckades");
                });
            } else {
                this.setState({loading: false});
            }
        });
    }

    getConceptLabel(id) {
        var concept = App.findCachedConcept(id);
        if(concept) {
            return concept.label;
        } else {
            return id;
        }
    }

    onShowClicked() {
        if(this.state.selected) {
            EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
                title:this.renderHeader(this.state.selected),
                content: this.renderHistoryDialog(),
            });
        }
    }

    onItemSelected(item) {
        this.setState({selected: item});
    }

    renderHistoryDialog() {
        return (
            <HistoryDetails 
                concept={this.state.concept} 
                item={this.state.selected}/>            
        );
    }

    renderHeader(item) {        
        return (
            <div className="item_history_item">
                <div>
                    {new Date(item.timestamp).toLocaleString()}
                </div>
                <div>
                    {Localization.get(item.event)}
                </div>
                <div>
                    {item.user}
                </div>
            </div>
        );
    }

    renderItem(item) {
        var isRelation = (attribute) => {
            switch(attribute) {
                case "broader":
                case "narrower":
                case "broad_match":
                case "close_match":
                case "exact_match":
                case "narrow_match":
                case "related":
                case "replaces":
                case "replaced_by":
                case "substitutes":
                case "substituted_by":
                case "possible_combinations":
                case "unlikely_combinations":
                    return true;
                default:
                    return false;
                }
        };
        var event = Localization.get(item.event);
        if(item.changes) {
            var removed = [];
            var added = [];
            var changedAttributes = [];
            var reactivated = false;
            for(var i=0; i<item.changes.length; ++i) {
                var change = item.changes[i];
                if(isRelation(change.attribute)) {
                    removed.push(...Util.findMissingInArray(change.old_value, change.new_value));
                    added.push(...Util.findMissingInArray(change.new_value, change.old_value));                    
                } else {
                    if(item.event == "Updated") {
                        if(change.attribute == "deprecated") {
                            reactivated = true;                            
                        } else {
                            changedAttributes.push(Localization.get(change.attribute));                            
                        }
                    }
                }
            }
            event = "";
            if(reactivated) {
                event += Localization.get("concept_reactivated");
            }
            if(changedAttributes.length > 0) {
                var part = changedAttributes[0] + " " + Localization.get("changed").toLowerCase();                
                for(var i=1; i<changedAttributes.length; ++i) {
                    part += ", " + changedAttributes[i];
                }
                if(event.length > 0) {
                    event += ", ";
                }
                event += part;
            }
            //om endast 1 händelse visa namn på koncept annars bygg sträng med laggt till / tagit bort...
            if(removed.length + added.length == 1) {
                if(added.length > 0) {
                    part = "Relation skapad mot " + this.getConceptLabel(added[0].id);
                } else {
                    part = "Relation borttagen mot " + this.getConceptLabel(removed[0].id);
                }
                if(event.length > 0) {
                    event += ", ";
                }
                event += part;
            } else {
                if(added.length > 0) {
                    var part = added.length + " relationer skapade";
                    if(event.length > 0) {
                        event += ", ";
                    }
                    event += part;
                }
                if(removed.length > 0) {
                    var part = removed.length + " relationer borttagna";
                    if(event.length > 0) {
                        event += ", ";
                    }
                    event += part;
                }
            }
        } else if(item.event == "Created" && item.concept) {
            //count relations
            var count = item.concept.broader.length + 
                        item.concept.narrower.length + 
                        item.concept.related.length + 
                        item.concept.replaces.length +
                        item.concept.replaced_by.length +
                        item.concept.substitutes.length +
                        item.concept.substituted_by.length +
                        item.concept.broad_match.length + 
                        item.concept.close_match.length + 
                        item.concept.exact_match.length +
                        item.concept.narrow_match.length +
                        item.concept.possible_combinations.length +
                        item.concept.unlikely_combinations.length;
            if(count > 0) {
                event += " med " + count + " relationer";
            }
        }
        return (
            <div className="item_history_item" title={Localization.get("note") + ": " + item.comment}>
                <div>{new Date(item.timestamp).toLocaleString()}</div>
                <div>
                    {event}
                </div>
                <div>
                    {item.user}
                </div>
            </div>
        );
    }

    renderLoader() {
        if(this.state.loading) {
            return (<Loader/>);
        }        
    }

    render() {
        return (
            <div className="item_history">                
                <List 
                    eventId={this.LIST_EVENT_ID}
                    data={this.state.changes}
                    onItemSelected={this.onItemSelected.bind(this)}
                    dataRender={this.renderItem.bind(this)}>
                    {this.renderLoader()}
                </List>
                <div>
                    <Button 
                        isEnabled={this.state.selected != null}
                        text={Localization.get("show")}
                        onClick={this.onShowClicked.bind(this)}/>
                </div>
            </div>
        );
    }
	
}

export default ItemHistory;