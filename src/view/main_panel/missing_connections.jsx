import React from 'react';
import Rest from '../../context/rest.jsx';
import Util from '../../context/util.jsx';
import App from '../../context/app.jsx';
import CacheManager from '../../context/cache_manager.jsx';
import Constants from '../../context/constants.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import Button from '../../control/button.jsx';
import Label from '../../control/label.jsx';
import List from '../../control/list.jsx';
import Loader from '../../control/loader.jsx';
import Group from '../../control/group.jsx';
import Pager from '../../control/pager.jsx';
import Export from '../dialog/export.jsx';
import Excel from './../../context/excel.jsx';
import EditConcept from '../dialog/edit_concept.jsx';
import Settings from './../../context/settings.jsx';

class MissingConnections extends React.Component { 

    constructor() {
        super();
        this.EVENT_ID_MISSING_CONNECTIONS1 = "EVENT_ID_MISSING_CONNECTIONS1";
        this.EVENT_ID_MISSING_CONNECTIONS2 = "EVENT_ID_MISSING_CONNECTIONS2";
        this.state = {
            type1: "",
            type2: "",
            types: [],
            relationType: "",
            missingConnections: true,
            selected1: null,
            selected2: null,
            loadingConnectionsData: "",
            noConnectionsData1: [],
            noConnectionsData2: [],
            range1: null,
            range2: null,
        };
        this.concepts = [];
        this.edges = [];
    }

    componentDidMount() {
        this.init(this.props);
        this.props.showMissingContext.onShowMissingChanged = this.onShowMissingChanged.bind(this);
    }

    UNSAFE_componentWillReceiveProps(props) {
        this.init(props);
    }

    init(props) {
        this.concepts = [];
        this.edges = [];
        this.setState({
            types: props.types,
            relationType: props.relationType,
            missingConnections: props.showMissing,
            selected1: null,
            selected2: null,
            loadingConnectionsData: true,
            noConnectionsData1: [],
            noConnectionsData2: [],
        }, () => {
            if(props.concepts) {
                this.concepts = props.concepts;
            }
            if(props.edges) {
                this.edges = props.edges;
            }
            if(this.concepts) {
                this.saveData();
            }
            if(this.state.types != null && this.state.types.length > 1) {
                this.createReports();
            }
        });
    }

    hasEdge(id, edges) {
        return edges.find(e => e.source === id || e.target === id) != undefined;
    }

    getConceptsConnected(nodes, edges, withConnection) {
        var foundConnections = [];
        var matchingRelation = this.isMathingRelation(this.state.relationType);
        for(var i=0; i<nodes.length; ++i) {
            var node = nodes[i];            
            if(withConnection == this.hasEdge(node.id, edges)) {
                if(!matchingRelation  || ((matchingRelation && !node.no_esco_relation))) {
                    foundConnections.push(node);
                }
            }
        }
        return foundConnections.sort((a, b) => {
            if(a["ssyk-code-2012"] != null) {
                if(a["ssyk-code-2012"] < b["ssyk-code-2012"]) {
                    return -1;
                }
                if(a["ssyk-code-2012"] > b["ssyk-code-2012"]) {
                    return 1;
                }
                return 0;
            }
            if(a.preferredLabel < b.preferredLabel) {
                return -1;
            }
            if(a.preferredLabel > b.preferredLabel) {
                return 1;
            }
            return 0;
        });
    }    

    getConcepts(type) {
        var concepts = this.concepts.find((c) => {return c.type === type}); 
        return concepts == null ? null : concepts.data;
    }

    fetchConcepts(type) {
        this.state.selected1 = null;
        this.state.selected2 = null;
        this.setState({loadingConnectionsData: Localization.get("loading") + " " + Localization.getTypeName(type)}, () => {
            if(type == "ssyk-level-4") {
                Rest.getConceptsSsyk(type, (data) => {
                    data = data.filter((x) => { return !x.deprecated; });
                    this.concepts.push({type: type, data: data});
                    this.createReports();
                }, (status) => {
                    App.showError(Util.getHttpMessage(status) + " : misslyckades hämta ssyk concept");
                });
            } else {
                Rest.getConcepts(type, (data) => {
                    data = data.filter((x) => { return !x.deprecated; });
                    this.concepts.push({type: type, data: data});
                    this.createReports();
                }, (status) => {
                    App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
                });
            }    
        });
    }

    fetchGraph(type1, type2, relationType) {
        this.setState({loadingConnectionsData: Localization.get("loading") + " " + Localization.getTypeName(type1) + " <-> " + Localization.getTypeName(type2)}, () => {
            Rest.getGraph(relationType, type1, type2, (data) => {
                this.edges.push({
                    relationType: relationType,
                    type1: type1,
                    type2: type2,
                    data: data.graph.edges
                });
                this.saveData();
                this.createReports();
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades hämta graph");
            });
        });
    }

    saveData() {        
        CacheManager.cache(Constants.KEY_RELATIONS_SEARCH_DATA, {
            types: this.state.types,
            relationType: this.state.relationType,
            showMissing: this.state.missingConnections,
            concepts: this.concepts,
            edges: this.edges,
        });
    }

    getEdges(type1, type2, relationType) {
        var edges = this.edges.find((e) => {
            return e.type1 === type1 && e.type2 === type2 && e.relationType === relationType;
        });
        return edges == null ? null : edges.data;
    }

    createReports() {
        var getOrFetch = (type1, type2, relationType) => {
            var typeEdges = this.getEdges(type1, type2, relationType);
            if(typeEdges == null) {
                this.fetchGraph(type1, type2, relationType);
            }
            return typeEdges;
        }
        var concepts = [];
        for(var i=0; i<this.state.types.length; ++i) {
            concepts.push(this.getConcepts(this.state.types[i]));
            if(concepts[concepts.length-1] == null) {
                this.fetchConcepts(this.state.types[i]);
                return;
            }
        }
        var edges = [];
        for(var i=1; i<this.state.types.length; ++i) {
            if(this.state.relationType == "all_matching") {            
                var matchingEdges = getOrFetch(this.state.types[0], this.state.types[i], Constants.RELATION_BROAD_MATCH);
                if(matchingEdges == null) {
                    return;
                }
                edges.push(...matchingEdges);
                matchingEdges = getOrFetch(this.state.types[0], this.state.types[i], Constants.RELATION_NARROW_MATCH);
                if(matchingEdges == null) {
                    return;
                }
                edges.push(...matchingEdges);
                matchingEdges = getOrFetch(this.state.types[0], this.state.types[i], Constants.RELATION_CLOSE_MATCH);
                if(matchingEdges == null) {
                    return;
                }
                edges.push(...matchingEdges);
                matchingEdges = getOrFetch(this.state.types[0], this.state.types[i], Constants.RELATION_EXACT_MATCH);
                if(matchingEdges == null) {
                    return;
                }
                edges.push(...matchingEdges);
            } else {
                edges = getOrFetch(this.state.types[0], this.state.types[i], this.state.relationType);
                if(edges == null) {                
                    return;
                }
            }
        }

        var withoutConnection1 = this.getConceptsConnected(concepts[0], edges, !this.state.missingConnections);        

        var withoutConnection2 = [];
        for(var i=1; i<concepts.length; ++i) {
            withoutConnection2.push(...this.getConceptsConnected(concepts[i], edges, !this.state.missingConnections));
        }

        this.setState({
            noConnectionsData1: withoutConnection1, 
            noConnectionsData2: withoutConnection2, 
            loadingConnectionsData: false
        });
    }

    isMathingRelation(type) {
        return type.indexOf("match") >= 0;
    }

    getToName() {
        var name = Localization.getTypeName(this.state.types[1]);
        if(this.state.types.length > 2) {
            for(var i=2; i<this.state.types.length; ++i) {
                name += ", " + Localization.getTypeName(this.state.types[i]);
            }            
        }
        return name;
    }

    onShowMissingChanged(showMissing) {
        this.setState({missingConnections: showMissing}, 
            () => {
                this.createReports();
            });
    }

    onItemSelected1(item) {
        this.setState({selected1: item});
    }

    onItemSelected2(item) {
        this.setState({selected2: item});
    }

    onVisitClicked1() {
        if(this.state.selected1 != null) {
            EventDispatcher.fire(Constants.ID_NAVBAR, Constants.WORK_MODE_1);
            setTimeout(() => {
                EventDispatcher.fire(Constants.EVENT_MAINPANEL_ITEM_SELECTED, this.state.selected1);
            }, 500);
        }
    }

    onVisitClicked2() {
        if(this.state.selected2 != null) {
            EventDispatcher.fire(Constants.ID_NAVBAR, Constants.WORK_MODE_1);
            setTimeout(() => {
                EventDispatcher.fire(Constants.EVENT_MAINPANEL_ITEM_SELECTED, this.state.selected2);
            }, 500);
        }
    }

    onEditClicked1() {
        if(this.state.selected1 != null) {
            EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
                title: Localization.get("edit") + " " + this.state.selected1.preferredLabel,
                content: <EditConcept 
                            defaultType={Constants.EDIT_TYPE_ADD_RELATION}
                            item={App.getCachedConceptFromId(this.state.selected1.id)}
                            meta={{semanticType: this.state.type2}}
                            onItemUpdated={this.onItemSaved.bind(this)}/>
            });
        }
    }

    onEditClicked2() {
        if(this.state.selected2 != null) {
            EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
                title: Localization.get("edit") + " " + this.state.selected2.preferredLabel,
                content: <EditConcept 
                            defaultType={Constants.EDIT_TYPE_ADD_RELATION}
                            item={App.getCachedConceptFromId(this.state.selected2.id)}
                            meta={{semanticType: this.state.type1}}
                            onItemUpdated={this.onItemSaved.bind(this)}/>
            });
        }
    }

    onItemSaved() {

    }

    onExportClicked(title, data) {
        var onSaveExcel = () => {
            var context = Excel.create(Localization.get("relation_search"), title, "Next");
            // data
            context.addHeadlines("Databas-ID", "Namn");
            for(var i=0; i<data.length; ++i) {
                var item = data[i];
                var nr = context.addLeftRight();
                context.setCell("C" + nr, item.id);
                context.setCell("G" + nr, item.preferredLabel);
            }
            context.download(Localization.get("relation_search") + ".xlsx");
            EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
        };
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("export"),
            content: <Export 
                        onSaveExcel={onSaveExcel}/>
        });
    }

    getPageData(data, range) {
        if(data.length == 0 || range == null) {
            return [];
        }        
        return data.slice(range.start, range.end);
    }

    onNewRange1(range) {        
        this.setState({range1: range});
        if(this.state.selected1 != null) {
            EventDispatcher.fire(this.EVENT_ID_MISSING_CONNECTIONS1);
            this.onItemSelected1(null);
        } 
    }

    onNewRange2(range) {
        this.setState({range2: range});
        if(this.state.selected2 != null) {
            EventDispatcher.fire(this.EVENT_ID_MISSING_CONNECTIONS2);
            this.onItemSelected2(null);
        } 
    }

    renderInfo(data, from) {
        var totalCount = 0;
        var noEscoRelationCount = 0;
        var count = data.length;
        var startIndex = from ? 0 : 1;
        var endIndex = from ? 1 : this.state.types.length;
        for(var i=startIndex; i<endIndex; ++i) {
            var concepts = this.getConcepts(this.state.types[i]);
            if(concepts != null) {
                totalCount = concepts.length;
                noEscoRelationCount = concepts.filter((x) => x.no_esco_relation == true).length;
                if(this.isMathingRelation(this.state.relationType) && !this.state.missingConnections) {
                    count += noEscoRelationCount;
                }
            }
        }

        var info = count + " " + Localization.get("of") + " " + totalCount + " ";
        if(this.state.missingConnections) {
            info += Localization.get("are_missing_connections");
            if(count == totalCount) {
                info = Localization.get("no_items_connected");
            } else if(count == 0) {
                info = Localization.get("all_items_connected");
            }
        } else {
            info += Localization.get("are_connected");
            if(count == totalCount) {
                info = Localization.get("all_items_connected");
            } else if(count == 0) {
                info = Localization.get("no_items_connected");
            }
        }
        return (
            <Label text={info}/>
        );
    }

    renderItem(item) {
        return (
            <div className="main_content_4_list_item">
                <div>
                    {item.id}
                </div>                
                <div>
                    {item.preferredLabel}
                </div>
            </div>
        );
    }

    renderList(data, onItemSelected, eventId) {
        if(data != null && data.length > 0) {
            return (
                <List
                    eventId={eventId}
                    css="main_content_4_list"
                    data={data}
                    onItemSelected={onItemSelected}
                    dataRender={this.renderItem.bind(this)}
                />
            );
        }
    }

    renderResult() {
        if(this.state.types == null || this.state.types.length < 2) {
            return (
                <div className="missing_connection_content"></div>
            );
        }
        if(this.state.types.length == 2 && this.state.types[0] == this.state.types[1]) {
            var title = Localization.getTypeName(this.state.types[0]) + " " + Localization.get("to") + " " + this.getToName();
            return (
                <div className="missing_connection_content">
                    <Group text={title}>
                        <div className="main_content_4_connections">                        
                            {this.renderInfo(this.state.noConnectionsData1, true)}
                            {this.renderList(this.getPageData(this.state.noConnectionsData1, this.state.range1), this.onItemSelected1.bind(this), this.EVENT_ID_MISSING_CONNECTIONS1)}
                            <Pager
                                data={this.state.noConnectionsData1}
                                itemsPerPage={50}
                                onNewRange={this.onNewRange1.bind(this)}/>
                            <div className="main_content_4_button_container">
                                <Button
                                    isEnabled={this.state.noConnectionsData1.length > 0 && this.state.selected1 != null}
                                    text={Localization.get("visit")} 
                                    onClick={this.onVisitClicked1.bind(this)}/>
                                <Button
                                    isEnabled={this.state.noConnectionsData1.length > 0 && this.state.selected1 != null && Settings.isEditable(this.state.type1)}
                                    text={Localization.get("edit")} 
                                    onClick={this.onEditClicked1.bind(this)}/>
                                <Button
                                    isEnabled={this.state.noConnectionsData1.length > 0}
                                    onClick={this.onExportClicked.bind(this, title, this.state.noConnectionsData1)}
                                    text={Util.renderExportButtonText()}/>
                            </div>
                        </div>
                    </Group>                
                </div>
            );
        } else {
            var title1 = Localization.getTypeName(this.state.types[0]) + " " + Localization.get("to") + " " + this.getToName() + " - " + Localization.get(this.state.missingConnections ? "missing_connections" : "has_connections");
            var title2 = this.getToName() + " " + Localization.get("to") + " " + Localization.getTypeName(this.state.types[0]) + " - " + Localization.get(this.state.missingConnections ? "missing_connections" : "has_connections");
            return (
                <div className="missing_connection_content">
                    <Group text={title1}>
                        <div className="main_content_4_connections">                        
                            {this.renderInfo(this.state.noConnectionsData1, true)}
                            {this.renderList(this.getPageData(this.state.noConnectionsData1, this.state.range1), this.onItemSelected1.bind(this), this.EVENT_ID_MISSING_CONNECTIONS1)}
                            <Pager
                                data={this.state.noConnectionsData1}
                                itemsPerPage={50}
                                onNewRange={this.onNewRange1.bind(this)}/>
                            <div className="main_content_4_button_container">
                                <Button
                                    isEnabled={this.state.noConnectionsData1.length > 0 && this.state.selected1 != null}
                                    text={Localization.get("visit")} 
                                    onClick={this.onVisitClicked1.bind(this)}/>
                                <Button
                                    isEnabled={this.state.noConnectionsData1.length > 0 && this.state.selected1 != null && Settings.isEditable(this.state.types[0])}
                                    text={Localization.get("edit")} 
                                    onClick={this.onEditClicked1.bind(this)}/>
                                <Button
                                    isEnabled={this.state.noConnectionsData1.length > 0}
                                    onClick={this.onExportClicked.bind(this, title1, this.state.noConnectionsData1)}
                                    text={Util.renderExportButtonText()}/>
                            </div>
                        </div>
                    </Group>
                    <Group text={title2}>
                        <div className="main_content_4_connections">                        
                            {this.renderInfo(this.state.noConnectionsData2, false)}
                            {this.renderList(this.getPageData(this.state.noConnectionsData2, this.state.range2), this.onItemSelected2.bind(this), this.EVENT_ID_MISSING_CONNECTIONS2)}
                            <Pager
                                data={this.state.noConnectionsData2}
                                itemsPerPage={50}
                                onNewRange={this.onNewRange2.bind(this)}/>
                            <div className="main_content_4_button_container">
                                <Button
                                    isEnabled={this.state.noConnectionsData2.length > 0 && this.state.selected2 != null}
                                    text={Localization.get("visit")} 
                                    onClick={this.onVisitClicked2.bind(this)}/>
                                <Button
                                    isEnabled={this.state.noConnectionsData2.length > 0 && this.state.selected2 != null && Settings.isEditable(this.state.types[1])}
                                    text={Localization.get("edit")} 
                                    onClick={this.onEditClicked2.bind(this)}/>
                                <Button
                                    isEnabled={this.state.noConnectionsData2.length > 0}
                                    onClick={this.onExportClicked.bind(this, title2, this.state.noConnectionsData2)}
                                    text={Util.renderExportButtonText()}/>
                            </div>
                        </div>
                    </Group>                
                </div>
            );
        }
    }

    render() {
        if(this.state.loadingConnectionsData.length > 0) {
            return (<Loader text={this.state.loadingConnectionsData}/>);
        }
        return this.renderResult();
    }
}

export default MissingConnections;