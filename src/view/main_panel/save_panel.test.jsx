import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import SavePanel from "./save_panel"
import React from "react"

describe("SavePanel", () => {
  test("renders", () => {
    render(<SavePanel />);
    expect(screen.getByText(/Anteckning/)).toBeDefined();
    expect(screen.getByText(/Stäng/)).toBeDefined();
    expect(screen.getByText(/Spara/)).toBeDefined();
    expect(screen.getAllByRole("textbox", { hidden: true })).toBeDefined();
    expect(SavePanel).toBeInstanceOf(Function);
    expect(SavePanel).toHaveProperty("prototype");
    expect(SavePanel.prototype).toBeInstanceOf(Object);
    expect(SavePanel.prototype.constructor).toBe(SavePanel);
  });
});