import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import MainPanel from "./main_panel"
import React from "react"

describe("MainPanel", () => {
  test("renders", () => {
    render(<MainPanel />);
    expect(screen.getByText(/Värdeförråd/)).toBeDefined();
    expect(MainPanel).toBeInstanceOf(Function);
    expect(MainPanel).toHaveProperty("prototype");
    expect(MainPanel.prototype).toBeInstanceOf(Object);
    expect(MainPanel.prototype.constructor).toBe(MainPanel);
  });
});