import React from 'react';
import Button from '../../../control/button.jsx';
import Group from '../../../control/group.jsx';
import List from '../../../control/list.jsx';
import Constants from '../../../context/constants.jsx';
import Localization from '../../../context/localization.jsx';
import Keybindings from '../../../context/keybindings.jsx';
import Settings from '../../../context/settings.jsx';
import App from '../../../context/app.jsx';

class SettingsKeyBindings extends React.Component { 

    constructor() {
        super();
        this.state = {  
        };
    }

    onItemClicked(id) {
        document.getElementById(id).focus();
    }

    onKeyDown(item, e) {
        if(e.key == "Escape") {
            e.target.blur();
        } else if(e.key >= 'a' && e.key <= 'ö') {
            // if another keybinding with this key exists, remove it
            var binding = Keybindings.getBindingForKey(e.key);
            if(binding != null) {
                binding.key = null;
            }
            // update current
            item.key = e.key;
            // save bindings
            Keybindings.save();
        } else {
            // TODO: notify invalid key
        }
        this.forceUpdate();
    }

    renderItems() {
        return Keybindings.keys.map((item, index) => {
            return (
                <div 
                    className="settings_keybinding"
                    key={index}
                    onPointerUp={this.onItemClicked.bind(this, "key_" + index)}>
                    <div>{Localization.get(item.type)}</div>
                    <div>
                        <div>CTRL +</div>
                        <div 
                            className="settings_keybinding_key"
                            id={"key_" + index}
                            tabIndex="0"
                            onKeyDown={this.onKeyDown.bind(this, item)}>
                            {item.key}
                        </div>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
            <Group text={Localization.get("settings_key_bindings")}>
                <div className="settings_keybindings_description font">
                    Klicka på det kortkommando du vill ändra, klicka sedan på en tanget för att ändra kortkommandot.
                </div>
                <List css="settings_keybindings">
                    {this.renderItems()}
                </List>
            </Group>
        );
    }
	
}

export default SettingsKeyBindings;