import React from 'react';
import Group from '../../../control/group.jsx';
import List from '../../../control/list.jsx';
import Button from '../../../control/button.jsx';
import Localization from '../../../context/localization.jsx';
import App from '../../../context/app.jsx';
import Constants from '../../../context/constants.jsx';
import Util from '../../../context/util.jsx';

class SettingsInfo extends React.Component { 

    constructor() {
        super();
        this.data = [];
        this.data.push(...App.executionTimes);
        Util.sortByKey(this.data, "time", false);
        // info dump
        console.log("Analytics", App.analytics);        
        console.log("Usage", App.usage.flow);
    }

    getAverageExecutionTime() {
        var average = 0;
        if(this.data.length > 0) {
            this.data.forEach((v) => average += v.millis);
            average /= this.data.length;
        }
        return average;
    }

    getMinExecutionTime() {
        if(this.data.length > 0) {
            var min = this.data[0].millis;
            this.data.forEach((v) => {
                if(v.millis < min) min = v.millis;
            });
            return min;
        }
        return 0;
    }

    getMaxExecutionTime() {
        if(this.data.length > 0) {
            var max = this.data[0].millis;
            this.data.forEach((v) => {
                if(v.millis > max) max = v.millis;
            });
            return max;
        }
        return 0;
    }

    renderAverage() {
        return (
            <div className='font'>
                Genomsnittlig svarstid (ms): {this.getAverageExecutionTime().toFixed(2)}
            </div>
        );
    }

    renderMax() {
        return (
            <div className='font'>
                Längsta svarstid (ms): {this.getMaxExecutionTime()}
            </div>
        );
    }

    renderMin() {
        return (
            <div className='font'>
                Minsta svarstid (ms): {this.getMinExecutionTime()}
            </div>
        );
    }

    renderItem(item) {
        var toClipboard = (i)=>{Util.copyToClipboard(i.query);};
        var pos = item.query.indexOf("?");
        var name = pos > 0 ? item.query.substring(0, pos) : item.query;
        var progressColorClass = item.millis > 1500 ? "settings_info_red" : item.millis > 600 ? "settings_info_yellow" : "settings_info_green";
        var progress = item.millis > 600 ? 100 : Math.round(item.millis / 600 * 100);
        if(item.event == Constants.INFO_EVENT_CACHE_UPDATE_START || item.event == Constants.INFO_EVENT_CACHE_UPDATE_STOP) {
            return (
                <div className="settings_info_item settings_info_item_cache">
                    <div>
                        {new Date(item.time).toLocaleTimeString()}
                    </div>
                    <div>
                        {item.event}
                    </div>
                </div>
            );
        } else {
            return (
                <div className="settings_info_item">
                    <div>
                        {new Date(item.time).toLocaleTimeString()}
                    </div>
                    <div>
                        {item.event}
                    </div>
                    <div title={decodeURIComponent(item.query)}>
                        {name}
                    </div>
                    <div className="settings_info_progress">
                        <div 
                            style={{width: progress + "%"}}
                            className={"settings_info_progress_bar " + progressColorClass}/>                    
                        <div className="settings_info_progress_info font">
                            {item.millis}
                        </div>
                    </div>
                    <Button                    
                        title="Copy"
                        text={Constants.ICON_COPY_CLIPBOARD}
                        onClick={toClipboard.bind(this, item)}/>
                </div>
            );
        }
    }

    renderNetworkUsage() {
        var items = this.data.map((item, i) => {

        });
        return (
            <div>
                <div className="settings_info_header settings_info_item font">
                    <div>Time</div>
                    <div>Event</div>
                    <div>Label</div>
                    <div>Duration (ms)</div>
                    <div></div>
                </div>
                <List 
                    noItemSelect
                    data={this.data}
                    dataRender={this.renderItem}/>
            </div>
        );
    }

    render() {
        return (
            <Group text={Localization.get("information")}>
                <div className="settings_info">
                    {this.renderAverage()}
                    {this.renderMax()}
                    {this.renderMin()}
                    {this.renderNetworkUsage()}
                </div>
            </Group>
        );
    }
	
}

export default SettingsInfo;