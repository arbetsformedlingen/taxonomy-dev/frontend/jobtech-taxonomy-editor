import React from 'react';
import Label from '../../../control/label.jsx';
import Group from '../../../control/group.jsx';
import List from '../../../control/list.jsx';
import Constants from '../../../context/constants.jsx';
import Localization from '../../../context/localization.jsx';
import Settings from '../../../context/settings.jsx';
import App from '../../../context/app.jsx';

class SettingsVisible extends React.Component { 

    constructor() {
        super();
        this.state = {
            editableTypes: Settings.data.editableTypes,
            visibleTypes: Settings.data.visibleTypes,
            preselected: Settings.preferences.tab_1_type,
            useCompact: Settings.preferences.useCompact ? Settings.preferences.useCompact : false,
        };
    }
   
    onEditableTypeChanged(type, e) {
        var types = this.state.editableTypes;
        if(e.target.checked) {
            types.push(type);
        } else {
            var index = types.indexOf(type);
            types.splice(index, 1);
        }
        this.setState({editableTypes: types}, () => {
            Settings.save();
        });
    }

    onPreSelectedTypeChanged(e) {
        this.setState({preselected: e.target.value});
        Settings.preferences.tab_1_type = e.target.value;
        Settings.savePreferences();
    }

    onCompactChange(e) {
        this.setState({useCompact: e.target.checked});
        Settings.preferences.useCompact = e.target.checked;
        Settings.savePreferences();
    }

    onVisibleTypeChanged(type, e) {
        var types = this.state.visibleTypes;
        if(e.target.checked) {
            types.push(type);
        } else {
            var index = types.indexOf(type);
            types.splice(index, 1);
        }
        this.setState({visibleTypes: types}, () => {
            Settings.save();
        });
    }

    renderVisiblyType(type) {
        if(!Settings.isGeographyOrEducation(type)) {
            return (
                <input 
                    type="checkbox"                        
                    onChange={this.onVisibleTypeChanged.bind(this, type)}
                    checked={Settings.isVisible(type)}/>
            );
        }
    }

    renderEditableType(type) {
        if(type != "geography_" &&
            type != "education_") {
            return (
                <input 
                    type="checkbox"                        
                    onChange={this.onEditableTypeChanged.bind(this, type)}
                    checked={Settings.isEditable(type)}/>
            );
        }
    }

    render() {
        var list = [];
        var preTypes = [];
        var skillTypes = ["skill", "skill-headline"]
        for(var i=0; i<App.types.length; ++i) {
            if(!App.isSpecialType(App.types[i]) || skillTypes.includes(App.types[i])) {
                list.push(App.types[i]);
                preTypes.push(App.types[i]);
            }
        }
        
        list.push("geography_");
        list.push("education_");
        preTypes.push("geography_");
        preTypes.push("education_");
        list = list.filter((item) => {
            return !Settings.isGeographyOrEducation(item);
        });
        list.sort((a, b) => {
            var p1 = Localization.getTypeName(a === Constants.CONCEPT_SKILL_HEADLINE ? "skill_headline" : a);
            var p2 = Localization.getTypeName(b === Constants.CONCEPT_SKILL_HEADLINE ? "skill_headline" : b);
            if(p1 < p2) return -1;
            if(p1 > p2) return 1;
            return 0;
        });
        preTypes.sort((a, b) => {
            var p1 = Localization.getTypeName(a === Constants.CONCEPT_SKILL_HEADLINE ? "skill_headline" : a);
            var p2 = Localization.getTypeName(b === Constants.CONCEPT_SKILL_HEADLINE ? "skill_headline" : b);
            if(p1 < p2) return -1;
            if(p1 > p2) return 1;
            return 0;
        });
        var index = list.indexOf("geography_");        
        list.splice(index + 1, 0, ...Settings.geographyTypes);
        index = list.indexOf("education_");
        list.splice(index + 1, 0, ...Settings.educationTypes);
        var types = list.map((type, index) => {
            return (
                <div key={index}>
                    <Label 
                        css="settings_types_type"
                        text={Localization.getTypeName(type === Constants.CONCEPT_SKILL_HEADLINE ? "skill_headline" : type)}/>
                    <div className="settings_types_cb">
                        {this.renderVisiblyType(type)}
                    </div>
                    <div className="settings_types_cb">
                        {this.renderEditableType(type)}
                    </div>
                </div>
            );
        });
        preTypes = preTypes.map((type, index) => {
            return (
                <option 
                    key={index}
                    value={type}>
                    {Localization.getTypeName(type)}
                </option>
            );
        });
        return (
            <Group text={Localization.get("types")}>
                <div className="settings_types_pre_selected_type font">
                    <div>{Localization.get("preselected_type")}</div>
                    <select 
                        value={this.state.preselected}
                        onChange={this.onPreSelectedTypeChanged.bind(this)}>
                        {preTypes}
                    </select>
                </div>
                <div className="settings_types_pre_selected_type font">
                    <label htmlFor="useCompact">Använd kompakt utsende (ladda om sidan för att se ändringen)</label>
                    <input
                        id="useCompact"
                        type="checkbox"
                        defaultChecked={this.state.useCompact}
                        value={this.state.useCompact}
                        onChange={this.onCompactChange.bind(this)}/>
                </div>
                <div className="settings_types_head">
                    <Label 
                        css="settings_types_type"
                        text={Localization.get("type")}/>
                    <Label 
                        css="settings_types_cb"
                        text={Localization.get("visible")}/>
                    <Label 
                        css="settings_types_cb"
                        text={Localization.get("editable")}/>
                </div>
                <List css="settings_types">
                    {types}
                </List>
            </Group>
        );
    }
	
}

export default SettingsVisible;