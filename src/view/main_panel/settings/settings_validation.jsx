import React from 'react';
import Label from '../../../control/label.jsx';
import Group from '../../../control/group.jsx';
import List from '../../../control/list.jsx';
import Button from '../../../control/button.jsx';
import Util from '../../../context/util.jsx';
import Constants from '../../../context/constants.jsx';
import EventDispatcher from '../../../context/event_dispatcher.jsx';
import Localization from '../../../context/localization.jsx';
import Settings from '../../../context/settings.jsx';
import SettingsValidationTypes from '../../dialog/settings_validation_types.jsx';

class SettingsValidation extends React.Component { 

    constructor() {
        super();
        this.state = {
            items: Settings.data.validation_groups,
        };
    }
    
    onEnabledChanged(item, e) {
        item.enabled = e.target.checked;
        this.setState({}, () => {
            Settings.save();
        });
    }

    onRemoveClicked(item, parent) {
        // TODO: dialog?
        var index = parent.children.indexOf(item);
        parent.children.splice(index, 1);
        if(parent.children.length == 0) {
            index = Settings.data.validation_groups.indexOf(parent);
            Settings.data.validation_groups.splice(index, 1);
        }
        this.setState({}, () => {
            Settings.save();
        });
    }

    onItemsAdded(groups) {
        for(var i=0; i<groups.length; ++i) {
            var group = this.state.items.find((e) => {
                return e.type == groups[i].type;
            });
            if(group == null) {
                group = {
                    type: groups[i].type,
                    children: [],
                };
                this.state.items.push(group);
            }
            for(var k=0; k<groups[i].items.length; ++k) {
                var item = group.children.find((e) => {
                    return e.id == groups[i].items[k].id;
                });
                if(item == null) {
                    group.children.push({
                        label: groups[i].items[k].label,
                        id: groups[i].items[k].id,
                        enabled: true,
                    });
                }
            }
        }
        this.setState({}, () => {
            Settings.save();
        });
    }

    onAddClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("settings"),
            content: <SettingsValidationTypes callback={this.onItemsAdded.bind(this)}/>
        });
    }

    render() {
        var items = this.state.items.map((root, ri) => {
            Util.sortByKey(root.children, "label", true);
            var children = root.children.map((child, ci) => {
                return (
                    <div 
                        key={ci}
                        className="settings_validation_child">
                        <div>
                            <input 
                                type="checkbox"
                                onChange={this.onEnabledChanged.bind(this, child)}
                                checked={child.enabled}/>
                            <Label text={child.label}/>
                        </div>
                        <Button 
                            text={Localization.get("remove")}
                            onClick={this.onRemoveClicked.bind(this, child, root)}/>
                    </div>
                );
            });
            return (
                <div 
                    key={ri}
                    className="settings_validation_root">
                    <Label text={Localization.getTypeName(root.type)}/>
                    {children}
                </div>
            );
        });
        return (
            <Group text={Localization.get("ignored_validation_concepts")}>
                <div className="main_content_5_buttons">
                    <Button
                        text={Localization.get("add")}
                        onClick={this.onAddClicked.bind(this)}/>
                </div>
                <List css="settings_validation_list">
                    {items}
                </List>
            </Group>
        );
    }
	
}

export default SettingsValidation;