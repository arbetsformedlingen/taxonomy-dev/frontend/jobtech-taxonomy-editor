import React from 'react';
import Button from '../../../control/button.jsx';
import Label from '../../../control/label.jsx';
import Group from '../../../control/group.jsx';
import List from '../../../control/list.jsx';
import Constants from '../../../context/constants.jsx';
import Localization from '../../../context/localization.jsx';
import EventDispatcher from '../../../context/event_dispatcher.jsx';
import Rest from '../../../context/rest.jsx';
import Util from '../../../context/util.jsx';
import App from '../../../context/app.jsx';
import SettingsEditTypeLocalization from './../../dialog/settings_edit_type_localization.jsx';

class SettingsTypeLocalization extends React.Component { 

    constructor() {
        super();       
    }

    onEditClicked(type) {        
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: "Redigera benämning för " + type,
            content: <SettingsEditTypeLocalization 
                type={type}
                label_en={Localization.getTypeNameForLang(type, "label_en")}
                label_sv={Localization.getTypeNameForLang(type, "label_sv")}
                callback={this.onSave.bind(this)}/>
        });
    }

    onSave(type, en, sv) {
        App.addSaveRequest();
        Rest.postConceptTypes(type, en, sv, () => {
            App.removeSaveRequest();
            App.updateChachedType(type, sv);
            Localization.setTypeNameForLang(type, "label_en", en);
            Localization.setTypeNameForLang(type, "label_sv", sv);
            this.forceUpdate();
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades spara namn för typ");
            App.removeSaveRequest();
        });        
    }
  
    render() {
        var list = [];
        for(var i=0; i<App.types.length; ++i) {            
            list.push(App.types[i]);
        }
        list.sort((a, b) => {            
            if(a < b) return -1;
            if(a > b) return 1;
            return 0;
        });
        var types = list.map((type, index) => {
            return (
                <div key={index}>
                    <Label 
                        css="settings_edit_types_type"
                        text={type}/>
                    <Label 
                        css="settings_edit_types_type"
                        text={Localization.getTypeNameForLang(type, "label_en")} />
                    <Label 
                        css="settings_edit_types_type"
                        text={Localization.getTypeNameForLang(type, "label_sv")} />
                    <Button 
                        css="settings_edit_types_button"
                        text={Localization.get("edit")}
                        onClick={this.onEditClicked.bind(this, type)}/>
                </div>
            );
        });
        return (
            <Group text={Localization.get("types")}>
                <div className="settings_types_head">
                    <Label 
                        css="settings_edit_types_type"
                        text={Localization.get("type")}/>
                    <Label 
                        css="settings_edit_types_type"
                        text={Localization.get("label_en")}/>
                    <Label 
                        css="settings_edit_types_type"
                        text={Localization.get("label_sv")}/>
                </div>
                <List css="settings_types">
                    {types}
                </List>
            </Group>
        );
    }
	
}

export default SettingsTypeLocalization;