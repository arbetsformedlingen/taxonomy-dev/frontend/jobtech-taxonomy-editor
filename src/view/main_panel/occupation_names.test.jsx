import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import OccupationNames from "./occupation_names"
import React from "react"

describe("OccupationNames", () => {
  test("renders", () => {
    render(<OccupationNames />);
    expect(screen.getByText(/Yrkesbenämningar/)).toBeDefined();
    expect(OccupationNames).toBeInstanceOf(Function);
    expect(OccupationNames).toHaveProperty("prototype");
    expect(OccupationNames.prototype).toBeInstanceOf(Object);
    expect(OccupationNames.prototype.constructor).toBe(OccupationNames);
  });
});