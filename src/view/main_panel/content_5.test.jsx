import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Content5 from "./content_5"
import React from "react"

describe("Content5", () => {
  test("renders", () => {
    render(<Content5 />);
    expect(screen.getByText(/Förvald begrepps typ/)).toBeDefined();
    expect(screen.getByText(/Använd kompakt utsende/)).toBeDefined();
    expect(screen.getByText(/region/)).toBeDefined();
    expect(screen.getByText(/sun-education-level-2/)).toBeDefined();
    expect(screen.getAllByRole("img", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("combobox", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("option", { hidden: true })).toBeDefined();
    expect(screen.getAllByRole("checkbox", { hidden: true })).toBeDefined();
    expect(Content5).toBeInstanceOf(Function);
    expect(Content5).toHaveProperty("prototype");
    expect(Content5.prototype).toBeInstanceOf(Object);
    expect(Content5.prototype.constructor).toBe(Content5);
  });
});