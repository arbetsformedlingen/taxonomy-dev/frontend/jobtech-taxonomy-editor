import React from 'react';
import Label from '../../control/label.jsx';
import Constants from '../../context/constants.jsx';
import Localization from '../../context/localization.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import App from '../../context/app.jsx';
import MissingConnections from './missing_connections.jsx';
import SearchResult from './search_result.jsx';
import Referred from './referred.jsx';
import ImportData from './import_data.jsx';

class Content4 extends React.Component { 

    constructor() {
        super();        
        this.ssyks = [];
        this.PAGE_UNKNOWN = "PAGE_UNKNOWN";
        this.PAGE_CONNECTIONS = "PAGE_CONNECTIONS";
        this.PAGE_SEARCH_CHANGES = "PAGE_SEARCH_CHANGES";
        this.PAGE_REFERRED = "PAGE_REFERRED";
        this.PAGE_IMPORT = "PAGE_IMPORT";        
        //Storleken på värdeförråd mot varandra (hämta några olika)
        this.state = {            
            page: this.PAGE_UNKNOWN,
            showMissing: false,
        };        
        this.showMissingContext = {};
        this.selected1 = null;
        this.selected2 = null;
        this.boundOnConnectionsSelected = this.onConnectionsSelected.bind(this);
        this.boundSearchChanges = this.onSearchChanges.bind(this);
        this.boundReferredSelected = this.onReferredSelected.bind(this);
        this.boundImportData = this.onImportData.bind(this);
        this.boundClearView = this.onClearView.bind(this);
    }

    componentDidMount() {
        App.usage.addFlow(Constants.USAGE_CONTENT, Constants.USAGE_ENTER, "4");
        EventDispatcher.add(this.boundOnConnectionsSelected, Constants.EVENT_SIDEPANEL_CONNECTIONS_SELECTED);
        EventDispatcher.add(this.boundSearchChanges, Constants.EVENT_SEARCH_CHANGES);
        EventDispatcher.add(this.boundReferredSelected, Constants.EVENT_SIDEPANEL_REFERRED_SELECTED);
        EventDispatcher.add(this.boundImportData, Constants.EVENT_IMPORT_DATA);
        EventDispatcher.add(this.boundClearView, Constants.EVENT_SIDEPANEL_CLEAR);
    }

    componentWillUnmount() {
        App.usage.addFlow(Constants.USAGE_CONTENT, Constants.USAGE_LEAVE, "4");
        EventDispatcher.remove(this.boundOnConnectionsSelected);
        EventDispatcher.remove(this.boundSearchChanges);
        EventDispatcher.remove(this.boundReferredSelected);
        EventDispatcher.remove(this.boundImportData);
        EventDispatcher.remove(this.boundClearView);
    }

    onClearView() {
        this.setState({
            page: this.PAGE_UNKNOWN,
            data: null
        });
    }

    onShowMissingChanged(e) {        
        this.setState({showMissing: e.target.checked}, () => {
            if(this.showMissingContext.onShowMissingChanged) {
                this.showMissingContext.onShowMissingChanged(this.state.showMissing);
            }
        });
    }

    onConnectionsSelected(data) {
        this.setState({
            page: this.PAGE_CONNECTIONS, 
            data: data
        });
    }
    
    onSearchChanges(search) {
        this.setState({
            page: this.PAGE_SEARCH_CHANGES, 
            data: search
        });        
    }

    onReferredSelected() {
        this.setState({
            page: this.PAGE_REFERRED, 
            data: null
        });
    }

    onImportData(data) {
        this.setState({
            page: this.PAGE_IMPORT, 
            data: data
        });
    }

    render() {
        switch(this.state.page) {
            case this.PAGE_CONNECTIONS:
                return (
                    <div className="main_content_4">
                        <div className="main_content_title_container">
                            <Label
                                css="main_content_title" 
                                text={Localization.get("connections")}/>
                            <div className="main_content_4_title_cb">
                                <input
                                    type="checkbox"
                                    checked={this.state.showMissing}
                                    value=""
                                    onChange={this.onShowMissingChanged.bind(this)}/>
                                <Label
                                    text={Localization.get("show_missing")}/>
                            </div>
                        </div>
                        <MissingConnections 
                            types={this.state.data.types}
                            relationType={this.state.data.relationType}
                            showMissing={this.showMissing}
                            concepts={this.state.data.concepts}
                            edges={this.state.data.edges}
                            showMissingContext={this.showMissingContext}/>
                    </div>
                );
            case this.PAGE_IMPORT: 
                return (
                    <div className="main_content_4">
                        <Label
                            css="main_content_title" 
                            text={Localization.get("import_concepts")}/>
                        <ImportData importData={this.state.data}/>  
                    </div>
                );
            case this.PAGE_REFERRED:
                return (
                    <div className="main_content_4">
                        <Label
                            css="main_content_title" 
                            text={Localization.get("referred")}/>
                        <Referred />
                    </div>
                );
            case this.PAGE_SEARCH_CHANGES:
                return (
                    <div className="main_content_4">
                        <Label
                            css="main_content_title" 
                            text={Localization.get("search")}/>
                        <SearchResult search={this.state.data}/>
                    </div>
                );
            case this.PAGE_UNKNOWN:
                return (
                    <div className="main_content_4">
                    </div>
                );
        }
    }	
}

export default Content4;