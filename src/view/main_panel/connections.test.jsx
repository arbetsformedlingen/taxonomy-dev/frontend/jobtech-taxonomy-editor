import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import Connections from "./connections"
import React from "react"

describe("Connections", () => {
  test("renders", () => {
    render(<Connections />);
    expect(screen.getAllByRole("checkbox", { hidden: true })).toBeDefined();
    expect(screen.getByText(/avaktualiserade/)).toBeDefined();
    expect(screen.getByText(/grupperat på relationstyp/)).toBeDefined();
    expect(screen.getByText(/Gå till/)).toBeDefined();
  });
});