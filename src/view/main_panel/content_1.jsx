import React from 'react';
import ControlUtil from '../../control/util.jsx';
import Label from '../../control/label.jsx';
import Group from '../../control/group.jsx';
import Constants from '../../context/constants.jsx';
import Settings from '../../context/settings.jsx';
import Localization from '../../context/localization.jsx';
import EventDispatcher from '../../context/event_dispatcher.jsx';
import App from '../../context/app.jsx';
import Description from './description.jsx';
import Deprecated from './deprecated.jsx';
import Connections from './connections.jsx';
import ItemHistory from './item_history.jsx';

class Content1 extends React.Component { 

    constructor() {
        super();
        this.state = {
            item: null,
            components: [],
            enableQuickEdit: false,            
        };
        this.boundSideItemSelected = this.onSideItemSelected.bind(this);
        this.boundQuickEditEnable = this.onQuickEditEnable.bind(this);
        this.boundQuickEditSaved = this.onQuickEditSaved.bind(this);
        this.boundCacheUpdated = this.onCacheUpdated.bind(this);
    }

    componentDidMount() {
        App.usage.addFlow(Constants.USAGE_CONTENT, Constants.USAGE_ENTER, "1");
        EventDispatcher.add(this.boundSideItemSelected, Constants.EVENT_SIDEPANEL_ITEM_SELECTED);
        EventDispatcher.add(this.boundSideItemSelected, Constants.EVENT_CONCEPT_EDITED);
        EventDispatcher.add(this.boundQuickEditSaved, Constants.EVENT_QUICK_EDIT_SAVED);
        EventDispatcher.add(this.boundQuickEditEnable, Constants.EVENT_QUICK_EDIT);
        EventDispatcher.add(this.boundCacheUpdated, Constants.EVENT_CACHE_UPDATED);
        this.onSideItemSelected();
    }

    componentWillUnmount() {
        App.usage.addFlow(Constants.USAGE_CONTENT, Constants.USAGE_LEAVE, "1");
        EventDispatcher.remove(this.boundSideItemSelected);
        EventDispatcher.remove(this.boundQuickEditEnable);
        EventDispatcher.remove(this.boundQuickEditSaved);
        EventDispatcher.remove(this.boundCacheUpdated);
    }

    onCacheUpdated(ids) {
        if(this.state.item && ids.indexOf(this.state.item.id) != -1) {
            this.forceUpdate();
        }
    }

    onQuickEditEnable(enabled) {
        this.onSideItemSelected(this.state.item, enabled);
    }

    onQuickEditSaved() {
        this.onSideItemSelected(this.state.item, this.state.enableQuickEdit);
    }

    onSideItemSelected(item, enableQuickEdit) {
        var components = [];
        var key = 0;
        this.state.enableQuickEdit = enableQuickEdit ? true : false;
        if(item) {            
            var deprecated = item.deprecated != null ? item.deprecated : false;
            var css = item.type == Constants.CONCEPT_ISCO_LEVEL_4 ? "isco_color" : "";
            var noEscoRelation = item.type.indexOf("esco") >= 0 ? item.no_esco_relation : false;
            var lastChange = new Date(item.last_changed);

            if(deprecated) {
                components.push(
                    <Group 
                        text={Localization.get("referred_to")}
                        css={css}
                        key={key++}>
                        <Deprecated 
                            item={item}
                            groupContext={infoContext}/>
                    </Group>
                );
            }

            var preferences = Settings.getPreferences();
            // add content for item
            var infoContext = ControlUtil.createGroupContext();
            var connectionsContext = ControlUtil.createGroupContext();
            components.push(
                <Group 
                    text={
                        <div className="description_group_header">
                            <div>{Localization.get("info")}</div>
                            <div>{"Uppdaterad: " + lastChange.toLocaleString()}</div>
                        </div>
                    }
                    context={infoContext}
                    css={css + " description_group"}
                    key={key++}
                    expanded={preferences.groupVisability.info}
                    onChanged={(isExpanded) => {
                        preferences.groupVisability.info = isExpanded;
                        Settings.savePreferences();
                    }}>
                    <Description 
                        item={item}
                        groupContext={infoContext}
                        enableQuickEdit={this.state.enableQuickEdit}/>
                </Group>
            );
            components.push(                
                <Group 
                    text={Localization.get("connections") + (noEscoRelation ? " - " + Localization.get("no_esco_relation") : "")}
                    context={connectionsContext}
                    css={css + " connections_group"}
                    key={key++}
                    expanded={preferences.groupVisability.relations}
                    onChanged={(isExpanded) => {
                        preferences.groupVisability.relations = isExpanded;
                        Settings.savePreferences();
                    }}>
                    <Connections 
                        item={item}
                        groupContext={connectionsContext}
                        enableQuickEdit={this.state.enableQuickEdit}/>
                </Group>
            );
            components.push(
                <Group 
                    text={Localization.get("history")}
                    css={css}
                    key={key++}
                    expanded={preferences.groupVisability.history && !this.state.enableQuickEdit}
                    onChanged={(isExpanded) => {
                        preferences.groupVisability.history = isExpanded;
                        Settings.savePreferences();
                    }}>
                    <ItemHistory item={item}/>
                </Group>
            );
        }
        this.setState({
            isShowingSavePanel: false,
            item: item,
            components: components,
        });
    }
    
    renderTitle() {
        var item = this.state.item;
        if(item == null || !item.type) {
            return (
                <Label 
                    css="main_content_title" 
                    text={Localization.get("value_storage")}/>
            );
        } else {
            var key = 0;
            var isDeprecated = item.deprecated ? item.deprecated : false;
            var components = [];
            components.push(
                <Label 
                    key={key++}
                    text={item.preferredLabel}/>
            );
            if(isDeprecated) {
                components.push(
                    <Label 
                        key={key++}
                        css="main_content_title_deprecated" 
                        text={Localization.get("deprecated")}/>
                );
            }
            return (
                <div className="main_content_title_container">
                    <Label 
                        css="main_content_title" 
                        text={Localization.getTypeName(item.type)}/>
                    <div className="main_content_title_name">
                        {components}
                    </div>
                </div>
            );
        }
    }

    render() {
        return (
            <div className="main_content_1">
                {this.renderTitle()}             
                {this.state.components}
            </div>
        );
    }
	
}

export default Content1;