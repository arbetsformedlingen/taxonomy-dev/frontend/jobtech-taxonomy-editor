import {expect, test} from "vitest"
import { render, screen } from "@testing-library/react"
import VersionList from "./version_list"
import React from "react"

describe("VersionList", () => {
  test("renders", () => {
    render(<VersionList />);
    expect(screen.getByText(/Visa ändringar till:/)).toBeDefined();
    expect(screen.getByText(/Filter/)).toBeDefined();
    expect(screen.getByText(/Relationstyp/)).toBeDefined();
    expect(screen.getByText(/Laddar.../)).toBeDefined();
    expect(screen.getAllByRole("textbox", { hidden: true })).toBeDefined();
    expect(VersionList).toBeInstanceOf(Function);
    expect(VersionList).toHaveProperty("prototype");
    expect(VersionList.prototype).toBeInstanceOf(Object);
    expect(VersionList.prototype.constructor).toBe(VersionList);
  });
});