import React from 'react';
import QuickEditComment from './quick_edit_comment.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Button from './../../control/button.jsx';

class QuickEditItem extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            isSelected: false,
        };
        this.boundSelectAll = this.onSelectAll.bind(this);
        this.boundDeselectAll = this.onDeselectAll.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.boundSelectAll, Constants.EVENT_QUICK_EDIT_SELECT_ALL);
        EventDispatcher.add(this.boundDeselectAll, Constants.EVENT_QUICK_EDIT_DESELECT_ALL);
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.boundSelectAll);
        EventDispatcher.remove(this.boundDeselectAll);
    }

    UNSAFE_componentWillReceiveProps(props) {
        this.setState({item: props.item});
    }

    onSelectAll() {
        this.props.onSelected();
        this.setState({isSelected: true});
    }

    onDeselectAll() {
        this.props.onDeselected();
        this.setState({isSelected: false});
    }

    onSelectedChanged(e) {
        if(e.target.checked) {
            this.props.onSelected();
        } else {
            this.props.onDeselected();
        }
        this.setState({isSelected: e.target.checked});
    }

    onCommentedAdded(isSaved) {
        if(isSaved) {
            this.forceUpdate();
        }
    }

    onCommentClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: "Kommentar",
            content: <QuickEditComment 
                            item={this.state.item}
                            onClosed={this.onCommentedAdded.bind(this)}/>
        });
    }

    getAsString(list) {
        var s = "";
        if(list.length > 0) {
            s = list[0];
            for(var i=1; i<list.length; ++i) {
                s += ", " + list[i];
            }
        }
        return s;
    }

    renderName(item) {
        return (
            <div className="property_field">
                <div>Concept: </div>
                <div>{Localization.getTypeName(item.concept.type)}</div>
                <div> - </div>
                <div>{item.concept.name}</div>
            </div>
        );
    }

    renderEditType(item) {
        var name = "unknown";
        if(item.type == Constants.QUICK_EDIT_NAME) {
            name = "Namn";
        } else if(item.type == Constants.QUICK_EDIT_DEFINITION) {
            name = "Definition";
        } else if(item.type == Constants.QUICK_EDIT_ALTERNATIVE_LABEL) {
            name = Localization.get("alternative_names");
        } else if(item.type == Constants.QUICK_EDIT_HIDDEN_LABEL) {
            name = Localization.get("hidden_names");
        } else if(item.type == Constants.QUICK_EDIT_QUALITY_LEVEL) {
            name = Localization.get("quality_control");
        } else if(item.type == Constants.QUICK_EDIT_ADDED_RELATION) {
            name = Localization.get("relation_created");
        } else if(item.type == Constants.QUICK_EDIT_REMOVED_RELATION) {
            name = Localization.get("relation_removed");
        } else if(item.type == Constants.QUICK_EDIT_NO_ESCO_RELATION) {
            name = "Ingen ESCO-relation";
        } 
        return (
            <div className="property_field">
                <div>Redigerat värde: </div>
                <div>{name}</div>
            </div>
        );
    }

    renderValue(item) {
        var value = item.value;
        if(item.type == Constants.QUICK_EDIT_ALTERNATIVE_LABEL || item.type == Constants.QUICK_EDIT_HIDDEN_LABEL) {
            value = this.getAsString(item.value);
        }
        if(item.type == Constants.QUICK_EDIT_ADDED_RELATION || item.type == Constants.QUICK_EDIT_REMOVED_RELATION) {
            return (
                <div>
                    <div className="property_field">
                        <div>Till: </div>
                        <div>{Localization.getTypeName(value.type)}</div>
                        <div> - </div>
                        <div>{value.preferredLabel}</div>
                    </div>
                    <div className="property_field">
                        <div>Relations typ: </div>
                        <div>{value.relationType}</div>
                    </div>
                    {value.relationType == "substitutability" &&
                        <div className="property_field">
                            <div>Släktskap: </div>
                            <div>{value.substitutability == "25" ? "25 - Lägre släktskap" : "75 - Högt släktskap"}</div>
                        </div>
                    }
                </div>
            );
        } else {
            return (
                <div className="property_field">
                    <div>Nytt värde: </div>
                    <div>{"" + value}</div>
                </div>
            );
        }
    }

    render() {
        var isEnabled = this.props.isEnabled == null ? true : this.props.isEnabled;
        return (
            <div className={"quick_edit_container quick_edit_item" + (isEnabled ? "" : " quick_edit_disabled")}>
                <div className="quick_edit_item_header">
                    <div>{new Date(this.state.item.date).toLocaleTimeString()}</div>
                    <div className="header_row">
                        {this.state.item.comment != null &&
                            <div title={this.state.item.comment}>
                                <img src={Constants.ICON_MESSAGE}/>
                            </div>
                        }
                        <div/>
                        <input 
                            className="selection_checkbox"
                            type="checkbox"
                            checked={this.state.isSelected}
                            onChange={this.onSelectedChanged.bind(this)}/>
                    </div>
                </div>
                <div>
                    {this.renderName(this.state.item)}
                    {this.renderEditType(this.state.item)}
                    {this.renderValue(this.state.item)}
                </div>
                <div className="action_buttons">
                    <div onPointerUp={this.onCommentClicked.bind(this)}>
                        <img src={Constants.ICON_MESSAGE}/>
                    </div>
                    <div onPointerUp={this.props.onRemoved}>
                        <img src={Constants.ICON_REMOVE}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default QuickEditItem;