import React from 'react';
import Label from './../../control/label.jsx';
import App from './../../context/app.jsx';
import Localization from './../../context/localization.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import Rest from './../../context/rest.jsx';
import Util from './../../context/util.jsx';
import ConceptEdit from './../../context/concept_edit.jsx';

class EditConceptNoEscoRelation extends React.Component { 

    constructor(props) {
        super(props);
        var value = false;
        if(this.props.item != null && this.props.item.no_esco_relation != null) {
             value = this.props.item.no_esco_relation;
        }
        this.state = {
            value: value,
            isChanged: false,
		};
        this.props.editContext.onSave = this.onSave.bind(this);
    }

    onSave(message, callback) {
        App.addSaveRequest();
        ConceptEdit.noEscoRelation(this.props.item.id, this.state.value, message, () => {
            this.props.item.no_esco_relation = this.state.value;
            callback();
        }, () => {
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    onValueChanged(e) {
        this.props.editContext.no_esco_relation = e.target.checked;
        this.props.editContext.setEnableSave(true);
        this.setState({
			value: e.target.checked,
			isChanged: true,
		});
    }

    render() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Ingen relation"/>
                <div className="edit_concept_row">
                    <input 
                        type="checkbox"
                        className="rounded"
                        checked={this.state.value}
                        onChange={this.onValueChanged.bind(this)}/>
                </div>
            </div>
        );
    }
}

export default EditConceptNoEscoRelation;