import React from 'react';
import Label from './../../control/label.jsx';
import App from './../../context/app.jsx';
import Localization from './../../context/localization.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import Rest from './../../context/rest.jsx';
import Util from './../../context/util.jsx';
import ConceptEdit from './../../context/concept_edit.jsx';

class EditConceptQuality extends React.Component { 

    constructor(props) {
        super(props);
        var value = "undefined";
        if(this.props.item != null && this.props.item.quality_level != null) {
             value = this.props.item.quality_level;
        }
        this.state = {
            value: value,
            isChanged: false,
		};
        this.props.editContext.onSave = this.onSave.bind(this);
    }

    onSave(message, callback) {
        App.addSaveRequest();
        ConceptEdit.qualityLevel(this.props.item.id, this.state.value, message, () => {
            this.props.item.quality_level = this.state.value;
            callback();
        }, () => {
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    onValueChanged(e) {
        this.props.editContext.quality = e.target.value;
        this.props.editContext.setEnableSave(e.target.value != "undefined");
        this.setState({
			value: e.target.value,
			isChanged: true,
		});
    }

    render() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text={Localization.get("quality_classification")}/>
                <select 
                    className="rounded"
                    value={this.state.value}
                    onChange={this.onValueChanged.bind(this)}>
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option value="undefined">{Localization.get("undefined")}</option>
				</select>
            </div>
        );
    }
}

export default EditConceptQuality;