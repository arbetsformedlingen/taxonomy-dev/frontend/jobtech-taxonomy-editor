import React from 'react';
import Label from './../../control/label.jsx';
import List from './../../control/list.jsx';
import App from './../../context/app.jsx';
import Socket from './../../context/socket.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Util from './../../context/util.jsx';
import ControlUtil from './../../control/util.jsx';
import TreeView from './../../control/tree_view.jsx';
import EditConceptRemoveRelation from './edit_concept_remove_relation.jsx';
import { isThisHour } from 'date-fns/esm';

class EditConceptDeprecate extends React.Component { 

    constructor(props) {
		super(props);
        this.state = {
			defaultValue: this.props.item.deprecated != null ? this.props.item.deprecated : false,
            value: this.props.item.deprecated != null ? this.props.item.deprecated : false,
            isChanged: false,
            shouldReference: false,
            shouldRemoveRelations: false,
            removeRelationsEnableSave: false,
            items: [],
            selected: null,
            filter: "",
        };
        this.props.editContext.onSave = this.onSave.bind(this);
        this.editContextRemoveRelation = {
            setEnableSave: this.onRemoveRelationEnableSave.bind(this),
        };
    }

    setupTreeView(treeView, items) {
        treeView.shouldUpdateState = false;
        treeView.clear();
        for(var i=0; i<items.length; ++i) {
            var item = items[i];
            if(item.visible) {
                var rootNode = ControlUtil.createTreeViewItem(treeView, item);
                rootNode.setText(item.label);
                for(var j=0; j<item.children.length; ++j) {
                    var child = item.children[j];
                    if(child.visible) {
                        var childNode = ControlUtil.createTreeViewItem(treeView, child);
                        childNode.setText(child.label);
                        rootNode.addChild(childNode);
                    }
                }
                treeView.addRoot(rootNode);
            }
        }
        treeView.shouldUpdateState = true;
    }

    setupConcepts(type) {
        // TODO: check for all hierarchical types
        if(type == "skill") {
            type = "skill-headline";
        }
        var cache = App.getCachedTypes(type);
        var items = [];
        for(var i=0; i<cache.length; ++i) {
            var item = cache[i];
            var element = {
                visible: true,
                type: item.type,
                id: item.id,
                label: item.label,
                preferredLabel: item.preferredLabel,
                deprecated: item.deprecated,
                children: [],
            };
            if(item.skills != null) {
                for(var j=0; j<item.skills.length; ++j) {
                    var skill = item.skills[j];
                    element.children.push({
                        visible: true,
                        type: skill.type,
                        id: skill.id,
                        label: skill.label,
                        preferredLabel: skill.preferredLabel,
                        deprecated: skill.deprecated,
                        children: [],
                    });
                }
                Util.sortByKey(element.children, "label", true);
            }
            items.push(element);
        }
        Util.sortByKey(items, "label", true);
        var treeView = ControlUtil.createTreeView();
        treeView.onItemSelected = this.onReferenceSelected.bind(this);
        this.setupTreeView(treeView, items);
        this.setState({
            items: items,
            treeView: treeView,
        });
    }

    updateEnableSave() {
        var enable = this.state.isChanged;        
        if(enable && this.state.shouldReference) {
            enable = this.state.selected != null;
        }
        if(enable && this.state.shouldRemoveRelations) {
            enable = this.state.removeRelationsEnableSave;
        }
        this.props.editContext.setEnableSave(enable);
    }

    onRemoveRelationEnableSave(enable) {       
        this.setState({removeRelationsEnableSave: enable}, () => {
            this.updateEnableSave();
        });
    }

    onSaveReplacedConcept(message, callback) {
        var replace = () => {
            App.addSaveRequest();
            Rest.postReplaceConcept(this.props.item.id, this.state.selected.id, message, () => {
                this.props.item["replaced_by"] = [this.state.selected];
                if(App.removeSaveRequest()) {
                    callback();
                }
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades hänvisa begrepp");
                App.removeSaveRequest();
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
        };
        var remove = (references, index) => {
            App.addSaveRequest();
            Rest.postUnreplaceConcept(this.props.item.id, references[index].id, message, () => {
                App.removeSaveRequest();
                if(index < references.length - 1) {
                    remove(references, index + 1);
                } else {
                    replace();
                }
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades hänvisa begrepp");
                App.removeSaveRequest();
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
        };
        var references = this.props.item["replaced_by"];
        if(references && references.length > 0) {
            remove(references, 0);
        } else {
            replace();
        }
    }

    onSave(message, callback) {
        App.addSaveRequest();
        Rest.deleteConcept(this.props.item.id, message, () => {
            this.props.item.deprecated = true;
            if(this.state.shouldReference) {
                this.onSaveReplacedConcept(message, callback);   
            }
            if(this.state.shouldRemoveRelations) {
                this.editContextRemoveRelation.onSave(message, callback);
            }
            App.updateCachedItem(this.props.item);
            Socket.sendDeprecatedConcept(this.props.item);
            if(App.removeSaveRequest()) {
                callback();
            }
        }, () => {
            App.showError(Util.getHttpMessage(status) + " : Avaktualisering misslyckades");
            App.removeSaveRequest();
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    onValueChanged(e) {
		var isChanged = e.target.checked != this.state.defaultValue;
        this.setState({
            value: e.target.checked,
            isChanged: isChanged,
        }, () => {
            this.updateEnableSave();
        });
    }

    onShouldReferenceChanged(e) {
        if(this.state.items.length == 0) {
            this.setupConcepts(this.props.item.type);
        }        
        this.setState({shouldReference: e.target.checked}, () => {
            this.updateEnableSave();
        });
    }

    onReferenceSelected(item) {
        this.setState({selected: item.data}, () => {
            this.updateEnableSave();
        });        
    }

    onShouldRemoveRelationsChanged(e) {
        this.setState({shouldRemoveRelations: e.target.checked}, () => {
            this.updateEnableSave();
        });
    }

    onFilterChanged(e) {
        var query = e.target.value.trim().toLowerCase();
        var filter = (items) => {
            var found = false;
            for(var i=0; i<items.length; ++i) {
                var item = items[i];
                item.visible = filter(item.children) || item.label.toLowerCase().indexOf(query) > -1;
                if(item.visible) {
                    found = true;
                }
            }
            return found;
        };
        filter(this.state.items);
        this.setupTreeView(this.state.treeView, this.state.items);
        this.setState({filter: e.target.value});
    }

    renderDeprecate() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Avaktualisera begrepp"/>
                <div className="edit_concept_row">
                    <input
                        id="deprecate"
                        type="checkbox"
                        disabled={this.state.defaultValue ? "disabled" : ""}
                        value={this.state.value}
                        onChange={this.onValueChanged.bind(this)}/>
                    <label htmlFor="deprecate">{Localization.get("yes")}</label>
                </div>
            </div>
        );
    }

    renderReferenceCheckbox() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Ska begreppet hänvisas"/>
                <div className="edit_concept_row">
                    <input
                        id="shouldReference"
                        type="checkbox"
                        value={this.state.shouldReference}
                        onChange={this.onShouldReferenceChanged.bind(this)}/>
                    <label htmlFor="shouldReference">{Localization.get("yes")}</label>
                </div>
            </div>
        );
    }

    renderReferenceItems() {
        if(this.state.shouldReference && this.state.treeView) {
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Välj det begrepp som hänvisningen går till"/>
                    <div className="edit_concept_row">
                        <input 
                            type="text"
                            className="rounded"
                            value={this.state.filter}
                            placeholder={Localization.get("filter")}
                            onChange={this.onFilterChanged.bind(this)}/>
                    </div>
                    <TreeView 
                        css="deprecated_reference_list"
                        context={this.state.treeView}/>
                </div>
            );
        }
    }

    renderRemoveRelationsCheckbox() {
        var item = this.props.item;
        if(item.broader.length > 0 || item.narrower.length > 0 || item.related.length > 0) {
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Ska relationer tas bort"/>
                    <div className="edit_concept_row">
                        <input
                            id="shouldRemoveRelations"
                            type="checkbox"
                            value={this.state.shouldRemoveRelations}                        
                            onChange={this.onShouldRemoveRelationsChanged.bind(this)}/>
                        <label htmlFor="shouldRemoveRelations">{Localization.get("yes")}</label>
                    </div>
                </div>
            );
        }
    }

    renderRemoveRelations() {
        if(this.state.shouldRemoveRelations) {
            return (
                <EditConceptRemoveRelation item={this.props.item} editContext={this.editContextRemoveRelation}/>
            );
        }
    }

	render() {
        return (
            <div className="edit_concept_group_collection">
                {this.renderDeprecate()}
                {this.renderReferenceCheckbox()}
                {this.renderReferenceItems()}
                {this.renderRemoveRelationsCheckbox()}
                {this.renderRemoveRelations()}
            </div>
        );
    }
}

export default EditConceptDeprecate;