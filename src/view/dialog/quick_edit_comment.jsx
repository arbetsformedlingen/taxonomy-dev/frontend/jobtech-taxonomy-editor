import React from 'react';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Button from './../../control/button.jsx';

class QuickEditComment extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            value: props.item.comment == null ? "" : props.item.comment
        };
    }

    onCommentChanged(e) {
        var value = e.target.value;
        this.setState({value: value});
    }

    onCloseClicked() {
        this.props.onClosed(false);
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onSaveClicked() {
        this.props.item.comment = this.state.value.trim().length == 0 ? null : this.state.value;
        this.props.onClosed(true);
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    render() {
        return (
            <div className="dialog_content">
                {this.props.isGroup &&
                    <div>
                        {this.props.groupText}
                    </div>
                }
                <textarea
                    value={this.state.value}
                    onChange={this.onCommentChanged.bind(this)}/>
                <div className="dialog_content_buttons">
                    <Button
                        text={Localization.get("close")}
                        onClick={this.onCloseClicked.bind(this)}/>
                    <Button
                        text={Localization.get("save")}
                        onClick={this.onSaveClicked.bind(this)}/>
                </div>
            </div>
        );
    }
}

export default QuickEditComment;