import React from 'react';
import Label from './../../control/label.jsx';
import List from './../../control/list.jsx';
import Util from './../../context/util.jsx';
import App from './../../context/app.jsx';
import Socket from './../../context/socket.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Settings from '../../context/settings.jsx';
import Rest from './../../context/rest.jsx';
import ConceptEdit from './../../context/concept_edit.jsx';
import EditConceptQuality from './edit_concept_quality.jsx';
import EditConceptAddRelation from './edit_concept_add_relation.jsx';

class EditConceptNewValue extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            type: "--",
            relationType: "broader",
            substitutability: "",
            parentType: null,
            parent: null,
            parentConcepts: [],
            name: this.props.initialName ? this.props.initialName : "",
            definition: "",
            isNameInvalid: false,
            newTypeName: "",
            label_en: "",
            label_sv: "",
            isNewType: false,
		};
        this.props.editContext.onSave = this.onSave.bind(this);
        this.relationSaveStatus = true;
        this.qualityContext = {
            setEnableSave: (enabled) => {},
            onSave: null,
            quality: "",
        };
        this.relationContext = {
            setEnableSave: this.onRelationSaveStatusChanged.bind(this),
            onSave: null,
            quality: "",
        };
        this.relationMockItem = {
            id: "0",
            type: null,
        };
    }

    onSaveComplete(callback) {
        if(this.state.isNewType) {
            App.types.push(this.state.newTypeName);
            Settings.data.editableTypes.push(this.state.newTypeName);
            Settings.data.visibleTypes.push(this.state.newTypeName);
            Settings.save();
        }
        EventDispatcher.fire(Constants.EVENT_NEW_CONCEPT, {
            concept: this.concept,
            parent: this.state.parent,
        });
        callback();
    }

    onSaveConceptRelations(message, callback) {
        this.relationContext.onSave(message, () => {
            this.onSaveComplete(callback);
        });
    }

    onSaveParentRelation(callback, from, to, type, substitutability, message) {
        if(type != "substitutability" || (substitutability && substitutability.trim().length == 0)) {
            substitutability = null;
        }
        ConceptEdit.addRelation(from, to, type, substitutability, message, (data) => {
            if(!App.hasPendingSaveRequests()) {
                this.onSaveComplete(callback);
            }
        }, (status) => {
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    onSaveNewTypeName(callback) {
        App.addSaveRequest();
        Rest.postConceptTypes(this.state.type, this.state.label_en, this.state.label_sv, () => {
            if(App.removeSaveRequest()) {
                this.onSaveComplete(callback);
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades spara namn på ny benämning");
            App.removeSaveRequest();
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    onSave(message, callback) {
        // TODO: handle quality
        // this.qualityContext.quality

        var state = this.state;
        App.addSaveRequest();
        Rest.postConcept(state.isNewType ? state.newTypeName : state.type, message, state.name.trim(), encodeURIComponent(state.definition), this.qualityContext.quality, (data) => {
            this.concept = data.concept;
            this.concept.id = this.concept["concept/id"];
            this.concept.definition = this.concept["concept/definition"];
            this.concept.type = this.concept["concept/type"];
            this.concept.preferredLabel = this.concept["concept/preferredLabel"];
            this.concept.label = this.concept.preferredLabel;
            Socket.sendNewConcept(this.concept);
            if(state.parent) {
                // add relation between new concept and its selected parent
                this.onSaveParentRelation(callback, state.parent, data.concept, "narrower", null, message);
                if(state.parentType == "skill-headline") {
                    // update cache
                    var cachedParent = App.getCachedConceptFromId(state.parent.id);
                    cachedParent.skills.push(this.concept);
                }
            }
            if(this.props.item) {
                // add relation between new concept and source concept
                this.onSaveParentRelation(callback, this.props.item, data.concept, state.relationType, state.substitutability, message);
            }
            if(this.state.isNewType) {
                App.updateChachedType(this.state.type, this.state.label_sv);
                this.onSaveNewTypeName(callback);
            }
            this.relationMockItem.id = this.concept.id;
            this.onSaveConceptRelations(message, callback);
            App.updateCachedItem(this.concept);
            if(App.removeSaveRequest()) {
                this.onSaveComplete(callback);
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades skapa nytt begrepp");
            App.removeSaveRequest();
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    onTypeSelected(e) {
        var parentType = this.getParentType(e.target.value);
        var parentConcepts = parentType == this.state.parentType ? this.state.parentConcepts : [];
        var relationType = this.props.item == null ? null : Constants.getRelationType(this.props.item.type, e.target.value);
        this.relationMockItem.type = e.target.value;
        this.setState({
            name: this.props.initialName ? this.props.initialName : "",
            isNameInvalid: false,
            definition: "",
            type: e.target.value,
            parent: null,
            parentType: parentType,
            parentConcepts: parentConcepts,
            relationType: relationType == null ? this.state.relationType : relationType,
            newTypeName: "",
            isNewType: e.target.value == "create_new_type_",
        }, () => {
            this.updateSaveStatus();
        });
        if(parentType != null && parentConcepts.length == 0) {
            Rest.getConcepts(parentType, (data) => {
                if(parentType === "skill-headline") {
                    data = Util.sortByKey(data, "preferredLabel", true);
                }
                this.setState({parentConcepts: data});
            }, (status) => {

            });
        }
    }

    onNewTypeNameChanged(e) {
        this.setState({newTypeName: e.target.value.trim()}, () => {
            this.updateSaveStatus();
        });
    }

    onLabelEnChanged(e) {
        this.setState({label_en: e.target.value.trim()}, () => {
            this.updateSaveStatus();
        });
    }

    onLabelSvChanged(e) {
        this.setState({label_sv: e.target.value.trim()}, () => {
            this.updateSaveStatus();
        });
    }

    onRelationTypeSelected(e) {
        this.setState({relationType: e.target.value}, () => {
            this.updateSaveStatus();
        });
    }

    onSubstituabilityChanged(e) {
        this.setState({substitutability: e.target.value}, () => {
            this.updateSaveStatus();
        });
    }

    onParentSelected(parent) {
        this.setState({parent: parent}, () => {
            this.updateSaveStatus();
        });
    }

    onNameChanged(e) {
        var name = e.target.value.trim();
        this.setState({name: e.target.value});
        if(name.length > 0) {
            Rest.abort();
            Rest.getConceptByName(name, (data) => {
                if(data.length > 0) {
                    var errorString = "";
                    for(var i=0; i<data.length; ++i) {
                        errorString += data[i].id;
                        errorString += " (" + Localization.getTypeName(data[i].type) + ")";
                        if(data[i].deprecated) {
                            errorString += " - " + Localization.get("DEPRECATED");
                        }
                        errorString += "\n";
                    }
                    this.setState({
                        isNameInvalid: true,
                        errorString: errorString,
                    }, () => {
                        this.updateSaveStatus();
                    });
                } else {
                    this.setState({
                        isNameInvalid: false,
                    }, () => {
                        this.updateSaveStatus();
                    });
                }                
            }, () => {
    
            }); 
        } else {
            this.updateSaveStatus();
        }
    }
    
    onDefinitionChanged(e) {
        this.setState({definition: e.target.value}, () => {
            this.updateSaveStatus();
        });
    }

    onRelationSaveStatusChanged(enabled) {
        this.relationSaveStatus = enabled;
        this.updateSaveStatus();
    }

    updateSaveStatus() {
        var enabled = true;
        var state = this.state;
        if(state.type == "--") {
            enabled = false;
        }
        if(state.isNewType && (state.newTypeName.length == 0 || state.label_en.length == 0 || state.label_sv.length == 0)) {
            enabled = false;
        }
        if(state.parentType != null && state.parent == null) {
            enabled = false;
        }
        var name = state.name.trim();
        var definition = state.definition.trim();
        if(name.length == 0 || definition.length == 0) {
            enabled = false;
        }
        if(enabled) {
            enabled = this.relationSaveStatus;
        }
        this.props.editContext.setEnableSave(enabled);
    }

    getRootParent(type) {
        var parent = this.getParentType(type);
        while(parent) {
            var next = this.getParentType(parent);
            if(next) {
                parent = next;
            } else {
                return parent;
            }
        }
        return type;
    }

    getParentType(type) {
        if(type == "skill") {
            return "skill-headline";
        }
        return null;
    }

    renderParentSelector() {
        if(this.state.parentType) {
            var parentText = Localization.getTypeName(this.state.parentType).toLowerCase();
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text={"Välj " + parentText}/>
                    <List 
                        css="edit_concept_list"
                        onItemSelected={this.onParentSelected.bind(this)}
                        data={this.state.parentConcepts}
                        dataRender={(item) => {
                            return ( 
                                <div>
                                    {item.preferredLabel}
                                </div> 
                            );
                        }}/>
                </div>
            );
        }
    }

    renderType() {
        var types = App.types.filter((type) => Settings.isEditable(type));        
        types.sort((a, b) => {
            a = Localization.getTypeName(a);
            b = Localization.getTypeName(b);
            if(a < b) {
                return -1;
            }
            if(a > b) {
                return 1;
            }
            return 0;
        });
        types.push("create_new_type_");
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Välj typ av begrepp"/>
                <select
                    className="rounded"
                    value={this.state.type}
                    onChange={this.onTypeSelected.bind(this)}>
                    <option>--</option>
                    {types.map((value, index) => {
                        return ( <option key={index} value={value}>{Localization.getTypeName(value)}</option> );
                    })}
                </select>
            </div>
        );
    }

    renderNewType() {
        if(this.state.isNewType) {
            var label = <span>Nyckel för nytt värdeförråd, <b>t.ex ssyk-level-3, occupation-name, skill-headline</b></span>;
            return (
                <div>
                <div className="dialog_value_group">
                    <Label
                        css="dialog_value_title"
                        text={label}/>
                    <input
                        className="rounded"
                        value={this.state.newTypeName == null ? "" : this.state.newTypeName}
                        onChange={this.onNewTypeNameChanged.bind(this)}/>
                </div>
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text={Localization.get("label_en")}/>
                    <input
                        className="rounded"
                        value={this.state.label_en == null ? "" : this.state.label_en}
                        onChange={this.onLabelEnChanged.bind(this)}/>
                </div>
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text={Localization.get("label_sv")}/>
                    <input
                        className="rounded"
                        value={this.state.label_sv == null ? "" : this.state.label_sv}
                        onChange={this.onLabelSvChanged.bind(this)}/>
                    <div className="edit_concept_value_new_type_desc">                        
                        <span>För att kunna få åtkomst till det nya värdeförrådet måste ett nytt begrepp skapas.<br/>Ange det nya begreppets parametrar nedan.</span>
                    </div>
                </div>
                </div>
            );
        }
    }

    renderErrorText() {
        if(this.state.isNameInvalid) {
            return (
                <div className="edit_concept_error_text font">
                    <div>Finns redan begrepp som matchar det valda namnet:</div>
                    <div>{this.state.errorString}</div>
                </div>
            );
        }
    }

    renderNameField() {
        if(this.state.type != "--") {
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Namn"/>
                    <input
                        className="rounded"
                        value={this.state.name}
                        onChange={this.onNameChanged.bind(this)}/>
                    {this.renderErrorText()}
                </div>
            );
        }
    }

    renderDefinitionField() {
        if(this.state.type != "--") {
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Definition"/>
                    <textarea
                        rows="10" 
                        className="rounded"
                        value={this.state.definition}
                        onChange={this.onDefinitionChanged.bind(this)}/>
                </div>
            );
        }
    }

    renderRelationTypeList() {
        if(this.props.item) {
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Ange relationstyp till begreppet"/>
                    <select 
                        className="rounded"
                        value={this.state.relationType}
                        onChange={this.onRelationTypeSelected.bind(this)}>
                        <option value="broader">Broader</option>
                        <option value="related">Related</option>
                        <option value="narrower">Narrower</option>
                        <option value="substitutability">Substitutability</option>
                        <option value="broad-match">Broad match</option>
                        <option value="narrow-match">Narrow match</option>
                        <option value="close-match">Close match</option>
                        <option value="exact-match">Exact match</option>
                        <option value="possible-combinations">Possible combinations</option>
                        <option value="unlikely-combinations">Unlikely combinations</option>
                    </select>
                </div>
            );
        }
    }
    
    renderSubsitutability() {
        if(this.state.relationType == "substitutability") {
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Ange utbyttbarhets sannolikhet"/>
                    <input 
                        className="rounded"
                        value={this.state.substitutability}
                        onChange={this.onSubstituabilityChanged.bind(this)}/>
                </div>
            );
        }
    }

    render() {
        return (
            <div className="dialog_value_group">
                {this.renderType()}
                {this.renderNewType()}
                {this.renderRelationTypeList()}
                {this.renderSubsitutability()}
                {this.renderParentSelector()}
                {this.renderNameField()}
                <EditConceptQuality editContext={this.qualityContext}/>
                <EditConceptAddRelation 
                    editContext={this.relationContext}
                    saveDefaultTrue={true}
                    item={this.relationMockItem}
                    targetRelation={this.props.targetRelation}/>
                {this.renderDefinitionField()}
            </div>
        );
    }
}

export default EditConceptNewValue;