import React from 'react';
import Button from './../../control/button.jsx';
import Label from './../../control/label.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';

class ImportMarkItemAs extends React.Component { 

    constructor(props) {
        super(props);        
        this.state = {           
            comment: this.props.item.comment == null ? "" : this.props.item.comment,
        }
    }

    componentDidMount() {
        
    }

    onMarkAsInvestigateClicked(e) {
        if(e.target.checked) {
            this.props.item.trash = false;
            this.props.item.state = "investigate"
        } else {
            this.props.item.trash = false;
            this.props.item.state = "";
        }
        if(this.props.onItemUpdated) {
            this.props.onItemUpdated();
        }
        this.forceUpdate();
    }

    onMarkAsTrashClicked(e) {
        if(e.target.checked) {
            this.props.item.trash = true;
            this.props.item.state = "trash"
        } else {
            this.props.item.trash = false;
            this.props.item.state = "";
        }
        if(this.props.onItemUpdated) {
            this.props.onItemUpdated();
        }
        this.forceUpdate();
    }

    onCommentChanged(e) {
        this.setState({comment: e.target.value});
        this.props.item.comment = e.target.value;
    }

    render() {
        return (
            <div className="dialog_content">
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text={Localization.get("mark_as")}/>
                    <div className="import_work_with_trash">
                        <Label text={Localization.get("investigate")}/>
                        <input
                            type="checkbox"
                            checked={this.props.item.state == "investigate"}
                            onChange={this.onMarkAsInvestigateClicked.bind(this)}/>
                    </div>
                    <div className="import_work_with_trash">
                        <Label text={Localization.get("trash")}/>
                        <input
                            type="checkbox"
                            checked={this.props.item.state == "trash"}
                            onChange={this.onMarkAsTrashClicked.bind(this)}/>
                    </div>
                </div>
                <div className="dialog_value_group">
                    <Label
                        css="dialog_value_title"
                        text={Localization.get("comment")}/>                    
                    <textarea 
                            rows="6" 
                            className="rounded"
                            value={this.state.comment}
                            onChange={this.onCommentChanged.bind(this)}/>
                </div>

                <div className="dialog_content_buttons">                    
                    <Button 
                        onClick={() => EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY)}
                        text={Localization.get("close")}/>
                </div>
            </div>
        );
    }
}

export default ImportMarkItemAs;