import React from 'react';
import Button from './../../control/button.jsx';
import Label from './../../control/label.jsx';
import TreeView from './../../control/tree_view.jsx';
import ControlUtil from './../../control/util.jsx';
import Util from './../../context/util.jsx';
import App from './../../context/app.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';

class SettingsValidationTypes extends React.Component { 

    constructor(props) {
        super(props);
        this.selected = [];
        this.nodeTreeView = ControlUtil.createTreeView();
    }

    componentDidMount() {
        this.nodeTreeView.shouldUpdateState = false;
        for(var i=0; i<App.cachedTypes.length; ++i) {
            this.setupRoot(App.cachedTypes[i]);
        }
        Util.sortByKey(this.nodeTreeView.roots, "text", true);
        this.nodeTreeView.shouldUpdateState = true;
        this.nodeTreeView.invalidate();
    }

    onSaveClicked() {
        this.props.callback(this.selected);
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onCloseClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    getSelectedGroup(type) {
        return this.selected.find((e) => {
            return e.type == type;
        });
    }

    onValueChanged(element, e) {
        var group = this.getSelectedGroup(element.type);
		element.checked = e.target.checked;
		if(element.checked) {
            if(group == null) {
                group = {
                    type: element.type,
                    items: []
                };
                this.selected.push(group);
            }
            group.items.push(element);
		} else {
            var item = group.items.find((el) => {
                return el.id == element.id;
            });
            var index = group.items.indexOf(item);
            group.items.splice(index, 1);
            if(group.items.length == 0) {
                index = this.selected.indexOf(group);
                this.selected.splice(index, 1);
            }
        }
        this.setState({});
	}

    getItemName(item, type) {
        var key = null;
        if(type.startsWith("ssyk")) {
            key = "ssyk_code_2012";
        } else if(type.startsWith("isco")) {
            key = "isco_code_08";
        }
        if(key && item[key]) {
            return item[key] + " - " + item.preferredLabel;
        }
        return item.preferredLabel;
    }

    setupRoot(root) {
        var rootNode = ControlUtil.createTreeViewItem(this.nodeTreeView, root);
		rootNode.showingSelection = false;
        rootNode.setText(Localization.getTypeName(root.type));
        for(var i=0; i<root.children.length; ++i) {
            // TODO: check if node already is in settings?
            var childNode = this.setupNode(root.children[i], root.type);
            if(root.type == "skill-headline") {
                for(var k=0; k<root.children[i].skills.length; ++k) {
                    // TODO: check if node already is in settings?
                    var skillNode = this.setupNode(root.children[i].skills[k], "skill");
                    childNode.addChild(skillNode);
                }
            }
            rootNode.addChild(childNode);
        }
        rootNode.sortChildren((children) => {
            Util.sortByCmp(children, (v) => {
                return v.data.label;
            }, true);
        });
        if(root.type == "skill-headline") {
            for(var i=0; i<rootNode.children.length; ++i) {
                rootNode.children[i].sortChildren((children) => {
                    Util.sortByCmp(children, (v) => {
                        return v.data.label;
                    }, true);
                });
            }
        }
        this.nodeTreeView.addRoot(rootNode);
    }

    setupNode(item, type) {
        var element = {
            id: item.id,
            type: type,
            label: this.getItemName(item, type),
            checked: false,
        };
		var node = ControlUtil.createTreeViewItem(this.nodeTreeView, element);
		node.showingSelection = false;
        node.setText(this.renderElement(element));
        return node;
    }
	
	renderElement(element) {
		return (
			<div className="edit_concept_tree_view_checkbox_item">
				<input
					type="checkbox"
					id={element.label}
                    value={element.checked}
                    onChange={this.onValueChanged.bind(this, element)}/>
				<label htmlFor={element.label}>{element.label}</label>
			</div>
		);
	}

    render() {
        return (
            <div className="dialog_content edit_concept_dialog edit_concept_dialog_page">
                <div>
                    <div className="dialog_value_group">
                        <Label 
                            css="dialog_value_title"
                            text="Kryssa i de begrepp som ska undantas från validering"/>
                        <TreeView 
                            css="settings_validation_tree"
                            context={this.nodeTreeView}/>
                    </div>
                </div>
                <div className="dialog_content_buttons">
                    <Button
                        text={Localization.get("close")}
                        onClick={this.onCloseClicked.bind(this)}/>
                    <Button
                        text={Localization.get("save")}
                        onClick={this.onSaveClicked.bind(this)}/>
                </div>
            </div>
        );
    }
}

export default SettingsValidationTypes;