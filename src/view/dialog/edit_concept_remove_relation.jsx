import React from 'react';
import Label from './../../control/label.jsx';
import TreeView from './../../control/tree_view.jsx';
import ControlUtil from './../../control/util.jsx';
import Loader from './../../control/loader.jsx';
import Util from './../../context/util.jsx';
import App from './../../context/app.jsx';
import Socket from './../../context/socket.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import ConceptEdit from './../../context/concept_edit.jsx';

class EditConceptRemoveRelation extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.item.preferredLabel,
            isChanged: false,
        };
		this.props.editContext.onSave = this.onSave.bind(this);
		this.selected = [];
        this.relationTreeView = ControlUtil.createTreeView();
    }

    componentDidMount() {
        this.waitingForItem = null;
        var item = this.props.item;
	    if(this.hasRelations(item)) {
			this.waitingForItem = ControlUtil.createTreeViewItem(this.relationTreeView, null);
			this.waitingForItem.setText(<Loader/>);
            this.relationTreeView.addRoot(this.waitingForItem, async () => {
                this.relationTreeView.shouldUpdateState = false;
                if(item.broader) {
                    await this.setupRelations(item.broader, Constants.RELATION_BROADER);
                }
                if(item.narrower) {
                    await this.setupRelations(item.narrower, Constants.RELATION_NARROWER);
                }
                if(item.related) {
                    await this.setupRelations(item.related, Constants.RELATION_RELATED);
                }
                if(item.substitutes) {
                    await this.setupRelations(item.substitutes, Constants.RELATION_SUBSTITUTABILITY);
                }
                if(item.broad_match) {
                    await this.setupRelations(item.broad_match, Constants.RELATION_BROAD_MATCH);
                }
                if(item.narrow_match) {
                    await this.setupRelations(item.narrow_match, Constants.RELATION_NARROW_MATCH);
                }
                if(item.close_match) {
                    await this.setupRelations(item.close_match, Constants.RELATION_CLOSE_MATCH);
                }
                if(item.exact_match) {
                    await this.setupRelations(item.exact_match, Constants.RELATION_EXACT_MATCH);
                }
                if(item.possible_combinations) {
                    await this.setupRelations(item.possible_combinations, Constants.RELATION_POSSIBLE_COMBINATIONS);
                }
                if(item.unlikely_combinations) {
                    await this.setupRelations(item.possible_combinations, Constants.RELATION_UNLIKELY_COMBINATIONS);
                }
                for(var i=0; i<this.relationTreeView.roots.length; ++i) {
                    this.relationTreeView.roots[i].sortChildren();
                }
                var skillHeadline = this.findRootFor("skill-headline");
                if(skillHeadline) {
                    for(var i=0; i<skillHeadline.children.length; ++i) {
                        skillHeadline.children[i].sortChildren((children) => {
                            Util.sortByCmp(children, (v) => {
                                return v.data.preferredLabel;
                            }, true);
                        });
                    }
                }
                Util.sortByKey(this.relationTreeView.roots, "text", true);
                this.relationTreeView.shouldUpdateState = true;
                this.relationTreeView.invalidate();
                this.hideLoader();
            });
        }
    }

    hasRelations(item) {        
        return item.broader.length > 0 || 
                item.narrower.length > 0 || 
                item.related.length > 0 || 
                item.substitutes.length > 0 ||
                item.broad_match.length > 0 ||
                item.narrow_match.length > 0 ||
                item.close_match.length > 0 ||
                item.exact_match.length > 0 ||
                item.possible_combinations.length > 0 ||
                item.unlikely_combinations.length > 0;
    }

    onSave(message, callback) {
        var item = this.props.item;
		var conceptId = item.id;
		for(var i=0; i<this.selected.length; ++i) {
			var element = this.selected[i];
			var targetId = element.id;
			App.addSaveRequest();
            ConceptEdit.removeRelation(conceptId, targetId, element.relationType, message, () => {
                if(!App.hasPendingSaveRequests()) {
                    var filter = (connections) => {
                        return connections.filter((item) => {
                            return item.id != targetId;
                        });                        
                    };
                    item.narrower = filter(item.narrower);
                    item.broader = filter(item.broader);
                    item.related = filter(item.related);
                    item.substitutes = filter(item.substitutes);
                    item.broad_match = filter(item.broad_match);
                    item.narrow_match = filter(item.narrow_match);
                    item.close_match = filter(item.close_match);
                    item.exact_match = filter(item.exact_match);
                    item.possible_combinations = filter(item.possible_combinations);
                    item.unlikely_combinations = filter(item.unlikely_combinations);
					callback();
                }
            }, () => {
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
		}

    }

    onValueChanged(element, e) {
		element.checked = e.target.checked;
		if(element.checked) {
			this.selected.push(element);
		} else {
			var index = this.selected.indexOf(element);
			this.selected.splice(index, 1);
		}
		this.props.editContext.setEnableSave(this.selected.length > 0);
	}

    findRootFor(type) {
        return this.relationTreeView.roots.find((root) => {
            return root.data ? type == root.data.type : false;
        });
    }

    findSkillHeadline(root, id) {
        if(root) {
            return root.children.find((e) => {
                return e.data.id == id;
            });
        }
        return null;
    }

    async fetchSkillHeadline(item) {
        var root = this.findRootFor("skill-headline");
        if(!root) {
            root = ControlUtil.createTreeViewItem(this.relationTreeView, {type: "skill-headline"});
            root.setText(Localization.getTypeName("skill-headline"));
            root.setExpanded(true);
            this.relationTreeView.addRoot(root);
        }
        var data = await Rest.getConceptRelationsPromise(item.data.id, Constants.RELATION_BROADER, "skill-headline");
        if(Array.isArray(data)) {
            for(var i=0; i<data.length; ++i) {
                var headline = this.findSkillHeadline(root, data[i].id);
                if(headline == null) {
                    headline = ControlUtil.createTreeViewItem(this.relationTreeView, data[i]);
                    headline.setText(data[i].preferredLabel);
                    headline.setExpanded(true);
                    headline.showingSelection = false;
                    root.addChild(headline);
                }
                headline.addChild(item);
            }
        } else {
            App.showError("Fetch concept relations : Misslyckades hämta skill -> skill-headline relationer");
        }
    }
	
    async setupRelations(items, type) {
        for(var i=0; i<items.length; ++i) {
            items[i].relationType = type;
            if(items[i].type == "skill") {
                var child = ControlUtil.createTreeViewItem(this.relationTreeView, items[i]);
                child.showingSelection = false;
                child.setText(this.renderElement(items[i]));
                await this.fetchSkillHeadline(child);
            } else {
                this.addRelationToTree(items[i]);  
            }
        }
    }

    addRelationToTree(element) {
        var root = this.findRootFor(element.type);
        if(!root) {
            root = ControlUtil.createTreeViewItem(this.relationTreeView, element);
			root.showingSelection = false;
            root.setText(Localization.getTypeName(element.type));
            this.relationTreeView.addRoot(root);
            root.setExpanded(true);
		}
		element.checked = false;
		var child = ControlUtil.createTreeViewItem(this.relationTreeView, element);
		child.showingSelection = false;
        child.setText(this.renderElement(element));
        root.addChild(child);
    }

    hideLoader() {
        if(this.waitingForItem) {
            this.relationTreeView.removeRoot(this.waitingForItem);
            this.waitingForItem = null;
        }
	}
	
	renderElement(element) {
		return (
			<div className="edit_concept_tree_view_checkbox_item">
				<input
					type="checkbox"
					id={"rem_conn_" + element.id}
                    value={element.checked}
                    onChange={this.onValueChanged.bind(this, element)}/>
				<label htmlFor={"rem_conn_" + element.id}>
                    {element.preferredLabel}
                </label>
			</div>
		);
	}

    render() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Kryssa i de relationer som ska tas bort"/>
				<TreeView 
					css="edit_concept_tree_view"
					context={this.relationTreeView}/>
            </div>
        );
    }
}

export default EditConceptRemoveRelation;