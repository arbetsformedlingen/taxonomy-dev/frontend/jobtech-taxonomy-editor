import React from 'react';
import Button from './../../control/button.jsx';
import Label from './../../control/label.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import Localization from '../../context/localization.jsx';
import App from './../../context/app.jsx';

class SearchRelations extends React.Component { 

    constructor(props) {
        super(props);    
        this.options = this.populateOptions();
        this.typeOptions = this.populateTypeOptions();    
        this.state = {
            querryTypes: [Constants.CONCEPT_SSYK_LEVEL_4, Constants.CONCEPT_SKILL],
            relationType: Constants.RELATION_BROADER,
        };
    }

    populateOptions() {
        var options = [];
        for(var i=0; i<App.types.length; ++i) {
            if("skill-headline" != App.types[i]) {
                options.push({value: App.types[i], text: Localization.getTypeName(App.types[i])});
            }
        }        
        options.sort((a, b) => {
            if(a.text < b.text) {
                return -1;
            }
            if(a.text > b.text) {
                return 1;
            }
            return 0;
        });
        options.unshift({value: "", text: "--- Välj typ ---"});
        return options;
    }

    populateTypeOptions() {
        var relationTypes = [
            Constants.RELATION_BROADER,
            Constants.RELATION_NARROWER,
            Constants.RELATION_RELATED,
            Constants.RELATION_SUBSTITUTABILITY,
            Constants.RELATION_BROAD_MATCH,
            Constants.RELATION_NARROW_MATCH,
            Constants.RELATION_CLOSE_MATCH,
            Constants.RELATION_EXACT_MATCH,
            Constants.RELATION_POSSIBLE_COMBINATIONS,
            Constants.RELATION_UNLIKELY_COMBINATIONS,
            "all_matching"
        ];
        var options = [];
        for(var i=0; i<relationTypes.length; ++i) {
            options.push({value: relationTypes[i], text: Localization.get(relationTypes[i])});
        }
        options.sort((a, b) => {
            if(a.text < b.text) {
                return -1;
            }
            if(a.text > b.text) {
                return 1;
            }
            return 0;
        });
        return options;
    }

    onTypeChanged(id, e) {
        var value = e.target.value;
        if(value.length == 0 && id < this.state.querryTypes.length) {
            if(id > 0) {
                this.state.querryTypes.splice(id, 1);
            }
        } else {
            if(id < this.state.querryTypes.length) {
                this.state.querryTypes[id] = value;
            } else {
                this.state.querryTypes.push(value);
            }
        }
        this.setState({queryTypes: this.state.querryTypes});
    }

    onRelationTypeChanged(e) {
        this.setState({relationType: e.target.value});
    }

    onCloseClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onSearchClicked() {
        EventDispatcher.fire(Constants.EVENT_SIDEPANEL_CONNECTIONS_SELECTED, {
            types: this.state.querryTypes,
            relationType: this.state.relationType,
        });
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    renderOption(value, text, key) {
        return (
            <option value={value} key={key}>
                {Localization.get(text)}
            </option>
        );
    }

    renderOptions() {
        var options = this.options.map((o, i) => {
            return this.renderOption(o.value, o.text, i);
        });
        return options;
    }

    renderRelationTypeSelect() {
        var options = this.typeOptions.map((o, i) => {
            return this.renderOption(o.value, o.text, i);
        });
        return (
            <select 
                className="sub_panel_select rounded"
                value={this.state.relationType}
                onChange={this.onRelationTypeChanged.bind(this)}>
                {options}
            </select>
        );
    }

    renderTypeSelect(value, onChange) {
        return(
            <select 
                className="sub_panel_select rounded"
                value={value}
                onChange={onChange}>
                {this.renderOptions()}
            </select>
        );
    }

    renderSelectedTypes() {
        var dropDowns = [];
        for(var i=1; i<this.state.querryTypes.length; ++i) {
            dropDowns.push(                
                <select 
                    key={i}
                    className="sub_panel_select rounded"
                    value={this.state.querryTypes[i]}
                    onChange={this.onTypeChanged.bind(this, i)}>
                    {this.renderOptions()}            
                </select>
            );
        }
        if(this.state.querryTypes.length < 5) {
            dropDowns.push(
                <select 
                    key={this.state.querryTypes.length + 1}
                    className="sub_panel_select rounded"
                    value={this.state.querryTypes[i]}
                    onChange={this.onTypeChanged.bind(this, this.state.querryTypes.length + 1)}>
                    {this.renderOptions()}
                </select>
            );
        }
        return dropDowns;
    }

    render() {
        return (
            <div className="dialog_content">
                <div className="dialog_value_group search_relations_group">
                    <Label 
                        css="dialog_value_title"
                        text="Från begreppstyp"/>
                    {this.renderTypeSelect(this.state.querryTypes[0], this.onTypeChanged.bind(this, 0))}
                    <Label 
                        css="dialog_value_title"
                        text="Mot begreppstyp/er"/>
                    {this.renderSelectedTypes()}
                </div>
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Relations typ"/>
                    {this.renderRelationTypeSelect()}
                </div>                
                <div className="dialog_content_buttons">
                    <Button 
                        isEnabled={this.state.querryTypes.length > 1}
                        text={Localization.get("search")}
                        onClick={this.onSearchClicked.bind(this)}/>
                    <Button 
                        onClick={this.onCloseClicked.bind(this)}
                        text={Localization.get("close")}/>
                </div>
            </div>
        );
    }
}

export default SearchRelations;