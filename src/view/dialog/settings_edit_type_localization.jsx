import React from 'react';
import Button from './../../control/button.jsx';
import Label from './../../control/label.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';

class SettingsEditTypeLocalization extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            label_en: props.label_en ? props.label_en : "",
            label_sv: props.label_sv ? props.label_sv : "",
            edited: false,
        };
    }

    componentDidMount() {
        
    }

    onLabelEnChanged(e) {
        this.setState({
            edited: true,
            label_en: e.target.value});
    }

    onLabelSvChanged(e) {
        this.setState({
            edited: true,
            label_sv: e.target.value});
    }

    onSaveClicked() {
        this.props.callback(this.props.type, this.state.label_en, this.state.label_sv);
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onCloseClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    render() {
        return (
            <div className="dialog_content edit_concept_dialog edit_concept_dialog_page">
                <div>
                    <div className="dialog_value_group">
                        <Label 
                            css="dialog_value_title"
                            text={Localization.get("label_en")}/>
                        <input
                            className="rounded"
                            value={this.state.label_en}
                            onChange={this.onLabelEnChanged.bind(this)}/>
                    </div>
                    <div className="dialog_value_group">
                        <Label 
                            css="dialog_value_title"
                            text={Localization.get("label_sv")}/>
                        <input
                            className="rounded"
                            value={this.state.label_sv}
                            onChange={this.onLabelSvChanged.bind(this)}/>
                    </div>
                </div>
                <div className="dialog_content_buttons">
                    <Button
                        text={Localization.get("close")}
                        onClick={this.onCloseClicked.bind(this)}/>
                    <Button
                        isEnabled={this.state.edited}
                        text={Localization.get("save")}
                        onClick={this.onSaveClicked.bind(this)}/>
                </div>
            </div>
        );
    }
}

export default SettingsEditTypeLocalization;