import React from 'react';
import Button from './../../control/button.jsx';
import DialogWindow from './../../control/dialog_window.jsx';
import Label from './../../control/label.jsx';
import App from './../../context/app.jsx';
import Socket from './../../context/socket.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Rest from './../../context/rest.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Keybindings from './../../context/keybindings.jsx';
import Util from './../../context/util.jsx';
import EditConceptName from './edit_concept_name.jsx';
import EditConceptAlternativeLabel from './edit_concept_alternative_label.jsx';
import EditConceptDefinition from './edit_concept_definition.jsx';
import EditConceptDeprecate from './edit_concept_deprecate.jsx';
import EditConceptReactivate from './edit_concept_reactivate.jsx';
import EditConceptReason from './edit_concept_reason.jsx';
import EditConceptQuality from './edit_concept_quality.jsx';
import EditConceptAddRelation from './edit_concept_add_relation.jsx';
import EditConceptRemoveRelation from './edit_concept_remove_relation.jsx';
import EditConceptEditRelation from './edit_concept_edit_relation.jsx';
import EditConceptSetReference from './edit_concept_set_reference.jsx';
import EditConceptNewValue from './edit_concept_new_value.jsx';
import EditConceptNoEscoRelation from './edit_concept_no_esco_relation.jsx';

class EditConcept extends React.Component { 

    constructor(props) {
        super(props);
        // edit types
        this.state = {
            type: props.defaultType == null ? Constants.EDIT_TYPE_NONE : props.defaultType,
            isSaveEnabledEditState: false,
            isSaveEnabledReasonState: false,
            hasSaved: false,
            showQloseQuery: false,
        };
        // edit context
        this.editContext = {
            setEnableSave: (enabled) => { 
                this.setState({isSaveEnabledEditState: enabled});
            },
            onSave: null,
            lockToType: props.lockToType,
            meta: this.props.meta
        };
        // reason context
        this.reasonContext = {
            setEnableSave: (enabled) => { 
                this.setState({isSaveEnabledReasonState: enabled});
            },
            onSave: null,
        };        
        if(this.editContext.lockToType) {
            this.state.type = Constants.EDIT_TYPE_ADD_RELATION;
        } else if(props.lockToAlternativeLabel) {
            this.state.type = Constants.EDIT_TYPE_ALTERNATIVE_LABEL;
        }
        props.dialogContext.onClosed = this.onDialogCloseGate.bind(this);
        this.boundGlobalInputDown = this.onGlobalInputDown.bind(this);
    }

    componentDidMount() {
        document.addEventListener('keydown', this.boundGlobalInputDown);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.boundGlobalInputDown);
    }

    saveManualDaynote(message, callback) {
        Rest.postDayNote(this.props.item.id, message, () => {
            App.removeSaveRequest();
            callback();
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades spara manuell daganteckning");
            App.removeSaveRequest();
        });
    }
    
    onGlobalInputDown(e) {
        if(Keybindings.onKeyBinding(e, Keybindings.BINDING_SAVE_DIALOG, false) && this.isSaveEnabled()) {
            this.onSaveClicked();
        }
    }

    onDialogCloseGate() {
        var hasChanges = this.state.isSaveEnabledEditState || this.state.isSaveEnabledReasonState;
        if(hasChanges) {
            // if we have changes, dont allow DialogWindow to handle the closing process
            // we will handle it internally in this component
            this.onCloseClicked();
        }
        return !hasChanges;
    }

    onTypeSelected(e) {
        if(e.target.value == Constants.EDIT_TYPE_NONE) {
            Socket.sendClearState();
        } else {
            Socket.sendState({
                type: "edit",
                id: this.props.item.id,
                field: e.target.value
            });
        }
        this.setState({type: e.target.value});
    }

    onAbortClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onCloseClicked(force) {
        if((this.state.isSaveEnabledEditState || this.state.isSaveEnabledReasonState) && !force) {
            EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
                title: Localization.get("dialog_unsaved_changes"),
                content: this.renderCloseQuery()
            });
        } else {
            Socket.sendClearState();
            Rest.abort();
            if(force) {
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            }
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        }
    }

    onSaveClicked(isFromQuery) {
        this.setState({hasSaved: true}, () => {
            if(this.editContext.onSave) {
                EventDispatcher.fire(Constants.EVENT_SHOW_SAVE_INDICATOR);
                this.editContext.onSave(this.reasonContext.message, () => {
                    this.props.onItemUpdated();
                    if(isFromQuery) {
                        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
                    }
                    EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
                });
            } else if(this.state.type == Constants.EDIT_TYPE_ADD_COMMENT) {
                this.saveManualDaynote(this.reasonContext.message, () => {
                    this.props.onItemUpdated();
                    if(isFromQuery) {
                        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
                    }
                    EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
                });
            }
        });
    }

    isSaveEnabled() {
        if(this.state.type == Constants.EDIT_TYPE_ADD_COMMENT) {
            return this.state.isSaveEnabledReasonState && !this.state.hasSaved;
        } else {
            return this.state.isSaveEnabledEditState && this.state.isSaveEnabledReasonState && !this.state.hasSaved;
        }
    }

    renderTypeSelection() {
        var isDeprecated = this.props.item.deprecated != null ? this.props.item.deprecated : false;
        var options = [
            Constants.EDIT_TYPE_NAME,
            Constants.EDIT_TYPE_ALTERNATIVE_LABEL,
            Constants.EDIT_TYPE_DESCRIPTION,
            Constants.EDIT_TYPE_QUALITY,
            Constants.EDIT_TYPE_ADD_RELATION,
            Constants.EDIT_TYPE_REMOVE_RELATION,
            Constants.EDIT_TYPE_EDIT_RELATION,
            isDeprecated ? Constants.EDIT_TYPE_REFERENCED_TO : Constants.EDIT_TYPE_DEPRICATE,
            Constants.EDIT_TYPE_NEW_VALUE,
            Constants.EDIT_TYPE_NO_ESCO_RELATION,
            Constants.EDIT_TYPE_ADD_COMMENT,
        ];
        if(isDeprecated) {
            options = [
                Constants.EDIT_TYPE_REFERENCED_TO, 
                Constants.EDIT_TYPE_REACTIVATE,
                Constants.EDIT_TYPE_ADD_COMMENT,
            ];
        }
        var itemType = this.props.item.type;
        if(itemType != Constants.CONCEPT_OCCUPATION_NAME && itemType != Constants.CONCEPT_SKILL) {
            // remove quality level
            for(var i=0; i<options.length; ++i) {
                if(options[i] == Constants.EDIT_TYPE_QUALITY)  {
                    options.splice(i, 1);
                    break;
                }
            }
        }
        
        options = options.map((value, index) => {
            return ( 
                <option key={index} value={value}>
                    {Localization.get(value)}
                </option> 
            );
        });
        return (
            <select
                className="rounded"
                value={this.state.type}
                onChange={this.onTypeSelected.bind(this)}>
                <option value={Constants.EDIT_TYPE_NONE}>--</option>
                {options}
            </select>
        );
    }

    renderChooseEditType() {
        if(!this.editContext.lockToType && !this.props.lockToAlternativeLabel) {
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text={Localization.get("edit_type")}/>
                    {this.renderTypeSelection()}
                </div>
            );
        }
    }

    renderReason() {
        if(this.state.type != Constants.EDIT_TYPE_NONE) {
            return ( 
                <EditConceptReason 
                    useTitle={this.state.type == Constants.EDIT_TYPE_ADD_COMMENT}
                    initialMessage={this.props.initialMessage}
                    editContext={this.reasonContext}/>
            );
        }
    }

    renderCloseQuery() {
        return (
            <div className="dialog_content edit_concept_dialog_page">
                <div className="dialog_content_buttons">
                <Button
                    text={Localization.get("abort")}
                    onClick={this.onAbortClicked.bind(this)}/>
                <Button
                    text={Localization.get("close")}
                    onClick={this.onCloseClicked.bind(this, true)}/>
                <Button
                    isEnabled={this.isSaveEnabled()}
                    text={Localization.get("save")}
                    onClick={this.onSaveClicked.bind(this, true)}/>
                </div>
            </div>
        );
    }

    render() {
        var getEditPage = (type) => {
            if(type == Constants.EDIT_TYPE_NAME) {
                return ( <EditConceptName item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_ALTERNATIVE_LABEL) {
                return ( <EditConceptAlternativeLabel item={this.props.item} editContext={this.editContext} addAsAlternativeLabel={this.props.addAsAlternativeLabel}/> );
            } else if(type == Constants.EDIT_TYPE_DESCRIPTION) {
                return ( <EditConceptDefinition item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_QUALITY) {
                return ( <EditConceptQuality item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_DEPRICATE) {
                return ( <EditConceptDeprecate item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_REACTIVATE) {
                return ( <EditConceptReactivate item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_ADD_RELATION) {
                return ( <EditConceptAddRelation item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_REMOVE_RELATION) {
                return ( <EditConceptRemoveRelation item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_EDIT_RELATION) {
                return ( <EditConceptEditRelation item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_REFERENCED_TO) {
                return ( <EditConceptSetReference item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_NEW_VALUE) {
                return ( <EditConceptNewValue item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_NO_ESCO_RELATION) {
                return ( <EditConceptNoEscoRelation item={this.props.item} editContext={this.editContext}/> );
            } else if(type == Constants.EDIT_TYPE_ADD_COMMENT) {
                null;
            }
            return null;
        };
        return (
            <div className="dialog_content edit_concept_dialog edit_concept_dialog_page">
                <div>
                    {this.renderChooseEditType()}
                    {getEditPage(this.state.type)}
                    {this.renderReason()}
                </div>
                <div className="dialog_content_buttons">
                    <Button
                        text={Localization.get("close")}
                        onClick={this.onCloseClicked.bind(this)}/>
                    <Button
                        isEnabled={this.isSaveEnabled()}
                        text={Localization.get("save")}
                        onClick={this.onSaveClicked.bind(this)}/>
                </div>
            </div>            
        );
    }
}

export default EditConcept;