import React from 'react';
import Button from './../../control/button.jsx';
import Label from './../../control/label.jsx';
import Loader from './../../control/loader.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import App from './../../context/app.jsx';
import Rest from './../../context/rest.jsx';
import EditConcept from '../dialog/edit_concept.jsx';
import NewConcept from './../dialog/new_concept.jsx';
import ImportChooseOccupationName from './import_choose_occupation_name.jsx';

class ImportWorkWithItem extends React.Component { 

    constructor(props) {
        super(props);        
        this.state = {           
            comment: this.props.item.comment == null ? "" : this.props.item.comment,
            expandedIndex: -1,
            semantic: [],
            loading: true,
        }
    }

    componentDidMount() {
        this.state.semantic = [];
        this.state.loading = true;
        Rest.postSemanticSearch("", [this.props.item.name], 6, (data) => {
            // setup data for rendering
            this.setState({
                loading: false,
                semantic: data[this.props.item.name]});
        }, (status) => {
            console.log("Error: " + status);
            this.setState({loading: false});
        });
    }
    
    onItemSaved(item) {
        //set property on item
        this.props.item.alternateLabelCreatedOn = item;
        this.props.item.state = "alternative label";
        if(this.props.onItemUpdated) {
            this.props.onItemUpdated();
        }
        this.props.onItemUpdated();
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onItemCreated(item) {
        //set property on item
        this.props.item.itemCreated = true;
        this.props.item.state = "new";
        if(this.props.onItemUpdated) {
            this.props.onItemUpdated();
        }
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onChoosenOccupationName(item, noHide) {
        if(!noHide) {
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);            
        }
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("edit") + " " + item.preferredLabel,
            content: <EditConcept 
                        item={item}
                        initialMessage={this.props.message}
                        addAsAlternativeLabel={this.props.item.name}
                        lockToAlternativeLabel={true}
                        onItemUpdated={this.onItemSaved.bind(this, item)}/>
        });        
    }

    onAddLabelClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("add"),
            content: <ImportChooseOccupationName                         
                        onChoosenOccupationName={this.onChoosenOccupationName.bind(this)}/>
        });
    }

    onSemanticAddClicked(item) {
        //fetch correct item to update from cache
        this.onChoosenOccupationName(App.getCachedConceptFromId(item.id), true);
    }

    onNewConceptClicked(targetRelation) {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("new_value"),
            content: <NewConcept 
                        initialName={this.props.item.name}
                        initialMessage={this.props.message}
                        targetRelation={targetRelation}
                        onItemSaved={this.onItemCreated.bind(this)}/>
        });
    }

    onCommentChanged(e) {
        this.setState({comment: e.target.value});
    }

    renderSemantic() {
        var items = this.state.semantic.map((element, i) => {
            return (
                <div 
                    key={i}
                    className="import_semantic_item">
                    <div onPointerUp={() => {this.setState({expandedIndex: this.state.expandedIndex == i ? -1 : i})}}>
                        {Localization.getTypeName(element.type)} - {element.preferredLabel}
                    </div>
                    {this.state.expandedIndex == i &&
                        <div className="import_semantic_item_options">
                            <div className="import_work_with_trash">
                                <div>Skapa begrepp med relation</div>
                                <Button 
                                    text={Localization.get("create")}
                                    onClick={this.onNewConceptClicked.bind(this, element)}/>
                            </div>
                            <div className="import_work_with_trash">
                                <div>Lägg till som alternativ benämning</div>
                                <Button 
                                    text={Localization.get("choose")}
                                    onClick={this.onSemanticAddClicked.bind(this, element)}/>
                            </div>
                        </div>
                    }
                </div>
            );
        });
        if(this.state.semantic.length == 0 && this.state.loading) {
            items = <Loader />
        }
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Förslag"/>
                <div className="import_semantic_container">
                    {items}
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="dialog_content">
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text={Localization.get("new_concept")}/>
                    <div className="import_work_with_trash">
                        <Label text={Localization.get("create_new_concept")}/>
                        <Button 
                                onClick={this.onNewConceptClicked.bind(this)}
                                text={Localization.get("create")}/>
                    </div>
                </div>
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Alternativ yrkesbenämning"/>                    
                    <div className="import_work_with_trash">
                        <Label text={Localization.get("add_on_concept")}/>
                        <Button 
                            onClick={this.onAddLabelClicked.bind(this)}
                            text={Localization.get("choose")}/>                        
                    </div>
                </div>
                {this.renderSemantic()}
                <div className="dialog_content_buttons">                    
                    <Button 
                        onClick={() => EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY)}
                        text={Localization.get("abort")}/>
                </div>
            </div>
        );
    }
}

export default ImportWorkWithItem;