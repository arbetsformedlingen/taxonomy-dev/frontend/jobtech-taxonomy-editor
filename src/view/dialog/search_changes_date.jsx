import React from 'react';
import DatePicker, { registerLocale } from 'react-datepicker';
import sv from "date-fns/locale/sv";
registerLocale("sv", sv);
import Button from './../../control/button.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Rest from './../../context/rest.jsx';
import Util from './../../context/util.jsx';
import Label from './../../control/label.jsx';
import { isThisSecond } from 'date-fns';

class SearchChangesDate extends React.Component { 

    constructor(props) {
        super(props);
        //"DIVERTED",
        //"MOVED",
        this.actions = [
            "CREATED",
            "UPDATED",
            "DEPRECATED",
            "COMMENTED",
        ];
        this.relations = [
            Constants.RELATION_NARROWER,
            Constants.RELATION_BROADER,
            Constants.RELATION_RELATED,
            Constants.RELATION_SUBSTITUTABILITY,
            Constants.RELATION_BROAD_MATCH,
            Constants.RELATION_NARROW_MATCH,
            Constants.RELATION_CLOSE_MATCH,
            Constants.RELATION_EXACT_MATCH,
            Constants.RELATION_POSSIBLE_COMBINATIONS,
            Constants.RELATION_UNLIKELY_COMBINATIONS
        ]
        
        this.state = {
            actions: this.props.actions,
            relations: this.props.relations,
            fromDate: new Date("2000-01-01T00:00:01.000Z"),
            fromVersion: "default",
            toDate: new Date(),
            toVersion: "default",
            searchMode: "",
            versions: [],
        };
    }

    componentDidMount() {
        this.props.onSetFromDate(this.state.fromDate);
        this.props.onSetToDate(this.state.toDate);
        Rest.getVersions((data)=>{
            var latestVersion = 0;
            for(var i=0; i<data.length; ++i) {
                data[i].date = new Date(data[i].timestamp);
                if(latestVersion < data[i].version) {
                    latestVersion = data[i].version;
                }
            }
            data = Util.sortByKey(data, "version", false),
            this.setState({
                versions: data,
            });
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades med att hämta verisioner");
        });
    }

    isActionSelected(type) {
        return this.state.actions.indexOf(type) != -1;
    }

    isRelationSelected(type) {
        return this.state.relations.indexOf(type) != -1;
    }

    onActionSelectedChanged(action, e) {
        if(e.target.checked) {
            this.state.actions.push(action);
        } else {
            var index = this.state.actions.indexOf(action);
            this.state.actions.splice(index, 1);
        }
        this.state.relations.length = 0;
        this.setState({
            actions: this.state.actions,
            relations: this.state.relations});
    }

    onRelationSelectedChanged(relation, e) {
        if(e.target.checked) {
            this.state.relations.push(relation);
        } else {
            var index = this.state.relations.indexOf(relation);
            this.state.relations.splice(index, 1);
        }
        this.state.actions.length = 0;
        this.setState({
            actions: this.state.actions,
            relations: this.state.relations});
    }

    onSetFromDate(date) {
        this.setState({
            fromDate: date,
            fromVersion: "",
        });
        this.props.onSetFromDate(date);
    }

    onSetToDate(date) {
        this.setState({
            toDate: date,
            toVersion: "",
        });
        this.props.onSetToDate(date);
    }

    onSearchTypeSelected(e) {
        this.setState({searchMode: e.target.value});
    }

    onSetFromVersion(e) {
        if(e.target.value != "default") {
            var date = this.state.versions[e.target.value].date;
            this.setState({
                fromDate: date,
                fromVersion: e.target.value,
            });
            this.props.onSetFromDate(date);
        }
    }

    onSetToVersion(e) {
        if(e.target.value != "default") {
            var date = this.state.versions[e.target.value].date;
            this.setState({
                toDate: date,
                toVersion: e.target.value,
            });
            this.props.onSetToDate(date);
        }        
    }

    renderOption(value, text, key) {
        return (
            <option 
                value={value} 
                key={key}>
                {text}
            </option>
        );
    }

    renderActions() {
        if("actions" === this.state.searchMode) {
            var actions = this.actions.map((action, index) => {
                return (
                    <div 
                        className="search_changes_row"
                        key={index}>
                        <Label 
                            css="search_changes_label"
                            text={Localization.get(action)}/>
                        <div className="search_changes_cb">
                        <input 
                            type="checkbox"                        
                            onChange={this.onActionSelectedChanged.bind(this, action)}
                            checked={this.isActionSelected(action)}/>
                        </div>
                    </div>
                );
            });
            return (
                <div>
                    <Label 
                        css="search_changes_title"
                        text={Localization.get("select_actions")}/>
                    {actions}
                </div>
            );
        }
    }

    renderRelations() {
        if("relations" === this.state.searchMode) {
            var relations = this.relations.map((relation, index) => {
                return (
                    <div 
                        className="search_changes_row"
                        key={index}>
                        <Label 
                            css="search_changes_label"
                            text={relation}/>
                        <div className="search_changes_cb">
                        <input 
                            type="checkbox"                        
                            onChange={this.onRelationSelectedChanged.bind(this, relation)}
                            checked={this.isRelationSelected(relation)}/>
                        </div>
                    </div>
                );
            });
            return(
                <div>
                    <Label 
                        css="search_changes_title"
                        text={Localization.get("select_relations")}/>
                    {relations}
                </div>
            );
        }
    }

    renderNextButton() {
        return(
            <Button 
                isEnabled={this.state.actions.length > 0 || this.state.relations.length > 0}                        
                onClick={this.props.onNextClicked}
                text={Localization.get("next")}/>
        );
    }

    renderDateSelection() {
        var versionOptions = this.state.versions.map((version, i) => {
            return this.renderOption(i, Localization.get("version") + " " + version.version + " - " + version.date.toLocaleDateString(), i);
        });
        //add empty default
        versionOptions.unshift(
            <option 
                value="default"
                key="-1">                
            </option>
        );

        return(
            <div>
                <Label
                    css="search_changes_title"
                    text={Localization.get("select_date")}/>
                <div className="search_changes_column">
                    <div className="search_changes_date">
                        <Label text={Localization.get("from") + ":"}/>
                        <div className="search_changes_date_picker">
                        <select 
                            className="rounded"
                            value={this.state.fromVersion}
                            onChange={this.onSetFromVersion.bind(this)}>
                            {versionOptions}
                        </select>
                        </div>
                    </div>
                    <div className="search_changes_date">
                        <div></div>
                        <div className="search_changes_date_picker">
                            <DatePicker 
                                selected={this.state.fromDate} 
                                onChange={this.onSetFromDate.bind(this)}
                                locale={Localization.get("locale")}
                                dateFormat="yyyy-MM-dd"/>
                        </div>
                    </div>
                    <div className="search_changes_date">
                        <Label text={Localization.get("to") + ":"}/>
                        <div className="search_changes_date_picker">
                        <select 
                            className="rounded"
                            value={this.state.toVersion}
                            onChange={this.onSetToVersion.bind(this)}>
                            {versionOptions}
                        </select>
                        </div>
                    </div>
                    <div className="search_changes_date">
                        <div></div>
                        <div className="search_changes_date_picker">
                            <DatePicker 
                                selected={this.state.toDate} 
                                onChange={this.onSetToDate.bind(this)}
                                locale={Localization.get("locale")}
                                dateFormat="yyyy-MM-dd"/>
                        </div>                            
                    </div>
                </div>
            </div>
        );

    }

    render() {        
        return(
            <div className="search_changes">
                {this.renderDateSelection()}
                <div>
                    <Label
                        css="search_changes_title"
                        text={Localization.get("select_search")}/>
                    <select 
                        className="rounded search_changes_select"
                        value={this.state.type}
                        onChange={this.onSearchTypeSelected.bind(this)}>
                        <option>--</option>
                        <option value="actions">{Localization.get("actions")}</option>
                        <option value="relations">{Localization.get("relations")}</option>
                    </select>
                </div>
                {this.renderActions()}
                {this.renderRelations()}
                <div className="dialog_content_buttons search_content_buttons">
                    <Button 
                        onClick={this.props.onCloseClicked}
                        text={Localization.get("close")}/>
                    {this.renderNextButton()}
                </div>
            </div>
        );
    }
}

export default SearchChangesDate;