import React from 'react';
import Label from './../../control/label.jsx';
import App from './../../context/app.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Util from './../../context/util.jsx';

class EditConceptReactivate extends React.Component { 

    constructor(props) {
		super(props);
        this.state = {
            isChanged: false,
            value: false,
        };
        this.props.editContext.onSave = this.onSave.bind(this);
    }

    onValueChanged(e) {
		var isChanged = e.target.checked != this.state.defaultValue;
        if(isChanged != this.state.isChanged) {
            this.props.editContext.setEnableSave(isChanged);
        }
        this.setState({
            value: e.target.checked,
            isChanged: isChanged,
        });
    }

    onSave(message, callback) {
        App.addSaveRequest();
        Rest.patchConcept(this.props.item.id, message, "&deprecated=false", () => {
            this.props.item.deprecated = false;
            App.updateCachedItem(this.props.item);
            if(App.removeSaveRequest()) {
                callback();
            }
        }, () => {
            App.showError(Util.getHttpMessage(status) + " : Återaktualisering misslyckades");
            App.removeSaveRequest();
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    renderReferenceCheckbox() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Återaktualisera begrepp"/>
                <div className="edit_concept_row">
                    <input
                        id="shouldReference"
                        type="checkbox"
                        value={this.state.value}
                        onChange={this.onValueChanged.bind(this)}/>
                    <label htmlFor="shouldReference">{Localization.get("yes")}</label>
                </div>
            </div>
        );
    }

	render() {
        return (
            <div className="edit_concept_group_collection">
                {this.renderReferenceCheckbox()}
            </div>
        );
    }
}

export default EditConceptReactivate;