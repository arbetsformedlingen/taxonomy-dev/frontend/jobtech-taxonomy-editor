import React from 'react';
import Button from './../../control/button.jsx';
import Label from './../../control/label.jsx';
import List from '../../control/list.jsx';
import App from './../../context/app.jsx';
import Rest from './../../context/rest.jsx';
import Socket from './../../context/socket.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Util from './../../context/util.jsx';

class EditConceptAlternativeLabel extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            value: "",
            data: [],
        };
        this.addLabels(this.props.item.alternativeLabels, false);
        this.addLabels(this.props.item.hiddenLabels, true);
        if(props.addAsAlternativeLabel && props.addAsAlternativeLabel.length > 0) {
            this.state.data.unshift({
                name: props.addAsAlternativeLabel.trim(),
                hidden: false,
            });
        }
        this.props.editContext.onSave = this.onSave.bind(this);
    }

    componentDidMount() {
        this.updateEnableSave();
    }

    addLabels(labels, hidden) {
        for(var i=0; i<labels.length; ++i) {
            this.state.data.push({
                name: labels[i],
                hidden: hidden,
            });
        }
    }

    addButtonIsEnabled() {
        var value = this.state.value.trim();
        if(value.length > 0) {
            //check for duplicate
            return this.state.data.filter((item) => {return item.name == value}).length == 0;            
        }
        return false;
    }

    getItemsToAdd(hidden) {
        return this.state.data.filter((item) => {
            if(item.hidden != hidden) {
                return false;
            }
            if(hidden) {
                return(this.props.item.hiddenLabels.indexOf(item.name) < 0);
            } else {
                return(this.props.item.alternativeLabels.indexOf(item.name) < 0);
            }
        });
    }

    getItemsToRemove(hidden) {
        var hasString = (s) => {
            return this.state.data.filter((item) => {
                return item.hidden == hidden && item.name == s;
            }).length == 0;
        };
        if(hidden) {
            return (this.props.item.hiddenLabels.filter((s) => {
                return hasString(s);
            }));
        } else {
            return(this.props.item.alternativeLabels.filter((s) => {
                return hasString(s);
            }));
        }
    }

    updateEnableSave() {
        this.props.editContext.setEnableSave(this.getItemsToAdd(true).length > 0 || 
            this.getItemsToAdd(false).length > 0 || 
            this.getItemsToRemove(true).length > 0 || 
            this.getItemsToRemove(false).length > 0);
    }

    onSave(message, callback) {
        var buildAddReq = (list) => {
            if(list.length == 0) {
                return "";
            }
            var req = list[0].name;
            for(var i=1; i<list.length; ++i) {
                req += "|" + list[i].name;
            }
            return req;
        };        
        var item = this.props.item;
        var addHidden = this.getItemsToAdd(true);
        var addAlt = this.getItemsToAdd(false);
        var remHidden = this.getItemsToRemove(true);
        var remAlt = this.getItemsToRemove(false);
        
        if(addHidden.length > 0 || addAlt.length > 0) {
            var req = "";
            if(addAlt.length > 0) {
                req = "&alternative-labels=" + encodeURIComponent(buildAddReq(addAlt));
            }
            if(addHidden.length > 0) {
                req += "&hidden-labels=" + encodeURIComponent(buildAddReq(addHidden));
            }
            App.addSaveRequest();
            Rest.patchConcept(item.id, message, req, () => {
                if(addAlt.length > 0) {
                    for(var i=0; i<addAlt.length; ++i) {
                        item.alternativeLabels.push(addAlt[i].name);
                    }
                }
                if(addHidden.length > 0) {
                    for(var i=0; i<addHidden.length; ++i) {
                        item.hiddenLabels.push(addHidden[i].name);
                    }
                }
                App.updateCachedItem(item);
                App.removeSaveRequest();
                if(addAlt.length > 0) {
                    Socket.sendEditSave(item.id, "alternativeLabels", item.alternativeLabels);
                }
                if(addHidden.length > 0) {
                    Socket.sendEditSave(item.id, "hiddenLabels", item.hiddenLabels);
                }
                callback();
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera alternativa benämningar");
                App.removeSaveRequest();
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
        }
        for(var i=0; i<remAlt.length; ++i) {    
            App.addSaveRequest();
            Rest.patchRemoveAlternativeLabel(item.id, message, remAlt[i], (label) => {
                var i = item.alternativeLabels.indexOf(label);
                if(i >= 0) {
                    item.alternativeLabels.splice(i, 1);
                }
                App.updateCachedItem(item);
                App.removeSaveRequest();
                Socket.sendEditSave(item.id, "alternativeLabels", item.alternativeLabels);
                callback();
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera alternativa benämningar");
                App.removeSaveRequest();
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
        }
        for(var i=0; i<remHidden.length; ++i) {
            App.addSaveRequest();
            Rest.patchRemoveHiddenLabel(item.id, message, remHidden[i], (label) => {
                var i = item.hiddenLabels.indexOf(label);
                if(i >= 0) {
                    item.hiddenLabels.splice(i, 1);
                }
                App.updateCachedItem(item);
                App.removeSaveRequest();
                Socket.sendEditSave(item.id, "hiddenLabels", item.hiddenLabels);
                callback();
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera alternativa benämningar");
                App.removeSaveRequest();
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
        }

    }

    onValueChanged(e) {
        this.setState({
            value: e.target.value,
        });
    }

    onAddClick() {
        if(this.state.value.trim().length > 0) {            
            this.state.data.unshift({
                name: this.state.value.trim(),
                hidden: false,                
            });
            this.setState({
                value: "",
                data: this.state.data
            });
            this.updateEnableSave();
        }
    }

    onHiddenChanged(item) {
        item.hidden = !item.hidden;
        this.updateEnableSave();
        this.forceUpdate();
    }

    onRemoveClick(item) {
        var ix = this.state.data.indexOf(item);
        if(ix >=0) {
            this.state.data.splice(ix, 1);
            this.updateEnableSave();
            this.forceUpdate();
        }
    }

    renderItem(item) {
        return(
            <div className="edit_concept_row edit_concept_alternate_item">
                <input 
                    type="checkbox"                        
                    onChange={this.onHiddenChanged.bind(this, item)}
                    checked={item.hidden}/>
                <div>{item.name}</div>
                <Button 
                    onClick={this.onRemoveClick.bind(this, item)}
                    text="Ta bort"/>
            </div>
        );
    }

    renderErrorText() {
        if(this.state.isNameInvalid) {
            return (
                <div className="edit_concept_error_text font">
                    {this.state.isDuplicateDeprecated ? Localization.get("name_invalid_deprecated") : Localization.get("name_invalid")}{" (" + this.state.idOfDuplicate + ")"}
                </div>
            );
        }
    }

    render() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Ange alternativa/dolda benämningar av begrepp"/> 
                <div className="edit_concept_row edit_concept_alternate_add"> 
                    <input
                        className="rounded"
                        value={this.state.value}
                        onChange={this.onValueChanged.bind(this)}/>
                    <Button 
                        isEnabled={this.addButtonIsEnabled()}
                        text="Lägg till"
                        onClick={this.onAddClick.bind(this)}/>
                </div> 
                <Label text="Kryssa i kryssrutan för att markera som dold benämning"/>
                <List 
                    css="edit_concept_list"
                    data={this.state.data}
                    dataRender={this.renderItem.bind(this)}
                    noItemSelect={true}>                   
                </List>               
            </div>
        );
    }
}

export default EditConceptAlternativeLabel;