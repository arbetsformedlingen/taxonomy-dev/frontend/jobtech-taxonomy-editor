import React from 'react';
import Label from './../../control/label.jsx';
import App from './../../context/app.jsx';
import Socket from './../../context/socket.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Constants from './../../context/constants.jsx';
import Util from './../../context/util.jsx';
import ConceptEdit from './../../context/concept_edit.jsx';

class EditConceptDefinition extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.item.definition,
            isChanged: false,
        };
        this.props.editContext.onSave = this.onSave.bind(this);
    }

    onSave(message, callback) {
        var item = this.props.item;
        App.addSaveRequest();
        ConceptEdit.definition(item.id, this.state.value, message, () => {
            item.definition = this.state.value;
            callback();
        }, () => {
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    onValueChanged(e) {
        var isChanged = e.target.value != this.props.item.definition;
        if(isChanged != this.state.isChanged) {
            this.props.editContext.setEnableSave(isChanged);
        }
        this.setState({
            value: e.target.value,
            isChanged: isChanged,
        });
    }

    render() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Ange definition på begrepp"/>
                <textarea
                    rows="10" 
                    className="rounded"
                    value={this.state.value}
                    onChange={this.onValueChanged.bind(this)}/>
            </div>
        );
    }
}

export default EditConceptDefinition;