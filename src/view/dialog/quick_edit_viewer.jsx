import React from 'react';
import QuickEditComment from './quick_edit_comment.jsx';
import QuickEditItem from './quick_edit_item.jsx';
import App from './../../context/app.jsx';
import Rest from './../../context/rest.jsx';
import Socket from './../../context/socket.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Util from './../../context/util.jsx';
import ConceptEdit from './../../context/concept_edit.jsx';
import Button from './../../control/button.jsx';

class QuickEditViewer extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            isSaving: false,
            groups: [],
            selected: [],
        };
    }

    componentDidMount() {
        this.setupGroups(App.getQuickEdits().getEdits());
    }

    setupGroups(data) {
        Util.sortByKey(data, "date", 1);
        var groups = [];
        var addToGroup = (item) => {
            var date = new Date(item.date);
            var year = date.getFullYear();
            var month = date.getMonth();
            var day = date.getDay();
            var query = year + "-" + month + "-" + day;
            var group = groups.find((x) => {
                return x.id == query;
            });
            if(group == null) {
                group = {
                    date: date,
                    id: query,
                    items: []
                };
                groups.push(group);
            }
            group.items.push(item);
        };
        for(var i=0; i<data.length; ++i) {
            addToGroup(data[i]);
        }
        this.setState({groups: groups});
    }

    removeFromSelection(item) {
        var index = this.state.selected.indexOf(item);
        if(index != -1) {
            this.state.selected.splice(index, 1);
        }
    }

    save(item, onProcessed) {
        var boundSuccess = onProcessed.bind(this, true);
        var boundError = onProcessed.bind(this, false);
        if(item.type == Constants.QUICK_EDIT_NAME) {
            ConceptEdit.name(item.concept.id, item.value, item.comment, boundSuccess, boundError);
        } else if(item.type == Constants.QUICK_EDIT_DEFINITION) {
            ConceptEdit.definition(item.concept.id, item.value, item.comment, boundSuccess, boundError);
        } else if(item.type == Constants.QUICK_EDIT_ALTERNATIVE_LABEL) {
            ConceptEdit.alternativeLabels(item.concept.id, item.originalValue, item.value, item.comment, boundSuccess, boundError);
        } else if(item.type == Constants.QUICK_EDIT_HIDDEN_LABEL) {
            ConceptEdit.hiddenLabels(item.concept.id, item.originalValue, item.value, item.comment, boundSuccess, boundError);
        } else if(item.type == Constants.QUICK_EDIT_QUALITY_LEVEL) {
            ConceptEdit.qualityLevel(item.concept.id, item.value, item.comment, boundSuccess, boundError);
        } else if(item.type == Constants.QUICK_EDIT_ADDED_RELATION) {
            var from = App.getRecentlyTouchedConcept(item.value.from);
            var to = App.getRecentlyTouchedConcept(item.value.to);
            ConceptEdit.addRelation(from, to, item.value.relationType, item.value.substitutability, item.comment, boundSuccess, boundError);
        } else if(item.type == Constants.QUICK_EDIT_REMOVED_RELATION) {
            ConceptEdit.removeRelation(item.value.from, item.value.to, item.value.relationType, item.comment, boundSuccess, boundError);
        } else if(item.type == Constants.QUICK_EDIT_NO_ESCO_RELATION) {            
            ConceptEdit.noEscoRelation(item.concept.id, item.value, item.comment, boundSuccess, boundError);
        } else {
            App.showError("Misslyckades spara snabbredigering, okänd typ \"" + item.type + "\"");
            App.removeSaveRequest();
            onProcessed(false);
        }
    }

    pushChanges() {
        var concepts = [];
        var pushChange = (index) => {
            var count = this.state.selected.length;
            if(index < count) {
                EventDispatcher.fire(Constants.EVENT_SET_POPUP_MESSAGE, Localization.get("saving") + " " + (index + 1) + "/" + count + "...");
                var item = this.state.selected[index];
                this.save(item, (success, status) => {
                    if(success) {
                        var cached = concepts.find((x) => x.id == item.concept.id);
                        if(cached) {
                            Util.applyQuickEdit(cached, item);
                            App.updateCachedItem(cached);
                        }
                        App.getQuickEdits().remove(item, true);
                        item.processed = true;
                        this.forceUpdate();
                    } else if(status == 409) {
                        App.getQuickEdits().remove(item, true);
                        item.processed = true;
                        this.forceUpdate();
                    }
                    pushChange(index + 1);
                });
            } else {
                // remove processed items
                this.state.selected = this.state.selected.filter((x) => {
                    return x.processed == null;
                });
                this.setState({isSaving: false});
                EventDispatcher.fire(Constants.EVENT_QUICK_EDIT_SAVED);
            }
        };
        // find all touched concepts and setup save popup
        for(var i=0; i<this.state.selected.length; ++i) {
            App.addSaveRequest();
            if(concepts.find((x) => x.id == this.state.selected[i].concept.id) == null) {
                concepts.push(App.getCachedConceptFromId(this.state.selected[i].concept.id));
            }
        }
        EventDispatcher.fire(Constants.EVENT_SHOW_SAVE_INDICATOR);
        // start saving
        this.setState({isSaving: true}, () => {
            pushChange(0);
        });
    }

    onItemSelected(item) {
        this.state.selected.push(item);
        this.forceUpdate();
    }
    
    onItemDeselected(item) {
        this.removeFromSelection(item);
        this.forceUpdate();
    }

    onItemRemoved(item) {
        var qe = App.getQuickEdits();
        qe.remove(item);
        this.removeFromSelection(item);
        this.setupGroups(qe.getEdits());
    }

    onCloseClicked() {
        if(this.props.onClosed) {
            this.props.onClosed();
        }
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onSaveClicked() {
        var uncommented = this.state.selected.filter((x) => {
            return x.comment == null;
        });
        if(uncommented.length > 0) {
            var tmp = {};
            var callback = (isSaved) => {
                if(isSaved) {
                    for(var i=0; i<uncommented.length; ++i) {
                        uncommented[i].comment = tmp.comment;
                    }       
                    this.pushChanges();
                }
            };
            EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
                title: "Gruppkommentar",
                content: <QuickEditComment 
                            isGroup={true}
                            groupText={"" + uncommented.length + " av " + this.state.selected.length + " förändringar saknar kommentar. Här kan du ange en kommentar för samtliga förändringar som saknar en kommentar."}
                            item={tmp}
                            onClosed={callback}/>
            });
        } else {
            this.pushChanges();
        }
    }

    onRemoveSelectedClicked() {
        var qe = App.getQuickEdits();
        for(var i=0; i<this.state.selected.length; ++i) {
            qe.remove(this.state.selected[i]);
        }
        this.state.selected = [];
        this.setupGroups(qe.getEdits());
    }

    renderGroup(group, key) {
        var items = group.items.filter((x) => {
            return x.processed == null;
        });
        items = items.map((element, index) => {
            return <QuickEditItem 
                        key={index}
                        item={element} 
                        isEnabled={!this.state.isSaving}
                        onSelected={this.onItemSelected.bind(this, element)}
                        onDeselected={this.onItemDeselected.bind(this, element)}
                        onRemoved={this.onItemRemoved.bind(this, element)}/>
        });
        return (
            <div className="quick_edit_container" key={key}>
                <div className="quick_edit_date_separator">
                    <div/>
                    <div>{group.date.toLocaleDateString()}</div>
                    <div/>
                </div>
                <div className="quick_edit_group_content">
                    {items}
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="dialog_content edit_concept_dialog edit_concept_dialog_page">
                <div className="quick_edit_container quick_edit_list">
                    {this.state.groups.map(this.renderGroup.bind(this))}
                </div>
                <div className="quick_edit_buttons">
                    <div>
                        <Button
                            text={"Markera alla"}
                            isEnabled={!this.state.isSaving}
                            onClick={EventDispatcher.bindEvent(Constants.EVENT_QUICK_EDIT_SELECT_ALL)}/>
                        <Button
                            text={"Avmarkera alla"}
                            isEnabled={!this.state.isSaving}
                            onClick={EventDispatcher.bindEvent(Constants.EVENT_QUICK_EDIT_DESELECT_ALL)}/>
                        <Button
                            text={Localization.get("remove_selected")}
                            isEnabled={this.state.selected.length > 0 && !this.state.isSaving}
                            onClick={this.onRemoveSelectedClicked.bind(this)}/>
                    </div>
                    <div>
                        <Button
                            text={Localization.get("close")}
                            isEnabled={!this.state.isSaving}
                            onClick={this.onCloseClicked.bind(this)}/>
                        <Button
                            text={Localization.get("save")}
                            isEnabled={this.state.selected.length > 0 && !this.state.isSaving}
                            onClick={this.onSaveClicked.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default QuickEditViewer;