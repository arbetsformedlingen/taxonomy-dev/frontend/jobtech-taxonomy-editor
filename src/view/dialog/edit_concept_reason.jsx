import React from 'react';
import Label from './../../control/label.jsx';
import Localization from './../../context/localization.jsx';

class EditConceptReason extends React.Component { 

    constructor(props) {
        super(props);
        var head = "";
        var body = "";
        if(props.initialMessage) {
            if(props.useTitle) {            
                head = props.initialMessage;
                var linebreak = head.indexOf('\n');
                if(linebreak > 0) {
                    body = head.substring(linebreak + 1).trim();
                    head = head.substring(0, linebreak);
                }
            } else {
                body = props.initialMessage;
            }
        }
        this.state = {
            head: head,
            body: body,
            isChanged: head.length > 0 || body.length > 0 ? true : false,
        };
    }

    componentDidMount() {
        if(this.state.isChanged) {
            this.updateContext();
            this.props.editContext.setEnableSave(true);
        }
    }

    updateContext() {
        if(this.props.useTitle) {
            this.props.editContext.message = this.state.head;
            if(this.state.body.length > 0) {
                this.props.editContext.message += "\n" + this.state.body;
            }
        } else {
            this.props.editContext.message = this.state.body;
        }
    }

    onTitleChanged(e) {
        var isChanged = e.target.value.length > 0;
        if(isChanged != this.state.isChanged) {
            this.props.editContext.setEnableSave(isChanged);
		}
        this.setState({
			head: e.target.value,
			isChanged: isChanged,
		}, () => {
            this.updateContext();
        });
    }

    onBodyChanged(e) {
        var isChanged = this.state.isChanged;
        if(!this.props.useTitle) {
            isChanged = e.target.value.length > 0;
            if(isChanged != this.state.isChanged) {
                this.props.editContext.setEnableSave(isChanged);
            }
        }
        this.setState({
			body: e.target.value,
            isChanged: isChanged,
		}, () => {
            this.updateContext();
        });
    }

    render() {
        return (
            <div className="dialog_value_group">
                {this.props.useTitle &&
                <Label 
                    css="dialog_value_title"
                    text={"Ange rubrik för anteckning"}/>
                }
                {this.props.useTitle &&
                <input
                    className="rounded dialog_bottom_margin"
                    value={this.state.head}
                    onChange={this.onTitleChanged.bind(this)}/>
                }
                <Label 
                    css="dialog_value_title"
                    text={Localization.get("change_note")}/>
                <textarea 
                    rows="6" 
                    className="rounded"
                    value={this.state.body}
                    onChange={this.onBodyChanged.bind(this)}/>
            </div>
        );
    }
}

export default EditConceptReason;