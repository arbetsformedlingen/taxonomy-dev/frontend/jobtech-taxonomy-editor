import React from 'react';
import Label from './../../control/label.jsx';
import List from './../../control/list.jsx';
import App from './../../context/app.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Util from './../../context/util.jsx';

class EditConceptSetReference extends React.Component { 

    constructor(props) {
        super(props);
        var replacedBy = this.props.item["replaced-by"];
        this.state = {
            defaultValue: replacedBy != null ? replacedBy[0] : null,
            selected: replacedBy != null ? replacedBy[0] : null,
            items: [],
            filter: "",
        };
        this.props.editContext.onSave = this.onSave.bind(this);
    }

    componentDidMount() {
        var data = JSON.parse(JSON.stringify(App.getCachedTypes(this.props.item.type)));
        data = data.filter((item) => {
            return !item.deprecated && item.id != this.props.item.id;
        });
        Util.sortByKey(data, "label", true);
        this.setState({items: data});
    }

    onSave(message, callback) {
        var replace = () => {
            App.addSaveRequest();
            Rest.postReplaceConcept(this.props.item.id, this.state.selected.id, message, () => {
                this.props.item["replaced_by"] = [this.state.selected];
                if(App.removeSaveRequest()) {
                    callback();
                }
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades hänvisa begrepp");
                App.removeSaveRequest();
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
        };
        var remove = (references, index) => {
            App.addSaveRequest();
            Rest.postUnreplaceConcept(this.props.item.id, references[index].id, message, () => {
                App.removeSaveRequest();
                if(index < references.length - 1) {
                    remove(references, index + 1);
                } else {
                    replace();
                }
            }, (status) => {
                App.showError(Util.getHttpMessage(status) + " : misslyckades hänvisa begrepp");
                App.removeSaveRequest();
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
        };
        var references = this.props.item["replaced_by"];
        if(references && references.length) {
            remove(references, 0);
        } else {
            replace();
        }
    }

    onFilterChanged(e) {
        var filter = e.target.value.trim().toLowerCase();
        var items = this.state.items;
        for(var i=0; i<items.length; ++i) {
            items[i].visible = items[i].label.toLowerCase().indexOf(filter) != -1;
        }
        this.setState({
            filter: e.target.value,
            items: items,
        });
    }

    onReferenceSelected(item) {
        this.props.editContext.setEnableSave(item != this.state.defaultValue);
        this.setState({selected: item});
    }

    render() {
        var items = this.state.items.filter((item) => {
            return item.visible == null || item.visible;
        });
        items = items.map((item, index) => {
            var id = this.state.selected ? this.state.selected.id : null;
            var css = id == item.id ? "deprecated_reference_selected" : "";
            return (
                <div 
                    key={index}
                    className={css}
                    onMouseUp={this.onReferenceSelected.bind(this, item)}>
                    {item.preferredLabel}
                </div>
            );
        });
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Välj det begrepp som hänvisningen går till"/>
                <div className="edit_concept_row">
                    <input 
                        type="text"
                        className="rounded"
                        value={this.state.filter}
                        placeholder={Localization.get("filter")}
                        onChange={this.onFilterChanged.bind(this)}/>
                </div>
                <List css="deprecated_reference_list">
                    {items}
                </List>
            </div>
        );
    }
}

export default EditConceptSetReference;