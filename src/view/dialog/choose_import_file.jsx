import React from 'react';
import ExcelLib from 'exceljs';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from '../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Button from './../../control/button.jsx';
import Loader from './../../control/loader.jsx';

class SearchChanges extends React.Component { 

    constructor(props) {
        super(props);        
        this.state = {
            isImporting: false
        };
        this.boundGlobalDragEnter = this.onGlobalDragEnter.bind(this);
        this.boundGlobalDragOver = this.onGlobalDragOver.bind(this);
        this.boundGlobalDrop = this.onGlobalDrop.bind(this);
    }

    componentDidMount() {
        this.enableFileDrag();
    }

    componentWillUnmount() {
        this.disableFileDrag();
    }

    enableFileDrag() {
        window.addEventListener("dragenter", this.boundGlobalDragEnter, false);
        window.addEventListener("dragover", this.boundGlobalDragOver);
        window.addEventListener("drop", this.boundGlobalDrop);
    }

    disableFileDrag() {
        window.removeEventListener("dragenter", this.boundGlobalDragEnter);
        window.removeEventListener("dragover", this.boundGlobalDragOver);
        window.removeEventListener("drop", this.boundGlobalDrop);
    }

    async readFile(file) {
        if(file.name.endsWith(".xlsx")) {
            var workbook = new ExcelLib.Workbook();
            var reader = new FileReader();
            reader.readAsArrayBuffer(file);
            reader.onload = () => {
                workbook.xlsx.load(reader.result).then((workbook) => {
                    var worksheet = workbook.getWorksheet(1);
                    var values = [];
                    worksheet.eachRow({ includeEmpty: false }, (row, rowNumber) => {
                        var value = row.values[1];
                        var state = row.values[2];
                        var comment = row.values[3];
                        if(state == undefined || state == null || state.length == 0){
                            state = "";
                        }
                        if(comment == undefined || comment == null || comment.length == 0){
                            comment = "";
                        }
                        if(value != undefined && value != null && value.length > 0) {
                            var item = {
                                name: value,
                                trash: false,
                                state: state,
                                comment: comment,
                            };
                            values.push(item);
                        }
                    });
                    EventDispatcher.fire(Constants.EVENT_IMPORT_DATA, {
                        name: file.name,
                        data: values,
                    });
                    EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
                });
            };
        } else {
            this.setState({
                error: "Ogiltigt filformat, import understödjer endast .xlsx",
                isImporting: false
            });
        }
    }

    onGlobalDragEnter(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onGlobalDragOver(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }
    
    onGlobalDrop(e) {
        if(e.target.id != "dropzone") {
            e.preventDefault();
            e.dataTransfer.effectAllowed = "none";
            e.dataTransfer.dropEffect = "none";
        }
    }

    onDragEnter(e) {
        e.preventDefault();
    }

    onDragOver(e) {
        e.preventDefault();
    }

    onDrop(e) {
        e.preventDefault();
        var file = e.dataTransfer.files[0];
        this.setState({
            error: null,
            isImporting: true,
        }, () => {
            this.readFile(file);
        });
    }

    onFilePicker(e) {
        var file = e.target.files[0];      
        this.setState({
            error: null,
            isImporting: true,
        }, () => {
            this.readFile(file);
        });
    }

    onChooseFileClicked() {
        var filePicker = document.getElementById("file_picker");
        filePicker.click();
    }

    onCloseClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    renderDropzone() {
        if(!this.state.isImporting) {
            return(
                <div 
                    className="choose_import_dropzone"
                    id="dropzone">
                    {Localization.get("drop_files_here")}
                </div> 
            );
        }
    }

    renderImportingInProgress() {
        if(this.state.isImporting) {
            return(
                <Loader
                    text={Localization.get("importing") + "..."}/>
            );
        }
    }

    render() {
       
        return (
            <div className="dialog_content">
                <div
                    className="choose_import_drag_drop"
                    id="drag_drop"
                    accept=".xlsx"
                    onDragEnter={this.onDragEnter.bind(this)}
                    onDragOver={this.onDragOver.bind(this)}
                    onDrop={this.onDrop.bind(this)}>
                        {this.renderDropzone()}
                        {this.renderImportingInProgress()}                                           
                </div>
                {this.state.error != null &&
                    <div className="import_file_error">{this.state.error}</div>
                }
                <div className="dialog_content_buttons">
                    <Button
                        isEnabled={!this.state.isImporting}
                        text={Localization.get("choose_file")}
                        onClick={this.onChooseFileClicked.bind(this)}
                    />
                    <Button 
                        text={Localization.get("close")}
                        onClick={this.onCloseClicked.bind(this)}
                    />
                </div>
                <input 
                    id="file_picker" 
                    className="choose_import_button_file_picker" 
                    type="file"
                    accept=".xlsx"
                    onChange={this.onFilePicker.bind(this)} />
            </div>
        );
    }
}

export default SearchChanges;