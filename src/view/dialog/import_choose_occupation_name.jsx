import React from 'react';
import Button from './../../control/button.jsx';
import Label from './../../control/label.jsx';
import TreeView from './../../control/tree_view.jsx';
import ControlUtil from './../../control/util.jsx';
import App from './../../context/app.jsx';
import Util from './../../context/util.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';

class ImportChooseOccupationName extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            filter: "",
            selected: null,
            message: "",
        }
        this.treeView = ControlUtil.createTreeView();
        this.treeView.onItemSelected = this.onItemSelected.bind(this);
        this.state.items.push({
            visible: true,
            preferredLabel: Localization.getTypeName(Constants.CONCEPT_OCCUPATION_NAME),
            children: Util.sortByKey(JSON.parse(JSON.stringify(App.getCachedTypes(Constants.CONCEPT_OCCUPATION_NAME))), "label", true)});

        for(var i=0; i<this.state.items.length; ++i) {
            var item = this.state.items[i];
            for(var k=0; k<item.children.length; ++k) {
                var child = item.children[k];
                child.visible = true;
                if(child.skills) {
                    for(var j=0; j<child.skills.length; ++j) {
                        child.skills[j].visible = true;
                    }
                }
                if(child.narrower) {
                    for(var j=0; j<child.narrower.length; ++j) {
                        child.narrower[j].visible = true;
                    }
                }
            }
        }
    }

    componentDidMount() {
        this.populateTree(this.state.items, false);
    }

    populateTree(data, shouldExpand) {
        this.treeView.clear();
        this.treeView.shouldUpdateState = false;
        // add roots
        for(var i=0; i<data.length; ++i) {
            var root = data[i];
            if(!root.visible) {
                continue;
            }
            var rootNode = ControlUtil.createTreeViewItem(this.treeView, root);
            rootNode.setText(root.preferredLabel);
            rootNode.setExpanded(shouldExpand);
            // setup children
            for(var k=0; k<root.children.length; ++k) {
                var item = root.children[k];
                if(!item.visible) {
                    continue;
                }
                var childNode = ControlUtil.createTreeViewItem(this.treeView, item);
                
                childNode.setText(item.label);
                childNode.setExpanded(shouldExpand);

                if(item.skills) {
                    for(var j=0; j<item.skills.length; ++j) {
                        if(!item.skills[j].visible) {
                            continue;
                        }
                        var skillNode = ControlUtil.createTreeViewItem(this.treeView, item.skills[j]);
                        skillNode.setText(item.skills[j].label);
                        skillNode.setExpanded(shouldExpand);
                        childNode.addChild(skillNode);
                    }
                }
                if(item.narrower) {
                    for(var j=0; j<item.narrower.length; ++j) {
                        if(!item.narrower[j].visible) {
                            continue;
                        }
                        var occupationNode = ControlUtil.createTreeViewItem(this.treeView, item.narrower[j]);
                        occupationNode.setText(item.narrower[j].label);
                        occupationNode.setExpanded(shouldExpand);
                        childNode.addChild(occupationNode);
                    }
                }

                rootNode.addChild(childNode);
            }
            // add node to tree
            this.treeView.addRoot(rootNode);
        }
        this.treeView.shouldUpdateState = true;
    }
    
    onItemSaved() {
        console.log(item);
        //set property on item
        this.props.item.alternateLabelCreatedOn = selected.data;
        if(this.props.onItemUpdated) {
            this.props.onItemUpdated();
        }
        this.props.onItemUpdated();
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onItemCreated(item) {
        console.log(item);
        //set property on item
        this.props.item.itemCreated = true;
        if(this.props.onItemUpdated) {
            this.props.onItemUpdated();
        }
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    onItemSelected(item) {        
        this.setState({selected: item.data});
    }

    onFilterChanged(e) {
        var filter = e.target.value.trim().toLowerCase();
        var filterChildren = (children) => {
            var found = false;
            for(var i=0; i<children.length; ++i) {
                var child = children[i];
                child.visible = false;
                if(child.label.toLowerCase().indexOf(filter) != -1 || 
                   child.id.toLowerCase() === filter) {
                    child.visible = true;
                    found = true;
                }
                if(child.skills) {
                    for(var k=0; k<child.skills.length; ++k) {
                        var skill = child.skills[k];
                        skill.visible = false;
                        if(skill.label.toLowerCase().indexOf(filter) != -1 ||
                           skill.id.toLowerCase() === filter) {
                            child.visible = true;
                            skill.visible = true;
                            found = true;
                        }
                    }
                }
                if(child.narrower) {
                    for(var k=0; k<child.narrower.length; ++k) {
                        var occupationName = child.narrower[k];
                        occupationName.visible = false;
                        if(occupationName.label.toLowerCase().indexOf(filter) != -1 ||
                        occupationName.id.toLowerCase() === filter) {
                            child.visible = true;
                            occupationName.visible = true;
                            found = true;
                        }
                    }
                }
            }
            return found;
        };
        for(var i=0; i<this.state.items.length; ++i) {
            this.state.items[i].visible = filterChildren(this.state.items[i].children);
        }
        this.populateTree(this.state.items, filter.length > 1);
        this.setState({filter: e.target.value});
    }

    onAddLabelClicked() {
        this.props.onChoosenOccupationName(this.state.selected);
    }

    render() {
        return (
            <div className="dialog_content"> 
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Alternativ yrkesbenämning"/> 
                    <div>
                        <input 
                            type="text"
                            className="rounded"
                            value={this.state.filter}
                            placeholder={Localization.get("filter")}
                            onChange={this.onFilterChanged.bind(this)}/>
                    </div>
                    <TreeView 
                        css="import_work_with_tree_view"
                        context={this.treeView}/>
                    <div className="dialog_content_buttons">                        
                        <Button 
                            isEnabled={this.state.selected != null && this.state.selected.type != null}
                            onClick={this.onAddLabelClicked.bind(this)}
                            text={Localization.get("add")}/>
                    </div>
                </div>

                <div className="dialog_content_buttons">
                    <Button 
                        onClick={() => EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY)}
                        text={Localization.get("abort")}/>
                </div>
            </div>
        );
    }
}

export default ImportChooseOccupationName;