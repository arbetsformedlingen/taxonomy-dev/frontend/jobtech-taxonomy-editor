import React from 'react';
import Button from './../../control/button.jsx';
import Label from './../../control/label.jsx';
import TreeView from './../../control/tree_view.jsx';
import ControlUtil from './../../control/util.jsx';
import ContextUtil from './../../context/util.jsx';
import App from './../../context/app.jsx';
import Settings from './../../context/settings.jsx';
import Socket from './../../context/socket.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import ConceptEdit from './../../context/concept_edit.jsx';

class EditConceptAddRelation extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            groups: [],
        };
        this.props.editContext.onSave = this.onSave.bind(this);
        this.items = [];
    }

    componentDidMount() {
        var data = [];
        if(this.props.editContext.lockToType) {
            for(var i=0; i<App.cachedTypes.length; ++i) {
                if(this.props.editContext.lockToType == App.cachedTypes[i].type) {
                    this.setupDataItem(App.cachedTypes[i]);
                    data.push(App.cachedTypes[i]);
                }
            }
        } else {
            for(var i=0; i<App.cachedTypes.length; ++i) {
                this.setupDataItem(App.cachedTypes[i]);
                data.push(App.cachedTypes[i]);
            }
        }
        ContextUtil.sortByKey(data, "preferredLabel", true);
        this.items = data;
        this.limitSsykIsco = this.props.item.type == "occupation-name";
        if(this.limitSsykIsco) {
            this.hasSsyk = this.hasSsykRelation();
            this.hasIsco = this.hasIscoRelation();
        } else {
            this.hasSsyk = false;
            this.hasIsco = false;
        }
        if(this.props.targetRelation) {
            this.onAddClicked(false, this.props.targetRelation);
        }
        if(this.props.editContext.meta && this.props.editContext.meta.semanticType) {
            this.onAddClicked(true);
            var group = this.state.groups[this.state.groups.length - 1];
            var ev = {
                target: {
                    value: this.props.editContext.meta.semanticType
                }
            };
            this.onSemanticTypeChanged(group, ev);
        }
    }

    getOtherGroups(group) {
        var groups = [];
        for(var i=0; i<this.state.groups.length; ++i) {
            if(this.state.groups[i] != group) {
                groups.push(this.state.groups[i]);
            }
        }
        return groups;
    }

    setupDataItem(data, sortKey) {
        var key = null;
        if(data.type.startsWith("ssyk")) {
            key = "ssyk_code_2012";
        } else if(data.type.startsWith("isco")) {
            key = "isco_code_08";
        }
        data.visible = true;
        for(var k=0; k<data.children.length; ++k) {
            var child = data.children[k];
            child.visible = true;
            if(key) {
                child.label = child[key] + " - " + child.preferredLabel;
            }
            if(child.skills) {
                for(var j=0; j<child.skills.length; ++j) {
                    child.skills[j].visible = true;
                }
                ContextUtil.sortByKey(child.skills, "label", true);
            }
        }
        ContextUtil.sortByKey(data.children, sortKey ? sortKey : "label", true);
    }

    hasSsykRelation() {
        return this.checkForRelationProp((x) => {
            return x.type == "ssyk-level-4";
        });
    }
    
    hasIscoRelation() {
        return this.checkForRelationProp((x) => {
            return x.type == "isco-level-4";
        });
    }

    hasRelation(id) {
        return this.checkForRelationProp((x) => {
            return x.id == id;
        });
    }

    getPreferredRelationType(group) {
        var groupType = group.selected.length > 0 ? group.selected[0].type : null;
        for(var i=1; i<group.selected.length; ++i) {
            if(group.selected[i].type != groupType) {
                groupType = null;
                break;
            }
        }
        if(groupType != null) {
            return Constants.getRelationType(this.props.item.type, groupType);
        }
        return null
    }

    updateSaveButton() {
        var canSave = (this.state.groups.length == 0 && this.props.saveDefaultTrue == null) ? false : true;
        for(var i=0; i<this.state.groups.length; ++i) {
            if(this.state.groups[i].selected.length == 0) {
                canSave = false;
                break;
            }
        }
        this.props.editContext.setEnableSave(canSave);
    }

    checkForRelationProp(func) {
        var broaderId = this.props.item.broader == null ? null : this.props.item.broader.find(func);
        var narrowerId = this.props.item.narrower == null ? null : this.props.item.narrower.find(func);
        var relatedId = this.props.item.related == null ? null : this.props.item.related.find(func);
        return broaderId != null || narrowerId != null || relatedId != null;
    }

    checkGroupConnectionForType(group, type) {
        return group.selected.find((x) => {
            return x.type == type;
        }) != null;
    }

    renderTreeLabel(group, item, node) {
        return (
            <div className="edit_concept_row">
                <input 
                    id={"check_" + item.id + "_" + group.id}
                    type="checkbox"
                    value={node.checkState}
                    defaultChecked={node.checkState}
                    onChange={this.onCheckChanged.bind(this, group, item, node)}/>
                <label htmlFor={"check_" + item.id + "_" + group.id}>
                    {item.label}
                </label>
            </div>
        );
    }

    setupTreeView(group, queryTreeView, data, shouldExpand) {
        var hasItem = (item) => {
            return group.selected.indexOf(item) != -1;
        };
        // clear previous content
        queryTreeView.clear();
        queryTreeView.shouldUpdateState = false;
        // add roots
        for(var i=0; i<data.length; ++i) {
            var root = data[i];
            if(!root.visible) {
                continue;
            }
            var rootNode = ControlUtil.createTreeViewItem(queryTreeView, root);
            rootNode.setText(root.preferredLabel);
            rootNode.setExpanded(shouldExpand);
            rootNode.selectable = false;
            rootNode.showingSelection = false;
            if(root.type == "ssyk-level-4") {
                group.ssykRootNode = rootNode;
            } else if(root.type == "isco-level-4") {
                group.iscoRootNode = rootNode;
            }
            // setup children
            for(var k=0; root.children && k<root.children.length; ++k) {
                if(!root.children[k].visible) {
                    continue;
                }
                var childNode = ControlUtil.createTreeViewItem(queryTreeView, root.children[k]);
                var type = root.children[k].type;
                if((type.startsWith("ssyk-level-4") && this.hasSsyk) || 
                   (type.startsWith("isco-level-4") && this.hasIsco)) {
                    childNode.disabled = true;
                    childNode.tooltip = Localization.get("tip_relation_sinlge_requirment");
                } else if(this.hasRelation(root.children[k].id)) {
                    childNode.disabled = true;
                    childNode.tooltip = Localization.get("tip_relation_exists");
                }
                //childNode.setText(root.children[k].label);
                childNode.checkState = hasItem(root.children[k]);
                childNode.labelRender = this.renderTreeLabel.bind(this, group, root.children[k], childNode);
                childNode.selectable = false;
                childNode.showingSelection = false;
                childNode.setExpanded(shouldExpand);

                if(root.children[k].skills) {
                    var headline = root.children[k];
                    for(var j=0; j<headline.skills.length; ++j) {
                        if(!headline.skills[j].visible) {
                            continue;
                        }
                        var skillNode = ControlUtil.createTreeViewItem(queryTreeView, headline.skills[j]);
                        if(this.hasRelation(headline.skills[j].id)) {
                            skillNode.disabled = true;
                            skillNode.tooltip = Localization.get("tip_relation_exists");
                        }
                        //skillNode.setText(headline.skills[j].label);
                        skillNode.checkState = hasItem(headline.skills[j]);
                        skillNode.labelRender = this.renderTreeLabel.bind(this, group, headline.skills[j], skillNode);
                        skillNode.selectable = false;
                        skillNode.showingSelection = false;
                        skillNode.setExpanded(shouldExpand);
                        childNode.addChild(skillNode);
                    }
                }

                rootNode.addChild(childNode);
            }
            // add node to tree
            queryTreeView.addRoot(rootNode);
        }
        queryTreeView.shouldUpdateState = true;
        //this.queryTreeView.invalidate();
    }

    setGroupsSsykStatus(groups, newStatus) {
        for(var i=0; i<groups.length; ++i) {
            var g = groups[i];
            for(var j=0; j<g.ssykRootNode.children.length; ++j) {
                g.ssykRootNode.children[j].disabled = newStatus;
                g.ssykRootNode.children[j].refresh();
            }
        }
    }

    setGroupsIscoStatus(groups, newStatus) {
        for(var i=0; i<groups.length; ++i) {
            var g = groups[i];
            for(var j=0; j<g.iscoRootNode.children.length; ++j) {
                g.iscoRootNode.children[j].disabled = newStatus;
                g.iscoRootNode.children[j].refresh();
            }
        }
    }

    saveGroup(group, message, callback) {
        var sub = group.substitutability.trim();
        if(sub.length == 0) {
            sub = "0";
        }
        var from = this.props.item;
        for(var i=0; i<group.selected.length; ++i) {
            App.addSaveRequest();
        }
        for(var i=0; i<group.selected.length; ++i) {
            var to = group.selected[i];
            var type = group.type;
            ConceptEdit.addRelation(from, to, type, sub, message, () => {
                if(!App.hasPendingSaveRequests()) {
                    callback();
                }
            }, () => {
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            });
        }
    }

    addGroup(preSelected, isSemantic) {
        var group = {
            id: new Date().getTime(),
            selected: [],
            type: "broader",
            semanticType: "-",
            filter: "",
            substitutability: "25",
            isSemantic: isSemantic,
            isExpanded: false,
            iscoRootNode: null,
            ssykRootNode: null,
            items: isSemantic ? [] : JSON.parse(JSON.stringify(this.items)),
            queryTreeView: ControlUtil.createTreeView(),
            showDeprecated: false,
        };
        this.state.groups.push(group);
        this.runFilter(group);
        if(this.limitSsykIsco) {
            var groups = this.getOtherGroups(group);
            var hasSsyk = false;
            var hasIsco = false;
            for(var i=0; i<groups.length; ++i) {
                if(this.checkGroupConnectionForType(groups[i], "ssyk-level-4")) {
                    hasSsyk = true;
                }
                if(this.checkGroupConnectionForType(groups[i], "isco-level-4")) {
                    hasIsco = true;
                }
            }
            if(hasSsyk) {
                for(var i=0; i<group.ssykRootNode.children.length; ++i) {
                    group.ssykRootNode.children[i].disabled = true;
                }
            }
            if(hasIsco) {
                for(var i=0; i<group.iscoRootNode.children.length; ++i) {
                    group.iscoRootNode.children[i].disabled = true;
                }
            }
        }
        if(preSelected) {
            var items = group.items.find((x) => { return x.type == preSelected.type; });
            if(items) {
                var item = items.children.find((x) => { return x.id == preSelected.id; });
                if(item) {
                    group.selected.push(item);
                }
            }
        }
    }

    onShowDeprecatedChanged(group, e) {
        group.showDeprecated = e.target.checked;
        this.runFilter(group);
    }

    onAddClicked(isSemantic, preSelected) {
        this.addGroup(preSelected, isSemantic);
        this.updateSaveButton();
        this.forceUpdate();
    }

    onExpandClicked(group) {
        group.isExpanded = !group.isExpanded;
        this.forceUpdate();
    }
    
    onRemoveClicked(group) {
        var index = this.state.groups.indexOf(group);
        this.state.groups.splice(index, 1);
        if(this.limitSsykIsco) {
            var hadSsyk = this.checkGroupConnectionForType(group, "ssyk-level-4");
            var hadIsco = this.checkGroupConnectionForType(group, "isco-level-4");
            if(hadSsyk) {
                this.setGroupsSsykStatus(this.state.groups, false);
            }
            if(hadIsco) {
                this.setGroupsIscoStatus(this.state.groups, false);
            }
        }
        this.updateSaveButton();
        this.forceUpdate();
    }

    onCheckChanged(group, item, node, e) {
        var hadSsyk = this.checkGroupConnectionForType(group, "ssyk-level-4");
        var hadIsco = this.checkGroupConnectionForType(group, "isco-level-4");
        node.checkState = e.target.checked;
        if(e.target.checked) {
            group.selected.push(item);
        } else {
            var index = group.selected.indexOf(item);
            if(index != -1) {
                group.selected.splice(index, 1);
            }
        }
        if(this.limitSsykIsco) {
            var hasSsyk = this.checkGroupConnectionForType(group, "ssyk-level-4");
            var hasIsco = this.checkGroupConnectionForType(group, "isco-level-4");
            var groups = this.getOtherGroups(group);
            if(hasSsyk || hadSsyk) {
                this.setGroupsSsykStatus(groups, hasSsyk ? true : !hadSsyk);
                if(hasSsyk) {
                    this.setGroupsSsykStatus([group], true);
                    node.disabled = false;
                } else if(hadSsyk) {
                    this.setGroupsSsykStatus([group], false);
                }
            }
            if(hasIsco || hadIsco) {
                this.setGroupsIscoStatus(groups, hasIsco ? true : !hadIsco);
                if(hasIsco) {
                    this.setGroupsIscoStatus([group], true);
                    node.disabled = false;
                } else if(hadIsco) {
                    this.setGroupsIscoStatus([group], false);
                }
            }
        }
        var type = this.getPreferredRelationType(group);
        if(type) {
            group.type = type;
        }
        this.updateSaveButton();
        this.forceUpdate();
    }

    onSave(message, callback) {
        for(var i=0; i<this.state.groups.length; ++i) {
            this.saveGroup(this.state.groups[i], message, callback);
        }
    }

    onTypeSelected(group, e) {
        group.type = e.target.value;
        this.updateSaveButton();
        this.forceUpdate();
    }

    onSubstituabilityChanged(group, e) {
        group.substitutability = e.target.value;
        this.forceUpdate();
    }

    onFilterChanged(group, e) {
        group.filter = e.target.value.toLowerCase();
        this.runFilter(group);
    }

    onSemanticTypeChanged(group, e) {
        group.semanticType = e.target.value;
        if(group.semanticType == "-") {
            group.items = [];
            this.forceUpdate();
        } else {
            Rest.postSemanticSearch(group.semanticType, [this.props.item.preferredLabel], 30, (data) => {
                // setup data for rendering
                var children = data[this.props.item.preferredLabel];
                for(var i=0; i<children.length; ++i) {
                    children[i].label = children[i].preferredLabel;
                    children[i].visible = true;
                    children[i].deprecated = false;
                }
                // TODO: remove duplicates? (api fault)
                var roots = {
                    type: group.semanticType,
                    children: [{
                        visible: true,
                        preferredLabel: Localization.getTypeName(group.semanticType),
                        children: children
                    }]
                };
                this.setupDataItem(roots, "distance");
                group.items = roots.children;
                this.runFilter(group);
            }, () => {

            });
        }
    }

    runFilter(group) {
        var filter = group.filter;
        var showDeprecated = group.showDeprecated;
        var filterChildren = (children) => {
            var found = false;
            for(var i=0; children && i<children.length; ++i) {                
                var child = children[i];
                child.visible = false;
                if((child.label.toLowerCase().indexOf(filter) != -1 || 
                   child.id.toLowerCase() === filter) &&
                   child.deprecated == showDeprecated) {
                    child.visible = true;
                    found = true;
                } 
                if(child.skills) {
                    for(var k=0; k<child.skills.length; ++k) {
                        var skill = child.skills[k];
                        skill.visible = false;
                        if((skill.label.toLowerCase().indexOf(filter) != -1 ||
                           skill.id.toLowerCase() === filter) &&
                           skill.deprecated == showDeprecated) {
                            child.visible = true;
                            skill.visible = true;
                            found = true;
                        }
                    }
                }
            }
            return children ? found : true;
        };
        for(var i=0; i<group.items.length; ++i) {
            group.items[i].visible = filterChildren(group.items[i].children);
        }
        this.setupTreeView(group, group.queryTreeView, group.items, filter.length > 1);
        this.forceUpdate();
    }

    renderSemanticSearch(group) {
        var validTypes = App.cachedTypes.filter((x) => {
            return Settings.isVisible(x.type);
        });
        var types = ContextUtil.sortByKey(validTypes, "preferredLabel", true).map((element, key) => {
            return (
                <option 
                    key={key}
                    value={element.type}>
                    {element.preferredLabel}
                </option>
            );
        });
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Välj vilken typ av begrepp du vill ha rekommendationer för"/>
                <div className="edit_concept_row edit_concept_items_center">
                    <select
                        value={group.semanticType}
                        onChange={this.onSemanticTypeChanged.bind(this, group)}>
                        <option value="-">-- Välj typ --</option>
                        {types}
                    </select>
                </div>
            </div>
        );
    }

    renderConceptTree(group) {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text={Localization.get("edit_concept_relation_text")}/>
                <div className="edit_concept_row edit_concept_items_center">
                    <input 
                        type="text"
                        className="rounded"
                        value={group.filter}
                        placeholder={Localization.get("filter")}
                        onChange={this.onFilterChanged.bind(this, group)}/>
                    <input 
                        type="checkbox"                        
                        onChange={this.onShowDeprecatedChanged.bind(this, group)}
                        checked={group.showDeprecated}/>
                    <Label text={Localization.get("show_deprecated")}/>
                </div>
                <TreeView 
                    css="add_connection_tree"
                    context={group.queryTreeView}/>
            </div>
        );
    }

    renderTypeHint(group) {
        var type = this.getPreferredRelationType(group);
        if(type && type != group.type) {
            return (
                <div className="edit_concept_error_text font">
                    {Localization.get("edit_concept_recommended_relation")} "{type}"
                </div>
            );
            
        }
    }

    renderTypeList(group) {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Ange relationstyp till begreppet"/>
                <select 
                    className="rounded"
                    value={group.type == null ? "broader" : group.type}
                    onChange={this.onTypeSelected.bind(this, group)}>
                    <option value="broader">Broader</option>
                    <option value="related">Related</option>
                    <option value="narrower">Narrower</option>
                    <option value="substitutability">Substitutability</option>
                    <option value="broad-match">Broad match</option>
                    <option value="narrow-match">Narrow match</option>
                    <option value="close-match">Close match</option>
                    <option value="exact-match">Exact match</option>
                    <option value="possible-combinations">Possible combinations</option>
                    <option value="unlikely-combinations">Unlikely combinations</option>
                </select>
                {this.renderTypeHint(group)}
            </div>
        );
    }

    renderSubsitutability(group) {
        if(group.type == "substitutability") {
            return (
                <div className="dialog_value_group">
                    <Label 
                        css="dialog_value_title"
                        text="Ange värde"/>
                    <select 
                        className="rounded"
                        value={group.substitutability}
                        onChange={this.onSubstituabilityChanged.bind(this, group)}>
                        <option value="25">25 - Lägre släktskap</option>
                        <option value="75">75 - Högt släktskap</option>
                    </select>
                </div>
            );
        }
    }

    renderGroup(group, key) {
        var names = group.selected.map((element, index) => {
            return (
                <div
                    className="edit_concept_relation_name" 
                    key={index}>
                    {element.label}
                </div>
            );
        });
        return (
            <div 
                className="edit_concept_group_collection edit_concept_group_frame"
                key={key}>
                <div className="edit_concept_group_header">
                    <div 
                        className="font no_select"
                        onPointerUp={this.onExpandClicked.bind(this, group)}>
                        <img src={group.isExpanded ? Constants.ICON_EXPAND_UP : Constants.ICON_EXPAND_DOWN}/>
                        <div className="edit_concept_group_header_label">
                            {group.selected.length == 0 &&
                                <div>Ingen relation vald</div>
                            }
                            {group.selected.length != 0 &&
                                <div className="edit_concept_row">
                                    ({group.type}) {names}
                                </div>
                            }
                        </div>
                    </div>
                    <Button 
                        text={Localization.get("remove")}
                        onClick={this.onRemoveClicked.bind(this, group)}/>
                </div>
                {group.isExpanded &&
                    <div className="edit_concept_group_collection">
                        {group.isSemantic &&
                            this.renderSemanticSearch(group)
                        }
                        {this.renderConceptTree(group)}
                        {this.renderTypeList(group)}
                        {this.renderSubsitutability(group)}
                    </div>
                }
            </div>
        );
    }

    render() {
        var groups = this.state.groups.map((element, index) => {
            return this.renderGroup(element, index);
        });
        return (
            <div className="edit_concept_group_collection">
                {groups}
                <div className="edit_concept_group_collection edit_concept_group_frame">
                    <div className="edit_concept_group_header edit_concept_group_button_list">
                        <div className="font no_select"/>
                        {this.props.item.no_esco_relation &&
                            <b className="no_esco_relation_warning">
                                Begreppet är markerat med "Ingen ESCO relation"
                            </b>
                        }
                        <Button 
                            text={Localization.get("Lägg till från förslag")}
                            onClick={this.onAddClicked.bind(this, true)}/>
                        <Button 
                            text={Localization.get("Lägg till ny relation")}
                            onClick={this.onAddClicked.bind(this, false)}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditConceptAddRelation;