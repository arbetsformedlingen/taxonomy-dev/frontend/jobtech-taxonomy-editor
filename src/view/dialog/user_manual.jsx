import React from 'react';
import Button from './../../control/button.jsx';
import App from './../../context/app.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';

class UserManual extends React.Component { 

    constructor(props) {
        super(props);
        this.TAG_ALL = "__ALL__";
        this.TAG_CREATE = "create";
        this.TAG_REMOVE = "remove";
        this.TAG_SETTINGS = "settings";
        this.TAG_OTHER = "other";
        this.state = {
            filter: "",
            tags: [this.TAG_ALL],
            items: App.userManualItems,
            /*items: [{
                title: "Hur skapar jag ett nytt begrepp?",
                tags: [this.TAG_CREATE],
                text: 
                    "1. Navigera till den första taben <b>Värdefråd</b><br/>" +
                    "2. Längst ner under listan till vänster finns en knapp <b>Nytt begrepp</b>"
            }, {
                title: "Hur skapar jag en ny relation?",
                tags: [this.TAG_CREATE],
                text: 
                    <div>
                        1. Navigera till den första taben <b>Värdefråd</b><br/>
                        2. Navigera till det begrepp som relation ska utgå ifrån<br/>
                        3. Klicka på <b>Redigera</b> högst upp till höger<br/>
                        4. I dialogen som öppnas, under <b>Välj typ av ändring</b> välj <b>Ny relation</b>
                    </div>
            }, {
                title: "Hur tar jag bort / avaktualierar jag ett begrepp?",
                tags: [this.TAG_REMOVE],
                text: 
                    <div>
                        1. Navigera till den första taben <b>Värdefråd</b><br/>
                        2. Navigera till det begrepp som ska avaktualiseras<br/>
                        3. Klicka på <b>Redigera</b> högst upp till höger<br/>
                        4. I dialogen som öppnas, under <b>Välj typ av ändring</b> välj <b>Avaktualisera</b>
                    </div>
            }, {
                title: "Hur tar jag bort en relation från ett begrepp?",
                tags: [this.TAG_REMOVE],
                text: 
                    <div>
                        1. Navigera till den första taben <b>Värdefråd</b><br/>
                        2. Navigera till det begrepp som har relationen som ska tas bort<br/>
                        3. Klicka på <b>Redigera</b> högst upp till höger<br/>
                        4. I dialogen som öppnas, under <b>Välj typ av ändring</b> välj <b>Ta bort relation</b>
                    </div>
            }, {
                title: "Jag ser inte en typ begrepp",
                tags: [this.TAG_SETTINGS],
                text: 
                    <div>
                        1. Navigera till den femte taben <b>Inställningar</b><br/>
                        2. Klicka på <b>Synlighet</b><br/>
                        3. Kryssa i <b>Synlig</b> för de begrepp som du saknar
                    </div>
            }, {
                title: "Jag kan inte redigera ett begrepp",
                tags: [this.TAG_SETTINGS],
                text: 
                    <div>
                        1. Navigera till den femte taben <b>Inställningar</b><br/>
                        2. Klicka på <b>Synlighet</b><br/>
                        3. Kryssa i <b>Redigerbar</b> för de begrepp som du saknar
                    </div>
            }, {
                title: "Hur undantar jag ett begrepp / grupp från versionsvalidering?",
                tags: [this.TAG_SETTINGS],
                text: 
                    <div>
                        1. Navigera till den femte taben <b>Inställningar</b><br/>
                        2. Klicka på <b>Versions undantag</b> i menyn till vänster<br/>
                        3. Klicka på <b>Lägg till</b><br/>
                        4. I dialogen som öppnas, kryssa i den eller de begrepp som ska undantas<br/>
                        5. Klicka på <b>Spara</b><br/>
                        <br/>
                        Alternativt om begreppet du söker redan finns i listan för undantagna begrepp. <br/>
                        Kryssa i de begrepp som ska undantas.
                    </div>
            }, {
                title: "Hur publicerar jag en ny version?",
                tags: [this.TAG_OTHER],
                text: 
                    <div>
                        1. Navigera till den andra taben <b>Version</b><br/>
                        2. Klicka på <b>Ej publicerad</b> i menyn till vänster<br/>
                        3. Kontrollera att de förändringar som är listade är korrekt<br/>
                        4. Klicka på <b>Ny version</b> högst upp till höger<br/>
                    </div>
            }, {
                title: "Hur importerar jag nya begrepp?",
                tags: [this.TAG_CREATE, this.TAG_OTHER],
                text: 
                    <div>
                        1. Navigera till den fjärde taben <b>Verktyg</b><br/>
                        2. Klicka på <b>Importera nya begrepp</b> i menyn till vänster<br/>
                        <br/>
                        Importeringen förväntar sig en excel fil (.xlsx)<br/>
                        Filens struktur bör vara som sådan att första kolumnen innehåller begreppets namn, övriga kolumner ignoreras.
                    </div>
            }]*/
        };
        this.tagNames = {
            "__ALL__": "ALLA",
            "create": "SKAPA",
            "remove": "TA BORT",
            "settings": "INSTÄLLNINGAR",
            "other": "ÖVRIGT",
        };
    }

    onFilterChanged(e) {
        this.setState({filter: e.target.value});
    }

    onItemClicked(item) {
        item.visible = item.visible == null ? true : !item.visible;
        this.forceUpdate();
    }

    onTagClicked(tag) {
        var isSelected = this.state.tags.indexOf(tag) != -1;
        if(tag == this.TAG_ALL) {
            this.state.tags = [this.TAG_ALL];
        } else {
            var index = this.state.tags.indexOf(this.TAG_ALL);
            if(index != -1) {
                this.state.tags.splice(index, 1);
            }
            if(isSelected) {
                index = this.state.tags.indexOf(tag);
                if(index != -1) {
                    this.state.tags.splice(index, 1);
                }   
            } else {
                this.state.tags.push(tag);
            }
        }
        this.setState({tags: this.state.tags});
    }

    onCloseClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    getFilteredItems() {
        var filter = this.state.filter.trim().toLowerCase();
        var items = [];
        if(filter.length == 0 && this.state.tags.indexOf(this.TAG_ALL) != -1) {
            items = this.state.items;
        } else {
            for(var i=0; i<this.state.items.length; ++i) {
                var item = this.state.items[i];
                var title = item.title.toLowerCase();
                var text = item.text.toString().toLowerCase();
                var tagVisible = this.state.tags.indexOf(this.TAG_ALL) != -1;
                for(var j=0; j<item.tags.length; ++j) {
                    if(this.state.tags.indexOf(item.tags[j]) != -1) {
                        tagVisible = true;
                        break;
                    }
                }
                if(tagVisible && (title.indexOf(filter) != -1 || text.indexOf(filter) != -1)) {
                    items.push(item);
                }
            }
        }
        return items;
    }

    renderTagButton(tag) {
        var isSelected = this.state.tags.indexOf(tag) != -1;
        return (
            <div 
                className={"user_manual_tag font" + (isSelected ? " user_manual_tag_selected" : "")}
                onPointerUp={this.onTagClicked.bind(this, tag)}>
                {this.tagNames[tag]}
            </div>
        );
    }

    renderTips() {
        return (
            <div className="user_manual_tips">
                <div>
                    <div><b>Tips</b></div>
                    <div>
                        I de flesta listor och träd-vyer går det att högerklicka på begrepp för att snabb komma åt dess egenskaper, <b>id</b>, <b>namn</b>, <b>ssyk-kod</b> etc.
                        Det valda värdet hamnar i "urklippet" och man får åtkomst till det genom att trycka CTRL + V (klistra in).
                    </div>
                </div>
                <div>
                    <div><b>Synpunkter / förslag / felrapporter</b></div>
                    <a href="https://gitlab.com/taxonomy-dev/frontend/jobtech-taxonomy-editor" target="_blank">
                        https://gitlab.com/taxonomy-dev/frontend/jobtech-taxonomy-editor
                    </a>
                </div>
            </div>
        );
    }

    renderQA() {
        var items = this.getFilteredItems().map((item, index) => {
            return (
                <div 
                    key={index}
                    className="user_manual_item">
                    <div onPointerDown={this.onItemClicked.bind(this, item)}>
                        <div className={"arrow " + (item.visible ? "down" : "right")}/>
                        <div>{item.title}</div>
                    </div>
                    {item.visible &&
                        <div dangerouslySetInnerHTML={{__html: item.text}} />
                    }
                </div>
            );
        });
        return (
            <div className="user_manual_qa">
                <input 
                    type="text"
                    className="rounded"
                    value={this.state.filter}
                    placeholder={Localization.get("search")}
                    onChange={this.onFilterChanged.bind(this)}/>
                <div className="user_manual_tags">
                    {this.renderTagButton(this.TAG_ALL)}
                    {this.renderTagButton(this.TAG_CREATE)}
                    {this.renderTagButton(this.TAG_REMOVE)}
                    {this.renderTagButton(this.TAG_SETTINGS)}
                    {this.renderTagButton(this.TAG_OTHER)}
                </div>
                {items}
            </div>
        );
    }

    render() {
        return (
            <div className="dialog_content edit_concept_dialog edit_concept_dialog_page">
                <div className="user_manual_dialog">
                    {this.renderTips()}
                    {this.renderQA()}
                </div>
                <div className="dialog_content_buttons">
                    <Button 
                        text={Localization.get("close")}
                        onClick={this.onCloseClicked.bind(this)}/>
                </div>
            </div>
        );
    }
}

export default UserManual;