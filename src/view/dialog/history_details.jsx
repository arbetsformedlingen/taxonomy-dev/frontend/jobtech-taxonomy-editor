import React from 'react';
import Button from './../../control/button.jsx';
import Loader from './../../control/loader.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import App from './../../context/app.jsx';
import Util from './../../context/util.jsx';

class HistoryDetails extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            otherConcept: null,
        }
    }
    
    componentDidMount() {
        var item = this.props.item;
        if(item.relation) {
            var fetchConcept = null;
            if(this.props.concept.id != item.relation.source["concept/id"] && this.isTypeSsykOrIsco(item.relation.source["concept/type"])) {
                fetchConcept = item.relation.source;
            } else if(this.props.concept.id != item.relation.target["concept/id"] && this.isTypeSsykOrIsco(item.relation.target["concept/type"])) {
                fetchConcept = item.relation.target;
            }

            if(fetchConcept != null) {
                Util.getConcept(fetchConcept["concept/id"], fetchConcept["concept/type"], (data) => {
                    this.setState({otherConcept: data[0]})
                }, (status) => {
                    App.showError(Util.getHttpMessage(status) + " : misslyckades att hämta koncept");
                });
            }
        }
    }

    isTypeSsykOrIsco(type) {
        switch(type) {
            case "ssyk-level-4":
            case "ssyk-level-3":
            case "ssyk-level-2":
            case "ssyk-level-1":
            case "isco-level-4":
                return true;
        }
        return false;
    }

    getNameFor(item) {
        var name = item["concept/preferredLabel"];
        if(this.isTypeSsykOrIsco(item["concept/type"])) {
            var concept = null;
            if(item["concept/id"] === this.props.concept.id) {
                concept = this.props.concept;
            } else if(this.state.otherConcept != null && item["concept/id"] === this.state.otherConcept.id) {
                concept = this.state.otherConcept;
            }

            if(concept) {
                if(concept.isco) {
                    name = concept.isco + " - " + name;
                } else if(concept.ssyk) {
                    name = concept.ssyk + " - " + name;
                }
            } else {
                name = <div><Loader text=""/>{name}</div>;
            }
        }
        return name;
    }

    onCloseClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }

    renderInfoItem(name, value, key, isHeader) {
        return (
            <div 
                key={key}
                className={"item_history_dialog_item " + (isHeader ? "item_history_dialog_item_header" : "")}>
                <div>
                    {name}
                </div>
                <div>
                    {value}
                </div>
            </div>
        );
    }

    renderHistoryShowItem(item, concept) {
        var isRelation = (attribute) => {
            switch(attribute) {
                case "broader":
                case "narrower":
                case "broad_match":
                case "close_match":
                case "exact_match":
                case "narrow_match":
                case "related":
                case "replaces":
                case "replaced_by":
                case "substitutes":    
                case "substituted_by":
                case "possible_combinations":
                case "unlikely_combinations":
                    return true;
                default:
                    return false;
                }
        };
        var info = [];
        var key = 0;
        if(item.event == "Created" || item.event == "Updated" || item.event == "Commented" || item.event == "Deprecated") {
            info.push(this.renderInfoItem(Localization.get("name"), concept.label, key++));
            info.push(this.renderInfoItem(Localization.get("id"), concept.id, key++));
            info.push(this.renderInfoItem(Localization.get("type"), Localization.getTypeName(concept.type), key++));
            info.push(this.renderInfoItem(Localization.get("note"), item.comment, key++));
            if(item.changes) {
                for(var i=0; i<item.changes.length; ++i) {
                    var change = item.changes[i];
                    if(isRelation(change.attribute)) {
                        var removed = Util.findMissingInArray(change.old_value, change.new_value);
                        var added = Util.findMissingInArray(change.new_value, change.old_value);
                        if(removed.length > 0) {
                            info.push(this.renderInfoItem(Localization.get("action"), Localization.get("removed_relation") + " " + change.attribute, key++, true));
                            for(var j=0; j<removed.length; ++j) {
                                var c = App.findCachedConcept(removed[j].id);
                                info.push(
                                    <div key={key++} className="item_history_dialog_item_group">
                                        {this.renderInfoItem(Localization.get("name"), c ? c.label : "", key++)}
                                        {this.renderInfoItem(Localization.get("id"), removed[j].id, key++)}
                                        {this.renderInfoItem(Localization.get("type"), c ? Localization.getTypeName(c.type) : "", key++)}
                                        {removed[j].substitutability_percentage ? this.renderInfoItem(Localization.get("weight"), "" + removed[j].substitutability_percentage, key++) : ""}
                                    </div>);
                            }
                        }
                        if(added.length > 0) {
                            info.push(this.renderInfoItem(Localization.get("action"), Localization.get("added_relation") + " " + change.attribute, key++, true));
                            for(var j=0; j<added.length; ++j) {
                                var c = App.findCachedConcept(added[j].id);
                                info.push(
                                    <div key={key++} className="item_history_dialog_item_group">
                                        {this.renderInfoItem(Localization.get("name"), c ? c.label : "", key++)}
                                        {this.renderInfoItem(Localization.get("id"), added[j].id, key++)}
                                        {this.renderInfoItem(Localization.get("type"), c ? Localization.getTypeName(c.type) : "", key++)}
                                        {added[j].substitutability_percentage ? this.renderInfoItem(Localization.get("weight"), "" + added[j].substitutability_percentage, key++) : ""}
                                    </div>);
                            }
                        }
                    } else {
                        info.push(this.renderInfoItem(Localization.get("action"), Localization.get("changed") + " " + Localization.get(change.attribute), key++, true));
                        info.push(this.renderInfoItem(Localization.get("from"), ""+change.old_value, key++));
                        info.push(this.renderInfoItem(Localization.get("to"), ""+change.new_value, key++));
                    }
                }
            } else if(item.concept) {                
                for(var x in item.concept) {
                    if(x != "preferred_label" && x != "type" && typeof item.concept[x] === 'string' && item.concept[x] != null) {
                        info.push(this.renderInfoItem(Localization.get(x), item.concept[x], key++));
                    }
                }
                for(var x in item.concept) {
                    if(isRelation(x) && item.concept[x].length > 0) {                                                
                        info.push(this.renderInfoItem(Localization.get("relation_type"), Localization.get(x), key++, true));                        
                        for(var j=0; j<item.concept[x].length; ++j) {
                            var relation = item.concept[x][j];
                            var c = App.findCachedConcept(relation.id);
                            info.push(
                                <div key={key++} className="item_history_dialog_item_group">
                                    {this.renderInfoItem(Localization.get("name"), c ? c.label : "", key++)}
                                    {this.renderInfoItem(Localization.get("id"), relation.id, key++)}
                                    {this.renderInfoItem(Localization.get("type"), c ? Localization.getTypeName(c.type) : "", key++)}
                                    {relation.substitutability_percentage ? this.renderInfoItem(Localization.get("weight"), "" + relation.substitutability_percentage, key++) : ""}
                                </div>);
                        }
                    }
                }
            }
        } 
        return info;
    }

    render() {
        var info = null;
        info = this.renderHistoryShowItem(this.props.item, this.props.concept);
        return (
            <div className="dialog_content item_history_dialog">
                <div>
                    {info}
                </div>
                <div>
                    <Button 
                        text={Localization.get("close")}
                        onClick={this.onCloseClicked.bind(this)}/>                
                </div>
            </div>
        );
    }
}

export default HistoryDetails;