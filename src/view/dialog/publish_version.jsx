import React from 'react';
import Button from './../../control/button.jsx';
import App from './../../context/app.jsx';
import Settings from './../../context/settings.jsx';
import Label from './../../control/label.jsx';
import List from './../../control/list.jsx';
import Loader from './../../control/loader.jsx';
import SortArrow from './../../control/sort_arrow.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Util from '../../context/util.jsx';
import EditConcept from './edit_concept.jsx';

class PublishVersion extends React.Component { 

    constructor(props) {
        super(props);
        // constants
        this.ERROR_OCCUPATION_SSYK = 0;
        this.ERROR_OCCUPATION_ISCO = 1;
        this.ERROR_SKILL_SSYK = 2;
        this.ERROR_SKILL_HEADLINE = 3;
        this.ERROR_NO_MATCHING = 4;
        this.SORT_TYPE = "SORT_TYPE";
        this.SORT_NAME = "SORT_NAME";
        this.SORT_ERROR = "SORT_ERROR";
        this.PUBLISH_VERSION_LIST_EVENT_ID = "PUBLISH_VERSION_LIST_EVENT_ID";
        // state
        this.state = {            
            result: [],
            okToPublish: false,
            skills: null,
            occupationNames: null,
            escos: null,
            isError: false,
        }
        this.sortBy = this.SORT_TYPE;
        this.sortAsc = true;
    }

    componentDidMount() {
        this.afterVersion = this.props.afterVersion;
        this.versionTimestamp = this.props.versionTimestamp;
        if(this.props.ids) {
            this.setupPrefetched(this.props.ids);
        } else {
            // fetch data needed for check
            Rest.getOccupationNamesWithBroaderAndMatchingTypes((data) => {
                this.setState({occupationNames: data}, () => {
                    this.performChecks();
                });            
            }, (status) => {          
                this.setState({isError: true});
                App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
            });
            Rest.getSkillsWithBroaderHedlinesRelatedSsyks((data) => {
                this.setState({skills: data}, () => {
                    this.performChecks();
                });
            }, (status) => {
                this.setState({isError: true});
                App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
            });
            Rest.getEscoOccupationsWithMatchingTypes((data) => {
                this.setState({escos: data}, () => {
                    this.performChecks();
                });
            }, (status) => {
                this.setState({isError: true});
                App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
            });
        }
    }

    async setupPrefetched(ids) {
        var splitIds = (list, type) => {
            var result = [];
            for(var i=0; i<list.length; ++i) {
                if(list[i].type == type) {
                    result.push(list[i].id);
                }
            }
            return result;
        };
        var batchFetch = async (data, fetchMethod, onComplete) => {
            var items = [];
            while(data.length > 0) {
                var elements = data.splice(0, data.length > 20 ? 20 : data.length);
                var result = await fetchMethod(elements);
                for(var i=0; i<elements.length; ++i) {
                    items.push(...result.data["result" + i]);
                }
            }
            onComplete(items);
        };
        var occupations = splitIds(ids, "occupation-name");
        var skills = splitIds(ids, "skill");
        var escos = splitIds(ids, "esco-occupation");
        var occupationQuery = batchFetch(occupations, Rest.getOccupationNamesWithBroaderAndMatchingTypesFor.bind(Rest), (items) => {
            this.setState({occupationNames: items}, () => {
                this.performChecks();
            }); 
        });
        var skillsQuery = batchFetch(skills, Rest.getSkillsWithBroaderHedlinesRelatedSsyksFor.bind(Rest), (items) => {
            this.setState({skills: items}, () => {
                this.performChecks();
            }); 
        });
        var escoQuery = batchFetch(escos, Rest.getEscoOccupationsWithMatchingTypesFor.bind(Rest), (items) => {
            this.setState({escos: items}, () => {
                this.performChecks();
            }); 
        });
        await occupationQuery;
        await skillsQuery;
        await escoQuery;
    }

    async setupPrefetchedItems(ids, onFetch, onComplete) {
        if(!this.state.isError) {
            var data = [];
            for(var i=0; i<ids.length; ++i) {
                var result = await onFetch(ids[i]);
                if(result != null) {
                    if(result.data.concepts.length > 0) {
                        data.push(result.data.concepts[0]);
                    }
                } else {
                    this.state.isError = true;
                    this.setState({isError: true});
                    return;
                }
            }
            onComplete(data);
        }
    }

    hasEdge(id, edges) {
        return edges.find(e => e.source === id || e.target === id) != undefined;
    }

    checkConcepts(occupationNames, skills, escos) {
        var result = [];
        // occupation to ssyk = 0
        // occupation to isco = 1        
        for(var i=0; i<occupationNames.length; ++i) {
            var item = occupationNames[i];
            var ssyk = false;
            var isco = false;
            var skip = Settings.shouldSkipValidation(item.type, item.id);
            for(var j=0; j<item.broader.length; ++j) {
                if(Constants.CONCEPT_SSYK_LEVEL_4 === item.broader[j].type) {
                    ssyk = true;
                }                
                if(Constants.CONCEPT_ISCO_LEVEL_4 === item.broader[j].type) {
                    isco = true;
                }                
            }
            var matching = (item.exact_match && item.exact_match.length > 0) ||
                            (item.broad_match && item.broad_match.length > 0) ||
                            (item.narrow_match && item.narrow_match.length > 0) ||
                            (item.close_match && item.close_match.length > 0);
            if(!ssyk) {
                result.push({
                    item: item,
                    skip,
                    error: this.ERROR_OCCUPATION_SSYK,
                });
            }
            if(!isco) {
                result.push({
                    item: item,
                    skip,
                    error: this.ERROR_OCCUPATION_ISCO,
                });
            }
            if(!matching) {
                result.push({
                    item: item,
                    skip,
                    error: this.ERROR_NO_MATCHING,
                });
            }
        }
        // skill to ssyk = 2
        // skill to skillHeadline = 3
        for(var i=0; i<skills.length; ++i) {
            var item = skills[i];
            var ssyk = false;
            var headline = false;
            var skip = Settings.shouldSkipValidation(item.type, item.id);
            if(!skip && item.broader.length > 0) {
                skip = Settings.shouldSkipValidation(item.broader[0].type, item.broader[0].id);
            }
            for(var j=0; j<item.related.length; ++j) {
                if(Constants.CONCEPT_SSYK_LEVEL_4 === item.related[j].type) {
                    ssyk = true;
                    break;
                }                
            }
            for(var j=0; j<item.broader.length; ++j) {
                if(Constants.CONCEPT_SKILL_HEADLINE === item.broader[j].type) {
                    headline = true;
                    break;
                }                
            }
            if(!ssyk) {
                result.push({
                    item: item,
                    skip,
                    error: this.ERROR_SKILL_SSYK,
                });
            }
            if(!headline) {
                result.push({
                    item: item,
                    skip,
                    error: this.ERROR_SKILL_HEADLINE,
                });
            }
        }        
        // esco has matching
        for(var i=0; i<escos.length; ++i) {
            var item = escos[i];
            var matching = (item.exact_match && item.exact_match.length > 0) ||
                            (item.broad_match && item.broad_match.length > 0) ||
                            (item.narrow_match && item.narrow_match.length > 0) ||
                            (item.close_match && item.close_match.length > 0);
            if(!matching) {
                result.push({
                    item: item,
                    skip,
                    error: this.ERROR_NO_MATCHING,
                });
            }
        }
        return result;
    }

    getErrorCount(items) {
        var count = 0;
        for(var i=0; i<items.length; ++i) {
            if(!items[i].skip) {
                count++;
            }
        }
        return count;
    }

    checkPublishStatus(items) {
        var skips = 0;
        for(var i=0; i<items.length; ++i) {
            if(items[i].skip) {
                skips++;
            }
        }
        return skips == items.length;
    }

    performChecks() {
        if(this.doneFetchingData()) {
            var res = this.checkConcepts(this.state.occupationNames, this.state.skills, this.state.escos);            
            this.setState({
                result: this.sortData(res),
                okToPublish: this.checkPublishStatus(res)
            });            
        }
    }

    doneFetchingData() {
        return this.state.occupationNames && this.state.skills && this.state.escos;
    }

    sortData(data) {       
        var cmp;
        switch(this.sortBy) {
            default:
            case this.SORT_TYPE:
                cmp = (a) => {return Localization.getTypeName(a.item.type);};
                break;
            case this.SORT_NAME:
                cmp = (a) => {return a.item.preferredLabel;};
                break;
            case this.SORT_ERROR:
                cmp = (a) => {return Localization.get("publish_error_" + a.error);};
                break;
        }
        return Util.sortByCmp(data, cmp, this.sortAsc);
    }

    onSortClicked(sortBy) {
        if(this.state.selected != null) {
            EventDispatcher.fire(this.PUBLISH_VERSION_LIST_EVENT_ID);
            this.onItemSelected(null);
        }      
        if(this.sortBy == sortBy) {
            this.sortAsc = !this.sortAsc;
        } else {
            this.sortBy = sortBy;
            this.sortAsc = true;
        }
        this.setState({ data: this.sortData(this.state.result) });
    }

    onItemSelected(item) {
        this.setState({
            selected: item,
        });        
    }

    onVisitClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        EventDispatcher.fire(Constants.ID_NAVBAR, Constants.WORK_MODE_1);
        setTimeout(() => {
            EventDispatcher.fire(Constants.EVENT_MAINPANEL_ITEM_SELECTED, this.state.selected.item);
        }, 500);
    }

    onEditClicked() {        
        var type = Constants.CONCEPT_SSYK_LEVEL_4;
        if(this.state.selected.error == this.ERROR_OCCUPATION_ISCO) {
            type = Constants.CONCEPT_ISCO_LEVEL_4;
        } else if(this.state.selected.error == this.ERROR_OCCUPATION_ISCO) {
            type = Constants.CONCEPT_SKILL_HEADLINE;
        } else if(this.state.selected.error == this.ERROR_NO_MATCHING) {
            type = null;
        }

        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("edit") + " " + this.state.selected.item.preferredLabel,
            content: <EditConcept 
                        lockToType={type}
                        item={this.state.selected.item}
                        onItemUpdated={this.onItemSaved.bind(this)}/>
        });
    }

    onItemSaved(item) {
        var selected  = this.state.selected;
        var type = Constants.CONCEPT_SSYK_LEVEL_4;
        if(selected.error == this.ERROR_OCCUPATION_ISCO) {
            type = Constants.CONCEPT_ISCO_LEVEL_4;
        } else if(selected.error == this.ERROR_OCCUPATION_ISCO) {
            type = Constants.CONCEPT_SKILL_HEADLINE;
        } else if(selected.error == this.ERROR_NO_MATCHING) {
            type = null;
        }
        Util.getConcept(selected.item.id, selected.item.type, (data) => {
            var pos = this.state.result.indexOf(selected);
            this.state.result.splice(pos, 1);
            EventDispatcher.fire(this.PUBLISH_VERSION_LIST_EVENT_ID);
            this.setState({selected: null});
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades hämta concept");
        });
    }

    onPublishClicked() {
        Rest.postNewVersion(this.afterVersion + 1, this.versionTimestamp, () => {
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades att skapa version");
        });
    }

    onCloseClicked() {
        EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
    }
    
    renderItem(item) {
        var css = "dialog_publish_list_item" + (item.skip ? " dialog_publish_list_item_no_validation" : "");
        return (
            <div className={css}>
                <div title={Localization.getTypeName(item.item.type)}>
                    {Localization.getTypeName(item.item.type)}
                </div>
                <div title={item.item.preferredLabel}>
                    {item.item.preferredLabel}
                </div>
                <div title={Localization.get("publish_error_" + item.error)}>
                    {Localization.get("publish_error_" + item.error)}
                </div>
            </div>
        );
    }

    renderTitle() {
        if(this.state.isError) {
            return (
                <div>
                    Ett problem inträffade medans data sammanställdes, testa stänga dialogen och försök igen.
                </div>
            );
        } else if(this.doneFetchingData()) {
            if(this.state.okToPublish) {
                return (
                    <Label 
                        css="main_content_title" 
                        text="Inga fel att åtgärda"/>
                );
            } else {
                return (
                    <Label 
                        css="main_content_title"
                        text={this.getErrorCount(this.state.result) + " fel att åtgärda"}/>
                );
            }
        } else {
            return (
                <div>
                    <Loader/>
                </div>
            );
        }
    }

    renderResultHeader() {
        var renderArrow = (type) => {
            if(type == this.sortBy) {
                return (
                    <SortArrow css={this.sortAsc ? "down" : "up"}/>
                );
            }
        };
        if(this.state.result.length > 0) {
            return (
                <div className="dialog_publish_list_header no_select font">
                    <div onClick={this.onSortClicked.bind(this, this.SORT_TYPE)}>
                        {Localization.get("type")}
                        {renderArrow(this.SORT_TYPE)}
                    </div>
                    <div onClick={this.onSortClicked.bind(this, this.SORT_NAME)}>
                        {Localization.get("name")}
                        {renderArrow(this.SORT_NAME)}
                    </div>
                    <div onClick={this.onSortClicked.bind(this, this.SORT_ERROR)}>
                        {Localization.get("error")}
                        {renderArrow(this.SORT_ERROR)}
                    </div>
                </div>
            );
        }
    }

    renderResultList() {
        if(this.state.result.length > 0) {
            return (
                <List
                    eventId={this.PUBLISH_VERSION_LIST_EVENT_ID}
                    css="dialog_publish_list"                     
                    data={this.state.result} 
                    onItemSelected={this.onItemSelected.bind(this)}
                    dataRender={this.renderItem.bind(this)}/>
            );
        }
    }

    renderPublish() {
        if(this.props.ids != null) {
            return (
                <Button 
                    isEnabled={this.state.okToPublish}
                    text={Localization.get("publish")}
                    onClick={this.onPublishClicked.bind(this)}/>
            );
        }
    }

    render() {        
        return(
            <div className="dialog_content dialog_publish_dialog">                
                {this.renderTitle()}
                {this.renderResultHeader()}
                {this.renderResultList()}
                <div className="dialog_publish_buttons">
                    <Button 
                        isEnabled={this.state.selected != null}
                        text={Localization.get("visit")}
                        onClick={this.onVisitClicked.bind(this)}/>
                    <Button 
                        isEnabled={this.state.selected != null}
                        text={Localization.get("fix")}
                        onClick={this.onEditClicked.bind(this)}/>
                    {this.renderPublish()}
                    <Button 
                        text={Localization.get("close")}
                        onClick={this.onCloseClicked.bind(this)}/>
                </div>
            </div>
        );
    }
}

export default PublishVersion;
