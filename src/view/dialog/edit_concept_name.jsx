import React from 'react';
import Label from './../../control/label.jsx';
import App from './../../context/app.jsx';
import Rest from './../../context/rest.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import Util from './../../context/util.jsx';
import ConceptEdit from './../../context/concept_edit.jsx';

class EditConceptName extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.item.preferredLabel,
            isChanged: false,
            isNameInvalid: false,
        };
        this.props.editContext.onSave = this.onSave.bind(this);
    }

    onSave(message, callback) {
        var item = this.props.item;
        App.addSaveRequest();
        ConceptEdit.name(item.id, this.state.value, message, () => {
            item.preferredLabel = this.state.value.trim();
            App.updateCachedItem(item);
            callback();
        }, () => {
            EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
        });
    }

    onValueChanged(e) {
        var name = e.target.value.trim();
        var isChanged = name != this.props.item.preferredLabel;
        this.setState({
            value: e.target.value,
            isChanged: isChanged,
        });
        if(name.length > 0) {
            if(isChanged) {
                Rest.abort();
                Rest.getConceptByName(name, (data) => {                                        
                    if(data.length > 0) {
                        var errorString = "";
                        for(var i=0; i<data.length; ++i) {
                            errorString += data[i].id;
                            errorString += " (" + Localization.getTypeName(data[i].type) + ")";
                            if(data[i].deprecated) {
                                errorString += " - " + Localization.get("DEPRECATED");
                            }
                            errorString += "\n";
                        }
                        this.setState({
                            isNameInvalid: true,
                            errorString: errorString,
                        });
                    } else {
                        this.setState({
                            isNameInvalid: false,
                        });
                    }
                    
                    this.props.editContext.setEnableSave(true);
                }, () => {
                }); 
            } else {
                this.props.editContext.setEnableSave(false);
            }
        }
    }

    renderErrorText() {
        if(this.state.isNameInvalid) {
            return (
                <div className="edit_concept_error_text font">
                    <div>Finns redan begrepp som matchar det valda namnet:</div>
                    <div>{this.state.errorString}</div>
                </div>
            );
        }
    }

    render() {
        return (
            <div className="dialog_value_group">
                <Label 
                    css="dialog_value_title"
                    text="Ange nytt namn på begrepp"/>
                <input
                    className="rounded"
                    value={this.state.value}
                    onChange={this.onValueChanged.bind(this)}/>
                {this.renderErrorText()}
            </div>
        );
    }
}

export default EditConceptName;