import LZString from 'lz-string';
import Constants from './constants.jsx';

class CacheManager { 

    constructor() {
    }

    getKey(key) {
        return Constants.INSTANCE_TYPE + "_tax_key_" + key;
    }

    getOldCachedValue(key) {
        var raw = localStorage.getItem("tax_key_" + key + "_value");
        try {
            raw = LZString.decompress(raw);
            return JSON.parse(raw);
        } catch(e) {
            return null;
        }
    }

    getCachedValue(key) {
        var raw = localStorage.getItem(this.getKey(key) + "_value");
        try {
            raw = LZString.decompress(raw);
            return JSON.parse(raw);
        } catch(e) {
            return null;
        }
    }

    getCachedDate(key) {
        var raw = localStorage.getItem(this.getKey(key) + "_date");
        return parseInt(raw);
    }

    hasKey(key) {
        return localStorage.getItem(this.getKey(key) + "_date") != null;
    }

    isCached(key, ignoreDate, maxTime) {
        if(localStorage.getItem(this.getKey(key) + "_date") != null) {
            return ignoreDate || !this.isOutdated(key, maxTime);
        }
        return false;
    }

    isOutdated(key, maxTime) {
        var t = maxTime == null ? 1000 * 60 * 60 * 1 : maxTime;
        var raw = localStorage.getItem(this.getKey(key) + "_date");
        if(raw != null) {
            return (new Date().getTime() - parseInt(raw)) > t;
        }
        return true;
    }

    cache(key, value) {
        try {
            localStorage.setItem(this.getKey(key) + "_value", LZString.compress(JSON.stringify(value)));
            localStorage.setItem(this.getKey(key) + "_date", new Date().getTime());
        } catch(e) {

        }
    }

    clearValue(key) {
        localStorage.removeItem(this.getKey(key) + "_value");
        localStorage.removeItem(this.getKey(key) + "_date");
    }
    
}

export default new CacheManager;