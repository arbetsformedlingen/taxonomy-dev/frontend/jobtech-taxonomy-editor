
import Constants from './constants.jsx';

class Keybindings {

    constructor() {
        // constants
        this.BINDING_FOCUS_SEARCH = "key_focus_search";
        this.BINDING_FOCUS_TYPES = "key_focus_types";
        this.BINDING_NEW_CONCEPT = "key_new_concept";
        this.BINDING_NEW_RELATION = "key_new_relation";
        this.BINDING_TOGGLE_QUICK_EDIT = "key_toggle_quick_edit";
        this.BINDING_OPEN_CONCEPT_EDIT = "key_open_concept_edit";
        this.BINDING_OPEN_QUICK_EDITS = "key_open_quick_edits";
        this.BINDING_SAVE_DIALOG = "key_save_dialog";
        // members
        this.keys = [{
            type: this.BINDING_FOCUS_SEARCH,
            key: "e",
        }, {
            type: this.BINDING_FOCUS_TYPES,
            key: "m",
        }, {
            type: this.BINDING_NEW_CONCEPT,
            key: "b",
        }, {
            type: this.BINDING_NEW_RELATION,
            key: "h",
        }, {
            type: this.BINDING_TOGGLE_QUICK_EDIT,
            key: "q",
        }, {
            type: this.BINDING_OPEN_CONCEPT_EDIT,
            key: "r",
        }, {
            type: this.BINDING_OPEN_QUICK_EDITS,
            key: "i",
        }, {
            type: this.BINDING_SAVE_DIALOG,
            key: "s",
        }];
    }

    getBindingForKey(key) {
        return this.keys.find((x) => { return x.key == key; });
    }

    onKeyBinding(e, type, propagateEvents) {
        if(e.ctrlKey && !e.repeat) {
            var binding = this.keys.find((x) => {
                return x.type == type;
            });
            if(e.key != null && binding && binding.key == e.key) {
                if(!propagateEvents) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                return true;
            }
        }
        return false;
    }

    save() {
        try {
            localStorage.setItem(Constants.INSTANCE_TYPE + "_keybindings", JSON.stringify(this.keys));
        } catch(e) {

        }
    }

    load() {
        var k = localStorage.getItem(Constants.INSTANCE_TYPE + "_keybindings");
        if(k != null) {
            try {
                // override existing keys
                var keys = JSON.parse(k);
                for(var i=0; i<keys.length; ++i) {
                    var binding = this.getBindingForKey(keys[i].type);
                    if(binding) {
                        binding.key = keys[i].key;
                    }
                }
            } catch(e) {

            }
        }
    }

}

export default new Keybindings;