
import Constants from './constants.jsx';
import App from './app.jsx';
import EventDispatcher from './event_dispatcher.jsx';

class Socket {

    constructor() {
        this.isConnected = false;
        this.users = [];
    }

    init() {
        var proto = window.location.protocol.startsWith("https") ? "wss" : "ws";
        var tail = window.location.hostname == "localhost" ? ":8080" : "/taxonomy_ws";
        var url = proto + "://" + window.location.hostname + tail;
        if(window.g_app_ws != null) {
            url = window.g_app_ws;
        }
        console.log("ws url: " + url);
        this.socket = null;
        try {
            this.socket = new WebSocket(url, "taxonomy-protocol");
        } catch(e) {
            console.log("websocket creation error", e);
            return;
        }
        this.socket.onopen = (e) => {
            console.log("socket connected");
            if(this.iid != null) {
                clearInterval(this.iid);
                this.iid = undefined;
            }
            this.isConnected = true;
            this.users = [];
            this.send("login", Constants.USER_ID); 
        };

        this.socket.onmessage = (e) => {
            try {
                var message = JSON.parse(e.data);
                this.onMessage(message.type, message.value);
            } catch(ex) {
                console.log("exeption on server message", ex);
            }
        };

        this.socket.onclose = (e) => {
            console.log("socket closed");
            this.isConnected = false;
            if(this.iid == null) {
                // the client wants to have a socket connection, so keep trying to re-establish
                this.iid = setInterval(() => {
                    this.init();
                    App.onSocketReconnected();
                }, 1000 * 60 * 5);
            }
        };

        this.socket.onerror = (e) => {
            console.log("socket error");
        };
    }

    close() {
        this.socket.close();
    }

    onMessage(type, value) {
        if(type == "userConnected") {
            this.users.push({
                name: value,
                states: [],
            });
        } else if(type == "userDisconnected") {
            var user = this.users.find((x) => {
                return x.name == value;
            });
            var index = this.users.indexOf(user);
            if(index != -1 && user) {
                this.users.splice(index, 1);
                if(user.states.length > 0) {
                    user.states = [];
                    EventDispatcher.fire(Constants.EVENT_SOCKET_USER_STATE, user);
                }
            }
        } else if(type == "userClients") {
            this.users = value;
            for(var i=0; i<this.users.length; ++i) {
                if(this.users[i].states) {
                    this.users[i].states = this.users[i].states.filter(Boolean);
                }
            }
        } else if(type == "userState") {
            var user = this.users.find((x) => {
                return x.name == value.name;
            });
            if(user) {
                var state = user.states.find((x) => {
                    return x.uniqueId == value.state.uniqueId;
                });
                if(state) {
                    // remove state
                    var index = user.states.indexOf(state);
                    user.states.splice(index, 1);
                    if(value.state.type != null) {
                        // re-add state
                        state = value.state;
                        user.states.push(state);
                    }
                } else if(value.state.type != null) {
                    // add new state
                    state = value.state;
                    user.states.push(state);
                }
                EventDispatcher.fire(Constants.EVENT_SOCKET_USER_STATE, user);
                if(state && state.temporary) {
                    // remove the state, everyone has had the chance to react to it now
                    var index = user.states.indexOf(state);
                    user.states.splice(index, 1);
                }
            }
        } else if(type == "typeCounters") {
            App.onTypeCounters(value);
        }
    }

    send(type, value) {
        if(this.isConnected) {
            this.socket.send(JSON.stringify({
                type: type,
                value: value,
            }));
        }
    }

    sendState(state) {
        this.send("state", state);
    }

    sendNewConcept(concept) {
        this.sendState({
            type: "new_concept",
            value: {
                id: concept.id,
                type: concept.type,
                preferredLabel: concept.preferredLabel,
            },
            temporary: true,
        });
    }

    sendDeprecatedConcept(concept) {
        this.sendState({
            type: "deprecated_concept",
            value: concept,
            temporary: true,
        });
    }

    sendEditSave(id, field, value) {
        var concept = App.getCachedConceptFromId(id);
        this.sendState({
            type: "edit_saved",
            id: id,
            field: field,
            value: value == null ? id : value,
            time: new Date().toLocaleString(),
            temporary: true,
            conceptType: concept ? concept.type : null,
        });
    }

    sendClearState() {
        this.sendState({});
    }

}

export default new Socket;