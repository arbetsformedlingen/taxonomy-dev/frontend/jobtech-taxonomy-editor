import React from 'react';

class Constants { 

    constructor() {
        // work modes
        this.WORK_MODE_1 = 0;
        this.WORK_MODE_2 = 1;
        this.WORK_MODE_3 = 2;
        this.WORK_MODE_4 = 3;
        this.WORK_MODE_5 = 4;
        // dialog options
        this.DIALOG_OPTION_OK       = 1;
        this.DIALOG_OPTION_CANCEL   = 2;
        this.DIALOG_OPTION_SAVE     = 4;
        this.DIALOG_OPTION_ABORT    = 8;
        this.DIALOG_OPTION_YES      = 16;
        this.DIALOG_OPTION_NO       = 32;
        // edit dialog types
        this.EDIT_TYPE_NONE = "--";
        this.EDIT_TYPE_NAME = "name";
        this.EDIT_TYPE_ALTERNATIVE_LABEL = "alternative_labels";
        this.EDIT_TYPE_DESCRIPTION = "description";
        this.EDIT_TYPE_QUALITY = "quality_control";
        this.EDIT_TYPE_REFERENCED_TO = "set_reference";
        this.EDIT_TYPE_DEPRICATE = "deprecate";
        this.EDIT_TYPE_REACTIVATE = "reactivate";
        this.EDIT_TYPE_ADD_RELATION = "add_connection";
        this.EDIT_TYPE_REMOVE_RELATION = "remove_connection";
        this.EDIT_TYPE_EDIT_RELATION = "edit_connection";
        this.EDIT_TYPE_NEW_VALUE = "new_value";
        this.EDIT_TYPE_ADD_COMMENT = "add_comment";
        this.EDIT_TYPE_NO_ESCO_RELATION = "no_esco_relation";
        // relation types
        this.RELATION_NARROWER = "narrower";
        this.RELATION_BROADER = "broader";
        this.RELATION_RELATED = "related";
        this.RELATION_AFFINITY = "----";
        this.RELATION_SUBSTITUTABILITY = "substitutability";
        this.RELATION_BROAD_MATCH = "broad-match";
        this.RELATION_NARROW_MATCH = "narrow-match";
        this.RELATION_CLOSE_MATCH = "close-match";
        this.RELATION_EXACT_MATCH = "exact-match";
        this.RELATION_POSSIBLE_COMBINATIONS = "possible-combinations";
        this.RELATION_UNLIKELY_COMBINATIONS = "unlikely-combinations";
        // settings pages
        this.SETTINGS_PAGE_VISIBILITY = 0;
        this.SETTINGS_PAGE_VALIDATION = 1;        
        this.SETTING_PAGE_TYPE_LOCALIZATION = 3;
        this.SETTINGS_PAGE_KEY_BINDINGS = 4;
        this.SETTINGS_PAGE_INFO = 5;
        // concept types
        this.CONCEPT_COUNTRY = "country";
        this.CONCEPT_DRIVING_LICENCE = "driving-licence";
        this.CONCEPT_EMPLOYMENT_DURATION = "employment-duration";
        this.CONCEPT_GENERIC_SKILL = "generic-skill";
        this.CONCEPT_GENERIC_SKILL_GROUP = "generic-skill-";
        this.CONCEPT_ISCO_LEVEL_4 = "isco-level-4";
        this.CONCEPT_KEYWORD = "keyword";
        this.CONCEPT_JOB_TITLE = "job-title";
        this.CONCEPT_LANGUAGE = "language";
        this.CONCEPT_MUNICIPALITY = "municipality";
        this.CONCEPT_ESCO_OCCUPATION = "esco-occupation";
        this.CONCEPT_ESCO_SKILL = "esco-skill";
        this.CONCEPT_OCCUPATION_NAME = "occupation-name";
        this.CONCEPT_OCCUPATION_EXPERIENCE_YEAR = "occupation-experience-year";
        this.CONCEPT_OCCUPATION_COLLECTION = "occupation-collection",
        this.CONCEPT_OCCUPATION_FIELD = "occupation-field",
        this.CONCEPT_REGION = "region";
        this.CONCEPT_SNI_LEVEL_1 = "sni-level-1";
        this.CONCEPT_SNI_LEVEL_2 = "sni-level-2";
        this.CONCEPT_SSYK_LEVEL_1 = "ssyk-level-1";
        this.CONCEPT_SSYK_LEVEL_2 = "ssyk-level-2";
        this.CONCEPT_SSYK_LEVEL_3 = "ssyk-level-3";
        this.CONCEPT_SSYK_LEVEL_4 = "ssyk-level-4";        
        this.CONCEPT_SKILL = "skill";
        this.CONCEPT_SKILL_GROUP = "skill-group";
        this.CONCEPT_SKILL_HEADLINE = "skill-headline"
        this.CONCEPT_SUN_EDUCATION_FIELD_1 = "sun-education-field-1";
        this.CONCEPT_SUN_EDUCATION_FIELD_2 = "sun-education-field-2";
        this.CONCEPT_SUN_EDUCATION_FIELD_3 = "sun-education-field-3";
        this.CONCEPT_SUN_EDUCATION_FIELD_4 = "sun-education-field-4";
        this.CONCEPT_SUN_EDUCATION_LEVEL_1 = "sun-education-level-1";
        this.CONCEPT_SUN_EDUCATION_LEVEL_2 = "sun-education-level-2";
        this.CONCEPT_SUN_EDUCATION_LEVEL_3 = "sun-education-level-3";
        this.CONCEPT_UNEMPLOYMENT_FUND = "unemployment-fund";
        this.CONCEPT_UNEMPLOYMENT_TYPE = "unemployment-type";
        // events
        this.EVENT_SET_WORKMODE = "EVENT_SET_WORKMODE";
        this.EVENT_SHOW_OVERLAY = "EVENT_SHOW_OVERLAY";
        this.EVENT_HIDE_OVERLAY = "EVENT_HIDE_OVERLAY";
        this.EVENT_SHOW_SAVE_INDICATOR = "EVENT_SHOW_SAVE_INDICATOR";
        this.EVENT_SHOW_POPUP_INDICATOR = "EVENT_SHOW_POPUP_INDICATOR";
        this.EVENT_HIDE_POPUP_INDICATOR = "EVENT_HIDE_POPUP_INDICATOR";
        this.EVENT_SET_POPUP_MESSAGE = "EVENT_SET_POPUP_MESSAGE";
        this.EVENT_SHOW_SAVE_BUTTON = "EVENT_SHOW_SAVE_BUTTON";
        this.EVENT_HIDE_SAVE_BUTTON = "EVENT_HIDE_SAVE_BUTTON";
        this.EVENT_HIDE_SAVE_PANEL = "EVENT_HIDE_SAVE_PANEL";
        this.EVENT_SHOW_ERROR = "EVENT_SHOW_ERROR";
        this.EVENT_SIDEPANEL_ITEM_SELECTED = "EVENT_SIDEPANEL_ITEM_SELECTED";
        this.EVENT_MAINPANEL_ITEM_SELECTED = "EVENT_MAINPANEL_ITEM_SELECTED";
        this.EVENT_SIDEPANEL_TIME_PERIOD_SELECTED = "EVENT_SIDEPANEL_TIME_PERIOD_SELECTED";
        this.EVENT_SIDEPANEL_STATISTICS_SELECTED = "EVENT_SIDEPANEL_STATISTICS_SELECTED";
        this.EVENT_SIDEPANEL_CONNECTIONS_SELECTED = "EVENT_SIDEPANEL_CONNECTIONS_SELECTED";
        this.EVENT_SIDEPANEL_REFERRED_SELECTED = "EVENT_SIDEPANEL_REFERRED_SELECTED";
        this.EVENT_SIDEPANEL_IMPORT_SELECTED = "EVENT_SIDEPANEL_IMPORT_SELECTED";
        this.EVENT_SIDEPANEL_RELATION_SEARCH_SELECTED = "EVENT_SIDEPANEL_RELATION_SEARCH_SELECTED";
        this.EVENT_SIDEPANEL_CHANGES_SEARCH_SELECTED = "EVENT_SIDEPANEL_CHANGES_SEARCH_SELECTED";
        this.EVENT_SIDEPANEL_CLEAR = "EVENT_SIDEPANEL_CLEAR";
        this.EVENT_GRAPH_MODE_SELECTED = "EVENT_GRAPH_MODE_SELECTED";
        this.EVENT_VISIBLE_TYPES_SELECTED = "EVENT_VISIBLE_TYPES_SELECTED";
        this.EVENT_VERSION_ITEM_SELECTED = "EVENT_VERSION_ITEM_SELECTED";
        this.EVENT_VERSION_ITEM_CONTENT_INFO = "EVENT_VERSION_ITEM_CONTENT_INFO";
        this.EVENT_NEW_CONCEPT = "EVENT_NEW_CONCEPT";
        this.EVENT_SEARCH_CHANGES = "EVENT_SEARCH_CHANGES";
        this.EVENT_SAVE_SEARCH_RESULT = "EVENT_SAVE_SEARCH_RESULT";
        this.EVENT_SET_SETTINGS_PAGE = "EVENT_SET_SETTINGS_PAGE";
        this.EVENT_SOCKET_USER_STATE = "EVENT_SOCKET_USER_STATE";
        this.EVENT_LOGIN_COMPLETE = "EVENT_LOGIN_COMPLETE";
        this.EVENT_IMPORT_DATA = "EVENT_IMPORT_DATA";
        this.EVENT_CONCEPT_EDITED = "EVENT_CONCEPT_EDITED";
        this.EVENT_CACHE_UPDATED = "EVENT_CACHE_UPDATED";
        this.EVENT_QUICK_EDIT = "EVENT_QUICK_EDIT";
        this.EVENT_QUICK_EDITS_CHANGED = "EVENT_QUICK_EDITS_CHANGED";
        this.EVENT_QUICK_EDITS_ADDED = "EVENT_QUICK_EDITS_ADDED";
        this.EVENT_QUICK_EDITS_REMOVED = "EVENT_QUICK_EDITS_REMOVED";
        this.EVENT_QUICK_EDIT_SELECT_ALL = "EVENT_QUICK_EDIT_SELECT_ALL";
        this.EVENT_QUICK_EDIT_DESELECT_ALL = "EVENT_QUICK_EDIT_DESELECT_ALL";
        this.EVENT_QUICK_EDIT_SAVED = "EVENT_QUICK_EDIT_SAVED";
        this.EVENT_CACHE_STATUS_SHOW = "EVENT_CACHE_STATUS_SHOW";
        this.EVENT_CACHE_STATUS_HIDE = "EVENT_CACHE_STATUS_HIDE";
        // id's
        this.ID_NAVBAR = "ID_NAVBAR";
        this.ID_SIDEPANEL_CONTAINER = "ID_SIDEPANEL_CONTAINER";
        this.ID_MAINPANEL_CONTAINER = "ID_MAINPANEL_CONTAINER";
        // key's
        this.KEY_IMPORT_DATA = "tax_key_import_data";
        this.KEY_RELATIONS_SEARCH_DATA = "tax_key_relations_search_data";
        this.KEY_CHANGES_SEARCH_DATA = "tax_key_changes_search_data";
        // quick edit types
        this.QUICK_EDIT_NAME = "QUICK_EDIT_NAME";
        this.QUICK_EDIT_DEFINITION = "QUICK_EDIT_DEFINITION";
        this.QUICK_EDIT_ALTERNATIVE_LABEL = "QUICK_EDIT_ALTERNATIVE_LABEL";
        this.QUICK_EDIT_HIDDEN_LABEL = "QUICK_EDIT_HIDDEN_LABEL";
        this.QUICK_EDIT_QUALITY_LEVEL = "QUICK_EDIT_QUALITY_LEVEL";
        this.QUICK_EDIT_ADDED_RELATION = "QUICK_EDIT_ADDED_RELATION";
        this.QUICK_EDIT_REMOVED_RELATION = "QUICK_EDIT_REMOVED_RELATION";
        this.QUICK_EDIT_EVENT_REFRESH = "QUICK_EDIT_EVENT_REFRESH";
        this.QUICK_EDIT_NO_ESCO_RELATION = "QUICK_EDIT_NO_ESCO_RELATION";
        // info event types
        this.INFO_EVENT_TYPE_WS = "WS";
        this.INFO_EVENT_TYPE_REST = "REST";
        this.INFO_EVENT_CACHE_UPDATE_START = "CACHE START";
        this.INFO_EVENT_CACHE_UPDATE_STOP = "CACHE STOP";
        // usage event sources
        this.USAGE_CONTENT = "CONTENT";
        this.USAGE_BUTTON = "BUTTON";
        this.USAGE_DIALOG = "DIALOG";
        // usage event types
        this.USAGE_ENTER = "ENTER";
        this.USAGE_LEAVE = "LEAVE";
        this.USAGE_CLICK = "CLICK";
        this.USAGE_SHOW = "SHOW";
        this.USAGE_HIDE = "HIDE";

        this.URL_SEARCH_MODEMAP = {
            "default": this.WORK_MODE_1,
            "list": this.WORK_MODE_1,
            "version": this.WORK_MODE_2,
            "changelog": this.WORK_MODE_3,
            "relations": this.WORK_MODE_4,
            "stats": this.WORK_MODE_5,
        };
        this.URL_MODE_SEARCHMAP = {
            "0": "list",
            "1": "version",
            "2": "changelog",
            "3": "relations",
            "4": "stats",
        },
        this.CONCEPT_RELATION_TYPES = {
            "ssyk-level-4": {
                "ssyk-level-3": this.RELATION_BROADER,
                "isco-level-4": this.RELATION_RELATED,
                "occupation-field": this.RELATION_BROADER,
                "skill": this.RELATION_RELATED,
            },
            "ssyk-level-3": {
                "ssyk-level-2": this.RELATION_BROADER,
            },
            "ssyk-level-2": {
                "ssyk-level-1": this.RELATION_BROADER,
            },
            "isco-level-4": {
                "ssyk-level-4": this.RELATION_RELATED,
            },
            "occupation-name": {
                "ssyk-level-4": this.RELATION_BROADER,
                "isco-level-4": this.RELATION_BROADER,
                "occupation-field": this.RELATION_BROADER,
                "occupation-name": this.RELATION_SUBSTITUTABILITY,
                "keyword": this.RELATION_RELATED,
                "job-title": this.RELATION_RELATED,
                "skill": this.RELATION_RELATED,
            },
            "occupation-collection": {

            },
            "keyword": {
                "occupation-name": this.RELATION_RELATED,
            },
            "job-title": {
                "occupation-name": this.RELATION_RELATED,
            },
            "skill": {
                "skill-headline": this.RELATION_BROADER,
                "ssyk-level-4": this.RELATION_RELATED,
                "keyword": this.RELATION_RELATED,
                "job-title": this.RELATION_RELATED,
                "occupation-field": this.RELATION_RELATED,
            },
            "municipality": {
                "region": this.RELATION_BROADER,
            },
            "region": {
                "country": this.RELATION_BROADER,
            },
            "country": {
                "continent": this.RELATION_BROADER,
            },
        };

        // settings
        this.REST_IP = window.g_app_api_path;
        this.SEMANTIC_IP = window.g_app_api_semantic;
        this.REST_API_KEY = "";
        this.INSTANCE_TYPE = window.g_app_instance_type;
        // resources
        this.ICON_TMP_1 = "./src/resource/icon_tmp_1.png";
        this.ICON_TMP_2 = "./src/resource/icon_tmp_2.png";
        this.ICON_TMP_3 = "./src/resource/icon_tmp_3.png";
        this.ICON_TMP_4 = "./src/resource/icon_tmp_4.png";
        this.ICON_TMP_5 = "./src/resource/icon_tmp_5.png";
        this.ICON_EXPAND_UP = "./src/resource/icon_expand_up.png";
        this.ICON_EXPAND_DOWN = "./src/resource/icon_expand_down.png";
        this.ICON_LOCKED = "./src/resource/icon_locked.png";
        this.ICON_UNLOCKED = "./src/resource/icon_unlocked.png";
        this.ICON_HARD_LOCKED = "./src/resource/icon_hard_locked.png";
        this.ICON_AF_EXPORT = "./src/resource/av_logo.png";
        this.ICON_AF_EXPORT_B64 = null;
        this.ICON_SEARCH = "./src/resource/icon_search.png";
        this.ICON_MESSAGE = "./src/resource/icon_message.png";
        this.ICON_REMOVE = "./src/resource/icon_remove.png";
        this.ICON_FILTER = "./src/resource/icon_filter.png";

        this.ICON_SVG_EXCEL = <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><title>Excel_64x</title><path d="M38,8H19.3335A2.33373,2.33373,0,0,0,17,10.3335V20H38Z" fill="#21a366"/><path d="M56.6665,8H38V20H59V10.3335A2.33373,2.33373,0,0,0,56.6665,8Z" fill="#33c481"/><rect x="38" y="32" width="21" height="12" fill="#107c41"/><path d="M38,44V32H17V53.6665A2.33373,2.33373,0,0,0,19.3335,56h37.333A2.33373,2.33373,0,0,0,59,53.6665V44Z" fill="#185c37"/><rect x="17" y="20" width="21" height="12" fill="#107c41"/><rect x="38" y="20" width="21" height="12" fill="#21a366"/><path d="M33,20.33008V46.66992a1.73444,1.73444,0,0,1-.04.3999A2.31378,2.31378,0,0,1,30.66992,49H17V18H30.66992A2.326,2.326,0,0,1,33,20.33008Z" opacity="0.2"/><path d="M34,20.33008V44.66992A3.36171,3.36171,0,0,1,30.66992,48H17V17H30.66992A3.34177,3.34177,0,0,1,34,20.33008Z" opacity="0.1"/><path d="M33,20.33008V44.66992A2.326,2.326,0,0,1,30.66992,47H17V18H30.66992A2.326,2.326,0,0,1,33,20.33008Z" opacity="0.2"/><path d="M32,20.33008V44.66992A2.326,2.326,0,0,1,29.66992,47H17V18H29.66992A2.326,2.326,0,0,1,32,20.33008Z" opacity="0.1"/><rect x="4" y="18.00002" width="27.99996" height="27.99996" rx="2.33333" fill="#107c41"/><path d="M11.2256,39.5835l4.90759-7.60465-4.4951-7.56235h3.61724l2.45379,4.83356q.33839.68763.46538,1.02594h.03173q.24318-.54984.50768-1.06825l2.623-4.79125H24.658l-4.61145,7.52,4.72779,7.647H21.24174L18.40718,34.274a4.43255,4.43255,0,0,1-.33845-.70864h-.04231a3.345,3.345,0,0,1-.32788.68749L14.77937,39.5835Z" fill="#fff"/></svg>
        this.ICON_COPY_CLIPBOARD = <svg width="16px" height="16px" viewBox="0 0 16 16"><path xmlns="http://www.w3.org/2000/svg" d="M8 2.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM9.45 2a2.5 2.5 0 0 0-4.9 0H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h2v-1.5H3.5v-9h1V5h5V3.5h1V7H12V3a1 1 0 0 0-1-1H9.45zM7.5 9.5h1.25a.75.75 0 0 0 0-1.5h-1.5C6.56 8 6 8.56 6 9.25v1.5a.75.75 0 0 0 1.5 0V9.5zm1.25 5H7.5v-1.25a.75.75 0 0 0-1.5 0v1.5c0 .69.56 1.25 1.25 1.25h1.5a.75.75 0 0 0 0-1.5zm3.75-5h-1.25a.75.75 0 0 1 0-1.5h1.5c.69 0 1.25.56 1.25 1.25v1.5a.75.75 0 0 1-1.5 0V9.5zm-1.25 5h1.25v-1.25a.75.75 0 0 1 1.5 0v1.5c0 .69-.56 1.25-1.25 1.25h-1.5a.75.75 0 0 1 0-1.5z"/></svg>;

        var apikey = this.getArg("apiuser");
        if(apikey) {
            this.REST_API_KEY = apikey;
        }
		this.lang = this.getArg("lang");
        this.initEncodedImages();
    }

    async initEncodedImages() {
		const toBase64 = (file) => new Promise((resolve, reject) => {
            var img = new Image();
            img.onload = () => {
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL("image/png");
                resolve(dataURL)
            };
            img.src = file;
        });
        this.ICON_AF_EXPORT_B64 = await toBase64(this.ICON_AF_EXPORT);
    }
    
    getArg(key) {
		var raw = window.location.hash.split('#');
		if(raw.length == 2) {
			var cmd = raw[1];
			var args = cmd.split('&');
			for(var i=0; i<args.length; ++i) {
				if(args[i].indexOf(key + "=") !== -1) {
					return args[i].split('=')[1]; 
				}
			}
		}
		return null;
    }
    
    getRelationType(from, to) {
        if(this.CONCEPT_RELATION_TYPES[from] && this.CONCEPT_RELATION_TYPES[from][to]) {
            return this.CONCEPT_RELATION_TYPES[from][to];
        } else if(this.CONCEPT_RELATION_TYPES[to]) {
            var type = this.CONCEPT_RELATION_TYPES[to][from];
            if(type == this.RELATION_NARROWER) {
                return this.RELATION_BROADER;
            } else if(type == this.RELATION_BROADER) {
                return this.RELATION_NARROWER;
            }
        }
        return null;
    }
}

export default new Constants;