import EventDispatcher from './event_dispatcher.jsx';
import Constants from './constants.jsx';
import CacheManager from './cache_manager.jsx';
import Util from './util.jsx';
import App from './app.jsx';

class QuickEdits { 

    constructor() {        
        this.edits = [];
    }

    find(type, id) {
        return this.edits.find(e => {
            return e.type == type && e.concept.id == id;
        });
    }

    getEdits() {
        return this.edits;
    }

    getEditsFor(id) {
        return this.edits.filter(e => {
            return e.concept.id ==id;
        });
    }

    save() {
        CacheManager.cache("quick_edit", this.edits);
    }

    load() {
        if(CacheManager.isCached("quick_edit", true)) {
            this.edits = CacheManager.getCachedValue("quick_edit");
        }
        // NOTE: this will be removed in a later update
        var oldCache = CacheManager.getOldCachedValue("quick_edit");
        if(oldCache != null) {
            this.edits.push(...oldCache);
        }
    }

    restoreChange(item, concept) {
        if(item.type == Constants.QUICK_EDIT_NAME) {
            concept.preferredLabel = item.originalValue;
        } else if(item.type == Constants.QUICK_EDIT_DEFINITION) {
            concept.defenition = item.originalValue;
        } else if(item.type == Constants.QUICK_EDIT_ALTERNATIVE_LABEL) {
            concept.alternativeLabel = item.originalValue;
        } else if(item.type == Constants.QUICK_EDIT_HIDDEN_LABEL) {
            concept.hiddenLabels = item.originalValue;
        } else if(item.type == Constants.QUICK_EDIT_QUALITY_LEVEL) {
            concept.quality_level = item.originalValue;
        } else if(item.type == Constants.QUICK_EDIT_ADDED_RELATION) {
            Util.removeRelation(concept, item.value.to, item.value.relationType, true);
        } else if(item.type == Constants.QUICK_EDIT_REMOVED_RELATION) {
            var value = {
                id: item.value.to,
                type: item.value.type,
                preferredLabel: item.value.preferredLabel,
                substitutability_percentage: item.value.substitutability,
            };
            Util.addRelation(concept, item.value.relationType, value);
        } else if(item.type == Constants.QUICK_EDIT_NO_ESCO_RELATION) {
            concept.no_esco_relation = item.originalValue;
        }
    }

    remove(item, shouldIgnoreRestoration) {
        var index = this.edits.indexOf(item);        
        this.edits.splice(index, 1);
        if(!shouldIgnoreRestoration) {
            var types = App.getCachedTypes(item.concept.type);
            if(types.length > 0) {
                var concept = types.find((x) => {
                    return x.id == item.concept.id;
                });
                if(concept) {
                    this.restoreChange(item, concept);
                }
            }
            EventDispatcher.fire(Constants.EVENT_QUICK_EDITS_REMOVED, item);
        }
        this.save();
    }
    
    add(concept, type, value, currentValue) {
        var isNew = false;
        var change = this.find(type, concept.id);
        if(change == null || type == Constants.QUICK_EDIT_REMOVED_RELATION || type == Constants.QUICK_EDIT_ADDED_RELATION) {
            isNew = true;
            // create new 
            change = {
                type: type, 
                comment: null,
                originalValue: currentValue,
                concept: {
                    id: concept.id,
                    name: concept.preferredLabel,
                    type: concept.type,
                }
            };
            this.edits.push(change);
        } 
        change.value = value;
        change.date = new Date().getTime();
        if(type == Constants.QUICK_EDIT_REMOVED_RELATION || type == Constants.QUICK_EDIT_ADDED_RELATION) {
            var findRelation = (eventType) => {
                return this.edits.find((x) => {
                    return x.type == eventType &&
                            x.value.from == value.from && 
                            x.value.to == value.to &&
                            x.value.type == value.type &&
                            x.value.substitutability == value.substitutability;
                });
            };
            var qe1 = findRelation(Constants.QUICK_EDIT_REMOVED_RELATION);
            var qe2 = findRelation(Constants.QUICK_EDIT_ADDED_RELATION);
            if(qe1 && qe2) {
                // add and remove quick edits have been negated
                var index = this.edits.indexOf(qe1);
                this.edits.splice(index, 1);
                index = this.edits.indexOf(qe2);
                this.edits.splice(index, 1);
                this.save();
                EventDispatcher.fire(Constants.QUICK_EDIT_EVENT_REFRESH, change);
                return;
            }
        }
        if(isNew) {
            EventDispatcher.fire(Constants.EVENT_QUICK_EDITS_ADDED, change);
        }
        EventDispatcher.fire(Constants.EVENT_QUICK_EDITS_CHANGED, change);
        if(type == Constants.QUICK_EDIT_REMOVED_RELATION || type == Constants.QUICK_EDIT_ADDED_RELATION) {
            this.save();
        } else {
            // check if value == originalValue then remove change
            var a = change.originalValue;
            var b = change.value;
            if(typeof(change.originalValue) != "string") {
                a = JSON.stringify(a);
                b = JSON.stringify(b);
            }
            if(a == b) {            
                this.remove(change);
            } else {
                // dont want to call save twice (since its called in remove)
                this.save();
            }
        }
        return change;
    }
}

export default QuickEdits;