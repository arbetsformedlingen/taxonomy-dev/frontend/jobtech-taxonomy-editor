import Constants from './constants.jsx';
import localization from './localization.jsx';
import App from './app.jsx';

class Rest {

    setupCallbacks(http, onSuccess, onError) {
        this.currentRequest = http;
        http.onerror = () => {
            if(onError != null) {
                onError(http.status);
            }
        }
        http.onload = () => {
            if(http.status >= 200 && http.status < 300) {
                if(onSuccess != null) {
                    try {
                        var response = http.response.split("\"taxonomy/").join("\"");
                        response = response.split("preferred-label").join("preferredLabel");
                        response = response.split("preferred_label").join("preferredLabel");
                        onSuccess(JSON.parse(response));
                    } catch(err) {
                        console.log("Exception", err);
                    }
                }
            } else {
                if(onError != null) {
                    onError(http.status);
                }
            }
        }
    }

    abort() {
        if(this.currentRequest) {
            this.currentRequest.abort();
        }
        if(this.currentErrorCallback) {
            this.currentErrorCallback(499); //Client Closed Request
        }
        this.currentRequest = null;
    }

    measureTime(http, func) {
        var begin;
        http.onreadystatechange = function () {
            if (this.readyState == 1) {
                begin = Date.now();
            }    
            if (this.readyState == 4 && this.status == 200) {
                App.addExecutionTime(Constants.INFO_EVENT_TYPE_REST, begin, Date.now() - begin, func);
                //console.log("Execution time:" + (Date.now() - begin), func);
            }
        };
    }

    get(func, onSuccess, onError) {
        var http = new XMLHttpRequest();        
        this.setupCallbacks(http, onSuccess, onError);
        this.measureTime(http, func);
        http.open("GET", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    postIp(ip, func, data, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        this.measureTime(http, func);
        http.open("POST", ip + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        if(data) {
            http.setRequestHeader("Content-Type", "application/json");
        }
        http.send(data ? JSON.stringify(data) : null);
    }

    post(func, onSuccess, onError) {
        this.postIp(Constants.REST_IP, func, null, onSuccess, onError);
    }

    patch(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        this.measureTime(http, func);
        http.open("PATCH", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    delete(func, onSuccess, onError) {
        var http = new XMLHttpRequest();
        this.setupCallbacks(http, onSuccess, onError);
        this.measureTime(http, func);
        http.open("DELETE", Constants.REST_IP + func, true);
        http.setRequestHeader("api-key", Constants.REST_API_KEY);
        http.setRequestHeader("Accept", "application/json");
        http.send();
    }

    getPromise(func) {
		return new Promise((resolve, reject) => {
			var http = new XMLHttpRequest();
            this.measureTime(http, func);
			http.onerror = () => {
				reject(http.status);
			}
			http.onload = () => {
				if(http.status >= 200 && http.status < 300) {
					try {
						var response = http.response.split("\"taxonomy/").join("\"");
						response = response.split("preferred-label").join("preferredLabel");
						response = JSON.parse(response);
                        // TODO: check if this is a graphql exception
						resolve(response);
					} catch(err) {
						console.log("Exception", err);
					}
				} else {
					reject(http.status);
				}
			}
			http.open("GET", Constants.REST_IP + func, true);
			http.setRequestHeader("api-key", Constants.REST_API_KEY);
			http.setRequestHeader("Accept", "application/json");
			http.send();
		});
    }

    getVersionsPromis() {
		return this.getPromise("/main/versions");
    }
    
    getGraphQlPromise(query, fragment) {
		return this.getPromise("/graphql?query=" + encodeURIComponent("query TaxonomyEditor { " + query + " }" + (fragment != null ? fragment : "")));
    }

    getConceptRelationsPromise(id, relationType, conceptType) {
        return this.getPromise("/private/concepts?related-ids=" + id + "&relation=" + relationType + "&type=" + conceptType);
    }

    getOccupationNamesWithBroaderAndMatchingTypesFor(ids) {
        var query = "";
        for(var i=0; i<ids.length; ++i) {
            query +=
                "result" + i + ": concepts(id: \"" + ids[i] + "\", type: \"occupation-name\", include_deprecated: false, version: \"next\") { " +
                    "...request_data " + 
                "}\n";
        }
        var fragment =
            " fragment request_data on Concept {" +
                "id type preferredLabel:preferred_label " +
                "broader(include_deprecated: false) { " +
                    "type id " +
                "} " +
                "exact_match(include_deprecated: false) { " +
                    "type id " +
                "} " +
                "broad_match(include_deprecated: false) { " +
                    "type id " +
                "} " +
                "narrow_match(include_deprecated: false) { " +
                    "type id " +
                "} " +
                "close_match(include_deprecated: false) { " +
                    "type id " +
                "} " +
            "}";
        return this.getGraphQlPromise(query, fragment);
    }

    getSkillsWithBroaderHedlinesRelatedSsyksFor(ids) {
        var query = "";
        for(var i=0; i<ids.length; ++i) {
            query +=
                "result" + i + ": concepts(id: \"" + ids[i] + "\", type: \"skill\", include_deprecated: false, version: \"next\") { " +
                    "...request_data " +
                "}\n";
        }
        var fragment =
            " fragment request_data on Concept {" +
                "id type preferredLabel:preferred_label " +
                "broader(type: \"skill-headline\") { " + 
                    "type id " + 
                "} " +
                "related(type: \"ssyk-level-4\") { " +
                    "type id " +
                "} " +
            "}";
        return this.getGraphQlPromise(query, fragment);
    }

    getEscoOccupationsWithMatchingTypesFor(ids) {
        var query = "";
        for(var i=0; i<ids.length; ++i) {
            query +=
                "result" + i + ": concepts(id: \"" + ids[i] + "\", type: \"esco-occupation\", include_deprecated: false, version: \"next\") { " +
                    "...request_data " +
                "}\n";
        }
        var fragment =
            " fragment request_data on Concept {" +
                "id type preferredLabel:preferred_label " +
                "exact_match(include_deprecated: false) { " +
                    "type id " +
                "} " +
                "broad_match(include_deprecated: false) { " +
                    "type id " +
                "} " +
                "narrow_match(include_deprecated: false) { " +
                    "type id " +
                "} " +
                "close_match(include_deprecated: false) { " +
                    "type id " +
                "} " +      
            "}";
        return this.getGraphQlPromise(query, fragment);
    }
    
    async awaitPromis(method) {
        try {
            return await method();
        } catch(error) {
            if(error) {
                console.log(error);
            }
        }
        return null;
    }

    async awaitVersionsPromis() {
        try {
            return await this.getVersionsPromis();
        } catch(error) {
            if(error) {
                console.log(error);
            }
        }
        return null;
    }

    async awaitGraphQlPromise(query) {
        try {
            return await this.getGraphQlPromise(query);
        } catch(error) {
            if(error) {
                console.log(error);
            }
        }
        return null;
    }

    getGraphQL(query, onSuccess, onError) {
        // TODO: private success callback and check for exception
        var encodedQuery = encodeURIComponent("query TaxonomyEditor { " + query + " }");      
        this.get("/graphql?query=" + encodedQuery, onSuccess, onError);
    }

    getConcept(id, onSuccess, onError) {
        this.get("/private/concepts?id=" + id, onSuccess, onError);
    }

    getConceptByName(name, onSuccess, onError) {
        this.get("/private/concepts?include-deprecated=true&preferred-label=" + encodeURIComponent(name), onSuccess, onError);
    }

    getChangesForConcept(id, onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.changelog);
        };
        var query = "changelog( id: \"" + id + "\" to: \"" + new Date().toISOString() + "\" ) {"
                + " event: __typename comment timestamp user"
                + " ... on Created {"
                    + " concept {"
                        + " narrower { id }"
                        + " broader { id }"
                        + " related { id }"
                        + " replaces {id }"
                        + " replaced_by { id }"
                        + " substitutes { id, substitutability_percentage }"
                        + " substituted_by { id, substitutability_percentage }"
                        + " broad_match { id }"
                        + " close_match { id }"
                        + " exact_match { id }"
                        + " narrow_match { id }"
                        + " possible_combinations { id }"
                        + " unlikely_combinations { id }"
                        + " alternative_labels"
                        + " definition"
                        + " driving_licence_code_2013"
                        + " esco_uri"
                        + " eures_code_2014"
                        + " hidden_labels"
                        + " isco_code_08"
                        + " iso_3166_1_alpha_2_2013"
                        + " iso_3166_1_alpha_3_2013"
                        + " iso_639_1_2002"
                        + " iso_639_2_1998"
                        + " iso_639_3_2007"
                        + " iso_639_3_alpha_2_2007"
                        + " iso_639_3_alpha_3_2007"
                        + " lau_2_code_2015"
                        + " national_nuts_level_3_code_2019"
                        + " nuts_level_3_code_2013"
                        + " nuts_level_3_code_2021"
                        + " preferred_label"
                        + " quality_level"
                        + " short_description"
                        + " sni_level_code_2007"
                        + " ssyk_code_2012"
                        + " sun_education_field_code_2000"
                        + " sun_education_field_code_2020"
                        + " sun_education_level_code_2000"
                        + " sun_education_level_code_2020"
                        + " type"
                        + " unemployment_fund_code"
                        + " unemployment_type_code"                
                    + " }"
                + " }"
                + " ... on Deprecated {"
                    + "concept {"
                        + " replaced_by { id }"
                    + " }"
                + " }"
                + " ... on Updated {"
                    + " changes {"
                        + " attribute old_value new_value"
                    + " }"
                + " }"
            + " }";
        this.getGraphQL(query, onSuccess, onError);
    }

    getConceptDayNotes(id, from, to, onSuccess, onError) {
        var query = "";
        if(id == null) {
            query = "from-timestamp=" + from.toISOString() + "&to-timestamp=" + to.toISOString();            
        } else {
            query = "id=" + id;
            if(from) {
                query += "&from-timestamp=" + from.toISOString();            
            }
            if(to) {
                query += "&to-timestamp=" + to.toISOString();            
            }
        }
        this.get("/private/concept/automatic-daynotes/?" + query, onSuccess, onError);
    }

    getRelationDayNotes(id, from, to , onSuccess, onError) {
        var query = "";
        if(id == null) {
            query = "from-timestamp=" + from.toISOString() + "&to-timestamp=" + to.toISOString();            
        } else {
            query = "id=" + id;
            if(from) {
                query += "&from-timestamp=" + from.toISOString();            
            }
            if(to) {
                query += "&to-timestamp=" + to.toISOString();            
            }
        }
        this.get("/private/relation/automatic-daynotes/?" + query, onSuccess, onError);
    }

    getConcepts(type, onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"" + type + "\", include_deprecated: true, version: \"next\") " 
            + "{ id type preferredLabel:preferred_label no_esco_relation deprecated }";
        this.getGraphQL(query, onSuccess, onError);
    }

    getConceptsSkillsAndHedlines(onSuccessCallback, onError) {
        var onSuccess = (data) => {            
            for(var i=0; i<data.data.q2.length; ++i) {
                if(data.data.q2[i].broader.length == 0) {
                    data.data.concepts.push(data.data.q2[i]);
                }
            }
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"skill-headline\", include_deprecated: true, version: \"next\")"
            + " { id type preferredLabel:preferred_label deprecated"
                + " children:narrower(type: \"skill\", include_deprecated: true) { id type preferredLabel:preferred_label deprecated }"
            + " }"
            + " q2:concepts(type: \"skill\", include_deprecated: true, version: \"next\")"
            + " { id type preferredLabel:preferred_label deprecated broader(include_deprecated: true) { id }"
            + " }";
        this.getGraphQL(query, onSuccess, onError);
    }

    getConceptsGenericSkillsAndGroups(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"skill-group\", include_deprecated: true, version: \"next\") "
            + "{ "
                + "id type preferredLabel:preferred_label deprecated "
                + "children: narrower(type: \"generic-skill-group\") "
                + "{ "
                    + "id type preferredLabel:preferred_label deprecated "
                    + "children: narrower(type: \"generic-skill\") " 
                    + "{ "
                        + "id type preferredLabel:preferred_label deprecated "   
                    + "}"
                + "}"
            + "}";
        this.getGraphQL(query, onSuccess, onError);
    }

    getConceptsGeography(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"continent\", include_deprecated: true, version: \"next\") "
            + "{ "
                + "id type preferredLabel:preferred_label deprecated "
                + "children: narrower(type: \"country\") "
                + "{ "
                    + "id type preferredLabel:preferred_label deprecated "
                    + "children: narrower(type: \"region\") "
                    + "{ "
                        + "id type preferredLabel:preferred_label deprecated "
                        + "nuts_level_3_code_2013 "
                        + "children: narrower(type: \"municipality\") "
                        + "{ "
                            + "id type preferredLabel:preferred_label deprecated "
                            + "lau_2_code_2015 "
                        + "}"                                
                    + "}"    
                + "}"
            + "}";
        this.getGraphQL(query, onSuccess, onError);
    }

    getConceptsEducation(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            var concepts = [];
            concepts.push({
                preferredLabel: localization.getTypeName("sun-education-level"),
                label: localization.getTypeName("sun-education-level"),
                children: data.data.sun_education_level});
            concepts.push({
                preferredLabel: localization.getTypeName("sun-education-field"),
                label: localization.getTypeName("sun-education-field"),
                children: data.data.sun_education_field});
            onSuccessCallback(concepts);
        };
        var query = "sun_education_level: concepts(type: \"sun-education-level-1\", include_deprecated: true, version: \"next\") "
            + "{ "
                + "id type preferredLabel:preferred_label deprecated "
                + "sun_education_level_code_2020 "                                                    
                + "children: narrower(type: \"sun-education-level-2\") "
                + "{ "
                    + "id type preferredLabel:preferred_label deprecated "
                    + "sun_education_level_code_2020 "                                                    
                    + "children: narrower(type: \"sun-education-level-3\") "
                    + "{ "
                        + "id type preferredLabel:preferred_label deprecated "
                        + "sun_education_level_code_2020 "                                                    
                    + "}"    
                + "}"
            + "} "
            + "sun_education_field: concepts(type: \"sun-education-field-1\", include_deprecated: true, version: \"next\") "
            + "{ "
                + "id type preferredLabel:preferred_label deprecated "
                + "sun_education_field_code_2020 "                                                    
                + "children: narrower(type: \"sun-education-field-2\") "
                + "{ "
                    + "id type preferredLabel:preferred_label deprecated "
                    + "sun_education_field_code_2020 "                                                    
                    + "children: narrower(type: \"sun-education-field-3\") "
                    + "{ "
                        + "id type preferredLabel:preferred_label deprecated "
                        + "sun_education_field_code_2020 "
                        + "children: narrower(type: \"sun-education-field-4\") "
                        + "{ "
                            + "id type preferredLabel:preferred_label deprecated "
                            + "sun_education_field_code_2020 "                                                    
                        + "}"  
                    + "}"    
                + "}"
            + "} ";
            
        this.getGraphQL(query, onSuccess, onError);
    }

    getOccupationNamesWithBroaderAndMatchingTypes(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"occupation-name\", include_deprecated: false, version: \"next\")"
            + " { broader(include_deprecated: false) { type id } "
            + "   exact_match(include_deprecated: false) { type id } "
            + "   broad_match(include_deprecated: false) { type id } "
            + "   narrow_match(include_deprecated: false) { type id } "
            + "   close_match(include_deprecated: false) { type id } "
            + "   id type preferredLabel:preferred_label }";
        this.getGraphQL(query, onSuccess, onError);
    }

    getSkillsWithBroaderHedlinesRelatedSsyks(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"skill\", include_deprecated: false, version: \"next\")"
            + " { broader(type: \"skill-headline\") { type id } id type preferredLabel:preferred_label related(type: \"ssyk-level-4\") { type id } }";
        this.getGraphQL(query, onSuccess, onError);
    }

    getEscoOccupationsWithMatchingTypes(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"esco-occupation\", include_deprecated: false, version: \"next\")"
            + " { exact_match(include_deprecated: false) { type id } "
            + "   broad_match(include_deprecated: false) { type id } "
            + "   narrow_match(include_deprecated: false) { type id } "
            + "   close_match(include_deprecated: false) { type id } "
            + "   id type preferredLabel:preferred_label }";
        this.getGraphQL(query, onSuccess, onError);
    }

    getConceptsSsyk(type, onSuccess, onError) {
        this.get("/specific/concepts/ssyk?type=" + type, onSuccess, onError);
    }

    getSsyksAndOccupationNameRelations(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"ssyk-level-4\", include_deprecated: false, version: \"next\")"
            + " { narrower(include_deprecated: false, type: \"occupation-name\") { id type preferredLabel:preferred_label alternativeLabels:alternative_labels hiddenLabels:hidden_labels } "
            + " id type preferredLabel:preferred_label alternativeLabels:alternative_labels hiddenLabels:hidden_labels ssyk_code_2012 }";
        this.getGraphQL(query, onSuccess, onError);
    }

    getConceptRelations(id, type, relationType, onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data);
        };
        this.get("/private/concepts?related-ids=" + id + "&relation=" + relationType + "&type=" + type, onSuccess, onError);
    }

    getAllConceptRelations(id, relationType, onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data);
        };
        this.get("/private/concepts?related-ids=" + id + "&relation=" + relationType, onSuccess, onError);
    }

    getDeprecatedConcepts(onSuccess, onError) {
        this.get("/private/concepts?deprecated=true", onSuccess, onError);
    }

    searchConcepts(query, onSuccessCallback, onError) {
        var onSuccess = (data) => {
            var result = data.data.narrow.length == 1 ? data.data.narrow : data.data.wide;
            onSuccessCallback(result);
        };
        var query = 
            "wide: concepts(preferred_label_contains: \"" + query + "\", include_deprecated: true, version: \"next\") {" + 
                "id type preferredLabel:preferred_label deprecated ssyk:ssyk_code_2012 isco:isco_code_08" +
            "}" +
            "narrow: concepts(id: \"" + query + "\", include_deprecated: true, version: \"next\") {" + 
                "id type preferredLabel:preferred_label deprecated ssyk:ssyk_code_2012 isco:isco_code_08" +
            "}";
        this.getGraphQL(query, onSuccess, onError);
    }

    getVersions(onSuccess, onError) {
        this.get("/main/versions", onSuccess, onError);
    }
    
    getTypes(onSuccess, onError) {
        this.get("/main/concept/types?version=next", onSuccess, onError);
    }

    getTypesLocalization(onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concept_types);
        };
        var query = "concept_types(version: \"next\")"
            + "{ id label_en label_sv }";
        this.getGraphQL(query, onSuccess, onError);        
    }

    getConceptChanges(fromVersion, toVersion, onSuccess, onError) {
        this.get("/private/concept/changes?after-version=" + fromVersion + "&to-version-inclusive=" + toVersion, onSuccess, onError);
    }

    getRelationChanges(fromVersion, toVersion, onSuccess, onError) {
        this.get("/private/relation/changes?after-version=" + fromVersion + "&to-version-inclusive=" + toVersion, onSuccess, onError);
    }

    getUnpublishedConceptChanges(fromVersion, onSuccess, onError) {
        this.get("/private/concept/changes?after-version=" + fromVersion, onSuccess, onError);
    }

    getUnpublishedRelationChanges(fromVersion, onSuccess, onError) {
        this.get("/private/relation/changes?after-version=" + fromVersion, onSuccess, onError);
    }

    getConceptsExtraField(type, extraField, onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(type: \"" + type + "\", include_deprecated: true, version: \"next\")"
            + " { id type preferredLabel:preferred_label deprecated sort_order " + extraField + " }"; 
        this.getGraphQL(query, onSuccess, onError);
    }

    getConceptExtraField(id, extraField, onSuccessCallback, onError) {
        var onSuccess = (data) => {
            onSuccessCallback(data.data.concepts);
        };
        var query = "concepts(id: \"" + id + "\", include_deprecated: true, version: \"next\")"
            + " {  id type preferredLabel:preferred_label alternativeLabels:alternative_labels hiddenLabels:hidden_labels definition deprecated no_esco_relation quality_level last_changed short_description sort_order " + extraField
                + " broader(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " narrower(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " related(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " replaced_by(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " substitutes(include_deprecated: true) { id type deprecated preferredLabel:preferred_label substitutability_percentage }"
                + " broad_match(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " narrow_match(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " close_match(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " exact_match(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " possible_combinations(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
                + " unlikely_combinations(include_deprecated: true) { id type deprecated preferredLabel:preferred_label isco:isco_code_08 ssyk:ssyk_code_2012 }"
            + " }";
        this.getGraphQL(query, onSuccess, onError);
    }

    deleteConcept(id, comment, onSuccess, onError) {
        this.delete("/private/delete-concept?id=" + id + "&comment=" + encodeURIComponent(comment), onSuccess, onError);
    }

    getGraph(relationType, sourceType, targetType, onSuccess, onError) {
        var removeDuplicateNodes = (data) => {
            return data.filter((item, i) => {
                var p = data.find((e) => {
                    return e.id == item.id;
                });
                return data.indexOf(p) == i;
            });
        }
        var removeDuplicateEdges = (data) => {
            return data.filter((item, i) => {
                var p = data.find((e) => {
                    return e.target == item.target && e.source == item.source;
                });
                return data.indexOf(p) == i;
            });
        };
        var onSuccessCallback = (data) => {
            data.graph.nodes = removeDuplicateNodes(data.graph.nodes);
            data.graph.edges = removeDuplicateEdges(data.graph.edges);
            onSuccess(data);
        };
        this.get("/private/graph?edge-relation-type=" + relationType + "&source-concept-type=" + sourceType + "&target-concept-type=" + targetType, onSuccessCallback, onError)
    }

    patchConcept(id, comment, args, onSuccess, onError) {
        this.patch("/private/accumulate-concept?id=" + id + "&comment=" + encodeURIComponent(comment) + args, onSuccess, onError);
    }

    postConcept(type, comment, preferredLabel, definition, qualityLevel, onSuccess, onError) {
        this.post("/private/concept?type=" + type + "&comment=" + encodeURIComponent(comment) + "&definition=" + definition + "&preferred-label=" + preferredLabel + ((qualityLevel == null || qualityLevel == "")  ? "" : "&quality-level=" + qualityLevel), onSuccess, onError);
    }

    postReplaceConcept(oldId, newId, comment, onSuccess, onError) {
        this.post("/private/replace-concept?old-concept-id=" + oldId + "&new-concept-id=" + newId + "&comment=" + encodeURIComponent(comment), onSuccess, onError);
    }
    
    postUnreplaceConcept(oldId, newId, comment, onSuccess, onError) {
        this.post("/private/unreplace-concept?old-concept-id=" + oldId + "&new-concept-id=" + newId + "&comment=" + encodeURIComponent(comment), onSuccess, onError);
    }

    postAddRelation(conceptId, relationId, relationType, substitutability, comment, onSuccess, onError) {
        var query = "concept-1=" + conceptId + "&concept-2=" + relationId + "&relation-type=" + relationType + "&comment=" + encodeURIComponent(comment);
        if(substitutability) {
            query += "&substitutability-percentage=" + substitutability;
        }
        this.post("/private/relation?" + query, onSuccess, onError);
    }

    deleteRelation(relationType, sourceId, targetId, comment, onSuccess, onError) {
        this.delete("/private/delete-relation?relation-type=" + relationType + "&concept-1=" + sourceId + "&concept-2=" + targetId + "&comment=" + encodeURIComponent(comment), onSuccess, onError);
    }

    postDayNote(conceptId, comment, onSuccess, onError) {
        this.post("/private/concept/automatic-daynotes/?id=" + conceptId + "&comment=" + encodeURIComponent(comment), onSuccess, onError);
    }

    postNewVersion(versionId, timestamp, onSuccess, onError) {
        this.post("/private/versions?new-version-id=" + versionId + "&new-version-timestamp=" + timestamp.toISOString(), onSuccess, onError)
    }

    patchRemoveAlternativeLabel(id, comment, label, onSuccessCallback, onError) {
        var onSuccess = () => {
            onSuccessCallback(label);
        };
        this.patch("/private/remove-alternative-label?id=" + id + "&comment=" + encodeURIComponent(comment) + "&alternative-label=" + encodeURIComponent(label), onSuccess, onError);
    }

    patchRemoveHiddenLabel(id, comment, label, onSuccessCallback, onError) {
        var onSuccess = () => {
            onSuccessCallback(label);
        };
        this.patch("/private/remove-hidden-label?id=" + id + "&comment=" + encodeURIComponent(comment) + "&hidden-label=" + encodeURIComponent(label), onSuccess, onError);
    }

    postConceptTypes(type, label_en, label_sv, onSuccess, onError) {
        this.post("/private/concept-types?concept-type=" + type + "&label-en=" + encodeURIComponent(label_en) + "&label-sv=" + encodeURIComponent(label_sv), onSuccess, onError);
    }

    patchRelation(sourceId, targetId, fromType, toType, substitutability, comment, onSuccess, onError) {
        var query = "concept-1=" + sourceId + "&concept-2=" + targetId + "&relation-type=" + fromType + "&new-relation-type=" + toType + "&comment=" + encodeURIComponent(comment);
        if(substitutability) {
            query += "&substitutability-percentage=" + substitutability;
        }
        this.patch("/private/accumulate-relation?" + query, onSuccess, onError);
    }

    postSemanticSearch(type, queryArray, limit, onSuccess, onError) {
        var data = {
            array_of_words: queryArray,
            concept_type: type,
            limit_number: limit
        };
        this.postIp(Constants.SEMANTIC_IP, "/semantic-concept-search/", data, onSuccess, onError);
    }

}

export default new Rest;