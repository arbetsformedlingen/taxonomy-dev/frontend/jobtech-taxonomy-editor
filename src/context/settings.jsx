import Constants from './constants.jsx';
import Localization from './localization.jsx';

class Settings { 

    constructor() {
        this.user = "default"; // when user accounts are available, set this with user id
        // settings configurable from UI
        this.data = {
            editableTypes: [
                "ssyk-level-4",
                "keyword",
                "occupation-collection",
                "occupation-field",
                "occupation-name",
                "skill",
            ],
            visibleTypes: [
                "ssyk-level-1",
                "ssyk-level-2",
                "ssyk-level-3",
                "ssyk-level-4",
                "isco-level-4",
                "driving-licence",
                "employment-duration",
                "generic-skill",
                "employment-type",
                "keyword",
                "language",
                "language-level",
                "occupation-collection",
                "occupation-field",
                "occupation-name",
                "occupation-experience-year",
                "skill",
                "skill-headline",
                "sni-level-1",
                "sni-level-2",
                "wage-type",
                "worktime-extent",
                "unemployment-fund",
                "unemployment-type",
                "geography_",
                "education_"
            ],
            validation_groups: [{
                type: "skill-headline",
                children: [{
                    label: Localization.get("generic_skills"),
                    id: "Ka2f_mQd_FvC",
                    enabled: true,
                }],
            }],            
        };
        this.geographyTypes = [
            "continent",
            "country",
            "region",
            "municipality"
        ];
        this.educationTypes = [
            "sun-education-field-1",
            "sun-education-field-2",
            "sun-education-field-3",
            "sun-education-field-4",
            "sun-education-level-1",
            "sun-education-level-2",
            "sun-education-level-3"
        ];
        // automatic settings depending on user behaviour
        this.preferences = {
            isLoaded: false,
            groupVisability: {
                info: true,
                relations: true,
                history: true,
            },
            tab_1_type: Constants.CONCEPT_SSYK_LEVEL_4,
        };
    }

    getKey(name) {
        return this.user + "_" + name;
    }

    isEditable(type) {
        return this.data.editableTypes.indexOf(type) != -1;
    }

    isVisible(type) {
        return this.data.visibleTypes.indexOf(type) != -1;
    }

    isGeographyOrEducation(type) {
        return this.geographyTypes.indexOf(type) != -1 || this.educationTypes.indexOf(type) != -1;
    }

    shouldSkipValidation(type, id) {
        var group = this.data.validation_groups.find((e) => {
            return e.type == type;
        });
        if(group) {
            var child = group.children.find((e) => {
                return e.id == id;
            });
            if(child) {
                return child.enabled;
            }
        }
        return false;
    }

    getPreferences() {
        if(!this.preferences.isLoaded) {
            var item = localStorage.getItem(this.getKey("preferences"));
            if(item != null) {
                this.preferences = JSON.parse(item);
                this.preferences.isLoaded = true;
            }
        }
        return this.preferences;
    }

    savePreferences() {
        localStorage.setItem(this.getKey("preferences"), JSON.stringify(this.preferences));
    }

    load() {
        var data = localStorage.getItem(this.getKey("data"));
        if(data) {
            this.data = JSON.parse(data);
        };
        // validate minimum settings requirment
        if(this.data.validation_groups == null) {
            this.data.validation_groups = [{
                type: "skill-headline",
                children: [{
                    label: Localization.get("generic_skills"),
                    id: "Ka2f_mQd_FvC",
                    enabled: true,
                }],
            }];
        }
        //TODO remove later
        if(!this.isVisible("geography_")) {
            this.data.visibleTypes.push("geography_");
        }
        if(!this.isVisible("education_")) {
            this.data.visibleTypes.push("education_");
        }
    }

    save() {
        localStorage.setItem(this.getKey("data"), JSON.stringify(this.data));
    }

}

export default new Settings;