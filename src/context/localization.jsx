
import Constants from './constants.jsx';

class Localization { 

    constructor() {
        // localization
        this.resources = {
            // swedish
            sv: {
                locale: "sv",
                db_create_new_type_: "Nytt värdeförråd",
                /* db types */
                db_geography_: "Geografi",
                db_education_: "Utbildning",
                db_continent: "Världsdel",
                db_country: "Land",
                "db_driving-licence": "Körkort",
                "db_employment-duration": "Anställningsvaraktighet",
                "db_employment-type": "Anställningsform",
                "db_employment-variety": "Anställningsvariation",
                "db_esco-occupation": "ESCO-yrke",
                "db_generic-skill": "Generell kompetens",
                "db_generic-skill-group": "Underkategori",
                "db_isco": "ISCO",
                "db_isco-level-4": "ISCO nivå 4",
                db_keyword: "Sökbegrepp",
                db_language: "Språk",
                "db_language-level": "Språknivå",
                db_municipality: "Kommun",
                "db_occupation-collection": "Yrkessamling",
                "db_occupation-experience-year": "Tid i yrke",
                "db_occupation-field": "Yrkesområde",
                "db_occupation-name": "Yrkesbenämning",
                db_region: "EU-Region",
                "db_self-employment-type": "Egenföretagandetyp",
                db_skill: "Kompetensbegrepp",
                "db_skill-group": "Kompetensgrupp",
                "db_skill-headline": "Kompetensgrupp",
                "db_sni": "SNI",
                "db_sni-level-1": "SNI nivå 1",
                "db_sni-level-2": "SNI nivå 2",
                "db_sni-level-3": "SNI nivå 3",
                "db_sni-level-4": "SNI nivå 4",
                "db_sni-level-5": "SNI nivå 5",
                "db_ssyk": "SSYK",
                "db_ssyk-level-1": "SSYK nivå 1",
                "db_ssyk-level-2": "SSYK nivå 2",
                "db_ssyk-level-3": "SSYK nivå 3",
                "db_ssyk-level-4": "SSYK nivå 4",
                "db_sun-education-field": "Utbildningsriktning",
                "db_sun-education-field-1": "Utbildningsinriktning nivå 1",
                "db_sun-education-field-2": "Utbildningsinriktning nivå 2",
                "db_sun-education-field-3": "Utbildningsinriktning nivå 3",
                "db_sun-education-field-4": "Utbildningsinriktning nivå 4",
                "db_sun-education-level": "Utbildningsnivå",
                "db_sun-education-level-1": "Utbildningsnivå nivå 1",
                "db_sun-education-level-2": "Utbildningsnivå nivå 2",
                "db_sun-education-level-3": "Utbildningsnivå nivå 3",
                "db_unemployment-fund": "A-kassa",
                "db_unemployment-type": "Sökandekategorier (SKAT)",
                "db_wage-type": "Löneform",
                "db_worktime-extent": "Arbetstid",
                "db_swedish-retail-and-wholesale-council-skill": "Handelsrådets kompetenser",
                "db_forecast-occupation": "Prognosyrken",
                "concept/definition": "Definition",
                "concept/short-description": "Kort beskrivning",
                "concept/id": "Concept-ID",
                "concept/preferredLabel": "Namn",
                "concept/type": "Värdeförråd",
                "concept/sort-order": "Sorteringsordning",
                "concept/quality-level": "Kvalitetsnivå",
                "concept.external-database.ams-taxonomy-67/id": "Externt id",
                "concept.external-standard/ssyk-code-2012": "SSYK-kod",
                "concept.external-standard/eures": "EURES",
                "concept.external-standard/nnuts": "NNUTS",
                "concept.external-standard/nuts-level-3-code-2013": "NUTS nivå 3",
                "concept.external-standard/isco-code-08": "ISCO-kod",
                "concept.external-standard/lau-2-code-2015": "LAU 2 2015",
                "concept.external-standard/driving-licence-code-2013": "Körkortskod 2013", 
                "concept.external-standard/implicit-driving-licences": "Implicit körkort",
                "concept.external-standard/iso-3166-1-alpha-2-2013": "ISO-3166-1 alpha 2",
                "concept.external-standard/iso-3166-1-alpha-3-2013": "ISO-3166-1 alpha 3",
                "concept.external-standard/sni-level-code-2007": "SNI nivå 2007",
                "concept.external-standard/iso-639-3-alpha-2-2007": "ISO-639-3 alpha 2",
                "concept.external-standard/iso-639-3-alpha-3-2007": "ISO-639-3 alpha 3",
                "concept.external-standard/sun-education-field-code-2020": "Utbildningsinriktning",
                "concept.external-standard/sun-education-level-code-2020": "Utbildningsnivå",     
                "preferredLabel": "Namn",
                "preferred_label": "Namn",
                "alternative-labels": "Alternativa benämningar",
                "alternative_labels": "Alternativa benämningar",
                "hidden-labels": "Dolda benämningar",
                "hidden_labels": "Dolda benämningar",
                "definition": "Definition",
                "short-description": "Kort beskrivning",
                "sort-order": "Sorteringsordning",
                "quality-level": "Kvalitetsnivå",
                "ams-taxonomy-67/id": "Externt id",
                "ssyk-code-2012": "SSYK-kod",
                "eures": "EURES",
                "nnuts": "NNUTS",
                "nuts-level-3-code-2013": "NUTS nivå 3",
                "isco-code-08": "ISCO-kod",
                "lau-2-code-2015": "LAU 2 2015",
                "driving-licence-code-2013": "Körkortskod 2013", 
                "implicit-driving-licences": "Implicit körkort",
                "iso-3166-1-alpha-2-2013": "ISO-3166-1 alpha 2",
                "iso-3166-1-alpha-3-2013": "ISO-3166-1 alpha 3",
                "sni-level-code-2007": "SNI nivå 2007",
                "iso-639-3-alpha-2-2007": "ISO-639-3 alpha 2",
                "iso-639-3-alpha-3-2007": "ISO-639-3 alpha 3",
                "sun-education-field-code-2020": "Utbildningsinriktning",
                "sun-education-level-code-2020": "Utbildningsnivå",     
                /* Actions */
                CREATED: "Skapad",
                UPDATED: "Uppdaterad",
                DEPRECATED: "Avaktualiserad",
                COMMENTED: "Anteckning",
                Created: "Skapad",
                Updated: "Uppdaterad",
                Deprecated: "Avaktualiserad",
                Commented: "Anteckning",
                /* regular words */          
                "occupation-name": "Yrkesbenämning",
                "occupation-field": "Yrkesområde",
                esco_uri: "ESCO uri",
                concept_reactivated: "Återaktualiserad",
                relation_type: "Relationstyp",
                added_relation: "Skapade relationer av typ",
                removed_relation: "Borttagna relationer av typ",
                info: "Info",
                database_id: "Databas-ID",
                competense: "Kompetens",
                connections: "Relationer",
                connection: "Relation",
                undefined: "Ej definierad",
                quality_control: "Kvalitetsnivå",
                add_connection: "Lägg till relation",
                remove_connection: "Ta bort relation",
                edit_connection: "Ändra relationstyp",
                add_comment: "Manuell anteckning",
                edit_type: "Välj typ av ändring",
                name: "Namn",
                description: "Definition",
                alternative_names: "Alternativa benämningar",
                hidden_names: "Dolda benämningar",
                alternative_label: "Alternativa/dolda benämningar",
                refresh_cache: "Uppdatera cache",
                history: "Historik",
                version: "Version",
                content: "Innehåll",
                changes: "Förändringar",
                show_deprecated_connections: "Visa avaktualiserade",
                relation_changes: "Relationsförändringar",
                concept_changes: "Begreppsförändringar",
                back: "Tillbaka",
                from: "Från",
                to: "Till",
                add: "Lägg till",
                remove: "Ta bort",
                remove_selected: "Ta bort markerade",
                new_value: "Nytt begrepp",
                quality_classification: "Kvalitetsnivå",
                quality_level_short: "KN",
                save: "Spara",
                abort: "Avbryt",
                yes: "Ja",
                no: "Nej",
                create: "Skapa",
                next: "Nästa",
                previous: "Föregående",
                ignore: "Ignorera",
                reset: "Återställ",
                size: "Storlek",
                clear: "Rensa",
                settings: "Inställningar",
                saving: "Sparar",
                change_log: "Förändringslogg",
                value_storage: "Värdeförråd",
                event: "Händelse",
                message: "Meddelande",
                visit: "Gå till",
                visit_concept: "Gå till begrepp",
                visit_replaced_by: "Gå till hänvisat begrepp",
                title_filter: "Filter",
                skill_headline: "Kompetens grupp",
                filter: "Filtrera",
                search: "Sök",
                loading: "Laddar",
                select: "Välj",
                referred_to: "Hänvisas till",
                set_reference: "Hänvisa",
                referred: "Hänvisningar",
                not_referred: "Saknar hänvisning",                 
                reactivate: "Återaktualisera",
                deprecated: "Avaktualiserad",
                deprecate: "Avaktualisera",
                dialog_deprecate: "Är du säker på att du vill avaktualisera",
                dialog_unsaved_changes: "Du har osparade förändringar, vill du spara?",
                close: "Stäng",
                show: "Visa",
                change_note: "Anteckning",
                add_connection: "Ny relation",
                of: "Av",
                to: "Till",
                missing_connections: "Saknar relationer",
                has_connections: "Har relationer",
                are_missing_connections: "saknar relation",
                are_connected: "har relation",
                no_items_connected: "Ingen relation existerar",
                all_items_connected: "Alla element har befintlig relation",
                statistics: "Statistik",     
                relation_type: "Relationstyp",
                relation: "Relation",
                relations: "Relationer",
                value_type: "Värde typ",
                note_title: "Anteckningsrubrik",
                note: "Anteckning",
                default_option: "-- välj",
                error_add_connection_1: "Anteckning kan inte vara blank, vänligen ange förklaring till relation",
                error_add_connection_2: "Ingen relation är markerad",
                error_add_connection_3: "Ogilltig relation, rötter kan inte väljas som relation",
                horizontal_connections: "Horisontella relationer",
                vertical_connections: "Vertikala relationer",
                type: "Typ",
                types: "Typer",
                editable: "Redigerbar",
                edit: "Redigera",
                visible: "Synlig",
                show_deprecated: "Visa avaktualiserade",
                concept: "Begrepp",
                replaced_by_concept: "Hänvisat begrepp",
                "replaced-by": "Hänvisning",
                action: "Händelse",
                actions: "Händelser",
                select_search: "Typ av sökning",
                select_date: "Datumintervall",
                select_actions: "Välj typ av händelse",
                select_relations: "Välj relationer",
                select_types: "Välj ett eller flera värdeförråd",
                search_result: "Sökresultat",
                not_published: "Ej publicerad",
                export_headline: "Exporterings inställningar",
                export_pdf: "PDF",
                export_excel: "Excel",
                export: "Exportera",
                exporting: "Exporterar",
                code: "Kod",
                date: "Datum",
                new_version: "Ny version",
                validate_concepts: "Validera begrepp",
                error: "Fel",
                publish_error_0: "Relation till SSYK-4 saknas",
                publish_error_1: "Relation till ISCO-4 saknas",
                publish_error_2: "Relation till SSYK-4 saknas",
                publish_error_3: "Relation till kompetensgrupp saknas",
                publish_error_4: "Matchande relation saknas",
                publish: "Publicera",
                from_name: "Från namn",
                from_type: "Från typ",
                from_id: "Från id",
                to_name: "Till namn",
                to_type: "Till typ",
                to_id: "Till id",
                graph: "Graf",
                tools: "Verktyg",
                against: "till",
                changed: "Uppdaterat",
                relation_created: "Relation skapad",
                relation_removed: "Relation borttagen",
                relation_updated: "Relation uppdaterad",
                relation_weight: "Relationsvikt",
                last_changed: "Senast uppdaterad",
                concept_validation: "Begrepps validering (hela databasen)",
                weight: "Vikt",
                edit_concept_relation_text: "Markera det begrepp som den nya relationen ska gå till",
                edit_concept_recommended_relation: "Den rekommenderade relationstypen är",
                visit_latest_search: "Visa senaste sökresultat",
                app_name: "Taxonomieditor",
                set_user_name: "Användarnamn",
                set_user_pass: "Lösenord",
                login: "Logga in",
                invalid_user_name_or_pass: "Ogiltig inloggning",
                event_info: "Information om händelse",
                time: "Tid",                
                publish_to: "Visa ändringar till:",
                timestamp: "tidpunkt",
                of: "av",
                tip_relation_exists: "En koppling till detta begrepp finns redan",
                tip_relation_sinlge_requirment: "En koppling till ett begrepp av denna typ finns redan",
                name_invalid: "Ett begrepp med detta namn existerar redan",
                name_invalid_deprecated: "Ett avaktualiserat begrepp med detta namn existerar redan",
                other_connections: "Kopplingar",
                substitutability_high: "Högt släktskap",
                substitutability_low: "Lägre släktskap",
                substitutability_connections: "Besläktade yrken",
                settings_visibility: "Synlighet",
                settings_validation: "Versions undantag",
                settings_user_manual: "Användarmanual",
                settings_type_localization: "Typbenämningar",
                settings_key_bindings: "Kortkommandon",
                label_en: "Engelsk benämning (en)",
                label_sv: "Svensk benämning (sv)",
                ignored_validation_concepts: "Undantagna begrepp",
                preselected_type: "Förvald begrepps typ",
                fix: "Åtgärda",
                import_new_concepts: "Importera nya begrepp",
                continue_import_new_concepts: "Fortsätt importera begrepp",
                clear_import_data: "Rensa listan",
                drop_files_here: "Dra och släpp fil här (*.xlsx)",
                choose_file: "Välj fil",
                importing: "importerar",
                import_concepts: "Importera begrepp",
                imported_from: "Importerad från",
                sort_order_short: "Ordning",
                add_label: "Lägg till",
                new_concept: "Nytt begrepp",
                create_new_concept: "Skapa som nytt begrepp",
                alternate_label_created: "skapad som alternativ benämning på",
                item_created: "skapad som nytt begrepp",
                already_exists: "finns redan som",
                marked_as_trash: "markerad som skräp",
                process: "Bearbeta",
                export_not_processed: "Exportera ej bearbetade",
                mark_as: "Markera begrepp som",
                trash: "Skräp",
                not_trash: "Ej skräp",
                show_processed: "Visa bearbetade",
                of: "av",
                generic_skills: "Generella kompetenser",
                left_to_process: "kvar att bearbeta",
                matches_connections: "Matchningar",
                broad_match: "Mer generella",
                narrow_match: "Mer specialiserade",
                close_match: "Snarlika",
                exact_match: "Motsvarar",
                combinations: "Kombinationer",
                possible_combinations: "Möjliga konbinationer",
                unlikely_combinations: "Osannolika kombinationer",
                latest_visited: "Senast besökt:",
                latest_searched: "Senaste sökningar:",
                grouped_by_type: "Visa grupperat på relationstyp",
                alternative_label: "alternativ benämning",
                hidden_label: "dold benämning",
                contains: "Innehåller",
                not_contains: "Innehåller inte",
                starts_with: "Börjar med",
                ends_with: "Slutar med",
                select_all: "Markera alla",
                unselect_all: "Avmarkera alla",
                all_matching: "alla mappningsrelationer",
                relation_search: "Relationssökning",
                add_on_concept: "Lägg till som alternativ benämning på en befintlig yrkesbenämning",
                choose: "Välj",
                mark: "Markera",
                duplicate: "Dublett",
                investigate: "Utred",
                not_investigate: "Ej Utred",
                comment: "Anteckning",
                order_name: "Namn",
                order_state: "Status",
                order_by: "Sortera efter",
                key_focus_search: "Fokusera i sök / filter",
                key_focus_types: "Fokusera värdeförråd",
                key_new_concept: "Nytt begrepp",
                key_new_relation: "Ny relation",
                key_toggle_quick_edit: "Växla mellan snabbredigering",
                key_open_concept_edit: "Öppna redigerings dialogen",
                key_open_quick_edits: "Öppna opublicerade förändringar",
                key_save_dialog: "Spara förändringar (gäller dialoger med spara knapp)",
                show_missing: "Saknar relationer",
                settings_info: "Information",
                information: "Information",
                no_esco_relation: "Ingen ESCO-relation",
                copy: "Kopiera",
            },
        }
        this.types = [];
        // find active language
        this.code = this.setLanguage();
    }
	
    setLanguage() {
        var lang = navigator.language || navigator.userLanguage;
        var langSimple = lang.split("-")[0];
        var key = null;
        console.log("local language: " + lang);
        if(Constants.lang) {
            console.log("override language: " + Constants.lang);
            lang = Constants.lang;
        }
        if(this.resources[lang] != null) {
            key = lang;
        } else if(this.resources[langSimple] != null) {
            key = langSimple;
        }
        if(key != null) {
            console.log("selecting localization: " + key);
            this.lang = this.resources[key];            
        } else {
            console.log("localization not found: '" + key + "' selecting default");
            this.lang = this.resources["sv"];            
        }
        return lang;
    }

    get(key) {
        if(this.lang[key] == null) {
            var en = this.resources["en"];
            if(en == null || en[key] == null) {
                return key;
            }
            return en[key];
        }
        return this.lang[key];
    }

    getUpper(key) {
        return this.lang[key].toUpperCase();
    }
    
    getLower(key) {
        return this.lang[key].toLowerCase();
    }

    getTypeName(type) {
        var key = this.code.indexOf("en") > 0 ? "label_en" : "label_sv";
        var name = this.getTypeNameForLang(type, key);
        if(name != null) {
            return name;
        }
        if(type && type.endsWith("_")) {
            return this.get("db_" + type);
        }
        return type;
    }

    getTypeNameForLang(type, key) {
        var t = this.types.filter((e) => {return e.id == type});
        if(t.length > 0) {
            var val = t[0][key];
            if(val && val.length > 0) {
                return val;
            }
        }
        if(type == "search") {
            if(key == "label_en") {
                return "Search";
            } else {
                return "Sök";
            }
        }
        return null;
    }

    setTypeNameForLang(type, key, name) {
        var t = this.types.filter((e) => {return e.id == type});
        if(t.length > 0) {
            t[0][key] = name;
        } else {
            t = {id: type};
            t[key] = name;
            this.types.push(t);
        }
    }
}

export default new Localization;