import Constants from './constants.jsx';
import Rest from './rest.jsx';
import Socket from './socket.jsx';
import App from './app.jsx';
import Util from './util.jsx';

class ConceptEdit {

    name(id, value, comment, onSuccess, onError) {
        Rest.patchConcept(id, comment, "&preferred-label=" + value.trim(), () => {
            App.removeSaveRequest();
            Socket.sendEditSave(id, "name");
            if(onSuccess) {
                onSuccess();
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera namn");
            App.removeSaveRequest();
            if(onError) {
                onError(status);
            }
        });
    }

    definition(id, value, comment, onSuccess, onError) {
        Rest.patchConcept(id, comment, "&definition=" + encodeURIComponent(value), () => {
            App.removeSaveRequest();
            Socket.sendEditSave(id, "description");
            if(onSuccess) {
                onSuccess();
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera definition");
            App.removeSaveRequest();
            if(onError) {
                onError(status);
            }
        });
    }

    noEscoRelation(id, value, comment, onSuccess, onError) {
        Rest.patchConcept(id, comment, "&no-esco-relation=" + value, () => {
            App.removeSaveRequest();
            Socket.sendEditSave(id, "no_esco_relation");
            if(onSuccess) {
                onSuccess();
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera ingen ESCO-relation");
            App.removeSaveRequest();
            if(onError) {
                onError(status);
            }
        });
    }
    
    alternativeOrHiddenLabels(isHidden, id, oldLabels, newLabels, comment, onSuccess, onError) {
        var added = [];
        var removed = [];
        // find added
        for(var i=0; i<newLabels.length; ++i) {
            if(oldLabels.indexOf(newLabels[i]) < 0) {
                added.push(newLabels[i]);
            }
        }
        // find removed
        for(var i=0; i<oldLabels.length; ++i) {
            if(newLabels.indexOf(oldLabels[i]) < 0) {
                removed.push(oldLabels[i]);
            }
        }
        if(added.length == 0 && removed.length == 0) {
            App.removeSaveRequest();
            onSuccess();
            return;
        }
        // setup save requests
        var totalRequests = (added.length > 0 ? 1 : 0) + removed.length - 1;
        for(var i=0; i<totalRequests; ++i) {
            App.addSaveRequest();
        }
        var buildString = () => {
            if(added.length == 0) {
                return "";
            }
            var result = added[0];
            for(var i=1; i<added.length; ++i) {
                result += "|" + added[i];
            }
            return (isHidden ? "&hidden-labels=" : "&alternative-labels=") + encodeURIComponent(result);
        };
        var patch = (index, errorCount) => {
            if(index == -1) {
                if(added.length > 0) {
                    Rest.patchConcept(id, comment, buildString(), () => {
                        App.removeSaveRequest();
                        Socket.sendEditSave(id, isHidden ? "hiddenLabels" : "alternativeLabels");
                        if(onSuccess) {
                            onSuccess();
                        }
                    }, (status) => {
                        if(isHidden) {
                            App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera dolda benämningar");
                        } else {
                            App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera alternativa benämningar");
                        }
                        App.removeSaveRequest();
                        if(onError) {
                            onError();
                        }
                    });
                } else if(errorCount > 0) {
                    if(onError) {
                        onError();
                    }
                } else if(onSuccess) {
                    Socket.sendEditSave(id, isHidden ? "hiddenLabels" : "alternativeLabels");
                    onSuccess();
                }
            } else {
                if(isHidden) {
                    Rest.patchRemoveHiddenLabel(id, comment, removed[index], () => {
                        App.removeSaveRequest();
                        patch(index - 1);
                    }, (status) => {
                        App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera dolda benämningar");
                        App.removeSaveRequest();
                        patch(index - 1, errorCount + 1);
                    });
                } else {
                    Rest.patchRemoveAlternativeLabel(id, comment, removed[index], () => {
                        App.removeSaveRequest();
                        patch(index - 1);
                    }, (status) => {
                        App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera alternativa benämningar");
                        App.removeSaveRequest();
                        patch(index - 1, errorCount + 1);
                    });
                }
            }
        };
        patch(removed.length - 1, 0);
    }

    alternativeLabels(id, oldLabels, newLabels, comment, onSuccess, onError) {
        this.alternativeOrHiddenLabels(false, id, oldLabels, newLabels, comment, onSuccess, onError);
    }
    
    hiddenLabels(id, oldLabels, newLabels, comment, onSuccess, onError) {
        this.alternativeOrHiddenLabels(true, id, oldLabels, newLabels, comment, onSuccess, onError);
    }

    qualityLevel(id, value, comment, onSuccess, onError) {
        value = value == "undefined" ? "" : value;
        Rest.patchConcept(id, comment, "&quality-level=" + value.trim(), () => {
            App.removeSaveRequest();
            Socket.sendEditSave(id, "qualityLevel");
            if(onSuccess) {
                onSuccess();
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera kvalitetsnivå");
            App.removeSaveRequest();
            if(onError) {
                onError(status);
            }
        });
    }

    addRelation(from, to, type, substitutability, comment, onSuccess, onError) {      
        var fromId = from.id;
        var toId = to.id;
        if(type == Constants.RELATION_NARROWER) {
            var tmp = fromId;
            fromId = toId;
            toId = tmp;
            type = Constants.RELATION_BROADER;
        }
        if(type == "possible-combinations") {
            type = "possible-combination";
        }
        if(type == "unlikely-combinations") {
            type = "unlikely-combination";
        }
        if(Util.shouldSwapRelationOrigin(from.type, to.type, type)) {
            var tmp = fromId;
            fromId = toId;
            toId = tmp;
        }
        Rest.postAddRelation(fromId, 
                            toId, 
                            type,
                            type == "substitutability" ? substitutability : null,
                            comment,
                            () => {
            Socket.sendEditSave(fromId, "add_connection", {
                id1: fromId,
                id2: toId
            });
            App.removeSaveRequest();
            if(onSuccess) {
                onSuccess();
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades lägga till relation");
            App.removeSaveRequest();
            if(onError) {
                onError(status);
            }
        });
    }
    
    removeRelation(from, to, type, comment, onSuccess, onError) {
        var id = from;
        var target = to;
        if(type == Constants.RELATION_NARROWER) {
            var tmp = from;
            from = to;
            to = tmp;
            type = Constants.RELATION_BROADER;
        } 
        type = type.replace('_', '-');
        if(type == "possible-combinations") {
            type = "possible-combination";
        }
        if(type == "unlikely-combinations") {
            type = "unlikely-combination";
        }
        Rest.deleteRelation(type, from, to, comment, () => {
            Socket.sendEditSave(id, "remove_connection", {
                id1: id,
                id2: target
            });
            App.removeSaveRequest();
            if(onSuccess) {
                onSuccess();
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades ta bort relation");
            App.removeSaveRequest();
            if(onError) {
                onError(status);
            }
        });
    }

    editRelation(from, to, current_type, new_type, substitutability, comment, onSuccess, onError) {
        var fromId = from.id;
        var toId = to.id;
        if(new_type == Constants.RELATION_NARROWER) {
            var tmp = fromId;
            fromId = toId;
            toId = tmp;
            new_type = Constants.RELATION_BROADER;
        }
        if(new_type == "possible-combinations") {
            new_type = "possible-combination";
        }
        if(new_type == "unlikely-combinations") {
            new_type = "unlikely-combination";
        }
        if(Util.shouldSwapRelationOrigin(from.type, to.type, new_type)) {
            var tmp = fromId;
            fromId = toId;
            toId = tmp;
        }
        Rest.patchRelation(fromId, 
                            toId, 
                            current_type,
                            new_type,
                            new_type == "substitutability" ? substitutability : null,
                            comment,
                            () => {
            Socket.sendEditSave(fromId, "edit_connection", {
                id1: fromId,
                id2: toId
            });
            App.removeSaveRequest();
            if(onSuccess) {
                onSuccess();
            }
        }, (status) => {
            App.showError(Util.getHttpMessage(status) + " : misslyckades uppdatera relation");
            App.removeSaveRequest();
            if(onError) {
                onError(status);
            }
        });
    }
}

export default new ConceptEdit;