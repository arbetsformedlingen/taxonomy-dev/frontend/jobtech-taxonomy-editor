import React from 'react';
import EventDispatcher from './event_dispatcher.jsx';
import Util from './util.jsx';
import CacheManager from './cache_manager.jsx';
import Constants from './constants.jsx';
import Rest from './rest.jsx';
import Localization from './localization.jsx';
import Save from './../view/dialog/save.jsx';
import Socket from './socket.jsx';
import Settings from './settings.jsx';
import QuickEdits from './quick_edits.jsx';
import Keybindings from './keybindings.jsx';
import AppUsage from './app_usage.jsx';

class App { 

    constructor() {
        this.editRequests = [];
        this.editNote = "";
        this.pendingSaveRequests = 0;
        this.cachedTypes = [];
        this.queuedCacheUpdates = [];
        this.types = [];
        this.userManualItems = [];
        this.selectedItem = null;
        this.quickEdits = new QuickEdits();
        this.isFetching = false;
        this.isLoggedIn = false;
        this.executionTimes = [];
        this.analytics = [];
        this.usage = new AppUsage();
    }

    onLoggedIn() {
        var pref = Settings.getPreferences();
        if(pref.useCompact) {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = 'src/style/compact.css';
            link.media = 'all';
            head.appendChild(link);
        }
        Keybindings.load();
        this.quickEdits.load();
        EventDispatcher.add(this.onUserState.bind(this), Constants.EVENT_SOCKET_USER_STATE);
        Rest.getTypesLocalization((data) => {
            Localization.types = data;
            // create cached types
            for(var i=0; i<data.length; ++i) {
                this.cachedTypes.push({
                    type: data[i].id,
                    preferredLabel: Localization.getTypeName(data[i].id),
                    children: [],
                });
            }
            // continue initialization
            this.onInitialize();
        }, (status) => {
            this.showError(Util.getHttpMessage(status) + " : misslyckades hämta lokalisering för typer");
        });
    }

    onInitialize() {

        // NOTE: this will be removed in a later update
        localStorage.removeItem(CacheManager.getKey("types"));
        localStorage.removeItem("tax_key_types_date");
        localStorage.removeItem("tax_key_types_value");

        Rest.getTypes((data) => {
            this.types = data;

            // default analytic containers
            for(var i=0; i<this.types.length; ++i) {
                this.setupAnalytic(this.types[i]);
            }
            // load cache
            var cachedAnalytics = CacheManager.getCachedValue("analytics");
            if(cachedAnalytics) {
                for(var i=0; i<cachedAnalytics.length; ++i) {
                    var analytic = this.analytics.find((x) => x.type == cachedAnalytics[i].type);
                    if(analytic) {
                        this.analytics[this.analytics.indexOf(analytic)] = cachedAnalytics[i];
                    }
                }
            }
            var outOfDate = [];
            for(var i=0; i<this.types.length; ++i) {
                if(this.types[i] == "skill") {
                    continue;
                }
                var cache = CacheManager.getCachedValue("cached_" + this.types[i]);
                if(cache) {
                    var current = this.cachedTypes.find((x) => x.type == cache.type);
                    if(current != null) {
                        current.children = cache.children;
                    }
                    // check if cache is out of date
                    var analytic = this.analytics.find((x) => x.type == this.types[i]);
                    if(this.needsRefetch(analytic)) {
                        outOfDate.push(this.types[i]);
                    }
                } else {
                    // no cache, need a fresh fetch
                    outOfDate.push(this.types[i]);
                }
            }
            this.validateCache();
            if(outOfDate.length > 0) {
                this.fetchAll(outOfDate);
            }

            var sourceUrl = "//jobtech-atlas-landingpage-jobtech-atlas-landingpage-develop.apps.testing.services.jtech.se";
            this.userManualReadFile(window.location.protocol + sourceUrl + "/editor/manual.html");

            Socket.init();
            this.isLoggedIn = true;
            EventDispatcher.fire(Constants.EVENT_LOGIN_COMPLETE);
        }, (status) => {
            // TODO: event, error on login?
            this.showError(Util.getHttpMessage(status) + " : misslyckades hämta typer");
        });
    }

    onSocketReconnected() {
        Rest.getTypes((data) => {
            this.types = data;
            this.tryUpdateCache();
        }, (status) => {
            // TODO: event, error on login?
            this.showError(Util.getHttpMessage(status) + " : misslyckades hämta typer");
        });
    }
    
    onUserState(user) {
        for(var i=0; i<user.states.length; ++i) {
            var state = user.states[i];
            if(state.type == "new_concept") {
                var value = state.value;
                var current = this.cachedTypes.find((x) => { 
                    return x.type == value.type; 
                });
                if(current == null) {
                    // a user created a new type for a concept
                    current = {
                        type: value.type,
                        preferredLabel: Localization.getTypeName(value.type),
                        children: [],
                    };
                    this.cachedTypes.push(current);
                    this.setupAnalytic(value.type);
                }
                current.children.push({
                    id: value.id,
                    type: value.type,
                    preferredLabel: value.preferredLabel,
                    label: value.preferredLabel,
                });
                CacheManager.cache("cached_" + value.type, current);
                this.incrementAnalytic(value.type);
            } else if(state.type == "deprecated_concept") {
                var value = state.value;
                var current = this.cachedTypes.find((x) => { 
                    return x.type == value.type; 
                });
                if(current) {
                    var child = current.children.find((x) => {
                        return x.id == value.id;
                    });
                    // NOTE: we dont handle when a concept becomes re-enabled (its super rare)
                    if(child && value.deprecated) {
                        var index = current.children.indexOf(child);
                        current.children.splice(index, 1);
                        CacheManager.cache("cached_" + value.type, current);
                        this.incrementAnalytic(value.type);
                    }
                }
            } else if(state.type == "edit_saved") {
                if(state.field == "add_connection" || state.field == "remove_connection" || state.field == "edit_connection") {
                    this.updateCahcedIds([state.value.id1, state.value.id2]);
                } else {
                    this.updateCahcedIds([state.value]);
                }
            }
            this.addExecutionTime(Constants.INFO_EVENT_TYPE_WS, Date.now(), 0, JSON.stringify(state));
        }
    }

    onStartInactiveCacheUpdate() {
        // sort based on priority, highest priority first (slower fetch times, most impact on user experience)
        var analytics = JSON.parse(JSON.stringify(this.analytics));
        Util.sortByKey(analytics, "inactiveFetchPriority", false);
        // remove all types that updated in the last hour
        var now = Date.now();
        analytics = analytics.filter((x) => {
            return (now - x.fetchDate) > 60 * 60 * 1000;
        });
        // remove types based on how often the users visits them
        analytics = analytics.filter((x) => {
            // only update this type if it has been visited the last 24 hours
            return x.visits.length > 0 ? (now - x.visits[x.visits.length - 1]) < 24 * 60 * 60 * 1000 : false;
        });
        // start update
        this.onInactiveCacheUpdate(analytics, 0);
    }
    
    onStopInactiveCacheUpdate() {
        if(this.inactiveCacheUpdateId) {
            clearTimeout(this.inactiveCacheUpdateId);
            this.inactiveCacheUpdateId = null;
        }
    }
    
    async onInactiveCacheUpdate(analytics, index) {
        if(index < analytics.length) {
            // TODO: show fetch indicator
            await this.fetchCacheType_NEW(analytics[index]);
            // TODO: clear fetch indicator
            this.inactiveCacheUpdateId = setTimeout(this.onInactiveCacheUpdate.bind(this, analytics, index + 1), 60 * 1000);
        }
    }
    
    async fetchCacheType_NEW(analytic) {
        var type = analytic.type;
        if(type == "skill") {
            type = "skill-headline";
        }
        var start = Date.now();
        var query = this.createFetchQuery("type: \"" + type + "\"", type == "skill-headline");
        var data = await Rest.awaitGraphQlPromise(query);
        if(data != null) {
            // update analytics
            analytic.fetchDate = Date.now();
            analytic.inactiveFetchPriority = analytic.fetchDate - start; // priority is based on how big / slow the fetch is
            // setup cache data
            data = {
                type: type,
                preferredLabel: Localization.getTypeName(type),
                children: data.data.concepts,
            };
            var current = this.cachedTypes.find((x) => { return x.type == data.type; });
            if(current) {
                current.children = data.children;
            } else {
                this.cachedTypes.push(data);
            }
            // save cache on disk
            CacheManager.cache("analytics", this.analytics);
            CacheManager.cache("cached_" + type, data);
        } else {
            this.showError("Fetch cache type - GraphQL exceptions");
            return false;
        }
        return true;
    }

    onViewType(type) {
        var analytic = this.analytics.find((x) => {
            return x.type == type;
        });
        if(analytic) {
            analytic.visits.push(Date.now());
            if(analytic.visits.length > 100) {
                analytic.visits.shift();
            }
            if(analytic.visits.length == 1) {
                analytic.refetchInterval = null;
            } else {
                var samples = [];
                for(var i=0; i<analytic.visits.length - 1; ++i) {
                    samples.push(analytic.visits[i + 1] - analytic.visits[i]);
                }
                var average = 0;
                for(var i=0; i<samples.length; ++i) {
                    average += samples[i] / samples.length;
                }
                if(average < 4 * 60 * 60 * 1000) {
                    analytic.refetchInterval = 1 * 60 * 60 * 1000;
                } else if(average < 24 * 60 * 60 * 1000) {
                    analytic.refetchInterval = 2 * 60 * 60 * 1000;
                } else if(average < 48 * 60 * 60 * 1000) {
                    analytic.refetchInterval = 4 * 60 * 60 * 1000;
                } else {
                    analytic.refetchInterval = null;
                }
            }
            CacheManager.cache("analytics", this.analytics);
        } else {
            console.error("missing analytics for type: " + type);
        }
    }

    onTypeCounters(types) {
        var outOfDate = [];
        for(var i=0; i<this.analytics.length; ++i) {
            var serverType = types.find((x) => {
                return x.type == this.analytics[i].type;
            });
            var counter = serverType == null ? 0 : serverType.counter;
            if(counter != this.analytics[i].modifications) {
                outOfDate.push(this.analytics[i].type);
                this.analytics[i].modifications = counter;
            }
        }
        if(outOfDate.length > 0) {
            this.fetchAll(outOfDate);
        }
    }

    needsRefetch(analytic) {
        if(analytic.refetchInterval == null) {
            return (Date.now() - analytic.fetchDate) > 2 * 24 * 60 * 60 * 1000;
        }
        return (Date.now() - analytic.fetchDate) > analytic.refetchInterval;
    }

    setupAnalytic(type) {
        this.analytics.push({
            type: type,                 // type of concept (ssyk-level-4, occupation-name etc...)
            visits: [],                 // timestamps of eachtime the user selected to view this type (does not account for default start type)
            fetchDate: null,            // last time the data was fetched
            refetchInterval: null,      // how long between each check we should refetch the data
            inactiveFetchPriority: 0,   // priority for when the user is inactive to refetch this type (queue based fetching)
            modifications: 0,           // counter for how many edits have occured on this type as a whole
        });
    }

    incrementAnalytic(type) {
        var analytic = this.analytics.find((x) => {
            return x.type == type;
        });
        if(analytic) {
            analytic.modifications++;
            CacheManager.cache("analytics", this.analytics);
        }
    }

    async updateCahcedIds(ids) {
        for(var i=0; i<ids.length; ++i) {
            var cached = this.findCachedConcept(ids[i]);
            if(cached == null) {
                this.tryUpdateCache();
                break;
            }
            var query = this.createFetchQuery("id: \"" + ids[i] + "\"", cached.type == "skill-headline");
            var result = await Rest.awaitGraphQlPromise(query);
            if(result) {
                var concept = result.data.concepts[0];
                if(concept.type == "skill") {
                    var headlines = this.getCachedTypes("skill-headline");
                    var found = false;
                    for(var j=0; j<headlines.length && !found; ++j) {
                        var headline = headlines[j];
                        for(var k=0; k<headline.skills.length; ++k) {
                            if(headline.skills[k].id == concept.id) {
                                headline.skills[k] = concept;
                                found = true;
                                break;
                            }
                        }
                    }
                    CacheManager.cache("cached_skill-headline", this.cachedTypes.find((x) => x.type == "skill-headline"));
                    this.incrementAnalytic("skill-headline");
                } else {
                    var cache = this.getCachedTypes(concept.type);
                    for(var j=0; j<cache.length; ++j) {
                        if(cache[j].id == concept.id) {
                            cache[j] = concept;
                            break;
                        }
                    }
                    CacheManager.cache("cached_" + concept.type, this.cachedTypes.find((x) => x.type == concept.type));
                    this.incrementAnalytic(concept.type);
                }
            } else {
                this.tryUpdateCache();
                break;
            }
        }
        EventDispatcher.fire(Constants.EVENT_CACHE_UPDATED, ids);
    }

    // special meaning for example geography or education, types that require special processing
    // to visualize trees or lists correctly
    isSpecialType(type) {
        if(type.startsWith("sun-education") || 
            type == "skill" || 
            type == "skill-headline" || 
            type == "continent" ||
            type == "country" ||
            type == "skill-group" ||
            type == "generic-skill-group" ||
            type == "region" ||
            type == "municipality") {
            return true;
        }
        return false;
    }

    getCachedTypes(type) {
        var cached = this.cachedTypes.find((x) => {
            return x.type == type;
        });
        if(cached != null) {
            return cached.children;
        }
        return [];
    }

    getCachedConcept(type, id) {
        return this.getCachedTypes(type).find((x) => {
            return x.id == id;
        });
    }

    getCachedConceptFromId(id) {
        for(var i=0; i<this.cachedTypes.length; ++i) {
            var concept = this.getCachedConcept(this.cachedTypes[i].type, id);
            if(concept != null) {
                return concept;
            }
            if(this.cachedTypes[i].type == "skill-headline") {
                var children = this.cachedTypes[i].children;
                for(var j=0; j<children.length; ++j) {
                    concept = children[j].skills.find((x) => {
                        return x.id == id;
                    });
                    if(concept != null) {
                        return concept;
                    }
                }
            }
        }
        console.error("getCachedConceptFromId: null for id " + id);
        return null;
    }

    getRecentlyTouchedConcept(id) {
        // TODO: impl
        return this.getCachedConceptFromId(id);
    }

    userManualFindTagContent(tag, raw) {
        var i1 = raw.indexOf("<" + tag + ">");
        if(i1 == -1) {
            return null;
        }
        var i2 = raw.indexOf("</" + tag + ">");
        if(i2 == -1) {
            return null;
        }
        i1 += ("<" + tag + ">").length;
        return raw.substr(i1, i2 - i1);
    }

    userManualFindAllEntries(raw) {
        var entries = [];
        var offset = 0;
        while(true) {
            var i1 = raw.indexOf("<entry>", offset);
            if(i1 == -1) {
                break;
            }
            var i2 = raw.indexOf("</entry>", i1);
            if(i2 == -1) {
                break;
            }
            offset = i2;
            i1 += "<entry>".length;
            var entryRaw = raw.substr(i1, i2 - i1);
            var entry = {
                title: this.userManualFindTagContent("title", entryRaw),
                tags: this.userManualFindTagContent("tags", entryRaw),
                text: this.userManualFindTagContent("text", entryRaw),
            };
            if(entry.title == null) {
                entry.title = "Saknar \"<title>Rubrik</title>\" i config";
            }
            if(entry.tags == null) {
                entry.tags = [];
            } else {
                var data = entry.tags.split(",");
                for(var i=0; i<data.length; ++i) {
                    data[i] = "\"" + data[i].trim().toLowerCase() + "\"";
                }
                entry.tags = JSON.parse("[" + data.join() + "]");
            }
            if(entry.text == null) {
                entry.text = "Saknar \"<text>Beskrivning</text>\" i config";
            } else {
                var lines = entry.text.split("\n");
                for(var i=0; i<lines.length; ++i) {
                    lines[i] = lines[i].trim();
                }
                entry.text = lines.join("");
            }
            entries.push(entry);
        }
        return entries;
    }

    async userManualCreateFile(url) {
        const response = await fetch(url);
        const data = await response.blob();
        return new File([data], url);
    }

    async userManualReadFile(url) {
        var file = await this.userManualCreateFile(url);
        var reader = new FileReader();
        reader.addEventListener("load", (e) => {
            this.userManualItems = this.userManualFindAllEntries(e.target.result);
        });
        reader.readAsText(file);
    }

    createFetchQuery(source, isSkillHeadline) {
        var query = 
            "concepts(" + source + ", version: \"next\", include_deprecated: true) { " + 
                "id type preferredLabel:preferred_label label:preferred_label ssyk_code_2012 ssyk:ssyk_code_2012 isco_code_08 isco:isco_code_08 alternativeLabels:alternative_labels hiddenLabels:hidden_labels deprecated " + 
            "}";
        if(isSkillHeadline) {
            query = 
                "concepts(" + source + ", version: \"next\", include_deprecated: true) { " + 
                    "id type preferredLabel:preferred_label label:preferred_label alternativeLabels:alternative_labels hiddenLabels:hidden_labels deprecated " + 
                    "skills:narrower(type: \"skill\", include_deprecated: true) { " +
                        "id type preferredLabel:preferred_label label:preferred_label alternativeLabels:alternative_labels hiddenLabels:hidden_labels deprecated " + 
                    "} " +
                "}";
        }
        return query;
    }

    async fetchAll(types) {
        this.isFetching = true;
        var failedTypes = [];        
        var roots = types ? types : this.types.filter((x) => { return x != "skill"; });
        this.addExecutionTime(Constants.INFO_EVENT_CACHE_UPDATE_START, Date.now(), 0, "");
        for(var i=0; i<roots.length; ++i) {
            EventDispatcher.fire(Constants.EVENT_CACHE_STATUS_SHOW, {
                current: i + 1,
                max: roots.length
            });
            var analytic = this.analytics.find((x) => x.type == roots[i]);
            if(!await this.fetchCacheType_NEW(analytic)) {                
                failedTypes.push(roots[i]);
            }
        }
        this.addExecutionTime(Constants.INFO_EVENT_CACHE_UPDATE_STOP, Date.now(), 0, "");
        if(failedTypes.length > 0) {
            setTimeout(this.fetchAll.bind(this, failedTypes), 5 * 60 * 1000);
        }
        this.tryFixCachedTypes();
        EventDispatcher.fire(Constants.EVENT_CACHE_STATUS_HIDE);
        this.isFetching = false;
    }

    updateChachedType(type, label) {
        var current = this.cachedTypes.find((x) => {
            return x.type == type;
        });
        if(current == null) {
            current = {
                type: type,
                preferredLabel: label,
                children: [],
            };
            this.cachedTypes.push(current);
        } else {
            current.preferredLabel = label;
        }
    }
    
    updateCachedItem(item) {
        // merge attributes to cached item
        var attributes = ["preferredLabel", "label", "alternativeLabels", "hiddenLabels"];
        var cachedItem = this.findCachedConcept(item.id, item.type);
        if(cachedItem == null) {
            if(item.type == "skill") {
                console.error("updateCachedItem with new skill");
            } else {
                var cache = this.getCachedTypes(item.type);
                cache.push(item);
            }
        }
        if(cachedItem) {
            for(var i=0; i<attributes.length; ++i) {
                if(item[attributes[i]] != null) {
                    cachedItem[attributes[i]] = item[attributes[i]];
                }
            }
            // save cache
            if(item.type == "skill") {
                CacheManager.cache("cached_skill-headline", this.cachedTypes.find((x) => x.type == "skill-headline"));
                this.incrementAnalytic("skill-headline");
            } else {
                CacheManager.cache("cached_" + item.type, this.cachedTypes.find((x) => x.type == item.type));
                this.incrementAnalytic(item.type);
            }
        }
    }

    tryUpdateCache(maxTime) {
        if(this.isLoggedIn) {
            var outOfDate = [];
            for(var i=0; i<this.analytics.length; ++i) {
                if(maxTime == 0 || this.needsRefetch(this.analytics[i])) {
                    outOfDate.push(this.analytics[i].type);
                }
            }
            if(outOfDate.length) {
                this.fetchAll(outOfDate);
            }
        }
    }

    tryFixCachedTypes() {
        var valid = true;
        var iterate = true;
        while(iterate) {
            iterate = false;
            for(var i=0; i<this.cachedTypes.length; ++i) {
                var current = this.cachedTypes[i];
                // ensure the correct preferredlable is set
                current.preferredLabel = Localization.getTypeName(current.type);
                // check for undefined / broken properties
                for(var j=0; j<current.children.length; ++j) {
                    var concept = current.children[j];
                    if(typeof(concept.id) != "string" || typeof(concept.preferredLabel) != "string") {
                        console.log("borken concept in cache", current.type, concept);
                        current.children.splice(current.children.indexOf(concept), 1);
                        valid = false;
                        j--;
                    }
                }
                // check for duplications
                for(var j=i + 1; j<this.cachedTypes.length; ++j) {
                    var next = this.cachedTypes[j];
                    if(current.type == next.type) {
                        this.cachedTypes.splice(j, 1);
                        iterate = true;
                        valid = false;
                        break;
                    }
                }   
                if(iterate) {
                    break;
                }
            }
        }
        if(!valid) {
            console.log("Invalid cache detected!");
        } else {
            // ensure all headlines have a skill array
            var headlines = this.getCachedTypes("skill-headline");
            for(var i=0; i<headlines.length; ++i) {
                if(headlines[i].skills == null) {
                    headlines[i].skills = [];
                }
            }
        }
        return valid;
    }

    validateCache() {
        this.tryFixCachedTypes();
    }

    findCachedConcept(id, type) {
        for(var i=0; i<this.cachedTypes.length; ++i) {
            var cachedType = this.cachedTypes[i];
            if(type == "skill") {
                if(cachedType.type != "skill-headline") {
                    continue;
                }
            } else if(type != null && type != cachedType.type) {
                continue;
            }
            var concept = cachedType.children.find((item) => {return item.id == id;});
            if(!concept && cachedType.type == "skill-headline") {
                for(var j=0; j<cachedType.children.length; ++j) {
                    if(cachedType.children[j].skills == null) {
                        continue;
                    }
                    concept = cachedType.children[j].skills.find((e) => {return e.id == id;});
                    if(concept) {
                        break;                        
                    }
                }
            }
            if(concept) {
                return concept;
            }
        }
        return null;
    }

    addSaveRequest() {
        if(this.pendingSaveRequests < 0) {
            this.pendingSaveRequests = 0;
        }
        this.pendingSaveRequests++;
    }

    removeSaveRequest() {
        if(--this.pendingSaveRequests == 0) {
            EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
        }
        if(this.pendingSaveRequests < 0) {
            this.pendingSaveRequests = 0;
        }
        return this.pendingSaveRequests == 0;
    }

    hasUnsavedChanges() {
        return this.editRequests.length > 0;
    }

    hasPendingSaveRequests() {
        return this.pendingSaveRequests > 0;
    }

    showError(message) {
        console.error(message);
        EventDispatcher.fire(Constants.EVENT_SHOW_ERROR, message);
    }

    showSaveDialog(callback) {
        EventDispatcher.fire(Constants.EVENT_SHOW_OVERLAY, {
            title: Localization.get("save"),
            content: <Save 
                callback={callback}
                changes={this.editRequests}/>,
        });
    }
	
    getQuickEdits() {
        return this.quickEdits;
    }

    addExecutionTime(event, time, millis, query) {
        this.executionTimes.push({
            event: event,
            time: time,
            millis: millis,
            query: query,
        });
        if(this.executionTimes.length > 300) {
            this.executionTimes.shift();
        }
    }
}

export default new App;