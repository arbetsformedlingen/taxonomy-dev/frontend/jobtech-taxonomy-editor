import React from 'react';
import Constants from './constants.jsx';

class AppUsage { 

    constructor() {        
        this.flow = [];
    }

    addFlow(source, event, extra) {
        this.flow.push({
            source: source,
            event: event,
            extra: extra,
            time: Date.now(),
        });
    }

}

export default AppUsage;