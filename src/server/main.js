
var Server = require('./server.js');
var WebSocketServer = require('websocket').server;
var http = require('http');

// Logical server
// ---------------------------------
var server = new Server();
server.onInit();

// HTTP
// ---------------------------------
var httpServer = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

httpServer.listen(8080, function() {
    console.log((new Date()) + ' Server is listening on port 8080');
});

// Websocket
// --------------------------------- 
wsServer = new WebSocketServer({
	httpServer: httpServer,
	autoAcceptConnections: false
});
 
function originIsAllowed(origin) {
	return true;
}

wsServer.on('request', function(request) {
    if(!originIsAllowed(request.origin)) {
		request.reject();
		return;
    }
	
    var connection = request.accept('taxonomy-protocol', request.origin);
    console.log((new Date()) + ' Connection accepted.');
	server.onConnection(connection);
});
wsServer.on('error', function(error) {
    console.log("server connection error");
    console.log(error);
});