
function Message(type, value) {
	return JSON.stringify({
		type: type,
		value: value
	});
}

function MessageUserConnected(username) {
	return Message("userConnected", username);
}

function MessageUserDisconnected(username) {
	return Message("userDisconnected", username);
}

function MessageUserClients(clients) {
	var data = [];
	for(var i=0; i<clients.length; ++i) {
		var c = data.find((x) => {
			return x.name == clients[i].username;
		});
		if(c) {
			c.states.push(clients[i].state);
		} else {
			data.push({
				name: clients[i].username,
				states: clients[i].state && clients[i].state.type != null ? [clients[i].state] : []
			});
		}
	}
	return Message("userClients", data);
}

function MessageUserState(data) {
	return Message("userState", data);
}

function MessageUserTypeCounters(types) {
	return Message("typeCounters", types);
}

class Client {

	constructor() {
		this.connection = null;
		this.username = null;
		this.state = null;
		this.id = 0;
	}
	
	send(data) {
		// TODO: check state?
		if(this.connection != null) {
			try {
				this.connection.sendUTF(data);
			} catch(e) {
				console.log("exception sending message to client: ", e);
			}
		}
	}

}

class Server {
	
	constructor() {
		this.clients = [];
		this.typeCache = [];
	}
	
	onInit() {
		// TODO: setup interval for checking stuff?
	}
	
	onConnection(connection) {
		var client = new Client();
		client.connection = connection;
		client.id = new Date().getTime();
		// client callbacks
		connection.on('message', (message) => {
			try {
				if(message.type == "utf8") {
					var msg = JSON.parse(message.utf8Data);
					this.onClientMessage(msg, client);
				}
			} catch(e) {
				console.log("exception on client message: ", e);
			}
		});	
		connection.on('close', (reasonCode, description) => {
			this.onClientDisconnected(client);
		});	
		connection.on('error', (err) => {
			this.onClientError(client, err);
		});
		// save client
		this.clients.push(client);
	}
	
	onClientMessage(message, client) {
		if(message.type == "login") {
			client.username = message.value;
			console.log("user connected: " + client.username + "(" + client.id + ")");
			// notify other clients that a new client is connected
			var msg = MessageUserConnected(client.username);
			var clients = this.getOtherClients(client);
			for(var i=0; i<clients.length; ++i) {
				clients[i].send(msg);
			}
			// send list of connected clients to the new client
			client.send(MessageUserClients(clients));
			client.send(MessageUserTypeCounters(this.typeCache));
		} else if(message.type == "state") {
			client.state = message.value;
			client.state.uniqueId = client.id;
			// update type cache
			if(client.state.type == "new_concept" || client.state.type == "edit_saved" || client.state.type == "deprecated_concept") {
				if(client.state.type == "edit_saved") {
					this.incrementCacheType(client.state.conceptType);
				} else {
					this.incrementCacheType(client.state.value.type);
				}
			}
			// notify other clients off state change
			var msg = MessageUserState({
				name: client.username,
				state: client.state,
			});
			console.log("user state: " + client.username + "(" + client.id + ") - " + JSON.stringify(client.state));
			var clients = this.getOtherClients(client);
			for(var i=0; i<clients.length; ++i) {
				clients[i].send(msg);
			}
		} else if(message.type == "typeCounters") {
			client.send(MessageUserTypeCounters(this.typeCache));
		}
	}
	
	onClientDisconnected(client) {
		var index = this.clients.indexOf(client);
		if(index != -1) {
			this.clients.splice(index, 1);
			console.log("user disconnected: " + client.username + "(" + client.id + ")");
		}
		// notify other clients
		var msg = MessageUserDisconnected(client.username);
		var clients = this.getOtherClients(client);
		for(var i=0; i<clients.length; ++i) {
			clients[i].send(msg);
		}
	}

	onClientError(client, error) {
		console.log("connection error caused by: " + client.username + "(" + client.id + ")");
		console.log(error);
	}

	incrementCacheType(type) {
		if(type == null) {
			console.log("incrementCacheType: type == null");
			return;
		}
		var current = this.typeCache.find((x) => x.type == type);
		if(current == null) {
			current = {
				type: type,
				counter: 0,
			};
			this.typeCache.push(current);
		}
		current.counter++;
	}

	getOtherClients(client) {
		// only match on username, this to allow the same user to be logged in from multiple locations
		return this.clients.filter((c) => {
			return c.id != null && c.id != client.id;
		});
	}
	
}

module.exports = Server;