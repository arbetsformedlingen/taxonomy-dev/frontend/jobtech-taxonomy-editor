import React from 'react';
import Util from './util.jsx';
import App from '../context/app.jsx';
import Constants from '../context/constants.jsx';

class Button extends React.Component { 
    
    constructor() {
        super();
        this.css = "button no_select font";
    }

    isEnabled() {
        return this.props.isEnabled == null ? true : this.props.isEnabled;
    }

    onClick() {
        if(this.isEnabled() && this.props.onClick) {
            App.usage.addFlow(Constants.USAGE_BUTTON, Constants.USAGE_CLICK, Util.getInnerText(this.props.text));
            this.props.onClick();
        }
    }

    render() {
        return (
            <div 
                className={Util.getStyle(this) + (this.isEnabled() ? "" : " button_disabled")}
                title={this.props.hint == null ? "" : this.props.hint}
                onMouseUp={this.onClick.bind(this)}>
                <div>{this.props.text}</div>
            </div>
        );
    }
	
}

export default Button;