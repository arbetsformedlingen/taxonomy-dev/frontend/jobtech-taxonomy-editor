import React from 'react';
import Label from './label.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Constants from './../context/constants.jsx';

class DialogWindow extends React.Component { 

    constructor(props) {
        super(props);
        this.dialogContext = {
            onClosed: null,
        };
        // insert casasdasda
        this.dialogContent = React.cloneElement(this.props.children, {
            dialogContext: this.dialogContext
        });

        this.boundGlobalInput = this.onGlobalInput.bind(this);
    }

    componentDidMount() {
        document.addEventListener('keydown', this.boundGlobalInput);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.boundGlobalInput);
    }

    onGlobalInput(e) {
        if(e.key == "Escape" && this.props.isTopMost) {
            if(this.dialogContext.onClosed == null || this.dialogContext.onClosed()) {
                EventDispatcher.fire(Constants.EVENT_HIDE_OVERLAY);
            }
        }
    }

    render() {
        return (
            <div className="dialog_window font">
                <Label text={this.props.title}/>
                <div>{this.dialogContent}</div>
            </div>
        );
    }
	
}

export default DialogWindow;