import React from 'react';
import List from './list.jsx';

class TreeView extends React.Component { 
    
    constructor() {
        super();
        this.state = {
            context: null,
            focused: null,
        };
    }

    __init(props) {
        if(props.context) {
            props.context.addRoot = this.addRoot.bind(this);
            props.context.removeRoot = this.removeRoot.bind(this);
            props.context.clear = this.clear.bind(this);
            props.context.invalidate = this.invalidate.bind(this);
            props.context.findChild = this.findChild.bind(this);
            this.state.context = props.context;
            this.setState({context: props.context});
        }
    }

    componentDidMount() {
        this.__init(this.props);
    }

    UNSAFE_componentWillReceiveProps(props) {
        this.__init(props);
    }

    addRoot(root, callback) {
        this.state.context.roots.push(root);
        root.attached = true;
        if(this.state.context.shouldUpdateState) {
            this.setState({context: this.state.context}, callback);
        }
    }

    removeRoot(root, callback) {
        root.setSelected(false);
        root.rebind();
        root.attached = false;
        var index = this.state.context.roots.indexOf(root);
        this.state.context.roots.splice(index, 1);
        this.setState({context: this.state.context}, callback);
    }

    clear() {
        if(this.state.context) {
            this.state.context.roots = [];
            this.state.context.selected = null;
            this.setState({context: this.state.context});
        }
    }

    invalidate(callback) {
        for(var i=0; i<this.state.context.roots.length; ++i) {
            var root = this.state.context.roots[i];
            if(root.expanded) {
                root.invalidate();
            }
        }
        this.setState({context: this.state.context}, callback);
    }

    findChild(predicate) {
        if(this.state.context) {
            for(var i=0; i<this.state.context.roots.length; ++i) {
                var root = this.state.context.roots[i];
                if(predicate(root)) {
                    return root;
                }
                if(root.findChild != null) {
                    var result = root.findChild(predicate);
                    if(result) {
                        return result;
                    }
                }
            }
        }
        return null;
    }

    focusItem(item) {
        if(this.state.focused) {
            this.state.focused.setFocused(false);
        }
        this.state.focused = item;
        if(item != null) {
            item.setFocused(true);
            var element = document.getElementById("" + item.uniqueId);
            if(element) {
                element.scrollIntoView({
                    behavior: "auto", 
                    block: "nearest", 
                    inline: "nearest"
                });
            }
        }
    }

    focusPrev() {
        var getPrevEnabled = (siblings, fromIndex) => {
            for(var i=fromIndex; i>=0; i) {
                if(!siblings[i].disabled) {
                    return siblings[i];
                }
            }
            return null;
        }
        var origin = this.state.focused ? this.state.focused : this.state.context.selected;        
        if(origin != null) {
            var siblings = origin.parent ? origin.parent.children : this.state.context.roots;
            var index = siblings.indexOf(origin);
            var next = getPrevEnabled(siblings, index - 1);
            if(next) {
                this.focusItem(next);
            }
        }
    }

    focusNext() {
        var getNextEnabled = (siblings, fromIndex) => {
            for(var i=fromIndex; i<siblings.length; ++i) {
                if(!siblings[i].disabled) {
                    return siblings[i];
                }
            }
            return null;
        }
        var origin = this.state.focused ? this.state.focused : this.state.context.selected;        
        if(origin == null) {
            var next = getNextEnabled(this.state.context.roots, 0);
            if(next) {
                this.focusItem(next);
            }
        } else {
            var siblings = origin.parent ? origin.parent.children : this.state.context.roots;
            var index = siblings.indexOf(origin);
            var next = getNextEnabled(siblings, index + 1);
            if(next) {
                this.focusItem(next);
            }
        }
    }

    onKeyDown(e) {
        if(this.state.context.roots.length > 0) {
            if(e.key == "ArrowUp") {
                e.preventDefault();
                e.stopPropagation();
                this.focusPrev();
            } else if(e.key == "ArrowDown") {
                e.preventDefault();
                e.stopPropagation();
                this.focusNext();
            } else if(e.key == "ArrowRight") {
                e.preventDefault();
                e.stopPropagation();
                if(this.state.focused) {
                    this.state.focused.setExpanded(true, ()=>{
                        if(this.state.focused.children.length > 0) {
                            this.focusItem(this.state.focused.children[0]);
                        }
                    });
                }
            } else if(e.key == "ArrowLeft") {
                e.preventDefault();
                e.stopPropagation();
                if(this.state.focused && this.state.focused.parent) {
                    this.focusItem(this.state.focused.parent);
                }
            } else if(e.key == "Enter" || e.key == " ") {
                e.preventDefault();
                e.stopPropagation();
                if(this.state.focused && this.state.focused != this.state.selected) {
                    this.state.context.setSelected(this.state.focused, true);
                }
            }
        }
    }

    onFocus() {
        
    }

    onBlur() {
        if(this.state.focused) {
            this.state.focused.setFocused(false);
            this.setState({focused: null});
        }
    }

    renderRoots() {
        if(this.state.context) {
            return this.state.context.roots.map((item, i) => {
                return item.reactType;
            });
        }
    }

    render() {
        return (
            <List 
                onKeyDown={this.onKeyDown.bind(this)}
                onFocus={this.onFocus.bind(this)}
                onBlur={this.onBlur.bind(this)}
                css={this.props.css}>
                {this.props.children}
                {this.renderRoots()}
            </List>
        );
    }
	
}

export default TreeView;