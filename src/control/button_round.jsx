import React from 'react';
import Util from './util.jsx';

class ButtonRound extends React.Component {

    constructor() {
        super();
        this.css = "button button_round no_select font";
    }

    render() {
        return (
            <div 
                className={Util.getStyle(this)}
                title={this.props.hint == null ? "" : this.props.hint}
                onMouseUp={this.props.onClick}>
                <div>{this.props.text}</div>                
            </div>
        );
    }
	
}

export default ButtonRound;