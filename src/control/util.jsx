import React from 'react';
import TreeViewItem from './tree_view_item.jsx';
import ContextUtil from './../context/util.jsx';

class Util { 

    constructor() {
        this.TREEVIEW_ITEM_ID = 0;
    }

    __treeViewIsSelected(context, item) {
        return context.selected == item;
    }

    __treeViewReselected(context, item) {
        var index = context.selection.indexOf(item);
        if(index < 0) {
            context.selection.push(item);
            if(item.onSelected) {
                item.onSelected();
            }
        }
    }

    __treeViewSetSelected(context, item, selected) {
        if(context.onAllowItemSelection && !context.onAllowItemSelection(item)) {
            return;
        }
        if(context.allowMultiSelect) {
            var index = context.selection.indexOf(item);
            if(index >= 0) {
                context.selection.splice(index, 1);
                if(item.onDeselected) {
                    item.onDeselected();
                }
            } else {
                if(!context.isCtrlDown) {
                    context.clearSelected();
                }
                context.selection.push(item);
                if(item.onSelected) {
                    item.onSelected();
                }
                if(context.onItemSelected) {
                    // notify listeners of the treeview of the change
                    context.onItemSelected(item);
                }
            }
        } else {
            if(this.__treeViewIsSelected(context, item)) {
                if(!selected) {
                    if(context.selected.onDeselected) {
                        context.selected.onDeselected();
                    }
                    context.selected = null;
                }
            } else if(selected) {
                context.clearSelected();
                context.selected = item;
                context.selection = [];
                if(context.selected) {
                    context.selection.push(item);
                    if(context.selected.onSelected) {
                        context.selected.onSelected();
                    }
                    if(context.onItemSelected) {
                        // notify listeners of the treeview of the change
                        context.onItemSelected(item);
                    }
                }
            }
        }
    }

    __treeViewGetSelected(context) {
        return context.selected;
    }
    
    __treeViewClearSelected(context) {
        for(var i=0; i<context.selection.length; ++i) {
            if(context.selection[i].onDeselected) {
                context.selection[i].onDeselected();
            }
        }
        context.selection = [];
        if(context.selected) {
            if(context.selected.onDeselected) {
                context.selected.onDeselected();
            }
            context.selected = null;
        }
    }

    __treeViewAddRoot(context, item) {
        context.roots.push(item);
    }

    __treeViewRemoveRoot(context, item) {
        root.setSelected(false);
        var index = context.roots.indexOf(item);
        context.roots.splice(index, 1);
    }

    __treeViewInvalidate(context) {
    }

    __treeViewClear(context) {
        context.roots = [];
    }

    createTreeView() {
        var context = {
            // members
            roots: [],
            selection: [],
            selected: null,
            shouldUpdateState: true,
            allowMultiSelect: false,
            isCtrlDown: false,
            // utility
            isSelected: null,
            setSelected: null,
            getSelected: null,
            // callbacks
            onItemSelected: null,
            onAllowItemSelection: null,
            // vtable
            addRoot: null,
            removeRoot: null,
            clear: null,
            invalidate: null,
        };
        // bind function pointers
        context.isSelected = this.__treeViewIsSelected.bind(this, context);
        context.reselect = this.__treeViewReselected.bind(this, context);
        context.setSelected = this.__treeViewSetSelected.bind(this, context);
        context.getSelected = this.__treeViewGetSelected.bind(this, context);
        context.clearSelected = this.__treeViewClearSelected.bind(this, context);
        context.addRoot = this.__treeViewAddRoot.bind(this, context);
        context.removeRoot = this.__treeViewRemoveRoot.bind(this, context);
        context.invalidate = this.__treeViewInvalidate.bind(this, context);
        context.clear = this.__treeViewClear.bind(this, context);
        return context;
    }

    __treeViewItemIsSelected(item) {
        return item.context.isSelected(item);
    }

    __treeViewItemIsLastChild(item, child) {
        return item.children.indexOf(child) == item.children.length - 1;
    }
    
    __treeViewItemRefresh(item) {
    }

    __treeViewItemInvalidate(item) {
    }

    __treeViewItemSetText(item, text) {
        item.text = text;
    }

    __treeViewItemSetExpanded(item, expanded, predicate) {
        item.expanded = expanded;
        item.callbackOnMount = predicate;
    }
    
    __treeViewItemSetSelected(item, selected) {
        item.context.setSelected(item, selected);
    }
    
    __treeViewItemSetShowButton(item, show) {
        item.showingButton = show;
    }

    __treeViewItemSetForceShowButton(item, show) {
        item.forceShowingButton = show;
    }

    __treeViewItemSetFocused(item, focused) {
        
    }
    
    __treeViewItemAddChild(item, child) {
        child.parent = item;
        child.attached = true;
        item.children.push(child);
    }

    __treeViewItemSortChildren(item, method) {
        if(method) {
            method(item.children);
        } else {
            ContextUtil.sortByKey(item.children, "text", true);
        }
    }

    __treeViewItemFindChild(item, predicate) {
        for(var i=0; i<item.children.length; ++i) {
            var child = item.children[i];
            if(predicate(child)) {
                return child;
            }
            if(child.findChild != null) {
                var result = child.findChild(predicate);
                if(result) {
                    return result;
                }
            }
        }
        return null;
    }

    __treeViewItemRebind(item) {
        item.isSelected = this.__treeViewItemIsSelected.bind(this, item);
        item.isLastChild = this.__treeViewItemIsLastChild.bind(this, item);  
        item.refresh = this.__treeViewItemRefresh.bind(this, item);
        item.invalidate = this.__treeViewItemInvalidate.bind(this, item);
        item.setText = this.__treeViewItemSetText.bind(this, item);
        item.setExpanded = this.__treeViewItemSetExpanded.bind(this, item);
        item.setSelected = this.__treeViewItemSetSelected.bind(this, item);
        item.setShowButton = this.__treeViewItemSetShowButton.bind(this, item);
        item.setForceShowButton = this.__treeViewItemSetForceShowButton.bind(this, item);
        item.setFocused = this.__treeViewItemSetFocused.bind(this, item);
        item.addChild = this.__treeViewItemAddChild.bind(this, item); 
        item.sortChildren = this.__treeViewItemSortChildren.bind(this, item);
        item.findChild = this.__treeViewItemFindChild.bind(this, item);
    }
    
    createTreeViewItem(context, data) {
        var pointer = {
            // members
            attached: false,
            expanded: false,
            showingButton: true,
            showingSelection: true,
            selectable: true,
            disabled: false,
            text: "item",
            tooltip: null,
            parent: null,
            children: [],
            reactType: null,
            context: context,
            data: data,
            // utility
            isLastChild: null,
            isSelected: null,            
            refresh: null,
            invalidate: null,
            rebind: null,
            // vtable
            setText: null,
            setExpanded: null,
            setSelected: null,
            setShowButton: null,
            setForceShowButton: null,
            setFocused: null,
            addChild: null,
            removeChild: null,
            clear: null,
            sortChildren: null,
            onDeselected: null,
            onSelected: null,
            onExpandClicked: null,            
        };
        pointer.uniqueId = this.TREEVIEW_ITEM_ID;
        pointer.rebind = this.__treeViewItemRebind.bind(this, pointer);
        pointer.rebind();
        // store type used for mounting the pointer
        pointer.reactType = <TreeViewItem 
                                key={"kid_" + this.TREEVIEW_ITEM_ID++} 
                                pointer={pointer}/>;
        return pointer;
    }

    createGroupContext() {
        return {
            // callbacks
            onLockChanged: null,
        };
    }

    getStyle(control) {
        return control.props.css ? control.css + " " + control.props.css : control.css;
    }
	
    getInnerText(obj) {
        if(obj && typeof obj == "object") {
            var buf = "";
            var children = null;
            if( Array.isArray(obj) ) {
                children = obj;
            } else {
                var props = obj.props;
                if( props ) {
                    children = props.children;
                }
            }
            if( children ) {
                if( Array.isArray(children) ) {
                    children.forEach((o) => {
                        buf += this.getInnerText(o);
                    });
                } else {
                    buf += this.getInnerText(children);
                }
            }
            return buf;
        }
        return obj;
    }
}

export default new Util;