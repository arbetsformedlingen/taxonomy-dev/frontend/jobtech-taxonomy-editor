import React from 'react';
import App from './context/app.jsx';
import Constants from './context/constants.jsx';
import Localization from './context/localization.jsx';
import Rest from './context/rest.jsx';
import Button from './control/button.jsx';
import Label from './control/label.jsx';

class Login extends React.Component { 

	constructor() {
        super();
    }

    onChangeUserId(e) {
        this.userId = e.target.value;
    }

    onChangeUserPass(e) {
        this.userPass = e.target.value;
    }

    onLoginClicked() {
        if(this.userId != null) {
            var apiKey = this.userId + "|" + (this.userPass != null ? this.userPass : "");
            Constants.REST_API_KEY = apiKey;
            Rest.getConcept("dwm2_1V3_MpP", ()=>{
                this.props.onSetUserId(apiKey);
            }, ()=>{
                Constants.REST_API_KEY = "";
                App.showError(Localization.get("invalid_user_name_or_pass"));
            });
        }
    }

    onKeyUp(e) {
        // Number 13 is the "Enter" key on the keyboard
        if (e.keyCode === 13) {
            // Cancel the default action, if needed
            e.preventDefault();
            this.onLoginClicked();
        }
    }

    render() {
        return (
            <div className="login_container">
                <div className="login_panel">
                    <Label 
                        text={Localization.get("app_name")} 
                        css="login_title"/>
                    <div className="sub_panel">
                        <Label text={Localization.get("set_user_name")}/>
                        <input 
                            onChange={this.onChangeUserId.bind(this)} 
                            onKeyUp={this.onKeyUp.bind(this)}/>
                        <Label text={Localization.get("set_user_pass")}/>
                        <input 
                            type="password" 
                            onChange={this.onChangeUserPass.bind(this)} 
                            onKeyUp={this.onKeyUp.bind(this)}/>
                        <div className="login_buttons">
                            <Button 
                                onClick={this.onLoginClicked.bind(this)} 
                                text={Localization.get("login")}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Login;