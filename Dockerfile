FROM node:22.14.0-alpine3.21 as builder

WORKDIR /app
COPY . .

RUN apk update && apk add --no-cache git &&\
    git rev-parse --short HEAD > version &&\
    npm run updateVersion &&\
# this part just makes sure there arent any compile errors
    npm install &&\
    npm run release &&\
# here we more or less ignore the build, and run a node instance, for the time being
    mkdir .npm

FROM node:22.14.0-alpine3.21 as runner
ENV NODE_ENV=production

COPY --from=builder /app/src /app/src/
COPY --from=builder /app/index.html /app/
COPY --from=builder /app/config.js /app/
COPY --from=builder /app/package.json /app/
COPY --from=builder /app/node_modules /app/node_modules/

WORKDIR /app
RUN npm install -g

EXPOSE 8000 8080
COPY config.js  /mnt/volumemount/config.js
RUN ln -sf /mnt/volumemount/config.js config.js

CMD npm run prod
